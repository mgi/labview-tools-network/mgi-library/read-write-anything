RSRC
 LVCCLBVW ��  x      �`   p!�            < � @�      ����            {�(�H�����M          {��FE��@�.;�����ُ ��	���B~  t; LVCCAll Prefs Cluster.ctl     - LVCC   EMC_CE Prefs Cluster.ctl PTH0   8   TestsConducted EmissionsEMC_CE Prefs Cluster.ctl   < @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @0����Spectrum Analyzer @!Show Diagnostics? @0����
Test Title  @
Attenuation @0����String  @@ �������� Scan Details  @!2Pts/BW?  @0����	RF SigGen ~ �       EMC_CE Test Type Enum.ctl \@ Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)	Test Type @
Numeric @@ ���� SigGenFreq[]   @@ ���� SigGenLevlOffset[]  @0����
Scope Name  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P    PID 
@
Tol+  
@
Tol-  @
Max SigGen Chg  @

Max SigGen  @

Resistance  @
	SigGenMin @P        Cal @0����CE Limit File @0����Bandwidth File  @0����Monitor Probe @0����Monitor Cable @
PID Max Freq  @
Start Frequency @
Stop Frequency  @0����
ScopeProbe  @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ) *BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  , - . / 0BWFreqRange @@ ���� 1BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P  ' ( + 2 3SA Cfg  Y ��)3�   "EMC_CE Test Type Prefs Cluster.ctl -@P         ! " # $ % & 4	TestPrefs @@ ���� 5Prefs[TestType] @0����Amplifier Name  @#Sweeps @
Ref Lvl Offset  @!
SA PreAmp?  M ��)O�   EMC_CE Prefs Cluster.ctl +@P     	 
     6 7 8 9 :Prefs  ;                       LVCC      EMC_Test Header Cluster.ctlPTH0   '   TestsEMC_Test Header Cluster.ctl    @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  F ��)3�   EMC_Test Header Cluster.ctl "@P        Test Header                         LVCC    EMC_CE Test Type Enum.ctlPTH0   9   TestsConducted EmissionsEMC_CE Test Type Enum.ctl    ~ �       EMC_CE Test Type Enum.ctl \@ Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)	Test Type                          LVCC    "EMC_CE Test Type Prefs Cluster.ctl PTH0   B   TestsConducted Emissions"EMC_CE Test Type Prefs Cluster.ctl   & @
Numeric @@ ����  SigGenFreq[]   @@ ����  SigGenLevlOffset[]  @0����
Scope Name  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P    PID 
@
Tol+  
@
Tol-  @
Max SigGen Chg  @

Max SigGen  @

Resistance  @
	SigGenMin @P    	 
   Cal @0����CE Limit File @0����Bandwidth File  @0����Monitor Probe @0����Monitor Cable @
PID Max Freq  @
Start Frequency @
Stop Frequency  @0����
ScopeProbe  @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P   BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P       BWFreqRange @@ ���� !BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P     " #SA Cfg  Y ��)3�   "EMC_CE Test Type Prefs Cluster.ctl -@P              $	TestPrefs  %                       LVCC     EMC_UI PID Cluster.ctl PTH0   #   SharedEMC_UI PID Cluster.ctl    @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P     PID Cluster                         LVCC     /EMC_Spectrum Anaylzer Configuration Cluster.ctlPTH0   <   Shared/EMC_Spectrum Anaylzer Configuration Cluster.ctl    @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P   BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      	BWFreqRange @@ ���� 
BW[]  @
Sweep Time Multiplier ^ ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl &@P       SA Config Cluster                         LVCC   EMC_SA BWTable RB.ctlPTH0   !   TestsEMC_SA BWTable RB.ctl    *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  : �       EMC_SA BWTable RB.ctl @P    BW UI Control                         LVCC      BWFreqRange Cluster.ctlPTH0   .   Shared	BandwidthBWFreqRange Cluster.ctl    @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P       BWFreqRange                         LVCC     EMC_AF Prefs Cluster.ctl PTH0   4   TestsAudio FrequencyEMC_AF Prefs Cluster.ctl   5 @0����Oscilloscope  @0����Arbitrary Generator @
Scope Tol+(dB)  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P    PID @
Max Gen Delta(Vrms) @

ArbGen Max  @0����Scope Probe @
Nominal Dwell @
Data 2  @@ ���� [Frequency] @@ ���� [Time]  7 �       DwellListbox.xctlData.ctl %@P   Dwell @0����
Test Title  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P        Freq  � �       EMC_AF Test Type Enum.ctl �@  Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)	Test Type @

ArbGen Min  @
Freq Stepback Y �       EMC_AF ArbGen Cmd Enum.ctl 5@ ArbGen from FileScope SetPoint
SigGen Cmd  @2����ArbGen Result(Rel)  @
Scope Tol-(dB)  @0����ScopeSP File  @0����PlotGen Err @
Cable Len(m)  A �       EMC_Length Unit Enum.ctl @ incmmLength units  @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P  " # $ % & 'Header  @!Show Diagnostics? @0����	EStop Str @
Scope SP Offset(dB) @0����	Amplifier @0����ScopeFilePrefix @
SigGen Offset(Vrms) @0����SigGen to Amp Cable @
Scope EStop(dB) @
Initial Gain  @
Delay(s)  @0����Transformer s ��)O�   EMC_AF Prefs Cluster.ctl Q@P !        	 
              ! ( ) * + , - . / 0 1 2 3Prefs  4                       LVCC      DwellListbox.xctlData.ctl PTH0   "   SharedDwellListboxData.ctl    @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P   Dwell                         LVCC    Frequency Specifier.xctlData.ctlPTH0   )   SharedFrequency SpecifierData.ctl    d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P         Data                          LVCC    Freq_Spec Method Enum.ctlPTH0   :   SharedFrequency SpecifierFreq_Spec Method Enum.ctl    d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method                           LVCC     EMC_AF Test Type Enum.ctlPTH0   5   TestsAudio FrequencyEMC_AF Test Type Enum.ctl    � �       EMC_AF Test Type Enum.ctl �@  Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)AF Test Type                           LVCC      EMC_AF ArbGen Cmd Enum.ctl PTH0   6   TestsAudio FrequencyEMC_AF ArbGen Cmd Enum.ctl    W �       EMC_AF ArbGen Cmd Enum.ctl 3@ ArbGen from FileScope SetPointCmd Type                           LVCC    EMC_Length Unit Enum.ctl PTH0   %   SharedEMC_Length Unit Enum.ctl    A �       EMC_Length Unit Enum.ctl @ incmmLength units                           LVCC     EMC_CS Prefs Cluster.ctl PTH0   =   TestsConducted SusceptibilityEMC_CS Prefs Cluster.ctl   Q @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header   @2����Calibration File (rel)  @0����RF Signal Generator @0����	Amplifier @
Dampening Factor  @
Max RF Change � �       EMC_CS Test Type Enum.ctl h@ CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search Type  V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev B �Œ�O   !Instrument_Modulation Cluster.ctl @P    AM Mod  @2����CW File(Rel)  @!Pulse?  @
Pulse Freq(Hz)  @!Show Diagnostics? @
RF Sig Gen Freq Change  @
Nominal Dwell @
Data 2  @@ ���� [Frequency] @@ ���� [Time]  7 �       DwellListbox.xctlData.ctl %@P   Dwell @0����
Test Title  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P        ! "Freq  @
Mod SigGen Offset B �Œ�O   !Instrument_Modulation Cluster.ctl @P    FM Mod  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  & ' ( ) *BWFreqRange @@ ���� +
Bandwidths  @0����BW File @0����SigGen Cable  @
SigGen Max Offset @0����Cal Injection Probe  @0����Forward Power Analyzer  @0����FP Cable  @
FP SA RefLevel  @
FP SA Atten @
FP Overshoot  @
FP Tol- @
FP Tol+ @
FP EStop Adjust @0����Output Power Analyzer @0����OP Cable  @0����Monitor Probe @
OP SA Atten @
OP Limit Adjust *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  > ?BW Table RB J �       EMC_CS OP Units.ctl .@ dBmdBuAAmpsInduced Current Units @OP SA #Points @FP SA #Points @0����Mod Adjust(dB)  s �       *EMC_CS Induced Current Limit Source RB.ctl ?@ Cal Category LevelCal Category LimitOP Limit Source @0����Cat File(For Test)  @
OP Target Tol+  @
OP Target Tol-  @0����Injection Probe @
OP Target EStop Offset  @
OP Ref Level  @0����Cat File(For Cal) @
Cal Category SP Offset  @0����Directional Coupler @
SA Time Multiplier  � ��)O�   EMC_CS Prefs Cluster.ctl y@P 5    	 
            # $ % , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = @ A B C D E F G H I J K L M N OPrefs  P                       LVCC   EMC_CS Test Type Enum.ctlPTH0   >   TestsConducted SusceptibilityEMC_CS Test Type Enum.ctl    � �       EMC_CS Test Type Enum.ctl h@ CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search Type                           LVCC   !Instrument_Modulation Cluster.ctlPTH0   >   Instruments
Instrument!Instrument_Modulation Cluster.ctl    V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P     Mod Channel                         LVCC     !Instrument_Waveform Type Enum.ctlPTH0   >   Instruments
Instrument!Instrument_Waveform Type Enum.ctl    ^ ��d��   !Instrument_Waveform Type Enum.ctl 4@ <None>SineTriangleSquareWaveform Type                          LVCC   EMC_CS OP Units.ctlPTH0   8   TestsConducted SusceptibilityEMC_CS OP Units.ctl    H �       EMC_CS OP Units.ctl ,@ dBmdBuAAmpsOutput Power Units                           LVCC     *EMC_CS Induced Current Limit Source RB.ctl PTH0   O   TestsConducted Susceptibility*EMC_CS Induced Current Limit Source RB.ctl    y �       *EMC_CS Induced Current Limit Source RB.ctl E@ Cal Category LevelCal Category LimitInduced Current Limit                          LVCC      EMC_CS101 Test Prefs Cluster.ctl PTH0   7   Tests
CS101 Test EMC_CS101 Test Prefs Cluster.ctl   9 @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @0����
Scope Name  @0����Signal Generator  @0����	Amplifier @!Show Diagnostics? @
Scope Tol+(dB)  @
Sig Gen Max Delta @
Scope EStop Offset(dB)  @
Sig Gen Freq Change @
P @
I @
D 9 �       EMC_UI PID Cluster.ctl @P    ScopePID  @0����	EStop Str d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P        Freq  @0����
Test Title  @
Scope Tol-(dB)  @
SigGen Min(Vrms)  @
SigGen Max(Vrms)  ,@ OscilloscopeSpectrum Analyzer Input  @0����Spectrum Analyzer Name  @0����SA Cable  @
Scope Target Offset(dB) @
Nominal Dwell(s)  @
Data 2  @@ ���� %[Frequency] @@ ���� %[Time]  7 �       DwellListbox.xctlData.ctl %@P  & 'Dwell @
Read Delay(s) \ �       )EMC_CS101 Test Ripple Graph Unit Enum.ctl *@ dBuVVrmsRipple Voltage Units  @
Initial Scope/SigGen Gain @0����SigGen to Amp Cable @0����
ScopeProbe  @2����CS101Cal File(Rel)   @
SigGen Target Offset(Vrms)  @0����Test Level Limit  @
	Line Freq @P  . / 0 1Test  @

Resistance  @0����
CategoryNm  @P  3 4Cal K �       EMC_CS101 Test Type Enum.ctl %@ CalibrationTest 	Test Type @0����Transformer w ��)O�    EMC_CS101 Test Prefs Cluster.ctl M@P     	 
              ! " # $ ( ) * + , - 2 5 6 7Prefs  8                       LVCC    )EMC_CS101 Test Ripple Graph Unit Enum.ctlPTH0   @   Tests
CS101 Test)EMC_CS101 Test Ripple Graph Unit Enum.ctl    \ �       )EMC_CS101 Test Ripple Graph Unit Enum.ctl *@ dBuVVrmsRipple Voltage Units                           LVCC   EMC_CS101 Test Type Enum.ctl PTH0   3   Tests
CS101 TestEMC_CS101 Test Type Enum.ctl    K �       EMC_CS101 Test Type Enum.ctl %@ CalibrationTest 	Test Type                          LVCC     EMC_MT Prefs Cluster.ctl PTH0   :   Tests
Mode Tuned
MT privateEMC_MT Prefs Cluster.ctl   j @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @!Show Diagnostics? @0����
Test Title  @0����Name  @0����BW File @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ���� [Bw]  @P  
 Test Params $@@ ���� TestParam (Freq, Test)  @
Attenuation @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 	         SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  	      !Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  	 # $ % 	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  	 ' (Amp @FreqSettling Time(ms) @
Nominal Dwell @
Data 2  @@ ���� ,[Frequency] @@ ���� ,[Time]  7 �       DwellListbox.xctlData.ctl %@P  - .Dwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  0 1 2 3 4 5 6Freq  � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  : ; <
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  9 = > ? @ A B C D E F G H ISeek  @2����Seek File (Rel) @
SigGen Offset @P  K LTest  @0����PV File/Level (dBm) @
PV Tol (dB) @P  N OCal @
App Pwr EStop Offset (dB) @
SigGen Max Offset @0����Cable @P  	 SSigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  U V W X Y Z [Timing  @P  	 \ArbGen  @
Fwd Pwr EStop Offset (dB) @
Numeric @@ ���� _[Probe Ranges]  @0����String  @@ ���� a[Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  	 d e fNetwork Analyzer  @@ ���� aProbe VISA[]  c ��)O�   EMC_MT Prefs Cluster.ctl A@P      " & ) * + / 7 8 J M P Q R T ] ^ ` b c g hPrefs  i                       LVCC   EMC_M Tuner Motor Cluster.ctlPTH0   <   Tests
Mode TunedprivateEMC_M Tuner Motor Cluster.ctl    @0����Name  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P        Motor                         LVCC     EMC_ML Motor Radio Button.ctlPTH0   ?   Tests
Mode Tuned
ML privateEMC_ML Motor Radio Button.ctl    V ��F��   EMC_ML Motor Radio Button.ctl 0@ Incremental
Continuous Motor Control                          LVCC      EMC_MT Test Type Enum.ctlPTH0   ;   Tests
Mode Tuned
MT privateEMC_MT Test Type Enum.ctl    � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type                          LVCC    #EMC_ML Controlled Variable Enum.ctlPTH0   E   Tests
Mode Tuned
ML private#EMC_ML Controlled Variable Enum.ctl    ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable                          LVCC      EMC_Timing Generator Params.ctlPTH0   +   TestsEMC_Timing Generator Params.ctl    @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp T �       EMC_Timing Generator Params.ctl ,@P         Timing Gen Cluster                          LVCC      EMC_MT Calc Type Enum.ctlPTH0   ;   Tests
Mode Tuned
MT privateEMC_MT Calc Type Enum.ctl    R ��8��   EMC_MT Calc Type Enum.ctl 0@ 
Rx AntennaRF Probe AveMT Calc Type                           LVCC      "EMC_M Network Analyzer Cluster.ctl PTH0   A   Tests
Mode Tunedprivate"EMC_M Network Analyzer Cluster.ctl    @0����Name  @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) M �       "EMC_M Network Analyzer Cluster.ctl !@P      NetworkAnalyzer                         LVCC     EMC_ML Prefs Cluster.ctl PTH0   :   Tests
Mode Tuned
ML privateEMC_ML Prefs Cluster.ctl   L @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @!Show Diagnostics? @0����
Test Title  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  	 
     Freq  @0����Bandwidth File  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ���� [Bandwidths]  i �� ��   EMC_ML Probe Location Enum.ctl A@ 
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR	Probe Pos @@ ���� [Probe Pos] @
Numeric @@ ���� [Probe Ranges]  @0����String  @@ ���� [Probe Names] @Probe Settling Time(mS) @0����Name  @
Attenuation @
	Ref Level @

Sweep Time  @
Start Factor  @
Stop Factor @0����Receive Antenna @0����Receive Gain  @P    ! " # $ % & 'SA  @0����Fwd Pwr Gain  @0����Rev Pwr Gain  @
App Pwr EStop @
Fwd Pwr EStop @
PV Tol  T �       #EMC_ML Controlled Variable Enum.ctl (@ 	App Power	Fwd Power	PV Source @0����SP File or Level  @0����SP File or Level(NA)   @P 	   ) * + , - . / 0PwrMtr  @
StepBack  @

Max Offset  @0����
Cable Gain  @

StartLevel  @P    2 3 4 5SigGen  @
P @
I @
D 8 ��6��   EMC_PID Cluster.ctl @P  7 8 9PID Cluster Z �       EMC_ML Test Type Enum.ctl 8@ Spectrum AnalyzerNetwork Analyzer 	Test Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) M �       "EMC_M Network Analyzer Cluster.ctl !@P    < = >NetworkAnalyzer 
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P    @ A B C DMotor @0����AntPos  @0����
Tx Antenna  @0����Stimulus Gain @P    G HAmp @@ ���� Probe VISA[]  Y ��)O�   EMC_ML Prefs Cluster.ctl 7@P            ( 1 6 : ; ? E F I JPrefs  K                       LVCC      EMC_ML Probe Location Enum.ctl PTH0   @   Tests
Mode Tuned
ML privateEMC_ML Probe Location Enum.ctl    i �� ��   EMC_ML Probe Location Enum.ctl A@ 
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR	Probe Pos                          LVCC    EMC_PID Cluster.ctlPTH0       SharedEMC_PID Cluster.ctl    @
P @
I @
D 8 ��6��   EMC_PID Cluster.ctl @P     PID Cluster                         LVCC   EMC_ML Test Type Enum.ctlPTH0   ;   Tests
Mode Tuned
ML privateEMC_ML Test Type Enum.ctl    Z �       EMC_ML Test Type Enum.ctl 8@ Spectrum AnalyzerNetwork Analyzer 	Test Type                          LVCC      EMC_RE Prefs Cluster.ctl PTH0   7   TestsRadiated EmissionsEMC_RE Prefs Cluster.ctl   0 @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @0����Spectrum Analyzer @!Show Diagnostics? @0����
Test Title  @0����Antenna @

Start Freq  @
	Stop Freq @0����Gain  @
Numeric $@@ �������� Stimulus[freq][col] @!Ant with Cal  K ��)3�   $AntennaList_Antenna Info Cluster.ctl @P  
     Antenna @@ ���� Antennas  @0����RE Limit File i �       EMC_RE Polarization Enum.ctl C@ 
HorizontalVerticalHorizontal and VerticalPolarization  @0����Bandwidth File  @
Attenuation @0����String  @@ �������� Scan Details  @
Ref Level above limit @!Auto Start/Stop @0����	RF SigGen @0����Antenna Y Graph Units @
Cal Offset(dB)  V �       EMC_RE Test Type Enum.ctl 4@ Emission TestEmission Calibration Type  @

SigGen Min  @

SigGen Max  @#Sweeps @!	SA PreAmp @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  % &BW Table RB @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P    ( ) *BWFreqRange @@ ���� +BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P  # $ ' , -SA Cfg  [ ��)O�   EMC_RE Prefs Cluster.ctl 9@P     	                ! " .Prefs  /                       LVCC   $AntennaList_Antenna Info Cluster.ctl PTH0   P   TestsRadiated EmissionsAntenna List$AntennaList_Antenna Info Cluster.ctl    @0����Antenna @

Start Freq  @
	Stop Freq @0����Gain  @
Numeric $@@ �������� Stimulus[freq][col] @!Ant with Cal  K ��)3�   $AntennaList_Antenna Info Cluster.ctl @P        Antenna                         LVCC      EMC_RE Polarization Enum.ctl PTH0   ;   TestsRadiated EmissionsEMC_RE Polarization Enum.ctl    i �       EMC_RE Polarization Enum.ctl C@ 
HorizontalVerticalHorizontal and VerticalPolarization                           LVCC   EMC_RE Test Type Enum.ctlPTH0   8   TestsRadiated EmissionsEMC_RE Test Type Enum.ctl    V �       EMC_RE Test Type Enum.ctl 4@ Emission TestEmission Calibration Type                           LVCC     EMC_RS Prefs Cluster.ctl PTH0   <   TestsRadiated SusceptibilityEMC_RS Prefs Cluster.ctl   n @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P    	 
   Freq  @!Show Diagnostics? @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P    Pulse Channel V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? @0����RF Signal Generator @
RF Sig Gen Freq Change  @
Nominal Dwell @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P  ! "Dwell ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  & ' (
PID Params  @0����
Test Title  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P  & ' (Amp PID @
Numeric @@ ���� -[Probe Ranges]  @0����String  @@ ���� /[Probe Names] @@ ���� /[Amp Names]  @@ �������� -[Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
FM Channel  @@ ���� /[Amp Cables]  @
Max RF Level Change @
Test Offset(%)  @0����Sensor Correction @@ ���� /AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  O P Q  R S TTiming  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  V W X Y ZBWFreqRange @@ ���� [
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ^ _
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P  < = > ? @ A B C D E F G H I J K L M N U \ ] ` a b c dPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @@ ���� /Probe VISA[]  @
SA Sweep Multiplier � ��)O�   EMC_RS Prefs Cluster.ctl a@P )              # $ % ) * + , . 0 1 2 3 4 5 6 7 8 9 : ; e f g h i j k lPrefs  m                       LVCC      Instrument_Pulse Cluster.ctl PTH0   9   Instruments
InstrumentInstrument_Pulse Cluster.ctl    @!Enabled @
Pulse Frequency(Hz) @
Width(S)  A ��b�   Instrument_Pulse Cluster.ctl @P     
Pulse Info                          LVCC   !EMC_RS Probe Read Method Enum.ctlPTH0   E   TestsRadiated Susceptibility!EMC_RS Probe Read Method Enum.ctl    ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod                          LVCC    EMC_RS Test Type Enum.ctlPTH0   =   TestsRadiated SusceptibilityEMC_RS Test Type Enum.ctl    � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type                           LVCC      #EMC_RS Pulse Type Prefs Cluster.ctlPTH0   G   TestsRadiated Susceptibility#EMC_RS Pulse Type Prefs Cluster.ctl   + @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P        Timing  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ����  
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  # $
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P            	 
          ! " % & ' ( )Pulse  *                          +	  ^ @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @0����Spectrum Analyzer @!Show Diagnostics? @0����
Test Title  @
Attenuation @0����String  @@ �������� Scan Details  @!2Pts/BW?  @0����	RF SigGen ~ �       EMC_CE Test Type Enum.ctl \@ Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)	Test Type @
Numeric @@ ���� SigGenFreq[]   @@ ���� SigGenLevlOffset[]  @0����
Scope Name  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P    PID 
@
Tol+  
@
Tol-  @
Max SigGen Chg  @

Max SigGen  @

Resistance  @
	SigGenMin @P        Cal @0����CE Limit File @0����Bandwidth File  @0����Monitor Probe @0����Monitor Cable @
PID Max Freq  @
Start Frequency @
Stop Frequency  @0����
ScopeProbe  @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ) *BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  , - . / 0BWFreqRange @@ ���� 1BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P  ' ( + 2 3SA Cfg  Y ��)3�   "EMC_CE Test Type Prefs Cluster.ctl -@P         ! " # $ % & 4	TestPrefs @@ ���� 5Prefs[TestType] @0����Amplifier Name  @#Sweeps @
Ref Lvl Offset  @!
SA PreAmp?  K ��)O�   EMC_CE Prefs Cluster.ctl )@P     	 
     6 7 8 9 :CE  @0����Oscilloscope  @0����Arbitrary Generator @
Scope Tol+(dB)  @
Max Gen Delta(Vrms) @

ArbGen Max  @0����Scope Probe @
Nominal Dwell @
Data 2  @@ ���� C[Frequency] @@ ���� C[Time]  7 �       DwellListbox.xctlData.ctl %@P  D EDwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  G H I J $ % KFreq  � �       EMC_AF Test Type Enum.ctl �@  Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)	Test Type @

ArbGen Min  @
Freq Stepback Y �       EMC_AF ArbGen Cmd Enum.ctl 5@ ArbGen from FileScope SetPoint
SigGen Cmd  @2����ArbGen Result(Rel)  @
Scope Tol-(dB)  @0����ScopeSP File  @0����PlotGen Err @
Cable Len(m)  A �       EMC_Length Unit Enum.ctl @ incmmLength units  @0����	EStop Str @
Scope SP Offset(dB) @0����	Amplifier @0����ScopeFilePrefix @
SigGen Offset(Vrms) @0����SigGen to Amp Cable @
Scope EStop(dB) @
Initial Gain  @
Delay(s)  @0����Transformer q ��)O�   EMC_AF Prefs Cluster.ctl O@P ! < = >  ? @ A B F 	 L M N O P Q R S T U V   W X Y Z [ \ ] ^ _ `AF   @2����Calibration File (rel)  @0����RF Signal Generator @
Dampening Factor  @
Max RF Change � �       EMC_CS Test Type Enum.ctl h@ CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search Type  V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev B �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h iAM Mod  @2����CW File(Rel)  @!Pulse?  @
Pulse Freq(Hz)  @
RF Sig Gen Freq Change  @
Mod SigGen Offset B �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h iFM Mod  @@ ���� 1
Bandwidths  @0����BW File @0����SigGen Cable  @
SigGen Max Offset @0����Cal Injection Probe  @0����Forward Power Analyzer  @0����FP Cable  @
FP SA RefLevel  @
FP SA Atten @
FP Overshoot  @
FP Tol- @
FP Tol+ @
FP EStop Adjust @0����Output Power Analyzer @0����OP Cable  @
OP SA Atten @
OP Limit Adjust J �       EMC_CS OP Units.ctl .@ dBmdBuAAmpsInduced Current Units @OP SA #Points @FP SA #Points @0����Mod Adjust(dB)  s �       *EMC_CS Induced Current Limit Source RB.ctl ?@ Cal Category LevelCal Category LimitOP Limit Source @0����Cat File(For Test)  @
OP Target Tol+  @
OP Target Tol-  @0����Injection Probe @
OP Target EStop Offset  @
OP Ref Level  @0����Cat File(For Cal) @
Cal Category SP Offset  @0����Directional Coupler @
SA Time Multiplier  � ��)O�   EMC_CS Prefs Cluster.ctl w@P 5  b c Y d e f j k l m  n B F 	 L o p q r s t u v w x y z { | } ~  ! � � + � � � � � � � � � � � � � � �CS  @0����Signal Generator  @
Sig Gen Max Delta @
Scope EStop Offset(dB)  @
Sig Gen Freq Change 9 �       EMC_UI PID Cluster.ctl @P    ScopePID  @
SigGen Min(Vrms)  @
SigGen Max(Vrms)  ,@ OscilloscopeSpectrum Analyzer Input  @0����Spectrum Analyzer Name  @0����SA Cable  @
Scope Target Offset(dB) @
Nominal Dwell(s)  @
Read Delay(s) \ �       )EMC_CS101 Test Ripple Graph Unit Enum.ctl *@ dBuVVrmsRipple Voltage Units  @
Initial Scope/SigGen Gain @2����CS101Cal File(Rel)   @
SigGen Target Offset(Vrms)  @0����Test Level Limit  @
	Line Freq @P  � � � �Test  @0����
CategoryNm  @P   �Cal K �       EMC_CS101 Test Type Enum.ctl %@ CalibrationTest 	Test Type w ��)O�    EMC_CS101 Test Prefs Cluster.ctl M@P    � Y  > � � � � W L 	 R � � � � � � � F � � � \ & � � � `CS101 @0����Name  @@ ���� 1[Bw]  @P  r �Test Params $@@ ���� �TestParam (Freq, Test)  @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 � � 
 � � � � � � �SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  � � � � � �Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  � � � � �	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  � � �Amp @FreqSettling Time(ms) � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) ; �       EMC_UI PID Cluster.ctl @P    
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  � � � � � � � � � � � � � �Seek  @2����Seek File (Rel) @
SigGen Offset @P  � �Test  @0����PV File/Level (dBm) @
PV Tol (dB) @P  � �Cal @
App Pwr EStop Offset (dB) @0����Cable @P  � �SigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  � � � h � � �Timing  @P  � �ArbGen  @
Fwd Pwr EStop Offset (dB) @@ ���� [Probe Ranges]  @@ ���� [Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  � � � �Network Analyzer  @@ ���� Probe VISA[]  a ��)O�   EMC_MT Prefs Cluster.ctl ?@P    	 � � � � � B F L � � � � � t � � � � � � � �MT  @@ ���� 1[Bandwidths]  i �� ��   EMC_ML Probe Location Enum.ctl A@ 
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR	Probe Pos @@ ���� �[Probe Pos] @Probe Settling Time(mS) @0����Receive Gain  @P  � 
 � * � � � �SA  @0����Fwd Pwr Gain  @0����Rev Pwr Gain  @
App Pwr EStop @
Fwd Pwr EStop @
PV Tol  T �       #EMC_ML Controlled Variable Enum.ctl (@ 	App Power	Fwd Power	PV Source @0����SP File or Level  @0����SP File or Level(NA)   @P 	 � � � � � � � � �PwrMtr  @
StepBack  @

Max Offset  @0����
Cable Gain  @

StartLevel  @P  � � � SigGen  8 ��6��   EMC_PID Cluster.ctl @P    PID Cluster Z �       EMC_ML Test Type Enum.ctl 8@ Spectrum AnalyzerNetwork Analyzer 	Test Type M �       "EMC_M Network Analyzer Cluster.ctl !@P  � � � �NetworkAnalyzer @0����AntPos  @0����
Tx Antenna  @P  � �Amp W ��)O�   EMC_ML Prefs Cluster.ctl 5@P    	 L   � � � � � � � � �ML  @0����Antenna @0����Gain  $@@ �������� Stimulus[freq][col] @!Ant with Cal  K ��)3�   $AntennaList_Antenna Info Cluster.ctl @P 
 , -Antenna @@ ����Antennas  @0����RE Limit File i �       EMC_RE Polarization Enum.ctl C@ 
HorizontalVerticalHorizontal and VerticalPolarization  @
Ref Level above limit @!Auto Start/Stop @0����Antenna Y Graph Units @
Cal Offset(dB)  V �       EMC_RE Test Type Enum.ctl 4@ Emission TestEmission Calibration Type  @

SigGen Min  @

SigGen Max  @!	SA PreAmp Y ��)O�   EMC_RE Prefs Cluster.ctl 7@P     	   
   8 4RE  @T 
Time Stamp  @@ ����Timestamp[] @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P Pulse Channel F �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h i
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop ; �       EMC_UI PID Cluster.ctl @P    
PID Params  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P    Amp PID @@ ���� [Amp Names]  @@ �������� [Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h i
FM Channel  @@ ���� [Amp Cables]  @
Test Offset(%)  @0����Sensor Correction @@ ���� AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  8 �       EMC_SA BWTable RB.ctl @P  ) *
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P 6789:;<=> s?@ABCDEF � q rGHIJKPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @
SA Sweep Multiplier � ��)O�   EMC_RS Prefs Cluster.ctl _@P )  L  !"#$%& c n B F'() 	*+ � �,-./012 �345LMNOPQ �RRS  @@ ����SRS[]  "@7���� Traditional DAQ Channel @@ ����U
DAQ Chan[]   @p InstrVISA resource name  @@ ����WVISA[]  @p IVI  IVI Logical Name  @@ ����YIVI[] "@7���� DAQmx Physical Channel  @@ ����[DAQmx PhysChan  L �       All Prefs Cluster.ctl .@P  ; a � � �	TVXZ\AllPrefs  ]     @-  	 
 c  x    
 d   0   `    
 P    
 c     @flg @oRt @eof @P    udf @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P  
     Header  @0����Spectrum Analyzer @!Show Diagnostics? @0����
Test Title  @
Attenuation @0����String  @@ �������� Scan Details  @!2Pts/BW?  @0����	RF SigGen ~ �       EMC_CE Test Type Enum.ctl \@ Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)	Test Type @
Numeric @@ ���� SigGenFreq[]   @@ ���� SigGenLevlOffset[]  @0����
Scope Name  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P     PID 
@
Tol+  
@
Tol-  @
Max SigGen Chg  @

Max SigGen  @

Resistance  @
	SigGenMin @P  ! " # $ % & 'Cal @0����CE Limit File @0����Bandwidth File  @0����Monitor Probe @0����Monitor Cable @
PID Max Freq  @
Start Frequency @
Stop Frequency  @0����
ScopeProbe  @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  3 4BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  6 7 8 9 :BWFreqRange @@ ���� ;BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P  1 2 5 < =SA Cfg  Y ��)3�   "EMC_CE Test Type Prefs Cluster.ctl -@P     ( ) * + , - . / 0 >	TestPrefs @@ ���� ?Prefs[TestType] @0����Amplifier Name  @#Sweeps @
Ref Lvl Offset  @!
SA PreAmp?  K ��)O�   EMC_CE Prefs Cluster.ctl )@P           @ A B C DCE  @0����Oscilloscope  @0����Arbitrary Generator @
Scope Tol+(dB)  @
Max Gen Delta(Vrms) @

ArbGen Max  @0����Scope Probe @
Nominal Dwell @
Data 2  @@ ���� M[Frequency] @@ ���� M[Time]  7 �       DwellListbox.xctlData.ctl %@P  N ODwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  Q R S T . / UFreq  � �       EMC_AF Test Type Enum.ctl �@  Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)	Test Type @

ArbGen Min  @
Freq Stepback Y �       EMC_AF ArbGen Cmd Enum.ctl 5@ ArbGen from FileScope SetPoint
SigGen Cmd  @2����ArbGen Result(Rel)  @
Scope Tol-(dB)  @0����ScopeSP File  @0����PlotGen Err @
Cable Len(m)  A �       EMC_Length Unit Enum.ctl @ incmmLength units  @0����	EStop Str @
Scope SP Offset(dB) @0����	Amplifier @0����ScopeFilePrefix @
SigGen Offset(Vrms) @0����SigGen to Amp Cable @
Scope EStop(dB) @
Initial Gain  @
Delay(s)  @0����Transformer q ��)O�   EMC_AF Prefs Cluster.ctl O@P ! F G H ! I J K L P  V W X Y Z [ \ ] ^ _ `   a b c d e f g h i jAF   @2����Calibration File (rel)  @0����RF Signal Generator @
Dampening Factor  @
Max RF Change � �       EMC_CS Test Type Enum.ctl h@ CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search Type  V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev B �Œ�O   !Instrument_Modulation Cluster.ctl @P  q r sAM Mod  @2����CW File(Rel)  @!Pulse?  @
Pulse Freq(Hz)  @
RF Sig Gen Freq Change  @
Mod SigGen Offset B �Œ�O   !Instrument_Modulation Cluster.ctl @P  q r sFM Mod  @@ ���� ;
Bandwidths  @0����BW File @0����SigGen Cable  @
SigGen Max Offset @0����Cal Injection Probe  @0����Forward Power Analyzer  @0����FP Cable  @
FP SA RefLevel  @
FP SA Atten @
FP Overshoot  @
FP Tol- @
FP Tol+ @
FP EStop Adjust @0����Output Power Analyzer @0����OP Cable  @
OP SA Atten @
OP Limit Adjust J �       EMC_CS OP Units.ctl .@ dBmdBuAAmpsInduced Current Units @OP SA #Points @FP SA #Points @0����Mod Adjust(dB)  s �       *EMC_CS Induced Current Limit Source RB.ctl ?@ Cal Category LevelCal Category LimitOP Limit Source @0����Cat File(For Test)  @
OP Target Tol+  @
OP Target Tol-  @0����Injection Probe @
OP Target EStop Offset  @
OP Ref Level  @0����Cat File(For Cal) @
Cal Category SP Offset  @0����Directional Coupler @
SA Time Multiplier  � ��)O�   EMC_CS Prefs Cluster.ctl w@P 5  l m c n o p t u v w  x L P  V y z { | } ~  � � � � � � � � � � + � � 5 � � � � � � � � � � � � � � �CS  @0����Signal Generator  @
Sig Gen Max Delta @
Scope EStop Offset(dB)  @
Sig Gen Freq Change 9 �       EMC_UI PID Cluster.ctl @P     ScopePID  @
SigGen Min(Vrms)  @
SigGen Max(Vrms)  ,@ OscilloscopeSpectrum Analyzer Input  @0����Spectrum Analyzer Name  @0����SA Cable  @
Scope Target Offset(dB) @
Nominal Dwell(s)  @
Read Delay(s) \ �       )EMC_CS101 Test Ripple Graph Unit Enum.ctl *@ dBuVVrmsRipple Voltage Units  @
Initial Scope/SigGen Gain @2����CS101Cal File(Rel)   @
SigGen Target Offset(Vrms)  @0����Test Level Limit  @
	Line Freq @P  � � � �Test  @0����
CategoryNm  @P  & �Cal K �       EMC_CS101 Test Type Enum.ctl %@ CalibrationTest 	Test Type w ��)O�    EMC_CS101 Test Prefs Cluster.ctl M@P    � c  H � � � � a V  \ � � � � � � � P � � � f 0 � � � jCS101 @0����Name  @@ ���� ;[Bw]  @P  | �Test Params $@@ ���� �TestParam (Freq, Test)  @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 � �  � � � � � � �SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  � � � � � �Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  � � � � �	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  � � �Amp @FreqSettling Time(ms) � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) ; �       EMC_UI PID Cluster.ctl @P     
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  � � � � � � � � � � � � � �Seek  @2����Seek File (Rel) @
SigGen Offset @P  � �Test  @0����PV File/Level (dBm) @
PV Tol (dB) @P  � �Cal @
App Pwr EStop Offset (dB) @0����Cable @P  � �SigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  � � � r � � �Timing  @P  � �ArbGen  @
Fwd Pwr EStop Offset (dB) @@ ���� [Probe Ranges]  @@ ���� [Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  � � � �Network Analyzer  @@ ���� Probe VISA[]  a ��)O�   EMC_MT Prefs Cluster.ctl ?@P     � � � � � L P V � � � � � ~ � � � � � � � �MT  @@ ���� ;[Bandwidths]  i �� ��   EMC_ML Probe Location Enum.ctl A@ 
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR	Probe Pos @@ ���� �[Probe Pos] @Probe Settling Time(mS) @0����Receive Gain  @P  �  � 4 � � � �SA  @0����Fwd Pwr Gain  @0����Rev Pwr Gain  @
App Pwr EStop @
Fwd Pwr EStop @
PV Tol  T �       #EMC_ML Controlled Variable Enum.ctl (@ 	App Power	Fwd Power	PV Source @0����SP File or Level  @0����SP File or Level(NA)   @P 	 � � PwrMtr  @
StepBack  @

Max Offset  @0����
Cable Gain  @

StartLevel  @P  �	
SigGen  8 ��6��   EMC_PID Cluster.ctl @P     PID Cluster Z �       EMC_ML Test Type Enum.ctl 8@ Spectrum AnalyzerNetwork Analyzer 	Test Type M �       "EMC_M Network Analyzer Cluster.ctl !@P  � � � �NetworkAnalyzer @0����AntPos  @0����
Tx Antenna  @P  � �Amp W ��)O�   EMC_ML Prefs Cluster.ctl 5@P     V * � � � � � � � �ML  @0����Antenna @0����Gain  $@@ �������� Stimulus[freq][col] @!Ant with Cal  K ��)3�   $AntennaList_Antenna Info Cluster.ctl @P  6 7Antenna @@ ����Antennas  @0����RE Limit File i �       EMC_RE Polarization Enum.ctl C@ 
HorizontalVerticalHorizontal and VerticalPolarization  @
Ref Level above limit @!Auto Start/Stop @0����Antenna Y Graph Units @
Cal Offset(dB)  V �       EMC_RE Test Type Enum.ctl 4@ Emission TestEmission Calibration Type  @

SigGen Min  @

SigGen Max  @!	SA PreAmp Y ��)O�   EMC_RE Prefs Cluster.ctl 7@P      *    !" B# >RE  @T 
Time Stamp  @@ ����%Timestamp[] @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P '()Pulse Channel F �Œ�O   !Instrument_Modulation Cluster.ctl @P  q r s
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop ; �       EMC_UI PID Cluster.ctl @P     
PID Params  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P     Amp PID @@ ���� [Amp Names]  @@ �������� [Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P  q r s
FM Channel  @@ ���� [Amp Cables]  @
Test Offset(%)  @0����Sensor Correction @@ ���� AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  8 �       EMC_SA BWTable RB.ctl @P  3 4
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P @ABCDEFGH }IJKLMNOP � { |QRSTUPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @
SA Sweep Multiplier � ��)O�   EMC_RS Prefs Cluster.ctl _@P )  V *+,-./0 m x L P123 45 � �6789:;< �=>?VWXYZ[ �\RS  @@ ����]RS[]  "@7���� Traditional DAQ Channel @@ ����_
DAQ Chan[]   @p InstrVISA resource name  @@ ����aVISA[]  @p IVI  IVI Logical Name  @@ ����cIVI[] "@7���� DAQmx Physical Channel  @@ ����eDAQmx PhysChan  *@P  E k � � �$&^`bdfAllPrefs   0����  T     P hiihjh  !  
  @ ��������h    @ ����m  P mmm  P qmmmmmm 
 P jm  P mmmmm  @ ����t  P m sum   P pphrhhhhmmmhv  @ ����w " P khlhmnlhoxh ml 
 P pp  2����  P o m{mm{ H P !hhmqmmhmzh|ommo{mhhmoklhmhhmhmmmh  P omm p P 5k{hhmmj~{lmlmmzh|m~uhhmhhhmmmmmmhhhmmsj  hjhmmhmmhmhm  P {mhm 
 P mh D P khhhlmmmmqh|hmmmjhhmmzmjmhh��oh 
 P hu  @ �����  P 
h�mmhhmmmm  P h   mj  P hhhmm  P hhh " P hqm{mmmommmm o 
 P {m 
 P hm 
 P hh  P lmmmmmm 
 P h�  @ ����h  P hhhm 8 P klh���� mz|j���mm��mp�j��  @ ����o  P hmmmmmhh  P 	hhhmmmohh  P hmmhm . P klh|hu�p� ���qo��h��  @ ��������m  P hmmh�l  @ ����� 0 P khlh�hohmnmlhhmomm lv  @ ����i  P lmm < P hhhhhhhhhhhhmmmmmmm�uhs {hh X P )k|l�~m{mmlhmmzomqhjqp���jhm~�mmh��jmmmm�m  @ ����� 
 7����   @ �����  p Instr  @ �����  p IVI    @ ����� 
 7����   @ ����� $@P y}����������dfd $@P y}����������txd $@P y}����������old $@P y}����������ext  P  	g����  @ �     P ���e  P ���c  P ���a  P ���_  P ���]  P ���   P hhj{hh  P �  P      P ��� ;  P ooo  @ ����j "  <None>SineTriangleSquare  @ ����� 
 P ��  P jjj �  		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File   @ ����� 
 P ��  @ �     P ���   P ���  @T Minimum @T Maximum @
	Increment  P ��� *  LogLinearFileSusceptibility Spec  @ ����� 
 P ��    MaxMinMeanFirst Probe   @ ����� 
 P ��  P ��� M  P ���% *  Emission TestEmission Calibration   @ ����� 
 P ��  P ���  2  
HorizontalVerticalHorizontal and Vertical  @ ����� 
 P ��  P ��� *  Spectrum AnalyzerNetwork Analyzer   @ ����� 
 P ��   	App Power	Fwd Power  @ ����� 
 P ��  P ��� � 4  
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR  @ ����� 
 P ��   
Rx AntennaRF Probe Ave  @ ����� 
 P ��  @ ���� � 
 P �� X  Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)  @ ����� 
 P ��  P ��� �   CalibrationTest   @ ����� 
 P �� ^  CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search   @ ����� 
 P ��   incmm  @ ����� 
 P �� &  ArbGen from FileScope SetPoint  @ ����� 
 P �� �   Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)  @ ����� 
 P ��  P ��� ? N  Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)  @ ����� 
 P �� 
 c   |  
 c      �g 
 c     
 c           `    > P         
 d      �      � �  �  �  �  �  q0 �  q0 q0 q0 q0 �  �0 q0 �  q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 �  q0 q0 �  q0 q0 �0 �  q0 �0 �  �  q0 �  �  �  q0 q0 q0 q0 �0 �0 �0 �0 q0 q0 q0 �  q0 q0 q0 �  �0 �  �0 q0 q0 �0 �  q0 q0 �0 �  q0 q0 �  q0 �  q0 �  q0 q0 q0 q0 �  �0 q0 �  q0 q0 q0 q0 q0 q0 �0 q0 �0 q0 q0 �0 �  q0 q0 �  q0 �0 �0 �0 �0 �  �  �  q0 q0 q0 �  q0 �0 �0 �0 q0 �0 �  q0 q0 q0 q0 q0 q0 �0 �  q0 q0 q0 q0 q0 q0 q0 q0 �0 �  �  q0 �  �0 �  �  q0 q0 q0 �  �0 �  �0 �  q0 q0 q0 q0 q0 �0 �0 �0 �  q0 �0 �  �  �  q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 �  �0 �  �0 q0 q0 q0 q0 �0 �  q0 q0 q0 �  q0 q0 q0 q0 �0 �  �  q0 q0 q0 �  �0 �  �0 �  q0 �  q0 q0 �0 q0 q0 q0 �0 �0 �0 q0 q0 q0 q0 q0 �  �  q0 q0 q0 q0 q0 q0 �0 �0 �0 �0 �  q0 q0 q0 �  q0 q0 �  q0 �  q0 q0 q0 q0 q0 q0 �0 �0 �0 �  q0 q0 q0 �  �0 �  �0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 �0 q0 q0 q0 �  q0 q0 �0 q0 q0 q0 q0 q0 q0 q0 q0 q0 �  q0 q0 q0 q0 q0 q0 q0 �0 �  q0 q0 �0 �  �  q0 �0 �0 �0 �  q0 q0 q0 �  �0 �  �0 �0 �  �  q0 �  q0 q0 q0 q0 q0 q0 q0 �0 �0 �0 �0 �  q0 q0 �  �0 �  q0 q0 �0 �  �  q0 q0 q0 �  �0 �  �0 q0 q0 q0 q0 �  q0 �  q0 q0 q0 q0 q0 �0 �  q0 �  q0 q0 q0 q0 q0 q0 �0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 q0 �  q0 �  q0 �0 �  �  q0 �0 �0 �0 � �     0 g  E    
         @  ?    ( >  2 5 <  k  ! P  V  Q T  W Z ` �  p t � � �  � � �  �  � �  �  �  �  �  � �  �  � �  � � �  �  �  �  � �  � �   �  �   $     & ^ ] *18V W` _ b d f e       ^  3�x��Z���@�;e=�-��AD;!9l�BX�@��@�?�F.�����X["6Vܵ�V�	ga���d&�M��Wf����ef�̼�z�I P� ��H�N	H1!�{����ڃ�Ѩ�h J6�������|{�)V�z��2�mΞ4�Tc��F�#�f6=�~���|�HH�	��zp}):+���5x?����B0��J�%�-n���Ax��oGR+	��Rx�u�0�~ �n�N��f	}AM^%E���*Hs�^n]��S�o���2��qE/���fc��QIf][��h�2l���������ѱ���wE��,d�X^����О�.��T�\:}N4A�QW���kua5D�(H����"庍�;Dc��ud��=y���J31��<r	M���ne�>��d)�>���[p�?�6�a+�-�@�?h���Z�	"�'�?�{��ݯd����R�d�-nW��K�{���WM�yNk7�E��B%<X]i������U��S>)T�B�8:�Zc��	% 9�Te�i�GCina��@��u��ki+�R��f��"�^W���1I�`SX���"l��
�`�s�4z�����:Y��pǽ v��#o��O�(.|�\�q���O�n�����_@�5{M;ZE���g�u������<0�c`�&��&�"��)G8v�.S����7=�3���6~�w�j�_M3�'r���Eu_rk�:z�,��`*����U����Rmb�T�ڋ֍����C`��oZ�v::<}�@v�;�<a
��p__)1>�'Y��봔}�f{�z�$۽��Qjw�"��gG���:B�a���+͢;Y�C�}��&]�3���|�)�����1�i     $ VIDSAll Prefs Cluster.ctl         =� A(x��}|���,M�����H%H� �M$J� �FJ� AQ�״	]V�NR�J�TC�%V��b%jVQ��J۴/mc�J?�-4�[|EH�ߜs��;3;�sw3���}�s������<��s��d���1��3���*���=X�d���ț��C�긽YSF�G��J�+�P_��U�� ��!p���a�=w�y����Jy�{���n�C�HNV�$�}�A��+e���+9n_N_�*�|9'A��rN�<��9�/��㾜#E��#eP�z~Lq���c���9W�b�ѯ�tĪ�19ǘ<�,>�T�����u$��	2we����eb��#��wV��� f�?���@��`���]N0y��]������c�}U(�Ry\�>����/:�잛�(�<Uq�����a}�@@I�U�U��'=o`�+��ԫ�����|{s�9=��=��9{6�&��Cԋe[T��	A5�jި��zJ����.�8�P��B��ۗ���/�pQozC��+H�~+>)w�<9�c=9]����=Շ]k�*9�br��y���7>��4��ʭɿ���	!�|��Xd�����;boN'��P���̜Ί+�s:}9�TƷ1�_ғ�T�+CȦ���ƨ����2��j���f���A�l��l-*�z+��[��1�ʠ��ʿi%���ɭQ���1�RK{M�)�mJ�֝�g�z�#��̕}8Je��2ߐ(��j���[��6]|hc2�{2�|�DH��i����i)R*[��V]�L��iZ��m�ne�E�lU�[�n�#�� �R�"�%�bkXG�~�S�/�Z��Ѫ^]��D`�.O�.5���/4��D��e�݂�T��hK�Z�'�of]x��e7uk��M�����d�L�E&���&��Y�w3��f���y�ב��%�䳾'��K�F�'`�r6�M��]4l��)�we�"71��g�0�w.����cVժ��V����auL֪i��Z�g�1Y̧a����@emP�:+��	ݤ0[�˥jy��������ɨ=1�L�+��Ke0_]]���I�hm'�[���F�H5���[���7x�뙬Q�(��ӄ�L�ܕ)�kR&�/�Ѷ~Os�7�$��I�C)lҎG������K���	�L��g@����|�L��.��PK�zX�u8O������[�Ϊ�l5��A��0�Uy1�vL�!������)eB)��Z��J�`�F,T�c:����	�d�V!F�cM���p}�="èt c�%���.k��ҹ�NMfQ,���{�7��q��sn�ް���`�F8ф>J��N�EX��[�k�ep�ߵ�i+����Fs��m-d�������ڬ�ۦ6}�R%p��� ��Ȥ`ZvXB�X�%��Q*��[:<M͔
�JЮ�]�a�j���eY���+8H%�'E����lʹ��+����;�s �wE����e-�,K�R0��ԧ�ۥ�������r0.��p�C��%���4�:[m��2�<_�H�e��|QO����?���tϠu����"(��MӚ�Ӵ� ���!^�y�Y��p���ѫ �^w	�
�^�[ٌ��p�0�	Q���띅Vz36���!�|Y�/��4`�!F��8ƹB�b�Gd��<Ɠ����p��g`��N����������C3�p�]��tIQpv��|�<]��� ��fp��F�k��<Q���i����]��������ܮ(�r^�����zR����V��o⻼����|(Xk�����d�IV������<[hn>1�5���-�G�h!W��K	g��e��+���!�������� �%24��a����?��_���L�G�"[֏o��,���ۜ��9�b{b��M����i}�����\U�8�G���h�L7$�z~ʚ�Ɛ�c�)>�LaVOA���Y
���z�)/9Ly���Hu������#CL�EJy��k�W0c#�����:w.C��0�ʚ�e��v܅�bx�@{���)a���Mܮ)B_MY>�X]}&Q}&i�ǆ��I����;��R����S�_kOPe�Y{� !�BYu��͊��(}�Xн�+F��g��9FL�Ƙ�M$j"�}���{u��?����.F����BA
]a(��)t	
1w��w^h�׶l�˃��=��;Pz����x�(�XmH��kgcE8�$��+���ʕ�1vXb8m� �e���Oh��	��ڇ�����d�r366���t
��᲎'L�J�&+hJ�qMǈ�c�����a��Pr�c�9���PC�7�v\�f�.<oGt�&�k�=Fߢ,����a�Y#����|��v���T떆�O�T�O16�)���x;�����a�r@��!��yx=�SpHP��B��u>]���Gg����G��0�'�;�O���~Cc���-��������k��	S��u=�*ʲ������r=�ʫ���j�խ��}��i��������nA�>4���X���4����m��[P������0:ɕuf�i\�[����~���7�34AϺt�*�{!e��²P/��y���=��
S�9�����A����%,�}�]:=:I�N��������%��D=�׿�F�.!⮣Cg�v�k�Ʈ�0v}����vm�V)��\�m�M�c�ئ����q�б��|~8n�	\����B����!-&��=d�V3�`f�L�e@3Sm��*HHGTf�pf� }ަ�����*���b���S�T��]�V����<��ho��PK�z��B�X�����_��v�Q�m�I�E�E7����^A��5���b�p �K1�&I���^�P�̡j�9T-s�Z��5�Pk��-�ڡjȡ��aWDK�/�^��t}N/I�[O�WC�W��W���8[�<�]���k�̵�P��������5ʬ�����
^-*r+��.V1eB)��:u��^�����ߐN���\�� �QvXAeUaYp�]��t1<]��BSVU��.vU	}5e��e��,��I)6����._�$�2Է��M�(o��4�s�tL��,�]�L�vEY��:����&���(���4���.HZ��w��XC�r�TKHw��ߴ�f6F�"mD/f��G􊁐�^�"z1zz>E��`DGu�"���2b��^@-�PK0����"z��@COQ�Xr��"���K0~�h���b��mZŁ<]ȥ���Q2/����R�� �s���?����D;�!�<?�bŅ<����.�+=���u��E��������1[蘅:�b�{��1�m�� ���6�K���3�N��a�!Q������]�q����B�!S����0��R�sz�c��L#o���H���to$:�܌��(��p�[��tI�t��2�y��s��:�ӈ�4�]�a��N
�+]ؕ���b�� �pK1��a��|$��1E�G
��cJ=�,V�)���D�D�̦SPwu�m��)M��@H�XN����I���d������0�c~���:�b�v��1���!�'���h/q@3�K��HHg:ڃ�0�KB��p��~�w,�z��@�A�c���'h�J�Ւd�A
u��������/�����"���=����eܨ�����Y_?�0ѯy�!�2�� ^���������rL�EJy����+��մyC:��~s����0@��R�a0�����zĒoa�F8Ѯ`��(ʺز�`>cc;ޯil'���������&46�>�~ll ����[4���1Ec;FH�'�U�����O����~r�cxv�����*!���?�rj Y��0�'��Ხ�t������x+�hr�K4L2�k	��'�m�u�	��=~�u�o|B������q�%����=`�a�!Q�5A�,M��E�D�?0���6���{�{������;�A�{�z�Y;���ώ����IͦS�l���b��C��%�l;��X^��*�=�	�S��k������g�A�)��2���t*�U�/��ĠZ����C�� `�o���|8��X+���P,1��R��D�'ɧ�;���Ò�~��d{?.Co���a��"�n'/�� ����w�`���1���_�b�8�H�κ]��� k����k�i���;^pѐ��Iu��o�Ɠ6��{a5���;�(}��%-,�T�D��4�����D��J��6��ީ��+�[�2P28���	̗�߆My����ɱaLޤ5����di2ܩ����[G&�	��Z9�n �	�C��LC"<��T��o�l=;�ųv�����d�Ʃj������?����f�m��@cʄ(R������
fl\ch��t�e-����Qv���"��tWPܨ��V�o\���9�Q��V����-�&U�8T�vUh�
c�r���GW��:}q|�\O2r�46�c��Ni�d~8n�ն�"�h�d���<�j�௿�hn�0w=��^cnms���*�UIY��d��=/�+���]�kf���wp�5�������c��v�>BDy���UXK5��V�Șޫ[S�tIQ����Zզ�s�*���t��k��9���t8��ؓ��kz��~��AB:r#Cg7a	�=��;s8Bu���2G1E�^L<j�����b%a����@
u�����x��+��J��%&����|FL>��M�dK3!���\"�P��.��@{����0�=����_�W�?�b����.vL�����:C2�������(�B���zV&��p���<+} �<+Mʳ�;�ڳ�ɳ5=k�ѳ���3��l��t�,�������P�+~i�L��\
���p5��j.9K57V����kn<��N��+4�	���:�õ�QP�T���`�M	W|�ԯy$��$*QSxR��/��"I�'b}�b���c<� ��`�� �oF�I0nr c�o���-0���PC�'c\��q��C>�p��Db�H9��Ԯ
Zԇ����ჯ�ŖZĊ&&�㉂荘C3��Z�}�oT�ـ�w�ރ#�9=Hp<�ŀ�ݯ0��Ӏ$�30�M0��y]�8` a?`��P�C,��&�À��%3���j*�������P��I?@I?�F0�[�+�i�b���Q������q�߃6�'�4p��* $V@�B5܁�j�>�>d[�>nA��!����3 �����K�.�� �`���7��*��I�J����/�'���IVp&J>��I7�1��C��;̪�e�f�w�����w¢�Z]�w�����@�����;����;dYCz/��m��P���;��ׁ����}��VA�����]�����#74*Z\����q����G���RyL�>�6p�^q\����{���+Vl�9��_�۫̍�yS��4�g���\�̛܏NP��̍�yS��?')#ft�������q�z�aa&�&C�ƽ�|�/�cU�O��cœ2\�v�q}���v�z��U�RyB�>n��wo�25;OA�'�ǿ���K���0�G�g��E���\i�,K;8��������cç�Z��^7�ٴ+_�V����ҷ��f�V^�7��&��H�^���-�B�U��!�*��"�Qohv؛ͳw��*TU1R��_쁋=��C�k�Y���4D��n��SY|�widq��揾����z����v���T�ӥ��x�� @���Vr:br:�����4Ou�!K��������h�2���w��Ҥ�����r{�ʄ�9[�J���īA�]�&%�W��W�US�竷v{v��fUoS�z[j�V��s*�ێ��Q��v���ڷ�F�ᾜ�����f%�E�d)��+����X%�W��WX���%Q��Y��T%7�ց��S)��{�E�c��^��ϩ���ݞwݷxN��v)�7i﩮38H���A?��&�֍ً�s&k��s5���[���56ր�5��̬!3kz�Eʿ���hg��[�z
�R�wEg(�ATp��y���A����AV��n�ŷ(Y�[����!b��z�G�t��������x�L|�����=B��x����� �/�_���g�儸�#�� �������Z�❈��A��� b�N@|e�s��q����q �֐;���F���u��x9����h���l[�@���c�&#B�A�كD	�(_��2�^|�*ޖ�x[�bj	��b����c~v���-�\J=���٥��l���9�.�[З{��uG;G-��Z�-,��pe; .o�e�zƅXHC����7��x/~�z���F��k�W�O����16�4��)m*��E� ��׏�İ1[}}�Y�����;�dc��������Z4|�9r��8�Aď_ă�9��]DTD���3_�qV���g�|'�FY١��le}ͦ%D�*w��xU�]�.,���<�.y�	�n5�v�B���j�b7��k��l��B���3郯^5�I�Lܶ@����_�?oc���z��s�6����Z��5��x(#�P��yk����=����|�b9�*8^dW��kG���
�������E�6�bB��
���
C��1t䜮D��Xq�8D�@
�v��g�x��g������?O^3�z�d}���^�l2���ʄ.��	���7�~l�Ө��T���A8��^���?8�f��2�_�u$�g�"��.b�qE�ʗ9���-�Zkfq,Z�7��Xfq,Z���$9�[G��,�������mF�Qu��xI���	��f�ܧNh}
?��4��/������g���fM^��g�K\�⯌�9%��K�^�,��dD�ci�C:��S���TF'{�Gm�����e���I��d^׽/�ƼC��r[^��љ8^�h����#e�����L��
hk�:��gJ�d�����p���kj�V5ţ�1];BQ�>�~D�}�� ?��A9�
��$���TH��U��R���7���ҬoX'�Ӯ�Y��g����z��<�tk�叉����:�zV��}�NąK�t�C�p�3�=���8a�\�t
���~�L�*����2D[,�f���r���y�O��s��V��V����A��hK �����Ѷq{ �o�=���޾R�m�Q���;[d�4���H�+��gy��`���n=��I�<�,���%R������B�X���������b�9�C��R
ͬ�����*ު��Zu��f3�n�����xl����v	����}w3��n����E�v#�]h�
�ύ��h���1Ďf>���1Qz���8�m�� kmR���`���3����[�qݧ�Q�eC�=?�l� �{q�
���5 �����l��5 Hk@ S1-��hߋ޹Q��<��?O�Pm��^��V>�=p��@��h{+�����6��5��m��6�=�GC�����C���F�[C�oe��P��l׿~h�u��D�Kh���j��YD�&�N�5]�j�5��9%�?�p�����,X2�ٮ9���?���dt�X�B�URh��_?��_����Y}����Ƭ��U�U�[���ҫ�}�{�&�U�c��k��L�r��r��2r��F@��Ѳ_��xT��QpO]<��2�5��E<�e��Q̀a���y�>������~_���Vȼ��6�4rƪc�!�o�W69��;�8G9���S8�}
�|�gw
=����EC���X�{ʉ1�����ϙ�@������D/�BJ';�	����3�4~�6~י��\D��ڗ�����K���Ch�����m���g���U;�r��=?�~Ў~���C�g,���!�-��i���q�"�����ظ�&�����8�U�+v�c���fz��e�7��:��R9�|fh��W�D�)��Uh3���(��7ŉ�⌅w�N��t�G�h���'����'��'���
�t�؇�����Ğ4i������A)�_e|%��<+�hV�%K����/��;��.������B�W��r)츼�V�K�����v�?��?9����Qp;6�ʝB����Q�8���'dP��f��k��?�4�}��y�f?�#F�y��}��ϼΠ��h[B��
-�x�)�ŝ�;�;��@����CI��yIE���;�-���!�BD[ hI����|x�z+�\���#.�81_�z��׳ );ĭ�4yͣ͢�Q��ȘӞ��|����΍hv,}�mgtz�ۀ��m����#�\)4;c��WI�&E�Mo������p�w�3Qc"�]�ڟj �w52hf���'�������B��d�Z���u��JD{��G�\]d���D�A�i������sp�[���>� -�q_�4��i�����9\����������6�>n|~����T�:�{��j��U����x�N�;)�����0vR�������}�h������g�W;��X���5Ѷ���8Ӻ��K���k�J�d��َ�tm3�FIXB�D	��D	���Ş���p��1�]��Έf���-gtj���j��9L�qG&`IqR%]�%M��>AX�z4;�?�&_D}�L=�|4��|4�y����`1�Ld�����M�h�:ŏ��ٔ�[�k�%j\l{G(gt�_V�'��B�W��'�ʨ`;�T�K�8�\*���ǣ.����v����ֳ������{i��i���඘�����>{�|�$~񄄝׊N�O)���P����p��?�o���V|�#m�g��C��A�bZ���p
^?zd���|����E�s<���|O�G�YVv��'&�?Vr�Ap�8�Z������0g����7�����X�)x�h[�	���]��@'}�N�{g��s��������]�ۣ�!~� ~��s�[q���q������ˇ;�����q�x�������c��C���O=l�F5-ݾ��:�<�Y8)X���!|����]z�!m���P�-�������������Aۀh�B��h�-K�2Y����>}b_=ۿ��L���L�?�ι8��rڳ�����x]v0����9V����z٦��6��m��&Q/3��n���n�(�Z���R(��U8�]�Y�&X��D�ʶ&��j��jb+��*a�+�����z�ۢ�
GP&��[�݌cW3�=��j���)���J������#�/�w��5#�:>b��o2n��u8bk6�؂��M4bC��ia��S�zo��w/��?w�q��.ttPG����9�-���#U��3&j����L�0&j��*�Dg����
�����P����+l�d��Kʩm��r~81_����B�~utX�mI�5k��1�t��)������q�V�HfD�k��i�#��������s��U��gR�������lg�e:*%*C�Je�?��P�>�ջ~|���@[�S	�C��X��U�BI��˹,\#���]�h:������r���/�0Mpi��A���b�[�fD�]�XnU3���i)��2:I����Ch�m��y,]*VdE,�\�����e����U:�v%�Mv��o �ߐA3�I�~C�/�kk�-�,4���YE�"�-�h51B�"�]j'P$��R��K�ƛ\�|��y�o>��'��¥��/�_�A��w$�;�R��F�o� Z{���%2�U�v�%�2W�e6q��,dKp9�D</\f#��(o-��3K�g&际%dJ��x	�3K�I}n���ڳ2�J�])�pb������Crv�������=��%"G[�h�B�h8���ŀ��bg��!�c�MG�+m���Q��O���O�e���Yl�	�{q�M8����Ϗ{mR~�Y��Y�TL�� ���{g�̿&����_��~���⽟�m9�7Z��-M2�4	-Mki�4	-M`�&qK��F��g`z���z7�_�;��f����N ���Y�0��w[��ϭ�}��p�4�D�[�)AľXl�(9��;eP��U�����w���v�/�m}}&�}~���� x�TL��S�̺����Xd�?4B�xI���i;����k�o�-���G�/�p�18;�g�?��UpQ�É��E�&!S��C �匾\P{� UH��U&�����v.͢�W*�]��o��7�o��#�rQ��z(�# �c��N���ܖc#�O����������|����W!);�A�D^3v'���9%�8�+� Ds���~�����N9R:I<�]��9a᫋�������i�"+o"gҾ����n�����.�yD���ݬo�ƾ���A8����-xocW���I^�콃>��6܆�oo2֣C���SD�x! \��wQqq6"�\h�����>���ܑ�=��{�v3ێ��ö3ێ��<,����[�G-G�k܄��|�a�g�v|������)h���QЂ4����QЂ41
Z8p�[,m���g��?M��4!��*M�zNH�!uH�����1Bꐐ���:Nb�s�q��>���dw�|K�51�t�c����kt�~��k�J�'X*JX#J�B��}w�|@3�ǝ5"�D��~��2��jD{�!������_�ڇ�d���q�L]�>��t�s�>W����sX�j��q׳B���Gy���R(���-�+D=F������7;�V�h�J���f��g	�wHT\��D��(W	��*�?�u�YF�Y�%�I�0_�Y.�,C=�=��rq�7��^.x�m�\�K�َ��:c��i��52>�FJu5RB5R�%�H���&\���[_*�/A�#A[�h�B�h�ğ�#I�m/o޾>����h_:�u�ň[$Q����5^,j���G���?�eP�y>��f�uN�.1��w��]��/���B��o>�+P��oC���ލ���(�S��o7ڜy)mDF��s����������o���j7�j7Kȕ(a�(!O���zʣ\��������8��q����!e��̐�ϟ�n�i`�2��)���ޥ�̚��OG���'4H?�K�^���c[�_�]�O����S������)x�L>����,Q�xm���L��(gt�k6_<G�zr�T��2�u.�"�/�rڕ���|�|�NA�4D�"�2S�&t1�X?��z� ��D��k�[j�4�!�Ǉ�f�a,ƇD�9�">�e�a,ƇD�p
^?�j�7��~�G��>�n,K���q�R8�wdr���n�	�
�i��$�4Wf��/��mq�o��BDc������BP�_��{�t��O�c�����S\�@C���zi������}_�1�=��q/�N��
ަDA�ȋs�`"��2��УMp�%8�	�%��Ľ�Z��ƨ<�z^���;����/U�z0NiE�vH�{���g��!6�����u�w�n�;�[=��]
��#���/�{?���/p��+�<�����o�u89
u��Zg�w�']J=�=92ð�}�L�~�yv�����Xlbh�H]�u3e]�:�!�b>E���S$�;$*���~p|y�%g��&�{h8V�d��.~�ɏ��6�]�]���ᾯ��kot�% 3
ަD��.�d� ���j�C��JتM��tamI[ybh�]!ƨM��v��&�}F;���Ae3$�c1~���������y��8~�m&��L����[��݊�m��@;���n�[\G(TA]}�3���`����#5�h��Ġ�N��$){f���W�2̘G���D
{MnBM��4�G;9��9]�l0�1}pЪ�V��'M�������8ij�'�6&���!3;l��W��ٸ�jn#��9U�Z1�43[�tlF�&��j�@�Ԛ���]s5��]ͧ�c��/h��<$6�6��E��s����!xE_�����A8�})&t���4�^C�)�hל����4\���+�FTft����i�!nA��".D�y"���o�����M-u4�M�j
�5�uX�c����VPH�Tr�4�Z�}���W+�W!���4q�
����Q�X
^?��e���P���{G�!n�Xؘ7�g΁��9��qC��9��16�̌yt��م��2<�p��)*l��r������RkF]��Č���Ț�F]	RWĨ+������S������}�'�E�<��|��,���Y6���٧Cf���R�!�yWr��>�xQ�g�V�?�/Wa�	_m�X?xf߸Y��4����2�൚x_Y�b�:?���Y&v�x1~��r&�y�j��|i�a&�����45�{�ss�����Rܙ2����q�w9�
�DW��!03��,9�:�mS*ڟj�m�#b�%�����/��M�D��p�~��>�Ŝ1����s�tr���n�Z P3j:�fp��e�s� ��^�`/:��'�6��c��|t��i:˧��i�F7Ţ��1YL�D��B�i\�)2��BU�M�a|t�;'����˶�C��5O�y2��!��I\�|�c�ƽ�B�;_ν�M��;_4���9�h�m�#�RKD�&8y�U<s�';���d|�!�9�D��v�>o��!&SCL9�Ns�?�9m�D��kL��H9u_I�4�D����Ӝ~%�"}�{0��p���	��=�ֿ�u�w�`�Ɲ4����8Â�iV�L<�5�rq��f��!WX�܌!72�b�9Cp�;;B�/C�/g�+�~�����Cu�E�n����|��s�K�.Ku�3�<�u���Ri'NI����Pڱ�4�� r�D|�A�[1w<�Ir0���*�!��CǷ?����鸮����㜃c2�r	��}	g��`��!�ɋq��b���-[�S�KH�
�çL	\�5B�`݆H�d*��@�S�z�5R��o�@���?Pc}��K���ٹ��.nv����hvWX�����hv3���G����s�m�k�x#��N�f��X�=V-mߧ��+��.���Ҷo�w��މ�ok{'��m��l��Ñ��"3�=����o'�_�H����z��-�h?j�!86��<����M�[��т� <�d��'�s(8�?�y��Jt�Le%�h��-�@[RO[B�������C�H�R�)�����{o���=����_3>��:�Z���?��F_1ȹ��cP.P�{�[1�ƯDshl �]moRj�s�}��;�1Jg ��d;FY����N�.�p���pjqNAZvXE���)U�tJ�Q�QB�5���� q�V3ߐ�W��j���c^Yؘ��ż���X��)x��ە����p�,���X�?�9�Zz>hr�����q�%������I����|�}� �/3_(F_(`�P�}�8��/1\�����D�Ap�.��<߀�y��x�!ʅ�e�S�N���`̙î>�ؗ�A�_�{�W�A�� ����v���2��ao>��s,���2=�����C<���� :�-���Aą�8�A��9X��8��2"�� �݈X� �HDj���f��*ϲh�
a�NCW�_>���CBv�E��\�,=���㟳���B���q����8@���JD\� ⅈ8�AĽ� �m��c��i܃�!�����X/�l���c�4̙{�|����z8�Kcc� ��׏v��n��_J9Eo�$��)��d)��B��⌦F�C�D�D����D��H|�5�9ĵ��͑f�/i���>��6^��^?����u���>�x]�Kr�)Z؀�ЅE���c	u<�o��݀8��_�D=�BڡR�k�K`ęco�Ğ{�{��W�.>��C�%�W�;7�_>�wn�?�v��a���0�D���!kq�_I4�W�_�;n!��+�دı~%���ȟ<k�~��(�=��L�������ݴ!��n���{�#�Q��������a�R��/�����_*��>?$�x��E��Lń�xK��9��������~5�[����C�=̭=����p��?lm-$��2���2��A,�G��84�j�C7�&��W��$���9D�ț����ab�7Ǝ�n�H�#=�H�5#=��d��1�����;�쏉����ݡ�w3�]v�wr���ٿ�ﴶ��������#��=����+5��� ��o��R%�-�䞻������#�Gg��co.��P��'�-���;�JWG�j��wS
~K�wSV���1���)�󾡞_�����&�`2~�����V�OJ�uLNP��U��ʋUy�ɫ���L���LN��ar����t����O��u$ɷGR:�Y$�΢�T9N�y�H�`27���0�:�d�]L�=��j&�2��dm<ɓL6$��Lf���֏f�L�8�����wN"Io"�_|���ɖ������~�$71����$����L�0龈�#L�K�&G�#�,��^Lr�c�KH>���$��􍿌����5H~�ə�dO�s'�u��މ���IL��Lo&?f����RI���m�����t~�T��t~�&�$��*�/3���M%�	��i$��ެi$�\Mz���U�)�;��|�������kI1������� ��I����:�O09���,:�/�d�ɕד���|���q6�����gS�Wf��a�9���J7���n$���O���M�Q&��a~�d�ɧnb��ez1�/&�ev�L򿘼��<{�o�O�c��+�3e�C&�u+�[���$. ��ϧ�?2��B�ww���X��`�BVo����N�߻��/"�}&���rk�L�|7�//&����}7����й�������/%t��J7c)��}���V�H�}�W���ɋ��\��&����2^�|��/��uɵL�f�h�ռ�ί���~�~:og�r�o9�m��:���%���v� �/���S����0��L��dNɸJ��U�|�1������2&��$����_�Mr����%�@r�O�j:�I�K��+tV�_����1�͍$�n ��ɿ1y��H����S$7�|f��Wv>���?�끧���L��${[����H���Q?&��ɕ�$_e�&�x��KL�o%���$�����d|ɍL��ɋ^&��ɉ�H�{�����$�9]?�����^;H�ͤ���/�I�~A�wL��ӣ���'���L��I��f��������~�%��ɵ{�L��"�������H.�5+�7��ߓ<�{����������_39��$�d��!�� ���LV����GT^�!��1��b�L��ӟ�9GIn`��L��e�3�W&w#���$O39�S��1��3��e��ze2�_$?fr��$_a�s&���~��I�ם&�ɤ��d� ��$/C��Mr�H����<�2�d�9$����Iν��$��.b��H����_B�K&Ǔ|�ɳ/%���_2y�e$�����H~��09�
����oS���+In�Jrش�|U^pu��!5��IK��?R��s�]�_9Q�&���j�k�������9.Ϳ�91���C�x      �   8.2     ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������  +	  ^ @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @0����Spectrum Analyzer @!Show Diagnostics? @0����
Test Title  @
Attenuation @0����String  @@ �������� Scan Details  @!2Pts/BW?  @0����	RF SigGen ~ �       EMC_CE Test Type Enum.ctl \@ Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)	Test Type @
Numeric @@ ���� SigGenFreq[]   @@ ���� SigGenLevlOffset[]  @0����
Scope Name  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P    PID 
@
Tol+  
@
Tol-  @
Max SigGen Chg  @

Max SigGen  @

Resistance  @
	SigGenMin @P        Cal @0����CE Limit File @0����Bandwidth File  @0����Monitor Probe @0����Monitor Cable @
PID Max Freq  @
Start Frequency @
Stop Frequency  @0����
ScopeProbe  @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ) *BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  , - . / 0BWFreqRange @@ ���� 1BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P  ' ( + 2 3SA Cfg  Y ��)3�   "EMC_CE Test Type Prefs Cluster.ctl -@P         ! " # $ % & 4	TestPrefs @@ ���� 5Prefs[TestType] @0����Amplifier Name  @#Sweeps @
Ref Lvl Offset  @!
SA PreAmp?  K ��)O�   EMC_CE Prefs Cluster.ctl )@P     	 
     6 7 8 9 :CE  @0����Oscilloscope  @0����Arbitrary Generator @
Scope Tol+(dB)  @
Max Gen Delta(Vrms) @

ArbGen Max  @0����Scope Probe @
Nominal Dwell @
Data 2  @@ ���� C[Frequency] @@ ���� C[Time]  7 �       DwellListbox.xctlData.ctl %@P  D EDwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  G H I J $ % KFreq  � �       EMC_AF Test Type Enum.ctl �@  Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)	Test Type @

ArbGen Min  @
Freq Stepback Y �       EMC_AF ArbGen Cmd Enum.ctl 5@ ArbGen from FileScope SetPoint
SigGen Cmd  @2����ArbGen Result(Rel)  @
Scope Tol-(dB)  @0����ScopeSP File  @0����PlotGen Err @
Cable Len(m)  A �       EMC_Length Unit Enum.ctl @ incmmLength units  @0����	EStop Str @
Scope SP Offset(dB) @0����	Amplifier @0����ScopeFilePrefix @
SigGen Offset(Vrms) @0����SigGen to Amp Cable @
Scope EStop(dB) @
Initial Gain  @
Delay(s)  @0����Transformer q ��)O�   EMC_AF Prefs Cluster.ctl O@P ! < = >  ? @ A B F 	 L M N O P Q R S T U V   W X Y Z [ \ ] ^ _ `AF   @2����Calibration File (rel)  @0����RF Signal Generator @
Dampening Factor  @
Max RF Change � �       EMC_CS Test Type Enum.ctl h@ CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search Type  V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev B �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h iAM Mod  @2����CW File(Rel)  @!Pulse?  @
Pulse Freq(Hz)  @
RF Sig Gen Freq Change  @
Mod SigGen Offset B �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h iFM Mod  @@ ���� 1
Bandwidths  @0����BW File @0����SigGen Cable  @
SigGen Max Offset @0����Cal Injection Probe  @0����Forward Power Analyzer  @0����FP Cable  @
FP SA RefLevel  @
FP SA Atten @
FP Overshoot  @
FP Tol- @
FP Tol+ @
FP EStop Adjust @0����Output Power Analyzer @0����OP Cable  @
OP SA Atten @
OP Limit Adjust J �       EMC_CS OP Units.ctl .@ dBmdBuAAmpsInduced Current Units @OP SA #Points @FP SA #Points @0����Mod Adjust(dB)  s �       *EMC_CS Induced Current Limit Source RB.ctl ?@ Cal Category LevelCal Category LimitOP Limit Source @0����Cat File(For Test)  @
OP Target Tol+  @
OP Target Tol-  @0����Injection Probe @
OP Target EStop Offset  @
OP Ref Level  @0����Cat File(For Cal) @
Cal Category SP Offset  @0����Directional Coupler @
SA Time Multiplier  � ��)O�   EMC_CS Prefs Cluster.ctl w@P 5  b c Y d e f j k l m  n B F 	 L o p q r s t u v w x y z { | } ~  ! � � + � � � � � � � � � � � � � � �CS  @0����Signal Generator  @
Sig Gen Max Delta @
Scope EStop Offset(dB)  @
Sig Gen Freq Change 9 �       EMC_UI PID Cluster.ctl @P    ScopePID  @
SigGen Min(Vrms)  @
SigGen Max(Vrms)  ,@ OscilloscopeSpectrum Analyzer Input  @0����Spectrum Analyzer Name  @0����SA Cable  @
Scope Target Offset(dB) @
Nominal Dwell(s)  @
Read Delay(s) \ �       )EMC_CS101 Test Ripple Graph Unit Enum.ctl *@ dBuVVrmsRipple Voltage Units  @
Initial Scope/SigGen Gain @2����CS101Cal File(Rel)   @
SigGen Target Offset(Vrms)  @0����Test Level Limit  @
	Line Freq @P  � � � �Test  @0����
CategoryNm  @P   �Cal K �       EMC_CS101 Test Type Enum.ctl %@ CalibrationTest 	Test Type w ��)O�    EMC_CS101 Test Prefs Cluster.ctl M@P    � Y  > � � � � W L 	 R � � � � � � � F � � � \ & � � � `CS101 @0����Name  @@ ���� 1[Bw]  @P  r �Test Params $@@ ���� �TestParam (Freq, Test)  @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 � � 
 � � � � � � �SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  � � � � � �Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  � � � � �	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  � � �Amp @FreqSettling Time(ms) � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) ; �       EMC_UI PID Cluster.ctl @P    
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  � � � � � � � � � � � � � �Seek  @2����Seek File (Rel) @
SigGen Offset @P  � �Test  @0����PV File/Level (dBm) @
PV Tol (dB) @P  � �Cal @
App Pwr EStop Offset (dB) @0����Cable @P  � �SigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  � � � h � � �Timing  @P  � �ArbGen  @
Fwd Pwr EStop Offset (dB) @@ ���� [Probe Ranges]  @@ ���� [Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  � � � �Network Analyzer  @@ ���� Probe VISA[]  a ��)O�   EMC_MT Prefs Cluster.ctl ?@P    	 � � � � � B F L � � � � � t � � � � � � � �MT  @@ ���� 1[Bandwidths]  i �� ��   EMC_ML Probe Location Enum.ctl A@ 
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR	Probe Pos @@ ���� �[Probe Pos] @Probe Settling Time(mS) @0����Receive Gain  @P  � 
 � * � � � �SA  @0����Fwd Pwr Gain  @0����Rev Pwr Gain  @
App Pwr EStop @
Fwd Pwr EStop @
PV Tol  T �       #EMC_ML Controlled Variable Enum.ctl (@ 	App Power	Fwd Power	PV Source @0����SP File or Level  @0����SP File or Level(NA)   @P 	 � � � � � � � � �PwrMtr  @
StepBack  @

Max Offset  @0����
Cable Gain  @

StartLevel  @P  � � � SigGen  8 ��6��   EMC_PID Cluster.ctl @P    PID Cluster Z �       EMC_ML Test Type Enum.ctl 8@ Spectrum AnalyzerNetwork Analyzer 	Test Type M �       "EMC_M Network Analyzer Cluster.ctl !@P  � � � �NetworkAnalyzer @0����AntPos  @0����
Tx Antenna  @P  � �Amp W ��)O�   EMC_ML Prefs Cluster.ctl 5@P    	 L   � � � � � � � � �ML  @0����Antenna @0����Gain  $@@ �������� Stimulus[freq][col] @!Ant with Cal  K ��)3�   $AntennaList_Antenna Info Cluster.ctl @P 
 , -Antenna @@ ����Antennas  @0����RE Limit File i �       EMC_RE Polarization Enum.ctl C@ 
HorizontalVerticalHorizontal and VerticalPolarization  @
Ref Level above limit @!Auto Start/Stop @0����Antenna Y Graph Units @
Cal Offset(dB)  V �       EMC_RE Test Type Enum.ctl 4@ Emission TestEmission Calibration Type  @

SigGen Min  @

SigGen Max  @!	SA PreAmp Y ��)O�   EMC_RE Prefs Cluster.ctl 7@P     	   
   8 4RE  @T 
Time Stamp  @@ ����Timestamp[] @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P Pulse Channel F �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h i
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop ; �       EMC_UI PID Cluster.ctl @P    
PID Params  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P    Amp PID @@ ���� [Amp Names]  @@ �������� [Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h i
FM Channel  @@ ���� [Amp Cables]  @
Test Offset(%)  @0����Sensor Correction @@ ���� AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  8 �       EMC_SA BWTable RB.ctl @P  ) *
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P 6789:;<=> s?@ABCDEF � q rGHIJKPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @
SA Sweep Multiplier � ��)O�   EMC_RS Prefs Cluster.ctl _@P )  L  !"#$%& c n B F'() 	*+ � �,-./012 �345LMNOPQ �RRS  @@ ����SRS[]  "@7���� Traditional DAQ Channel @@ ����U
DAQ Chan[]   @p InstrVISA resource name  @@ ����WVISA[]  @p IVI  IVI Logical Name  @@ ����YIVI[] "@7���� DAQmx Physical Channel  @@ ����[DAQmx PhysChan  L �       All Prefs Cluster.ctl .@P  ; a � � �	TVXZ\AllPrefs  ]     :  � @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @0����Spectrum Analyzer @!Show Diagnostics? @0����
Test Title  @
Attenuation @0����String  @@ �������� Scan Details  @!2Pts/BW?  @0����	RF SigGen ~ �       EMC_CE Test Type Enum.ctl \@ Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)	Test Type @
Numeric @@ ���� SigGenFreq[]   @@ ���� SigGenLevlOffset[]  @0����
Scope Name  @
P @
I @
D 3 �       EMC_UI PID Cluster.ctl @P    PID 
@
Tol+  
@
Tol-  @
Max SigGen Chg  @

Max SigGen  @

Resistance  @
	SigGenMin @P        Cal @0����CE Limit File @0����Bandwidth File  @0����Monitor Probe @0����Monitor Cable @
PID Max Freq  @
Start Frequency @
Stop Frequency  @0����
ScopeProbe  @
Max Stop/Start  @
#SA Points  *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ) *BW Table RB @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  , - . / 0BWFreqRange @@ ���� 1BW[]  @
Sweep Time Multiplier T ��I�   /EMC_Spectrum Anaylzer Configuration Cluster.ctl @P  ' ( + 2 3SA Cfg  Y ��)3�   "EMC_CE Test Type Prefs Cluster.ctl -@P         ! " # $ % & 4	TestPrefs @@ ���� 5Prefs[TestType] @0����Amplifier Name  @#Sweeps @
Ref Lvl Offset  @!
SA PreAmp?  K ��)O�   EMC_CE Prefs Cluster.ctl )@P     	 
     6 7 8 9 :CE  @0����Oscilloscope  @0����Arbitrary Generator @
Scope Tol+(dB)  @
Max Gen Delta(Vrms) @

ArbGen Max  @0����Scope Probe @
Nominal Dwell @
Data 2  @@ ���� C[Frequency] @@ ���� C[Time]  7 �       DwellListbox.xctlData.ctl %@P  D EDwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  G H I J $ % KFreq  � �       EMC_AF Test Type Enum.ctl �@  Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms)	Test Type @

ArbGen Min  @
Freq Stepback Y �       EMC_AF ArbGen Cmd Enum.ctl 5@ ArbGen from FileScope SetPoint
SigGen Cmd  @2����ArbGen Result(Rel)  @
Scope Tol-(dB)  @0����ScopeSP File  @0����PlotGen Err @
Cable Len(m)  A �       EMC_Length Unit Enum.ctl @ incmmLength units  @0����	EStop Str @
Scope SP Offset(dB) @0����	Amplifier @0����ScopeFilePrefix @
SigGen Offset(Vrms) @0����SigGen to Amp Cable @
Scope EStop(dB) @
Initial Gain  @
Delay(s)  @0����Transformer q ��)O�   EMC_AF Prefs Cluster.ctl O@P ! < = >  ? @ A B F 	 L M N O P Q R S T U V   W X Y Z [ \ ] ^ _ `AF   @2����Calibration File (rel)  @0����RF Signal Generator @
Dampening Factor  @
Max RF Change � �       EMC_CS Test Type Enum.ctl h@ CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search Type  V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev B �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h iAM Mod  @2����CW File(Rel)  @!Pulse?  @
Pulse Freq(Hz)  @
RF Sig Gen Freq Change  @
Mod SigGen Offset B �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h iFM Mod  @@ ���� 1
Bandwidths  @0����BW File @0����SigGen Cable  @
SigGen Max Offset @0����Cal Injection Probe  @0����Forward Power Analyzer  @0����FP Cable  @
FP SA RefLevel  @
FP SA Atten @
FP Overshoot  @
FP Tol- @
FP Tol+ @
FP EStop Adjust @0����Output Power Analyzer @0����OP Cable  @
OP SA Atten @
OP Limit Adjust J �       EMC_CS OP Units.ctl .@ dBmdBuAAmpsInduced Current Units @OP SA #Points @FP SA #Points @0����Mod Adjust(dB)  s �       *EMC_CS Induced Current Limit Source RB.ctl ?@ Cal Category LevelCal Category LimitOP Limit Source @0����Cat File(For Test)  @
OP Target Tol+  @
OP Target Tol-  @0����Injection Probe @
OP Target EStop Offset  @
OP Ref Level  @0����Cat File(For Cal) @
Cal Category SP Offset  @0����Directional Coupler @
SA Time Multiplier  � ��)O�   EMC_CS Prefs Cluster.ctl w@P 5  b c Y d e f j k l m  n B F 	 L o p q r s t u v w x y z { | } ~  ! � � + � � � � � � � � � � � � � � �CS  @0����Signal Generator  @
Sig Gen Max Delta @
Scope EStop Offset(dB)  @
Sig Gen Freq Change 9 �       EMC_UI PID Cluster.ctl @P    ScopePID  @
SigGen Min(Vrms)  @
SigGen Max(Vrms)  ,@ OscilloscopeSpectrum Analyzer Input  @0����Spectrum Analyzer Name  @0����SA Cable  @
Scope Target Offset(dB) @
Nominal Dwell(s)  @
Read Delay(s) \ �       )EMC_CS101 Test Ripple Graph Unit Enum.ctl *@ dBuVVrmsRipple Voltage Units  @
Initial Scope/SigGen Gain @2����CS101Cal File(Rel)   @
SigGen Target Offset(Vrms)  @0����Test Level Limit  @
	Line Freq @P  � � � �Test  @0����
CategoryNm  @P   �Cal K �       EMC_CS101 Test Type Enum.ctl %@ CalibrationTest 	Test Type w ��)O�    EMC_CS101 Test Prefs Cluster.ctl M@P    � Y  > � � � � W L 	 R � � � � � � � F � � � \ & � � � `CS101 @0����Name  @@ ���� 1[Bw]  @P  r �Test Params $@@ ���� �TestParam (Freq, Test)  @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 � � 
 � � � � � � �SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  � � � � � �Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  � � � � �	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  � � �Amp @FreqSettling Time(ms) � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) ; �       EMC_UI PID Cluster.ctl @P    
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  � � � � � � � � � � � � � �Seek  @2����Seek File (Rel) @
SigGen Offset @P  � �Test  @0����PV File/Level (dBm) @
PV Tol (dB) @P  � �Cal @
App Pwr EStop Offset (dB) @0����Cable @P  � �SigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  � � � h � � �Timing  @P  � �ArbGen  @
Fwd Pwr EStop Offset (dB) @@ ���� [Probe Ranges]  @@ ���� [Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  � � � �Network Analyzer  @@ ���� Probe VISA[]  a ��)O�   EMC_MT Prefs Cluster.ctl ?@P    	 � � � � � B F L � � � � � t � � � � � � � �MT  @@ ���� 1[Bandwidths]  i �� ��   EMC_ML Probe Location Enum.ctl A@ 
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR	Probe Pos @@ ���� �[Probe Pos] @Probe Settling Time(mS) @0����Receive Gain  @P  � 
 � * � � � �SA  @0����Fwd Pwr Gain  @0����Rev Pwr Gain  @
App Pwr EStop @
Fwd Pwr EStop @
PV Tol  T �       #EMC_ML Controlled Variable Enum.ctl (@ 	App Power	Fwd Power	PV Source @0����SP File or Level  @0����SP File or Level(NA)   @P 	 � � � � � � � � �PwrMtr  @
StepBack  @

Max Offset  @0����
Cable Gain  @

StartLevel  @P  � � � SigGen  8 ��6��   EMC_PID Cluster.ctl @P    PID Cluster Z �       EMC_ML Test Type Enum.ctl 8@ Spectrum AnalyzerNetwork Analyzer 	Test Type M �       "EMC_M Network Analyzer Cluster.ctl !@P  � � � �NetworkAnalyzer @0����AntPos  @0����
Tx Antenna  @P  � �Amp W ��)O�   EMC_ML Prefs Cluster.ctl 5@P    	 L   � � � � � � � � �ML  @0����Antenna @0����Gain  $@@ �������� Stimulus[freq][col] @!Ant with Cal  K ��)3�   $AntennaList_Antenna Info Cluster.ctl @P 
 , -Antenna @@ ����Antennas  @0����RE Limit File i �       EMC_RE Polarization Enum.ctl C@ 
HorizontalVerticalHorizontal and VerticalPolarization  @
Ref Level above limit @!Auto Start/Stop @0����Antenna Y Graph Units @
Cal Offset(dB)  V �       EMC_RE Test Type Enum.ctl 4@ Emission TestEmission Calibration Type  @

SigGen Min  @

SigGen Max  @!	SA PreAmp Y ��)O�   EMC_RE Prefs Cluster.ctl 7@P     	   
   8 4RE  @T 
Time Stamp  @@ ����Timestamp[] @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P Pulse Channel F �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h i
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop ; �       EMC_UI PID Cluster.ctl @P    
PID Params  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P    Amp PID @@ ���� [Amp Names]  @@ �������� [Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P  g h i
FM Channel  @@ ���� [Amp Cables]  @
Test Offset(%)  @0����Sensor Correction @@ ���� AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  8 �       EMC_SA BWTable RB.ctl @P  ) *
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P 6789:;<=> s?@ABCDEF � q rGHIJKPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @
SA Sweep Multiplier � ��)O�   EMC_RS Prefs Cluster.ctl _@P )  L  !"#$%& c n B F'() 	*+ � �,-./012 �345LMNOPQ �RRS  @@ ����SRS[]  "@7���� Traditional DAQ Channel @@ ����U
DAQ Chan[]   @p InstrVISA resource name  @@ ����WVISA[]  @p IVI  IVI Logical Name  @@ ����YIVI[] "@7���� DAQmx Physical Channel  @@ ����[DAQmx PhysChan  *@P  ; a � � �	TVXZ\AllPrefs    
@!Watts @!dBm  ! @!	SweepTime . P    	 L   � � � � � � � � �  P  � � � �   
Rx AntennaRF Probe Ave  P  � � � h � � � D P    � Y  > � � � � W L 	 R � � � � � � � F � � � \ & � � � ` @!Cal Category Level  @!Cal Category Limit  ,  Cal Category LevelCal Category Limit H P ! < = >  ? @ A B F 	 L M N O P Q R S T U V   W X Y Z [ \ ] ^ _ `  P  , - . / 0 
@!Auto  @!BWTable 
 P  ) *  P  ' ( + 2 3  P     *@P         ! " # $ % & 4	TestPrefs N  Emissions TestCalibration(CE101)Calibration(CE102)Calibration(CE106)  P         " P     	 
     6 7 8 9 : B@P 6789:;<=> s?@ABCDEF � q rGHIJKPulse "  <None>SineTriangleSquare  P  g h i �  		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File  *  LogLinearFileSusceptibility Spec  P  G H I J $ % K    MaxMinMeanFirst Probe  
 P  D E  P  X P )  L  !"#$%& c n B F'() 	*+ � �,-./012 �345LMNOPQ �R *  Emission TestEmission Calibration  2  
HorizontalVerticalHorizontal and Vertical @P 
 , -Antenna 0 P     	   
   8 4 @!Incremental @!
Continuous    Incremental
Continuous   P  � � � � � � *  Spectrum AnalyzerNetwork Analyzer    	App Power	Fwd Power 4  
	<Unknown>CTRBULBURFULFURBDLBDRFDLFDR X  Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File) 8 P    	 � � � � � B F L � � � � � t � � � � � � � �   CalibrationTest  
@!dBuV  
@!Vrms    dBuVVrms @!Oscilloscope  @!Spectrum Analyzer 
@!Amps  
@!dBuA    dBmdBuAAmps @P  ) *BW UI Control @P  g h iMod Channel ^  CS Calibration	CW Search	AM SearchCW FileAM File
Pulse FileFM FileZero Span Search  p P 5  b c Y d e f j k l m  n B F 	 L o p q r s t u v w x y z { | } ~  ! � � + � � � � � � � � � � � � � � �   incmm &  ArbGen from FileScope SetPoint �   Magnetic Field Equipment[A](rms)Magnetic Field Cables[A-m](rms)Electric Field Cables[V-m](rms) Electric Field Capacitor[V](rms) 
 7����  
 7���� N]]R ^ �QPON_`MKJaIH *bc ^ � � � � �d �e � ^ � ^ � � � � � h � � �f �g � � � � � � � ~  � � � � � d ea b o u � �hij � v w x y z { | } �k : 9 8 7 3 0 / . - ,l 1^ 2 *bmn )o + ( 'p 4 & % $ # " !            q    ^  ^ r 5^ 6s    ^^  
   a a   t  	  uSmn )oG r 0 / . - ,l 1^ q? � � � h � � �f �:FEDCBA@ s67>89=<;vL ^543 � ^2 i hw gx10/y. ^^- ^, ^ � ^ �   q+* 	   a a   t    q)a K % $ Ia Jz G H{ L i hw gx!"%&|'($ C^ E C^ D} Fa#~  B n  c 3 0 / . - ,l 1^ 2 *bmn )o + ( 'p 4 8�	  ^^  
     a a   t � ^^
 , -�^ 	  � ^ � � ���� � � � � � �� � � � � �d�   q  � � � � �� � � � � � � � � � � � � � * � 
 � � ^ � ^ �� �^ �a K % $ Ia Jz G H{ L 0 / . - ,l 1^ �     a a   t  	  � � � � � � t � � � � �a � � � � � � � �� � � � �a � �   q � � �� �a K % $ Ia Jz G H{ L C^ E C^ D} F B � � � � � � � � � � ���� � � � � � � �� � � � � � � � � r 0 / . - ,l 1^ � �^ � � 
 � 	   a a   t  � `� � �  � � � �a � � & \ ���� � � C^ E C^ D} F � � � ��� � � � R 	   a a   t a K % $ Ia Jz G H{ L W � � �   q � >  Y �  ��`�� � *bmn )� + ! c n s t 0 / . - ,l 1^ q r i hw g� p i hw g� ja k m l   a a   t a K % $ Ia Jz G H{ L 	� f B a C^ E C^ D} F  Y� ` _ ^ ] \ [ Z Y X W    a a   t � V U T S Ra Q� P O N� Ma K % $ Ia Jz G H{ L 	 ?   q  C^ E C^ D} F B A @ > = < ;^^aT^V^UX^\[Z^Y�W�   � FPHPAll Prefs Cluster.ctl     - TDCC   EMC_Test Header Cluster.ctlPTH0   '   TestsEMC_Test Header Cluster.ctl                                 .r  1�  %�  &  �  B  *G  <�PTH0         TDCC    EMC_CE Test Type Enum.ctlPTH0   9   TestsConducted EmissionsEMC_CE Test Type Enum.ctl                                 /PTH0         TDCC    EMC_UI PID Cluster.ctl PTH0   #   SharedEMC_UI PID Cluster.ctl                                 /�  $  �  8;  <y  =%PTH0         TDCC    EMC_SA BWTable RB.ctlPTH0   !   TestsEMC_SA BWTable RB.ctl                                 0�  3�  8�  @=PTH0         TDCC    BWFreqRange Cluster.ctlPTH0   .   Shared	BandwidthBWFreqRange Cluster.ctl                                 1
  3R  'a  "�  ;�  ?�PTH0         TDCC     /EMC_Spectrum Anaylzer Configuration Cluster.ctlPTH0   <   Shared/EMC_Spectrum Anaylzer Configuration Cluster.ctl                                 0q  +DPTH0         TDCC   "EMC_CE Test Type Prefs Cluster.ctl PTH0   B   TestsConducted Emissions"EMC_CE Test Type Prefs Cluster.ctl                                 /3PTH0         TDCC     EMC_CE Prefs Cluster.ctl PTH0   8   TestsConducted EmissionsEMC_CE Prefs Cluster.ctl                                  =PTH0         TDCC     DwellListbox.xctlData.ctl PTH0   "   SharedDwellListboxData.ctl                                 �     &�  7A  +�PTH0   +   SharedDwellListboxDwellListbox.xctl TDCC      Freq_Spec Method Enum.ctlPTH0   :   SharedFrequency SpecifierFreq_Spec Method Enum.ctl                                 $w  %?  %�  7�  (3  <PTH0         TDCC   Frequency Specifier.xctlData.ctlPTH0   )   SharedFrequency SpecifierData.ctl                                 $S  %  !�  7�  (  ,dPTH0   9   SharedFrequency SpecifierFrequency Specifier.xctl TDCC   EMC_AF Test Type Enum.ctlPTH0   5   TestsAudio FrequencyEMC_AF Test Type Enum.ctl                                 $�PTH0         TDCC    EMC_AF ArbGen Cmd Enum.ctl PTH0   6   TestsAudio FrequencyEMC_AF ArbGen Cmd Enum.ctl                                 0~PTH0         TDCC     EMC_Length Unit Enum.ctl PTH0   %   SharedEMC_Length Unit Enum.ctl                                 1�PTH0         TDCC    EMC_AF Prefs Cluster.ctl PTH0   4   TestsAudio FrequencyEMC_AF Prefs Cluster.ctl                                 �PTH0         TDCC     EMC_CS Test Type Enum.ctlPTH0   >   TestsConducted SusceptibilityEMC_CS Test Type Enum.ctl                                 %PTH0         TDCC   !Instrument_Waveform Type Enum.ctlPTH0   >   Instruments
Instrument!Instrument_Waveform Type Enum.ctl                                 2�  3  ,<  >PTH0         TDCC   !Instrument_Modulation Cluster.ctlPTH0   >   Instruments
Instrument!Instrument_Modulation Cluster.ctl                                 2�  2�  ,#  >PTH0         TDCC   EMC_CS OP Units.ctlPTH0   8   TestsConducted SusceptibilityEMC_CS OP Units.ctl                                 4'PTH0         TDCC   *EMC_CS Induced Current Limit Source RB.ctl PTH0   O   TestsConducted Susceptibility*EMC_CS Induced Current Limit Source RB.ctl                                 4�PTH0         TDCC    EMC_CS Prefs Cluster.ctl PTH0   =   TestsConducted SusceptibilityEMC_CS Prefs Cluster.ctl                                 pPTH0         TDCC    )EMC_CS101 Test Ripple Graph Unit Enum.ctlPTH0   @   Tests
CS101 Test)EMC_CS101 Test Ripple Graph Unit Enum.ctl                                 5�PTH0         TDCC     EMC_CS101 Test Type Enum.ctl PTH0   3   Tests
CS101 TestEMC_CS101 Test Type Enum.ctl                                 6SPTH0         TDCC       EMC_CS101 Test Prefs Cluster.ctl PTH0   7   Tests
CS101 Test EMC_CS101 Test Prefs Cluster.ctl                                 6PTH0         TDCC      EMC_ML Motor Radio Button.ctlPTH0   ?   Tests
Mode Tuned
ML privateEMC_ML Motor Radio Button.ctl                                 6�  ;/PTH0         TDCC      EMC_M Tuner Motor Cluster.ctlPTH0   <   Tests
Mode TunedprivateEMC_M Tuner Motor Cluster.ctl                                 5�  :�PTH0         TDCC     EMC_MT Test Type Enum.ctlPTH0   ;   Tests
Mode Tuned
MT privateEMC_MT Test Type Enum.ctl                                 8PTH0         TDCC      #EMC_ML Controlled Variable Enum.ctlPTH0   E   Tests
Mode Tuned
ML private#EMC_ML Controlled Variable Enum.ctl                                 8�  )�PTH0         TDCC      EMC_Timing Generator Params.ctlPTH0   +   TestsEMC_Timing Generator Params.ctl                                 9�  ?\PTH0         TDCC    EMC_MT Calc Type Enum.ctlPTH0   ;   Tests
Mode Tuned
MT privateEMC_MT Calc Type Enum.ctl                                 :fPTH0         TDCC      "EMC_M Network Analyzer Cluster.ctl PTH0   A   Tests
Mode Tunedprivate"EMC_M Network Analyzer Cluster.ctl                                 :w  :PTH0         TDCC      EMC_MT Prefs Cluster.ctl PTH0   :   Tests
Mode Tuned
MT privateEMC_MT Prefs Cluster.ctl                                 mPTH0         TDCC   EMC_ML Probe Location Enum.ctl PTH0   @   Tests
Mode Tuned
ML privateEMC_ML Probe Location Enum.ctl                                 (�PTH0         TDCC   EMC_PID Cluster.ctlPTH0       SharedEMC_PID Cluster.ctl                                 *PTH0         TDCC   EMC_ML Test Type Enum.ctlPTH0   ;   Tests
Mode Tuned
ML privateEMC_ML Test Type Enum.ctl                                 9�PTH0         TDCC      EMC_ML Prefs Cluster.ctl PTH0   :   Tests
Mode Tuned
ML privateEMC_ML Prefs Cluster.ctl                                 �PTH0         TDCC   $AntennaList_Antenna Info Cluster.ctl PTH0   P   TestsRadiated EmissionsAntenna List$AntennaList_Antenna Info Cluster.ctl                                 �PTH0         TDCC     EMC_RE Polarization Enum.ctl PTH0   ;   TestsRadiated EmissionsEMC_RE Polarization Enum.ctl                                 *8PTH0         TDCC      EMC_RE Test Type Enum.ctlPTH0   8   TestsRadiated EmissionsEMC_RE Test Type Enum.ctl                                 +	PTH0         TDCC     EMC_RE Prefs Cluster.ctl PTH0   7   TestsRadiated EmissionsEMC_RE Prefs Cluster.ctl                                 PTH0         TDCC      Instrument_Pulse Cluster.ctl PTH0   9   Instruments
InstrumentInstrument_Pulse Cluster.ctl                                 #;PTH0         TDCC    !EMC_RS Probe Read Method Enum.ctlPTH0   E   TestsRadiated Susceptibility!EMC_RS Probe Read Method Enum.ctl                                 +�PTH0         TDCC    EMC_RS Test Type Enum.ctlPTH0   =   TestsRadiated SusceptibilityEMC_RS Test Type Enum.ctl                                 =�PTH0         TDCC    #EMC_RS Pulse Type Prefs Cluster.ctlPTH0   G   TestsRadiated Susceptibility#EMC_RS Pulse Type Prefs Cluster.ctl                                 >�PTH0         TDCC    EMC_RS Prefs Cluster.ctl PTH0   <   TestsRadiated SusceptibilityEMC_RS Prefs Cluster.ctl                                 �PTH0              b     D    �� �� �����   ����   �                                     DAQmx Physical Channel     H     �   �� +�� n����   -����   m     ���                             Z          �� ��� �����   �����   �          
   
                       DAQmx PhysChan     H     �    �� �� ����   ����        ���                               \      D    �� �� n����   ����   n                                     IVI Logical Name   H     �   �� +�� n����   -����   m     ���                               Q         �� ��� �����   �����   �                                    IVI[]      H     �    �� �� ����   ����        ���                               ^      D    �f �s ����f   ���s   �                                     VISA resource name     H     �   �z +�� n���z   -����   m     ���                               R         �y ��� ����y   �����   �                                    VISA[]     H     �    �u �� ���u   ���u        ���                               c          �@ �M ����@   ���M   �                                      Traditional DAQ Channel    H     �   �V +�d n���V   -���c   m     ���                             V         �U ��b ����U   ����b   �          
   
                       
DAQ Chan[]     H     �    �Q �^ ���Q   ���Q        ���                               N     D    ����������  �����  �                                      RS     _     D    4Ap  4    A  p                                     SA Sweep Multiplier    H     �    5�B  5  �  5       ���                               R          ��  �  �                                         String     H     �   �;    �    :     ���                               X         F�    G    �                                     Probe VISA[]   H     �    
��  
  �  
  �     ���                               Z     D    �l  �      l                                     Overshoot(V/m)     H     �    ��   �  �  �       ���                               U     D    �"�O  �  #  �  O              	                       	Tol-(V/m)      H     �    ���  �  �  �       ���                               U     D    �"�S  �  #  �  S              	                       	Tol+(V/m)      H     �    ���  �  �  �       ���                               \     D    �!�s  �  "  �  s                                     Test Offset(V/m)   H     �    ���  �  �  �       ���                               Q     D    ��   #����         #                                  Watts      Q     D        2            2                               <   Watts      O     D    �� 4  I����   5      I                                  dBm    O     D      C  X      D      X                               <   dBm    P          ����  �  �  �  �                                     Pane   ^     D    o�|&  o  �  |  &                                  Output Graph Units     Q      D     /� <�   /  �   <  �                                      Pulse      d     D    {"    |  "                                        SpecAnalyzer(V/m)^2/W Eq   H     �   )�8�  )  �  )  �     ���                               `     D    ���  �  �  �                                        PwrMeter(V/m)^2/W Eq   H     �D   ��    �    �     ���                               U     D    ����  �  �  �  �                                      	File(Rel)      H     �   ���  �  �  �       ���                               \     D    F�SP  F  �  S  P                                      SA #Pts(-1:Auto)   H     �    K�X�  K  �  K  �     ���                               c     D     �� �6   �  �   �  6                                      Bandwidths & Sweep Time    V     D     �� ��   �  �   �  �                                      
BW Control     L     D      � & �      �   &   �                                       V     D      � " �      �   "   �              
                    
Sweep Time     H    �D      k # �      m      �                                   U     D      ]  �      ^      �              	                    	SweepTime      _     D      l  �      m      �                               <   Auto BW, User Sweep    P     D     ��     ����                                        Auto   P     D       !          !                                  <   Auto   S     D     ��      ����                                         BWTable    X     D     #  0 G   #      0   G                               <   Use BW Table   P                                                                Pane   e     D    ������ �������������   �                                  Bandwidths and Sweep Time      W     D    ������ <������������   <                                  BW Table RB    P           �� ��   �  �   �  �                                      Pane   L     D     �� ��   �  �   �  �                                          L     D     �� ��   �  �   �  �                                          S     D     � �7   �     �  7                                      BW File    H     �    ��   �  �   �       ���                               W     D    �    �                                        BWFreqRange    ]     D     e _ r �   e   `   r   �                                    Sweep/Hz or Sweep      H     �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                    Video BW   H     �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                    Res BW     H     �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �              	                      	Stop Freq      H     �     "  / X   "      "   W     ���                               V      D      _  �      `      �              
                      
Start Freq     H     �     
   X   
      
   W     ���                               P          �+�    �  +  �                                    Pane   L     D    ��    �    �                                         V          �	    �    	              
                      
Bandwidths     H     �    �'�    �    �     ���                               T     D    �k��  �  l  �  �                                      Rx Cable   H     �   �
�d  �    �  c     ���                               R     D    �	�(  �  
  �  (                                      Timing     U     D     � S � �   �   T   �   �              	                    	Amplitude      Y     D     }  � W   }      �   W                                  Amplitude Vpp      H    �D     �  � ?   �      �   >                                   R     D     y S � s   y   T   �   s                                  Offset     U     D     i  v @   i      v   @              	                    	Offset(V)      H    �D     {  � ?   {      {   >                                   [     D     
   `   
         `                                  Gating Enabled?    Y           ! ' d      "   '   d      
                        =   Enable Gating      P     D     Q c ^ z   Q   d   ^   z                                  Freq   P     D     A  N )   A      N   )                                  Freq   H    �D     S  ` O   S      S   N                                   W     D     U  b K   U      b   K                                  Pulse Width    W     D     e c r �   e   d   r   �                                  Pulse Width    H    �D     g  t O   g      g   N                                   R     D     * c 7 �   *   d   7   �                                  Gating     W     D     - c : �   -   d   :   �                                  Gating Freq    H    �D     +  8 O   +      +   N                                   V     D     > c K �   >   d   K   �              
                    
Duty Cycle     `     D     > � K   >   �   K                                    Gating Duty Cycle(%)   H    �D     ?  L O   ?      ?   N                                   P          ��&  �    �  &                                      Pane   L          �	�  �  
  �                                            V     D    k�    l    �                                      
Probe Name     H     �   
d        c     ���                               \     D    PK]�  P  L  ]  �                                      SafetyMargin(dB)   H     �    UbD  U    U  C     ���                               Z     D    8KE�  8  L  E  �                                      PM Trig Offset     H     �    =JD  =    =  C     ���                               [     D     K-�     L  -  �                                      PM Capture Time    H     �    %2D  %    %  C     ���                               Z     D    K�    L    �                                      SA Trig Offset     H     �    D        C     ���                               Y     D    �J��  �  K  �  �                                      SA Trig Delay      H     �    �C  �    �  B     ���                               ]     D    �J��  �  K  �  �                                      SA Atten(Auto:-1)      H     �    ��C  �    �  B     ���                               X     D    �J��  �  K  �  �                                      SA Ref Level   H     �    ��C  �    �  B     ���                               X     D    �k��  �  l  �  �                                      SigGen Cable   H     �   �
�d  �    �  c     ���                               W     D     �k ��   �  l   �  �                                      SigGen Name    H     �    �
 �d   �     �  c     ���                               T     D     �k ��   �  l   �  �                                      Amp Name   H     �    �
 �d   �     �  c     ���                               [     D    �k��  �  l  �  �                                      PM Fwd Pwr Gain    H     �   �
�d  �    �  c     ���                               V     D     �k ��   �  l   �  �                                      
Timing Gen     H     �    �
 �d   �     �  c     ���                               W     D     �k�   �  l    �                                      Power Meter    H     �    �
d   �     �  c     ���                               T     D    lky�  l  l  y  �                                      Tx Cable   H     �   l
{d  l    l  c     ���                               V     D    Qk^�  Q  l  ^  �                                      
Tx Antenna     H     �   U
dd  U    U  c     ���                               V     D    :kG�  :  l  G  �                                      
Rx Antenna     H     �   >
Md  >    >  c     ���                               S     D    #k0�  #  l  0  �                                      SA Name    H     �   '
6d  '    '  c     ���                               P           D� Q�   D  �   Q  �                                      Pane   Q      D     /� <�   /  �   <  �                                     Pulse      R           {� �   {  �   �                                       String     H     �    �� �3   �  �   �  2     ���                               X          �> ��   �  ?   �  �                                     AmpAntenna[]   H     �     �� ��   �  �   �  �     ���                               ]     D    R�_�  R  �  _  �                                     Sensor Correction      H     �   V$e~  V  &  V  }     ���                               Z     D    avn�  a  w  n  �                                     Test Offset(%)     H     �    _/lo  _  1  _  n     ���                               _     D    6$C�  6  %  C  �                                  Max RF Level Change    H     �D    H"U|  H  $  H  {                                   R          <�I  <  �  I                                       String     H     �   Q�`3  Q  �  Q  2     ���                               X         P>]  P  ?  ]                                       [Amp Cables]   H     �    M�Z�  M  �  M  �     ���                               V     D     � �:   �     �  :              
                       
FM Channel     U     D     � d � �   �   e   �   �              	                       	Depth/Dev      H     �     � 2 � ^   �   4   �   ]     ���                               P     D     � d � {   �   e   �   {                                     Freq   H     �     ~ 2 � ^   ~   4   ~   ]     ���                               P     D     _ � l �   _   �   l   �                                     Type   L           _ � l �   _   �   l   �                                         H     �     d � q �   d   �   d   �     ���                               H    �    d 2 s |   d   4   q   {     ���                               P           �A �Z   �  B   �  Z                                     Pane   L     D     � �   �     �                                            S     D     1 >;   1     >  ;                                     Tol-(%)    H     �     /� <   /  �   /       ���                               [      D    ��    �    �                                     Test Limit File    H    �   $~    &    }     ���                               P          ��    �    �                                     Type   L     D    ��    �    �                                       H     �    ��    �    �      
                            H    �   "�    $    �      
                            S      D    �    �                                         Numeric    H     �    �'&    �    %     ���                               Z         1y    2    y                                     [Amp Settings]     H     �    1�>�  1  �  1  �     ���                               H     �    �"�    �    �     ���                               R           �� �   �  �   �                                       String     H     �    ��3   �  �   �  2     ���                               W          �>   �  ?                                         [Amp Names]    H     �     �� �   �  �   �  �     ���                               R           �� �   �  �   �                                       String     H     �    �� �3   �  �   �  2     ���                               Y          �> ��   �  ?   �  �                                     [Probe Names]      H     �     �� ��   �  �   �  �     ���                               S      D     �� �   �  �   �                                       Numeric    H     �     �� �&   �  �   �  %     ���                               Z          �1 �}   �  2   �  }                                     [Probe Ranges]     H     �     �� ��   �  �   �  �     ���                               S     D    ;�H�  ;  �  H  �                                     Amp PID    M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P          :$G=  :  %  G  =                                     Pane   L     D    ;�H�  ;  �  H  �                                          b     D    �b��  �  c  �  �                                     Delay after output(ms)     H     �    �/�[  �  1  �  Z     ���                               V     D    ����  �  �  �  �                                     
Test Title     H     �   �$�~  �  &  �  }     ���                               R     D     (� 5   (  �   5                                       Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �    P�� _ !   P����   ]         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �    	��  !   	����            ���                               P           , 98   ,      9  8                                     Pane   L     D     (� 5�   (  �   5  �                                         V     D    r X  r  !    X              
                       
PID Params     M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P          �$�=  �  %  �  =                                     Pane   L     D    r '  r  !    '                                          P     D    #�0�  #  �  0  �                                     Freq   U     D     � s � �   �   t   �   �                                     	Spec Path      H     �    �  � l   �      �   k     ���                               Z     D     | v � �   |   w   �   �                                  Stop Frequency     H    �D     z  � a   z   	   z   `                                   [     D     e v r �   e   w   r   �                                  Start Frequency    H    �D     f  s a   f   	   f   `                                   V     D     : F G y   :   G   G   y              
                       
Delta Freq     H     �     8  E @   8      8   ?     ���                               U     D     N � [ �   N   �   [   �              	                       	Freq Path      H     �    O  ^ c   O      \   b     ���                               ^     D      v  �      w      �                                     Freq Select Method     L     D      v  }      w      }                                         H     �      |  �      ~      �      
                               H    �       e      	      d      
                               Y     D     " F / �   "   G   /   �                                     Points/Decade      H     �        - @              ?     ���                               P          '�4   '  �  4                                        Pane   L          #�0�  #  �  0  �                                         V     D     {  �Z   {  !   �  Z              
                       
AM Channel     U     D     � d � �   �   e   �   �              	                       	Depth/Dev      H     �     � 2 � ^   �   4   �   ]     ���                               P     D     � d � {   �   e   �   {                                     Freq   H     �     ~ 2 � ^   ~   4   ~   ]     ���                               P     D     _ � l �   _   �   l   �                                     Type   L           _ � l �   _   �   l   �                                         H     �     d � q �   d   �   d   �     ���                               H    �    d 2 s |   d   4   q   {     ���                               P           �$ �=   �  %   �  =                                     Pane   L     D     {  �'   {  !   �  '                                          Y     D     �� ��   �  �   �  �                                  Mod SG Adjust      H     �D     �" �|   �  $   �  {                                   Y     D    �a��  �  b  �  �                                     Gate Freq(Hz)      H     �    �/�[  �  1  �  Z     ���                               Q     D    �?�\  �  @  �  \                                     Gate?      [      D     H Ui   H     U  i                                     ProbeReadMethod    L      D     H U   H     U                                           H     �     N [   N     N        
                               H    �D    K� Z�   K  �   K  �      
                               [     D     e rg   e     r  g                                     Overshoot EStop    H     �     c� p   c  �   c       ���                               S     D    %v2�  %  w  2  �                                     Tol+(%)    H     �    #/0o  #  1  #  n     ���                               Q     D    ���  �  �  �                                       Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          ��$  �    �  $                                     Pane   L          ����  �  �  �  �                                         _     D     �� �    �  �   �                                        Mod File (Rel Path)    H     �    �-~   �  /    }     ���                               Y     D     $  1d   $  !   1  d                                     Pulse Channel      T     D     3 G @ r   3   H   @   r                                     Width(S)   H     �     1  > @   1      1   ?     ���                               _     D      G ! �      H   !   �                                     Pulse Frequency(Hz)    H     �       & @            ?     ���                               S     D        ;            ;                                  Enabled    R      D        <            <                                  OFF/ON     P           8$ E=   8  %   E  =                                     Pane   L           $  1'   $  !   1  '                                         Y     D    maz�  m  b  z  �                                     Nominal Dwell      H     �    n/{[  n  1  n  Z     ���                               b     D    &�3  &  �  3                                    RF Sig Gen Freq Change     H     �D    $"1|  $  $  $  {                                   ]      D     v� �5   v  �   �  5                                     Show Diagnostics?      _     D    ���  �  �    �                                     RF Signal Generator    H     �   �$~  �  &    }     ���                               P          ����������  �����  �                                     Pane   L     D    ����������  �����  �                                          P         ����������  �����  �                                    RS[]   H     �    ��r�������  t����  �     ���                               V          � '�" _���   (���"   _                                      
Time Stamp     H     �   �( +�B ����(   -���B        ���                             W         � � T���   ���   T                                    Timestamp[]    H     �    �$  �1 ���$   ���$        ���                               N     D    ��8��F����  9����  F                                      RE     R     D     R2 _T   R  3   _  T                                     SA Cfg     a     D     �  � t   �      �   t                                     Sweep Time Multiplier      H    �     ��� �    �����   �        ���                               W     D     ?�� L >   ?����   L   >                                     BWFreqRange    ]     D     e _ r �   e   `   r   �                                     Sweep/Hz or Sweep      H    �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                     Video BW   H    �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                     Res BW     H    �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �                                     	Stop Freq      H    �     "  / X   "      "   W     ���                               V      D      _  �      `      �                                     
Start Freq     H    �     
   X   
      
   W     ���                               P           R�� _    R����   _                                        Pane   L     D     ?�� L��   ?����   L����                                         P          ;�� H    ;����   H                                        BW[]   H     �     N�� [��   N����   N����     ���                               e     D    ������ T������������   T                                     Bandwidths and Sweep Time      W     D    ������ 	������������   	                                     BW Table RB    L     D      � % �      �   %   �                                       V     D      � ! �      �   !   �              
                    
Sweep Time     H    �D      k " �      m      �                                   U     D      ]  �      ^      �              	                    	SweepTime      _     D      l  �      m      �                               <   Auto BW, User Sweep    P     D     ��     ����                                        Auto   P     D                                                     <   Auto   S     D     ��      ����                                         BWTable    X     D     "  / G   "      /   G                               <   Use BW Table   P                                                               Pane   e     D    ������ �������������   �                                  Bandwidths and Sweep Time      W     D    ������ <������������   <                                  BW Table RB    P          ���� ����������   ����                                     Pane   L     D    ������������������������                                          L     D    ������������������������                                          V     D    �� �� C����   ����   C                                     
#SA Points     H    �    ������ ������������        ���                               Z     D    �� �� W����   ����   W                                     Max Stop/Start     H    �    ������ ������������        ���                               P           e6 rO   e  7   r  O                                     Pane   L           R2 _9   R  3   _  9                                         U     D    �Q��  �  R  �  �              	                    	SA PreAmp      W     D    �R��  �  S  �  �                               =   Pre Amp On?    L     D    ~���  ~  �  �  �                                       S     D    ~���  ~  �  �  �                                  #Sweeps    H    �D    E�s    G    r                                   p     D    '4N�  '  5  N  �              $                    $-1: Auto
0: Use BW Table
>0: seconds   V     D    Q�^�  Q  �  ^  �              
                    
SigGen Max     H     �D    SE`u  S  G  S  t                                   p     D    48�    5  8  �              $                    $-1: Auto
0: Use BW Table
>0: seconds   V     D    ;�H�  ;  �  H  �              
                    
SigGen Min     H     �D    =EJu  =  G  =  t                                   P     D    &�3�  &  �  3  �                                  Type   L          &�3�  &  �  3  �                                         H     �    +�8�  +  �  +  �      
                            H    �   (E7�  (  G  (  �      
                            Z     D    �"�    �  "  �                                     Cal Offset(dB)     H     �    R ~    T    }     ���                               a     D    ��  �  �                                         Antenna Y Graph Units      H     �   �G�  �  I  �  �     ���                               U     D    ����  �  �  �  �              	                       	RF SigGen      H     �   �G��  �  I  �  �     ���                               [     D    �b��  �  c  �  �                                     Auto Start/Stop    a     D    ���  �  �  �                                    Ref Level above limit      H    �D    �E��  �  G  �  �                                   R          r { �  r   |     �                                     String     H     �   � � �  �   �  �   �     ���                               X         | ��  |   �  �                                       Scan Details   H     �    � _� q  �   a  �   p     ���                               H     �    � _� q  �   a  �   p     ���                               W     D    j �w �  j   �  w   �                                  Attenuation    H    �D    h Pu �  h   R  h   �                                   Z     D    Q �^ �  Q   �  ^   �                                     Bandwidth File     H     �   Q R` �  Q   T  Q   �     ���                               R     D     N � [   N   �   [                                       Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �D    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �D    P�� _ !   P����   ]         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �D     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �D     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �D    	��  !   	����            ���                               P           R R _ k   R   S   _   k                                     Pane   L     D     N � [ �   N   �   [   �                                         X     D    < �I2  <   �  I  2                                  Polarization   L          < �I  <   �  I                                           H     �    = �J	  =   �  =        
                            H     �   8 PI �  8   R  8   �      
                            Y     D    ! �. �  !   �  .   �                                     RE Limit File      H     �   ! R0 �  !   T  !   �     ���                               S     D    2 {? �  2   |  ?   �                                     Antenna    X     D     �  � N   �      �   N                                  Ant with Cal   b     D     �  � �   �      �   �                               =   Use Antenna During Cal     S            = � d      >   �   d                                     Numeric    H     �     � A � m   �   C   �   l     ���                               _          v  � u   v      �   u                                     Stimulus[freq][col]    H     �     �  � (   �      �   '     ���                               H     �     �  � (   �      �   '     ���                               P     D     a z n �   a   {   n   �                                     Gain   H     �    a 	 p s   a      a   r     ���                               S     D        /            /                                     Antenna    H     �     	 ' s            r     ���                               V      D     - G : y   -   H   :   y                                     
Start Freq     H     �     2  ? @   2      2   ?     ���                               U      D     E G R w   E   H   R   w                                     	Stop Freq      H     �     J  W @   J      J   ?     ���                               P          E R �  E   �  R   �                                     Pane   S     D    2 {? �  2   |  ?   �                                     Antenna    T         * N7 }  *   O  7   }                                     Antennas   H     �    A _N q  A   a  A   p     ���                               V     D     � �     �     �              
                       
Test Title     H    �D    R" �     T      �     ���                               ]      D      m �      n     �                                     Show Diagnostics?      ]     D     � � �   �   �   �                                       Spectrum Analyzer      H     �D    � R � �   �   T   �   �     ���                               P          ��<��U����  =����  U                                     Pane   L     D    ��8��?����  9����  ?                                          N     D    ��������  	����                                        ML     R           �,     �  ,                                       String     H     �   4 �C8  4   �  4  7     ���                               X         &@      &  @                                     Probe VISA[]   H     �    0 �= �  0   �  0   �     ���                               Y     D     ; j H �   ;   k   H   �                                     Stimulus Gain      H     �    ; 	 J c   ;      ;   b     ���                               V     D     " j / �   "   k   /   �              
                       
Tx Antenna     H     �    " 	 1 c   "      "   b     ���                               P     D     	 j  �   	   k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          A%Z    B  %  Z                                     Pane   O     D    �    �                                         Amp    R     D    ����  �  �  �  �                                     AntPos     H     �   �5��  �  7  �  �     ���                               Q      D    � �� �  �   �  �   �                                     Motor      S     D     ~ Y � }   ~   Z   �   }                                  Control    W     D    ��   ?����         ?                                  Incremental    W     D        N            N                                  Incremental    V     D        <            <              
                    
Continuous     V     D       ! K         !   K              
                    
Continuous     P           � 	 � "   �   
   �   "                                     Pane   L           ~ Y � `   ~   Z   �   `                                          R     D     f G s q   f   H   s   q                                     Speed%     H     �     k  x @   k      k   ?     ���                               P     D     N G [ ^   N   H   [   ^                                     Step   H     �     S  ` @   S      S   ?     ���                               P     D     6 G C ^   6   H   C   ^                                     Stop   H    �     ;  H @   ;      ;   ?     ���                               Q     D      G + `      H   +   `                                     Start      H    �     #  0 @   #      #   ?     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          � S� l  �   T  �   l                                     Pane   L          � �� �  �   �  �   �                                          [     D    �!    �    !                                     NetworkAnalyzer    Y     D     W G d �   W   H   d   �                                     Freq Dwell(s)      H     �     U  b @   U      U   ?     ���                               S     D     ; R H u   ;   S   H   u                                     Cal Set    H     �    ; 	 J K   ;      ;   J     ���                               T     D      j + �      k   +   �                                     Fwd Gain   H     �    " 	 1 c   "      "   b     ���                               P     D     	 R  n   	   S      n                                     Name   H     �    	 	  K   	      	   J     ���                               P          5N    6    N                                     Pane   L          ��    �    �                                          U      D    ����  �  �  �  �              	                    	Test Type      L          ����  �  �  �  �                                          H     �    ����  �  �  �  �      
                            H    �   �3��  �  5  �  �      
                            W     D    � �� �  �   �  �   �                                     PID Cluster    M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P          � S� l  �   T  �   l                                     Pane   L          � �� �  �   �  �   �                                         V     D     n G { y   n   H   {   y                                     
StartLevel     H     �     l  y @   l      l   ?     ���                               V     D     R j _ �   R   k   _   �              
                       
Cable Gain     H     �    R 	 a c   R      R   b     ���                               V     D     = G J ~   =   H   J   ~                                     
Max Offset     H     �     ;  H @   ;      ;   ?     ���                               T     D     % G 2 t   %   H   2   t                                     StepBack   H     �     #  0 @   #      #   ?     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          R S_ l  R   T  _   l                                     Pane   R      D    | ��  |   �  �                                       SigGen     `     D     � j � �   �   k   �   �                                     SP File or Level(NA)   H    �D    � 	 � c   �      �   b     ���                               \     D     � j � �   �   k   �   �                                     SP File or Level   H    �    � 	 � c   �      �   b     ���                               U           � V � �   �   W   �   �                                     	PV Source      L     D     � V � ]   �   W   �   ]                                       H     �     � W � e   �   Y   �   d      
                            H    �    �  � @   �   	   �   ?      
                            R     D     � G � e   �   H   �   e                                     PV Tol     H     �     �  � @   �      �   ?     ���                               Y     D     o G | �   o   H   |   �                                     Fwd Pwr EStop      H     �     m  z @   m      m   ?     ���                               Y     D     W G d �   W   H   d   �                                     App Pwr EStop      H     �     U  b @   U      U   ?     ���                               X     D     ; j H �   ;   k   H   �                                     Rev Pwr Gain   H     �    ; 	 J c   ;      ;   b     ���                               X     D     " j / �   "   k   /   �                                     Fwd Pwr Gain   H     �    " 	 1 c   "      "   b     ���                               P     D     	 j  �   	   k      �                                     Name   H     �D    	 	  c   	         b     ���                               P          5N    6    N                                     Pane   R     D    z �#  z    �  #                                     PwrMtr     X     D     � j � �   �   k   �   �                                     Receive Gain   H     �    � 	 � c   �      �   b     ���                               [     D     � j � �   �   k   �   �                                     Receive Antenna    H     �    � 	 � c   �      �   b     ���                               P     D     	 j  �   	   k      �                                     Name   H     �D    	 	  c   	         b     ���                               W     D     v G � �   v   H   �   �                                     Stop Factor    H     �     {  � @   {      {   ?     ���                               U     D     6 v C �   6   w   C   �              	                    	Ref Level      H    �D     4  A a   4   	   4   `                                   V     D     M G Z �   M   H   Z   �                                     
Sweep Time     H     �     K  X @   K      K   ?     ���                               X     D     ^ G k �   ^   H   k   �                                     Start Factor   H    �     c  p @   c      c   ?     ���                               W     D     " v / �   "   w   /   �                                  Attenuation    H     �D        - a       	       `                                   P           � Q � j   �   R   �   j                                     Pane   N     D    %2  %    2                                       SA     c     D     �s ��   �  t   �  �                                     Probe Settling Time(mS)    H    �D     �@ �l   �  B   �  k     ���                               R          / �< �  /   �  <   �                                     String     H     �   D �S �  D   �  D   �     ���                               Y         C �R  C   �  C                                       [Probe Names]      H     �    @ dM v  @   f  @   u     ���                               S      D     � �     �     �                                     Numeric    H     �    " �/ �  "   �  "   �     ���                               Z          �#     �  #                                       [Probe Ranges]     H     �     `* r     b     q     ���                               U     D    ���  �  �    �                                     	Probe Pos      L     D    ���  �  �    �                                         H     �    ���  �  �  �  �     ���                               H    �   �`
�  �  b  �  �     ���                               W         ��
  �  �    
                                     [Probe Pos]    H     �    �5G  �  7  �  F     ���                               P     D     / <   /     <                                       Freq   U     D     � s � �   �   t   �   �                                     	Spec Path      H     �D    �  � l   �      �   k     ���                               Z     D     | v � �   |   w   �   �                                  Stop Frequency     H    �D     z  � a   z   	   z   `                                   [     D     e v r �   e   w   r   �                                  Start Frequency    H    �D     f  s a   f   	   f   `                                   V     D     : F G y   :   G   G   y              
                       
Delta Freq     H     �D     8  E @   8      8   ?     ���                               U     D     N � [ �   N   �   [   �              	                       	Freq Path      H     �D    O  ^ c   O      \   b     ���                               ^     D      v  �      w      �                                     Freq Select Method     L     D      v  }      w      }                                         H     �      |  �      ~      �      
                               H    �D       e      	      d      
                               Y     D     " F / �   "   G   /   �                                     Points/Decade      H     �D        - @              ?     ���                               P           3, @E   3  -   @  E                                     Pane   L           / <   /     <                                           W     D    `^m�  `  _  m  �                                     BWFreqRange    ]     D     e _ r �   e   `   r   �                                     Sweep/Hz or Sweep      H     �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                     Video BW   H     �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                     Res BW     H     �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �                                     	Stop Freq      H     �     "  / X   "      "   W     ���                               V      D      _  �      `      �                                     
Start Freq     H     �     
   X   
      
   W     ���                               P          sb�{  s  c  �  {                                     Pane   L     D    `^me  `  _  m  e                                         X         X�e%  X  �  e  %                                     [Bandwidths]   H     �    oB|T  o  D  o  S     ���                               Z     D     �� ��   �  �   �  �                                     Bandwidth File     H     �D    �5 ��   �  7   �  �     ���                               R     D     - � :   -   �   :                                       Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �D    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �D     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �D    P�� _ !   P����   ]         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �D     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �D     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �D    	��  !   	����            ���                               P           1 R > k   1   S   >   k                                     Pane   L     D     - � : �   -   �   :   �                                         V     D     � � � �   �   �   �   �              
                       
Test Title     H    �D    � R � �   �   T   �   �     ���                               ]      D     � r � �   �   s   �   �                                     Show Diagnostics?      P          ����%����  ����  %                                     Pane   L     D    ��������  	����                                            N     D    �� ��� �����   �����   �                                      MT     R          (�5  (  �  5                                       String     H     �   =�L2  =  �  =  1     ���                               X         "�/:  "  �  /  :                                     Probe VISA[]   H     �    9�F�  9  �  9  �     ���                               \     D    �`��  �  a  �  �                                     Network Analyzer   Y     D     W G d �   W   H   d   �                                     Freq Dwell(s)      H     �     U  b @   U      U   ?     ���                               S     D     ; R H u   ;   S   H   u                                     Cal Set    H     �    ; 	 J K   ;      ;   J     ���                               T     D      j + �      k   +   �                                     Fwd Gain   H     �    " 	 1 c   "      "   b     ���                               P     D     	 R  n   	   S      n                                     Name   H     �    	 	  K   	      	   J     ���                               P          � �� �  �   �  �   �                                     Pane   L          �`�g  �  a  �  g                                          U     D    �a��  �  b  �  �                                     	Calc Type      L          �a�h  �  b  �  h                                         H     �    �b�p  �  d  �  o     ���                               H     �   � ��Z  �   �  �  Y     ���                               R          ��)  �    �  )                                     String     H     �   � T  �    �  S     ���                               Y         �_��  �  `  �  �                                     [Probe Names]      H     �    � ��  �   �  �       ���                               S      D    >        >                                     Numeric    H     �    !G        F     ���                               Z         R�    S    �                                     [Probe Ranges]     H     �     �     �         ���                               e     D    ���!  �  �  �  !                                     Fwd Pwr EStop Offset (dB)      H     �    ���  �  �  �       ���                               R             � - �       �   -   �                                     Timing     U     D     A c N �   A   d   N   �              	                    	Amplitude      Y     D     R � _   R   �   _                                    Amplitude Vpp      H    �D     S e ` �   S   g   S   �                                   R     D      c ) �      d   )   �                                  Offset     U     D     - � :   -   �   :                	                    	Offset(V)      H    �D     . e ; �   .   g   .   �                                   [     D     
 �  �   
   �      �                                  Gating Enabled?    Y           � '      �   '        
                        =   Enable Gating      f    �D        �            �                                  Arbitrary Signal Generator     P     D       ) )         )   )                                  Freq   P     D     0 c = z   0   d   =   z                                  Freq   H    �D     .  ; O   .      .   N                                   W     D     A  N K   A      N   K                                  Pulse Width    W     D     R � _ �   R   �   _   �                                  Pulse Width    H    �D     S  ` O   S      S   N                                   R     D     . � ;   .      ;                                    Gating     W     D     1 � >8   1      >  8                                  Gating Freq    H    �D     / � < �   /   �   /   �                                   V     D     B � O4   B      O  4              
                    
Duty Cycle     `     D     B3 O�   B  4   O  �                                  Gating Duty Cycle(%)   H    �D     C � P �   C   �   C   �                                   P           " 	 / "   "   
   /   "                                     Pane   L     D       � - �       �   -   �                                          P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          $        $                                     Pane   R      D    �$	I  �  %  	  I                                     ArbGen     Q     D      j + �      k   +   �                                     Cable      H    �    " 	 1 c   "      "   b     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          $ 1 1  $     1   1                                     Pane   R     D      �- �      �  -   �                                     SigGen     ]     D    FPS�  F  Q  S  �                                     SigGen Max Offset      H     �    D	QI  D    D  H     ���                               e     D    .P;�  .  Q  ;  �                                     App Pwr EStop Offset (dB)      H     �    ,	9I  ,    ,  H     ���                               W     D     % [ 2 �   %   \   2   �                                     PV Tol (dB)    H     �     #  0 T   #      #   S     ���                               _     D      j  �      k      �                                     PV File/Level (dBm)    H     �    	 	  c   	      	   b     ���                               P          � �� �  �   �  �   �                                     Pane   O      D    ����  �  �  �  �                                     Cal    Y     D     % [ 2 �   %   \   2   �                                     SigGen Offset      H    �     #  0 T   #      #   S     ���                               [     D      �  �      �      �                                     Seek File (Rel)    H     �    	   c   	      	   b     ���                               P          f    g                                         Pane   P     D    7M    8    M                                     Test   \     D    6 TC �  6   U  C   �                                  Step Used in ACF   H     �    9 UF c  9   W  9   b      
                               H    �   6 E >  6   	  6   =      
                               W     D    # G0 �  #   H  0   �                                     CLF Ave Cnt    H     �    ! . @  !     !   ?     ���                               Z     D     G �     H     �                                     TxAntenna neta     H     �    	  @  	     	   ?     ���                               Z     D     � Y � �   �   Z   �   �                                  Initial SigGen     H    �D     �  � E   �   	   �   D                                   T     D     � [ � �   �   \   �   �                                     Tol (dB)   H     �     �  � T   �      �   S     ���                               W     D     � [ � �   �   \   �   �                                     Offset (dB)    H     �     �  � T   �      �   S     ���                               _           � V � �   �   W   �   �                                     Controlled Variable    L     D     � V � ]   �   W   �   ]                                       H     �     � W � e   �   Y   �   d      
                            H    �    �  � @   �   	   �   ?      
                            [     D     � Y � �   �   Z   �   �                                  SigGen Freq Chg    H    �D     �  � E   �   	   �   D                                   T     D     � G � q   �   H   �   q                                     R Factor   H     �     �  � @   �      �   ?     ���                               Z     D     n G { �   n   H   {   �                                     RxAntenna neta     H     �     l  y @   l      l   ?     ���                               \     D     R j _ �   R   k   _   �                                     UUT Result (rel)   H     �    R  a c   R      R   b     ���                               _     D     ; Y H �   ;   Z   H   �                                  Max RF Level Change    H    �D     <  I E   <   	   <   D                                   V     D      i + �      j   +   �              
                       
AppPwr PID     M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P           " 	 / "   "   
   /   "                                     Pane   L     D      i + p      j   +   p                                          [     D     	 j  �   	   k      �                                     Limit File(V/m)    H     �    	 	  c   	      	   b     ���                               P          �f�  �  g  �                                       Pane   P     D    �*�B  �  +  �  B                                     Seek   U     D    ��	  �  �  	                	                    	Test Type      L     D    ��	�  �  �  	  �                                       H     �    &        %      
                            H    �D   �    �           
                            P     D     k8 xO   k  9   x  O                                     Freq   U     D     � � � �   �   �   �   �                                     	Spec Path      H     �    �  � l   �      �   k     ���                               Z     D     x u � �   x   v   �   �                                  Stop Frequency     H    �D     y  � a   y   	   y   `                                   [     D     d u q �   d   v   q   �                                  Start Frequency    H    �D     e  r a   e   	   e   `                                   V     D     9 F F y   9   G   F   y              
                       
Delta Freq     H     �     7  D @   7      7   ?     ���                               U     D     M � Z �   M   �   Z   �              	                       	Freq Path      H     �    N  ] c   N      N   b     ���                               ^     D      z  �      {      �                                     Freq Select Method     L     D      z  �      {      �                                         H     �     
 |  �   
   ~   
   �      
                               H    �       e      	      d      
                               Y     D     ! F . �   !   G   .   �                                     Points/Decade      H     �       , @            ?     ���                               P           o_ |x   o  `   |  x                                     Pane   L           k8 x?   k  9   x  ?                                         Q     D    E R:  E  !  R  :                                     Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          IjV�  I  k  V  �                                     Pane   L          E R'  E  !  R  '                                         Y     D    ���  �  �  �                                       Nominal Dwell      H     �    ���  �  �  �       ���                               a     D     S� `   S  �   `                                       FreqSettling Time(ms)      H    �     Xq e�   X  s   X  �     ���                               Y     D     7 j D �   7   k   D   �                                     Stimulus Gain      H     �    ; 	 J c   ;      ;   b     ���                               \     D      j + �      k   +   �                                     Stimulus Antenna   H     �    " 	 1 c   "      "   b     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          � � 1  �     �   1                                     Pane   O      D    � �� �  �   �  �   �                                     Amp    Y     D     o G | �   o   H   |   �                                     TrigOffset(s)      H     �     m  z @   m      m   ?     ���                               [     D     W G d �   W   H   d   �                                     Capture Time(s)    H     �     U  b @   U      U   ?     ���                               Y     D     7 j D �   7   k   D   �                                     Rev Pwr Cable      H     �    ; 	 J c   ;      ;   b     ���                               Y     D      j + �      k   +   �                                     Fwd Pwr Cable      H     �    " 	 1 c   "      "   b     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          R _ 1  R     _   1                                     Pane   U     D    N �[ �  N   �  [   �                                     	Pwr Meter      Q      D    � �� �  �   �  �   �                                     Motor      S     D     ~ Y � }   ~   Z   �   }                                  Control    W     D    ��   ?����         ?                                  Incremental    W     D        N            N                                  Incremental    V     D        <            <              
                    
Continuous     V     D       ! K         !   K              
                    
Continuous     P           � 	 � "   �   
   �   "                                     Pane   L           ~ Y � `   ~   Z   �   `                                          R     D     f G s q   f   H   s   q                                     Speed%     H     �     k  x @   k      k   ?     ���                               P     D     N G [ ^   N   H   [   ^                                     Step   H     �     S  ` @   S      S   ?     ���                               P     D     6 G C ^   6   H   C   ^                                     Stop   H    �     ;  H @   ;      ;   ?     ���                               Q     D      G + `      H   +   `                                     Start      H    �     #  0 @   #      #   ?     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          � � 1  �     �   1                                     Pane   L          � �� �  �   �  �   �                                          Y     D    � G� �  �   H  �   �                                     TrigOffset(s)      H     �    � � @  �     �   ?     ���                               X     D    v G� �  v   H  �   �                                     TrigDelay(s)   H     �    t � @  t     t   ?     ���                               W     D    W Gd �  W   H  d   �                                     Stop Factor    H    �    \ i @  \     \   ?     ���                               X      D    ? GL �  ?   H  L   �                                     Start Factor   H    �    D Q @  D     D   ?     ���                               [     D    & j3 �  &   k  3   �                                     Receive Antenna    H     �   * 	9 c  *     *   b     ���                               Y     D     j �     k     �                                     Receive Cable      H     �    	  c          b     ���                               U     D     � H
 v   �   I  
   v              	                    	Ref Level      H     �D     �  3   �   	   �   2                                   S     D     	 �  �   	   �      �                                     BW File    H     �    	 	  �   	      	   �     ���                               W     D      2 # x      3   #   x                                     BWFreqRange    ]     D     e _ r �   e   `   r   �                                     Sweep/Hz or Sweep      H     �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                     Video BW   H     �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                     Res BW     H     �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �                                     	Stop Freq      H     �     "  / X   "      "   W     ���                               V      D      _  �      `      �                                     
Start Freq     H     �     
   X   
      
   W     ���                               P           ) 6 6 O   )   7   6   O                                     Pane   L     D      2 # 9      3   #   9                                         P          =  J ,   =      J   ,                                     [Bw]   H     �     %  2 (   %      %   '     ���                               P           9 6 F O   9   7   F   O                                     Pane   W     D     $ 2 1 n   $   3   1   n                                     Test Params    b            + v         +   v                                     TestParam (Freq, Test)     H     �     5  B (   5      5   '     ���                               P     D     	 �  �   	   �      �                                     Name   H     �D    	 	  �   	         �     ���                               W     D     � H � �   �   I   �   �                                  Attenuation    H    �D     �  � 3   �   	   �   2                                   P            1          1                                     Pane   N     D    LZ    M    Z                                     SA     V     D     � �    �   �                   
                       
Test Title     H    �D    � A �   �   C     �     ���                               R     D     N � [   N   �   [                                       Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �D    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �D     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �D    P�� _ !   P����   ]         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �D     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �D     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �D    	��  !   	����            ���                               P           R A _ Z   R   B   _   Z                                     Pane   L     D     N � [ �   N   �   [   �                                         ]      D     � \ � �   �   ]   �   �                                     Show Diagnostics?      P          �� �  �����   �      �                                     Pane   L     D    �� ��� �����   �����   �                                          Q     D      4 ) T      5   )   T                                      CS101      W     D    2�?�  2  �  ?  �                                     Transformer    H     �   4C  4    4  ~     ���                               U     D    ����  �  �  �  �              	                    	Test Type      L          ����  �  �  �  �                                         H     �    ����  �  �  �  �      
                            H     �   ���  �    �  �      
                            V     D     ! i . �   !   j   .   �              
                       
CategoryNm     H     �    ! 	 0 c   !      !   b     ���                               V      D      G  |      H      |                                     
Resistance     H     �     
   @   
      
   ?     ���                               P          � @� Y  �   A  �   Y                                     Pane   O      D    � )� 9  �   *  �   9                                     Cal    U     D     R G _ t   R   H   _   t                                     	Line Freq      H    �     P  ] @   P      P   ?     ���                               \     D     6 j C �   6   k   C   �                                     Test Level Limit   H    �    6 	 E c   6      6   b     ���                               f     D     " v / �   "   w   /   �                                  SigGen Target Offset(Vrms)     H     �D        - a       	       `                                   ^     D     	 �  �   	   �      �                                     CS101Cal File(Rel)     H     �    	   c   	      	   b     ���                               P          g Bt [  g   C  t   [                                     Pane   P      D    c %p ;  c   &  p   ;                                     Test   V     D    �n��  �  o  �  �                                     
ScopeProbe     H     �   ��g  �    �  f     ���                               _     D    dXq�  d  Y  q  �                                     SigGen to Amp Cable    H     �   dsQ  d    d  P     ���                               b     D    sE�  s  F  �                                    Max Scope/
SigGen Gain     e     D    }E��  }  F  �  �                                  Initial Scope/SigGen Gain      H    �D    {�1  {    {  0                                   Q     D    bzo�  b  {  o  �                                  Units      `     D    Q�^�  Q  �  ^  �                                  Ripple Voltage Units   P     D    ��   ����                                           dBuV   P     D        -            -                               <   dBuV   P     D                                                      Vrms   P     D       ! ,         !   ,                               <   Vrms   P          f�s�  f  �  s  �                                     Pane   L          bzo�  b  {  o  �                                         L          Q�^�  Q  �  ^  �                                         Y     D    �K�  �  L    �                                     Read Delay(s)      H     �    �D  �    �  C     ���                               Q     D    ���  �  �    �                                     Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          5        5                                     Pane   L          ���  �  �    �                                         \     D    �K��  �  L  �  �                                     Nominal Dwell(s)   H     �    ��D  �    �  C     ���                               L     D    �I�P  �  J  �  P                                       c     D    �I��  �  J  �  �                                  Scope Target Offset(dB)    H     �D    ��5  �    �  4                                   T     D    �n��  �  o  �  �                                     SA Cable   H     �   ��g  �    �  f     ���                               b     D    � ��   �   �  �                                        Spectrum Analyzer Name     H     �   � &� �  �   (  �        ���                               X     D    ��   @����         @                                  Oscilloscope   X     D        O            O                                  Oscilloscope   ]     D        `            `                                  Spectrum Analyzer      ]     D       ! o         !   o                                  Spectrum Analyzer      P          �S�l  �  T  �  l                                     Pane   Q     D    �O�j  �  P  �  j                                  Input      \     D     b �     c     �                                     SigGen Max(Vrms)   H     �     1 ]     3     \     ���                               \     D    � b� �  �   c  �   �                                     SigGen Min(Vrms)   H    �    � 1� ]  �   3  �   \     ���                               Z     D    � b� �  �   c  �   �                                     Scope Tol-(dB)     H    �    � 1� ]  �   3  �   \     ���                               V     D     | � � �   |   �   �   �              
                       
Test Title     H     �    | & � �   |   (   �        ���                               R     D     � �0   �     �  0                                     Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �D     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �D    P�� _ !   P����   ]         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �D     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �D     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �D    	��  !   	����            ���                               P           � �)   �     �  )                                     Pane   L     D     � �   �     �                                           P     D     � � �   �   �   �                                       Freq   U     D     � s � �   �   t   �   �                                     	Spec Path      H     �    �  � l   �      �   k     ���                               Z     D     { v � �   {   w   �   �                                  Stop Frequency     H    �D     y  � a   y   	   y   `                                   [     D     d v q �   d   w   q   �                                  Start Frequency    H    �D     e  r a   e   	   e   `                                   V     D     9 F F y   9   G   F   y              
                       
Delta Freq     H     �     7  D @   7      7   ?     ���                               U     D     M � Z �   M   �   Z   �              	                       	Freq Path      H     �    N  ] c   N      [   b     ���                               ^     D      {  �      |      �                                     Freq Select Method     L     D      {  �      |      �                                         H     �     
 |  �   
   ~   
   �      
                               H    �       e      	      d      
                               Y     D     ! F . �   !   G   .   �                                     Points/Decade      H     �       , @            ?     ���                               P           � & � ?   �   '   �   ?                                     Pane   L           � � � �   �   �   �   �                                         U      D    I�V�  I  �  V  �              	                       	EStop Str      H     �   KZ�  K    K       ���                               _     D    L �Y �  L   �  Y   �                                  Sig Gen Freq Change    H    �D    J $W ~  J   &  J   }                                   b     D    6 �C  6   �  C                                    Scope EStop Offset(dB)     H    �D    4 $A ~  4   &  4   }                                   ]     D      �- �      �  -   �                                  Sig Gen Max Delta      H    �D     $+ ~     &     }                                   T     D     {� ��   {  �   �  �                                     ScopePID   M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P           ; �T     <   �  T                                     Pane   L     D     {� ��   {  �   �  �                                          Z     D    � b� �  �   c  �   �                                     Scope Tol+(dB)     H    �    � 1� ]  �   3  �   \     ���                               ]      D    � A� �  �   B  �   �                                     Show Diagnostics?      U     D    u �� �  u   �  �   �              	                       	Amplifier      H     �   u &� �  u   (  u        ���                               \     D    \ �i �  \   �  i   �                                     Signal Generator   H     �   \ &k �  \   (  \        ���                               V     D    C �P �  C   �  P   �              
                       
Scope Name     H     �   C &R �  C   (  C        ���                               P           . 8 ; Q   .   9   ;   Q                                     Pane   L     D      4 ) ;      5   )   ;                                          N     D     <�� I    <����   I                                         CS     ^     D    � �� �  �   �  �   �                                     SA Time Multiplier     H     �    � �� �  �   �  �   �     ���                               _     D    ������  �����  �����                                     Directional Coupler    H    �   ��0���  ����2  �����     ���                               b     D     � � �   �   �   �                                    Cal Category SP Offset     H     �D     � � � �   �   �   �   �                                   ]     D     O � \   O   �   \                                       Cat File(For Cal)      H     �D    c � r   c   �   p       ���                               X     D     � � �   �   �     �                                  OP Ref Level   H     �D     � �     �     �                                   b     D     � � �   �   �   �                                    OP Target EStop Offset     H     �D     � � � �   �   �   �   �                                   [     D     y � �    y   �   �                                        Injection Probe    H     �D    � � �   �   �   �       ���                               Z     D    � �� �  �   �  �   �                                  OP Target Tol-     H     �D    � 1� p  �   3  �   o                                   Z     D    � �� �  �   �  �   �                                  OP Target Tol+     H     �D    � 1� p  �   3  �   o                                   Y     D    F qS �  F   r  S   �                                     OP SA #Points      H     �    K >X j  K   @  K   i     ���                               [     D    x �� �  x   �  �   �                                  OP Limit Adjust    H     �D    v 1� p  v   3  v   o                                   W     D    b �o �  b   �  o   �                                  OP SA Atten    H     �D    ` 1m p  `   3  `   o                                   T     D    - �: �  -   �  :   �                                  OP Cable   H     �   1 3@ �  1   5  1   �     ���                               a     D     �%     �  %                                       Output Power Analyzer      H     �    3' �     5     �     ���                               ^     D    � �� �  �   �  �   �                                    Cat File(For Test)     H     �   � 0� �  �   2  �   �     ���                               Z     D    � �� �  �   �  �   �                                     Mod Adjust(dB)     H     �   � 0� �  �   2  �   �     ���                               _     D     � �     �     �                                     Cal Injection Probe    H     �   
 0 �  
   2  
   �     ���                               ]     D    # l0 �  #   m  0   �                                     Mod SigGen Offset      H     �    $ ;1 g  $   =  $   f     ���                               b     D    � ��  �   �  �                                       Calibration File (rel)     H     �   � 9  �  �   ;  �   �     ���                               Y     D    S l` �  S   m  `   �                                     Max RF Change      H     �    T ;a g  T   =  T   f     ���                               \     D    ; lH �  ;   m  H   �                                     Dampening Factor   H     �    < ;I g  <   =  <   f     ���                               [     D    � s� �  �   t  �   �                                  OP Limit Source    ^     D        o            o                                  Cal Category Level     L     D                                                    <       ^     D       ! k         !   k                                  Cal Category Limit     L      D                                                      <       P          � �   �     �                                       Pane   L         � s� z  �   t  �   z                                         Y     D    � a �  �   b     �                                     FP SA #Points      H     �    � . Z  �   0  �   Y     ���                               [     D    � �� �  �   �  �   �                                  FP EStop Adjust    H    �D    � !� {  �   #  �   z                                   S     D    � a� �  �   b  �   �                                     FP Tol+    H     �    � .� Z  �   0  �   Y     ���                               S     D    � a� �  �   b  �   �                                     FP Tol-    H     �    � .� Z  �   0  �   Y     ���                               X     D    � �� �  �   �  �   �                                  FP Overshoot   H    �D    � !� {  �   #  �   z                                   W     D    � u� �  �   v  �   �                                  FP SA Atten    H     �D    � !� `  �   #  �   _                                   Z     D    o _| �  o   `  |   �                                     FP SA RefLevel     H     �    p .} Z  p   0  p   Y     ���                               T     D    T �a �  T   �  a   �                                  FP Cable   H     �   V #e }  V   %  V   |     ���                               b     D    = �J �  =   �  J   �                                     Forward Power Analyzer     H     �   = #L }  =   %  =   |     ���                               a     D    � ��  �   �  �                                    Induced Current Units      P     D    �� ]  x����   ^      x                                  Amps   P     D      l  �      m      �                               <   Amps   O     D    ��   ����                                           dBm    O     D        '            '                               <   dBm    P     D    �� .  H����   /      H                                  dBuA   P     D      =  W      >      W                               <   dBuA   P          � � 1  �     �   1                                     Pane   L          � �� �  �   �  �   �                                         e     D    _�'l��  _���(  l����                                     Bandwidths and Sweep Time      W     D    _�'l�e  _���(  l���e                                     BW Table RB    L     D      � % �      �   %   �                                       V     D      � ! �      �   !   �              
                    
Sweep Time     H    �D      k " �      m      �                                   U     D      ]  �      ^      �              	                    	SweepTime      _     D      l  �      m      �                               <   Auto BW, User Sweep    P     D     ��     ����                                        Auto   P     D                                                     <   Auto   S     D     ��      ����                                         BWTable    X     D     "  / G   "      /   G                               <   Use BW Table   P                                                               Pane   e     D    ������ �������������   �                                  Bandwidths and Sweep Time      W     D    ������ <������������   <                                  BW Table RB    P          s�+��D  s���,  ����D                                     Pane   Y     D    _�'l�l  _���(  l���l                                     BW UI Control      Y     D    _�'l�l  _���(  l���l                                     BW UI Control      Y     D    � x� �  �   y  �   �                                     Monitor Probe      H     �   � � r  �     �   q     ���                               ]     D    ��{���  ����|  �����                                  SigGen Max Offset      H    �D    ��-��g  ����/  ����f                                   X     D    ������  �����  �����                                  SigGen Cable   H     �   ��/���  ����1  �����     ���                               b     D    ����   �����  �                                     RF Sig Gen Freq Change     H    �D    ��-��z  ����/  ����y                                   _     D    m��z��  m����  z����                                     RF Signal Generator    H     �   m�/|��  m���1  m����     ���                               S      D    ������  �����  �����                                     BW File    H     �   ����   �����  �        ���                               W     D    ��S���  ����T  �����                                     BWFreqRange    ]     D     e _ r �   e   `   r   �                                     Sweep/Hz or Sweep      H     �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                     Video BW   H     �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                     Res BW     H     �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �                                     	Stop Freq      H     �     "  / X   "      "   W     ���                               V      D      _  �      `      �                                     
Start Freq     H    �     
   X   
      
   W     ���                               P          ��W��p  ����X  ����p                                     Pane   L     D    ��S��Z  ����T  ����Z                                         V         ��O���  ����P  �����                                     
Bandwidths     H     �    ��7��I  ����9  ����H     ���                               R     D     � � � �   �   �   �   �                                     FM Mod     U     D     � d � �   �   e   �   �              	                       	Depth/Dev      H     �     � 2 � ^   �   4   �   ]     ���                               P     D     � d � {   �   e   �   {                                     Freq   H     �     ~ 2 � ^   ~   4   ~   ]     ���                               P     D     _ � l �   _   �   l   �                                     Type   L           _ � l �   _   �   l   �                                         H     �     d � q �   d   �   d   �     ���                               H    �D    d 2 s |   d   4   q   {     ���                               P           �  �    �      �                                        Pane   W     D     � � � �   �   �   �   �                                     Mod Channel    R     D     N � [ �   N   �   [   �                                     AM Mod     U     D     � d � �   �   e   �   �              	                       	Depth/Dev      H     �D     � 2 � ^   �   4   �   ]     ���                               P     D     � d � {   �   e   �   {                                     Freq   H     �D     ~ 2 � ^   ~   4   ~   ]     ���                               P     D     _ � l �   _   �   l   �                                     Type   L           _ � l �   _   �   l   �                                         H     �     d � q �   d   �   d   �     ���                               H    �D    d 2 s |   d   4   q   {     ���                               P           R  _    R      _                                        Pane   W     D     N � [ �   N   �   [   �                                     Mod Channel    X     D    P��]��  P����  ]����                                     CW File(Rel)   H     �   S�7b��  S���9  S����     ���                               Z     D    ! E. �  !   F  .   �                                     Pulse Freq(Hz)     H     �    " / ?  "     "   >     ���                               R     D     G f     H     f                                     Pulse?     R     D     y�+ ��O   y���,   ����O                                     Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �    P�� _ !   P����   P         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �D     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �D    	��  !   	����            ���                               P           ��/ ��H   ����0   ����H                                     Pane   L     D     y�+ ��2   y���,   ����2                                         P     D    7�.D�E  7���/  D���E                                     Freq   U     D     � s � �   �   t   �   �                                     	Spec Path      H     �    �  � l   �      �   k     ���                               Z     D     | v � �   |   w   �   �                                  Stop Frequency     H    �D     z  � a   z   	   z   `                                   [     D     e v r �   e   w   r   �                                  Start Frequency    H    �D     f  s a   f   	   f   `                                   V     D     : F G y   :   G   G   y              
                       
Delta Freq     H     �     8  E @   8      8   ?     ���                               U     D     N � [ �   N   �   [   �              	                       	Freq Path      H     �    O  ^ c   O      O   b     ���                               ^     D      w  �      x      �                                     Freq Select Method     L     D      w  ~      x      ~                                         H     �      |  �      ~      �      
                               H    �       e      	      d      
                               Y     D     " F / �   "   G   /   �                                     Points/Decade      H     �        - @              ?     ���                               P          I�2V�K  I���3  V���K                                     Pane   L          7�.D�5  7���/  D���5                                         V     D     b�� o��   b����   o����              
                       
Test Title     H     �D    c�/ r��   c���1   p����     ���                               P           K�� X��   K����   X����                                     Type   L     D     K�� X��   K����   X����                                       H     �     O�� \��   O����   O����      
                            H    �D    K�- [��   K���/   X����      
                            Q     D    
��   
����                                          Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          �3�L  ���4  ���L                                     Pane   L          
����  
����  ����                                         Y     D    ��p��  ����q  ����                                     Nominal Dwell      H     �    ��>�j  ����@  ����i     ���                               ]      D    #�N0��  #���O  0����                                     Show Diagnostics?      U     D    n |{ �  n   }  {   �              	                       	Amplifier      H     �   n } u  n     n   t     ���                               P           N  [    N      [                                        Pane   L     D     <�� I    <����   I                                             N     D    �� �  �����   �      �                                      AF     W     D    �+�    �  +  �                                     Transformer    H     �   V-�    X    �     ���                               T     D    ����  �  �  �  �                                  Delay(s)   H     �D    �T��  �  V  �  �                                   X     D    � ��  �   �  �                                       Initial Gain   H    �    � ��&  �   �  �  %     ���                               [     D    G�T�  G  �  T  �                                     Scope EStop(dB)    H     �    H`U�  H  b  H  �     ���                               _     D    �    �                                         SigGen to Amp Cable    H     �   V�    X    �     ���                               _     D    ����  �  �  �  �                                     SigGen Offset(Vrms)    H    �    �a��  �  c  �  �     ���                               [     D    ���  �  �  �                                       ScopeFilePrefix    H    �   �V��  �  X  �  �     ���                               U     D    ����  �  �  �  �              	                       	Amplifier      H     �   �V��  �  X  �  �     ���                               _     D    � w� �  �   x  �   �                                     Scope SP Offset(dB)    H    �    � F� r  �   H  �   q     ���                               U     D    ����  �  �  �  �              	                       	EStop Str      H    �   �V��  �  X  �  �     ���                               ]      D    2p?�  2  q  ?  �                                     Show Diagnostics?      R     D    � 7� [  �   8  �   [                                     Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �    P�� _ !   P����   P         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �    	��  !   	����   	         ���                               P          � ;� T  �   <  �   T                                     Pane   L     D    � 7� >  �   8  �   >                                         X     D    % �2'  %   �  2  '                                  Length units   L          % �2 �  %   �  2   �                                         H     �    :G%  :    :  $      
                            H     �   7 �F   7   �  7   �      
                            X     D    � �+  �   �    +                                     Cable Len(m)   H     �     �&     �    %     ���                               W     D     z� ��   z  �   �  �                                     PlotGen Err    H     �D    ~W ��   ~  Y   �  �     ���                               X     D     �� ��   �  �   �  �                                     ScopeSP File   H     �D    �W ��   �  Y   �  �     ���                               Z      D    s w� �  s   x  �   �                                     Scope Tol-(dB)     H    �    t F� r  t   H  t   q     ���                               ^     D     �� �@   �  �   �  @                                     ArbGen Result(Rel)     H     �D    �` ��   �  b   �  �     ���                               V          s��7  s  �  �  7                                     
SigGen Cmd     L     D    s��  s  �  �                                            H     �    x��  x     x        
                               H    �   uT��  u  V  u  �      
                              Y     D     �� �
   �  �   �  
                                  Freq Stepback      H     �D     �U ��   �  W   �  �                                   V     D     h� u�   h  �   u  �              
                    
ArbGen Min     H    �D     fU s�   f  W   f  �                                   U          D �Q  D   �  Q                                       	Test Type      L     D    D �Q �  D   �  Q   �                                          H     �    G �T �  G   �  G   �      
                               H    �   D 9S �  D   ;  D   �      
                              P     D     d q*   d     q  *                                     Freq   U     D     � s � �   �   t   �   �                                     	Spec Path      H     �D    �  � l   �      �   k     ���                               Z     D     | v � �   |   w   �   �                                  Stop Frequency     H    �D     z  � a   z   	   z   `                                   [     D     e v r �   e   w   r   �                                  Start Frequency    H    �D     f  s a   f   	   f   `                                   V     D     : F G y   :   G   G   y              
                       
Delta Freq     H     �D     8  E @   8      8   ?     ���                               U     D     N � [ �   N   �   [   �              	                       	Freq Path      H     �D    O  ^ c   O      \   b     ���                               ^     D      w  �      x      �                                     Freq Select Method     L     D      w  ~      x      ~                                         H     �      |  �      ~      �      
                               H    �D       e      	      d      
                               Y     D     " F / �   "   G   /   �                                     Points/Decade      H     �D        - @              ?     ���                               P           h ; u T   h   <   u   T                                     Pane   L           d q   d     q                                           V     D    ]�j�  ]  �  j  �              
                       
Test Title     H    �   ^Vm�  ^  X  ^  �     ���                               _     D    � w� �  �   x  �   �                                     Max Gen Delta(Vrms)    H     �    � F� r  �   H  �   q     ���                               O     D    � m�   �   n  �                                        PID    M     D     7 x D �   7   y   D   �                                  D      H     �D     8 % E c   8   '   8   b                                   M     D     # x 0 }   #   y   0   }                                  I      H     �D     $ % 1 c   $   '   $   b                                   Z     D     O x \ �   O   y   \   �                                  Damping Factor     M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P          � q� �  �   r  �   �                                     Pane   L     D    � m� t  �   n  �   t                                          Q     D    '        '                                     Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          Wp    X    p                                     Pane   L                                                           Y     D     ���   �  �    �                                     Nominal Dwell      H     �D     �b�   �  d   �  �     ���                               W     D     �� ��   �  �   �  �                                     Scope Probe    H     �D    �W ��   �  Y   �  �     ���                               V     D     R� _    R  �   _                 
                    
ArbGen Max     H    �D     PU ]�   P  W   P  �                                   Z     D    [ wh �  [   x  h   �                                     Scope Tol+(dB)     H    �    \ Fi r  \   H  \   q     ���                               _     D    - �: �  -   �  :   �                                     Arbitrary Generator    H     �   - ;< �  -   =  -   �     ���                               X     D     �! �     �  !   �                                     Oscilloscope   H     �    ;# �     =     �     ���                               P            �  �      �      �                                     Pane   L     D    �� �  �����   �      �                                          N     D      k  y      l      y                                      CE     V     D    � !� \  �   "  �   \              
                    
SA PreAmp?     V     D    � 0� k  �   1  �   k              
                 =   
SA PreAmp?     Z     D    � i� �  �   j  �   �                                  Ref Lvl Offset     H    �D    � #� U  �   %  �   T                                   S     D    � e� �  �   f  �   �                                  #Sweeps    S     D    � e� �  �   f  �   �                                  #Sweeps    H    �D    � #� Q  �   %  �   P                                   Z     D    � �� �  �   �  �   �                                     Amplifier Name     H    �   � %�   �   '  �   ~     ���                               U      D     O � \,   O   �   \  ,                                      	TestPrefs      R     D     N � [ �   N   �   [   �                                    SA Cfg     a     D     �  � t   �      �   t                                    Sweep Time Multiplier      H    �     ��� �    �����   �        ���                               W     D     ?�� L >   ?����   L   >                                    BWFreqRange    ]     D     e _ r �   e   `   r   �                                    Sweep/Hz or Sweep      H    �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                    Video BW   H    �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                    Res BW     H    �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �              	                      	Stop Freq      H    �     "  / X   "      "   W     ���                               V      D      _  �      `      �              
                      
Start Freq     H    �     
   X   
      
   W     ���                               P           R�� _    R����   _                                       Pane   L     D     ?�� L��   ?����   L����                                         P          ;�� H    ;����   H                                       BW[]   H     �     N�� [��   N����   N����     ���                               e     D    ������ T������������   T                                      Bandwidths and Sweep Time      W     D    ������ 	������������   	                                      BW Table RB    L     D      � % �      �   %   �                                       V     D      � ! �      �   !   �              
                    
Sweep Time     H    �D      k " �      m      �                                   U     D      ]  �      ^      �              	                    	SweepTime      _     D      l  �      m      �                               <   Auto BW, User Sweep    P     D     ��     ����                                        Auto   P     D                                                     <   Auto   S     D     ��      ����                                         BWTable    X     D     "  / G   "      /   G                               <   Use BW Table   P                                                                Pane   e     D    ������ �������������   �                                  Bandwidths and Sweep Time      W     D    ������ <������������   <                                  BW Table RB    P          ���� ����������   ����                                      Pane   L     D    ������������������������                                          L     D    ������������������������                                          V     D    �� �� C����   ����   C              
                      
#SA Points     H    �    ������ ������������        ���                               Z     D    �� �� W����   ����   W                                    Max Stop/Start     H    �    ������ ������������        ���                               P           a � n �   a   �   n   �                                    Pane   L     D     N � [ �   N   �   [   �                                         V     D     1B >|   1  C   >  |                                      
ScopeProbe     H     �    5 � D;   5   �   5  :     ���                               L     D     # (*     $   (  *                                       Z     D     # )p     $   )  p                                  Stop Frequency     H     �D      � *      �                                        L     D     # *     $     *                                       [     D     # r     $     r                                  Start Frequency    H     �D      �       �                                        L     D    z Y� `  z   Z  �   `                                       X     D    z Y� �  z   Z  �   �                                  PID Max Freq   H    �D    | � E  |   	  |   D                                   Y     D    e jr �  e   k  r   �                                      Monitor Cable      H     �   e 	t c  e     e   b     ���                               Y     D    L jY �  L   k  Y   �                                    Monitor Probe      H     �   L 	[ c  L     L   b     ���                               Z     D    3 j@ �  3   k  @   �                                      Bandwidth File     H     �   3 	B c  3     3   b     ���                               Y     D     j' �     k  '   �                                      CE Limit File      H     �    	) c          b     ���                               U     D     � ] � �   �   ^   �   �              	                      	SigGenMin      H     �     �  � V   �      �   U     ���                               V     D     � G � |   �   H   �   |              
                      
Resistance     H     �     �  � @   �      �   ?     ���                               V     D     i u v �   i   v   v   �              
                    
Max SigGen     H    �D     k  x a   k   	   k   `                                   Z     D     X ] e �   X   ^   e   �                                    Max SigGen Chg     H     �     V  c V   V      V   U     ���                               P     D     @ ] M p   @   ^   M   p                                    Tol-   H     �     >  K V   >      >   U     ���                               P     D     ( ] 5 t   (   ^   5   t                                    Tol+   H     �     &  3 V   &      &   U     ���                               O     D      i  {      j      {                                      PID    M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P           	 	  "   	   
      "                                    Pane   L     D      i  p      j      p                                          P           h 	 u "   h   
   u   "                                    Pane   O     D     � � � �   �   �   �   �                                    Cal    V     D     O j \ �   O   k   \   �                                      
Scope Name     H     �    O 	 ^ c   O      \   b     ���                               S           ! = . d   !   >   .   d                                    Numeric    H     �     4 A A m   4   C   4   l     ���                               ^          2 x ? �   2   y   ?   �                                    SigGenLevlOffset[]     H     �     /  < (   /      /   '     ���                               S          �� =  d����   >      d                                    Numeric    H     �      A  m      C      l     ���                               X           x  �      y      �                                    SigGenFreq[]   H     �        (            '     ���                               P           d q   d     q                                        Pane   U      D     O � \,   O   �   \  ,              	                       	TestPrefs      [          I � V   I   �   V                                       Prefs[TestType]    H     �D     ` � m �   `   �   `   �     ���                               U     D    � �� �  �   �  �   �              	                    	Test Type      L          � �� �  �   �  �   �                                         H     �    � �� �  �   �  �   �      
                            H     �   � #� �  �   %  �   �      
                            U     D    " �/ �  "   �  /   �              	                       	RF SigGen      H     �   " %1   "   '  "   ~     ���                               T     D    s �� �  s   �  �   �                                     2Pts/BW?   R          B NO k  B   O  O   k                                     String     H     �   W Rf �  W   T  W   �     ���                               X         L �Y �  L   �  Y   �                                     Scan Details   H     �    o 2| D  o   4  o   C     ���                               H     �    S 2` D  S   4  S   C     ���                               W     D    : �G �  :   �  G   �                                  Attenuation    H    �D    8 #E }  8   %  8   |                                   R     D     � � � �   �   �   �   �                                     Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �D    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �D     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �D    P�� _ !   P����   ]         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �D     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �D     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �D    	��  !   	����            ���                               P           H % U >   H   &   U   >                                     Pane   L     D     � � � �   �   �   �   �                                         V     D     � �     �     �              
                       
Test Title     H     �   	 %   	   '  	   ~     ���                               ]      D     � @ �   �   A     �                                     Show Diagnostics?      ]     D     � � � �   �   �   �   �                                     Spectrum Analyzer      H     �D    � % �    �   '   �   ~     ���                               P            o + �      p   +   �                                     Pane   L     D      k  r      l      r                                          P           Y # f <   Y   $   f   <                                     Pane   T     D     D  Q D   D       Q   D                                   AllPrefs   P           /  <    /      <                                        Pane   =!�               displayFilter!�                    =!�               displayFilter!�         0          �!�               classString!�     0����      ?*::(INSTR|SOCKET)       displayFilter!�                  	typeClass!�     0����      Instr       r!�               displayFilter!�                  	typeClass!�     0����      Analog Input       
 o�x��}|E���%�$$HI�Ar�\h�@z�w�n D!�:6^QQQl��ްw	��U,(�XPAD=���3�w;;w�%����'|2��f�3ߧ�S�A�?�%ټ�"�~�o�{Pt�@��bJ����c#�cG��gc*ڽ7D���%^�DB��A(ӎ2;�׺D�$���L��:N��گ��8�_#�u���a.JR��?���8|i�>��@��I�B��'Ϯ����T����בC"�m�y�}���q`!��d�����	q��/R=(���c�<�$�y�}mF�x��ζ;�gۑxJ\r���Yn��8X�q�K������v#�U<���Ǆ'�J>�L�7�82�|�=�.��s�S�Y�Y� �O�'jO�.��D�*�<�X���k=�ӣ�9�֎���Ύ�>ؒ|0�#\\S+����c恸�]Q���k]#ţ�^����Cۑq�����?"7:��0��
�<�Enx�l��c�eE�W`.j�ơDTUѐ|f�t��/��܊t�i���WF&G��.��#\�F�̗���"Q�0fh%]�W骭����gJOT�.�ŏ�J�G�ԍ����^'O���d�I~$�7�$x-]�H�r���3������#���K�8��O�~Z~�(��ڍ�ᝀF~J	�c��+�&�؊�ľ~"���ξ̍Z�gK/%��IFs�����V�s�z�5nQ�然����W���crLF�_��D]��V4�!����qk^$����p �`�(h�b-РV�F��N h�AhZ�k�-*:�D�� r��_y�2|�zG����n7����
�DmQ{�j��c1!c ����E�=�\��a�=�3��S���QjHh�Z�����(��L.�O_w�u�i�O�H��Z	 g˦x,ߥ��4�f5�f ��ÇX�R��3-�B� ��s|�b�`u��B��@z��]i��{�_.W�UG��D��Y2׷�S��	&����VK�s�N�c�T->tG-R�����9}w�R��h�4��E
�yb���;�<m?<���~*��d_5�L��&��y1W�r��Q[�����{N�����(�i��:a�?M|G���4���(�	4�ua�0�$�JS}C�g��GK�����XL���ap���>킮�78M�� �2�]")o�X!�nT0o���I���]��� ��|���e�R� �	�XU���E/r+�.��ETT�~j���'!׀~z,X?E��Ip��i(������S+j�tI�$tI�$�0�����~��թm�~�X�OB_k���I��oN?[�O��S����	k�|�O��%�~B������0�u���i��+X?���I�p8���!�;2�-<�QPB�iTP�L�
*���SPB�%��+(a�����a�*�-WSP��E.��a���(� �@vuԀ�z.XA���_e5��@�fIA�Q3�DH*(�{H*(t���VAm������)+(�%�D�
�fNA�YQP-,(����
Z'�RAA�������y�P�䉳92m���'�7��[�U�PF&��=qh�i��M=i��N�֩��'n�V<qS�2�&�������0h��U�� 슋�$-5�M2�K�d_�&I�5���&)g4�ԯ-i�D
��!�I�~��d�!�d귬&y
V�Fj��?X�$S[�$S4�I��dJ�L�ق&�zļ&���I��	~�=�$�I�:o q�&��t<�6aOΨ����O�?��AŮ��~5r~$���O{��:��*���{���E���p�S�<�K����HȪ����d�cF!�II�˹��_���&c;� �~"!���3k��&�_���vdL!����d�����q7��72���r���d$�YRCFr�r�r���x��G��'���H���)��l�������k,�ػ)�2|'	���dm�Ӎ�x�� ����]5�x8*G��6�h���[��<�ĝT�Ș1G]X� L'�bց;I��������Մ5��RQ���T���9��t�]��$S���!j���V��9��g��_�#�@�9!<��<V���gjx��]�}7��]��|���3���~*W�C��@쐱�|������b�ԶQ�����_@����1��GA8�p���kz����>\��NP�WioJ��P��,xS(]���k��T��c�0�ɯ;V�4H��a+y�hGAc���i�u4���`��([g�m%��QЂ1���e�%g���T�3��Z�C�b+H����8+O4f�5���g��g�|㬼k��+�8K��YL���)z�Y6Β��YyG� ;�o����g�K�VVy'-�L}Z��q�'�8+O�3�.���g]�e�3��<[�m�'�N��̫a�8��V����vo�)Z�s�KwK�NT�G`��G92��9�~N��z:~GǧQ�j�:>Ҡ�Wې5����g^�j>ZO�OPWg�P�>�a�~��#d��"̑�(�d`\�6 ��0N�a\��i��]�K٩��q�'$M�2os��L� ��`�,�z
/b�(�a�ȊuD�0֎�H0^��ƿ�w�x9�0>�0���`|��/2 c`�,� �pWGVkGA4�C`|_0�3(��=0Τx�.4a�}h���f��a0^�cᐐ/��Q����{���Fj���ىV��D;%�¹�=Y��='��\j��N��V����s;$���"$�Ȏ�a��N`�:|O�)ۏ��[D�������K��������ݫ"�g+vY��g����ή�Q�D��x���M�3B��W�>W|����O���1��G+�v���K�È)V�bJ��ԓ��3:�oOa?�������5@���͢���0t��?_��gS5�R�#���O���@��
<���n�4J�%T|�G�Tž���ˡ\��"���T���	0w�r���푈��|�ʛo\k3AuZCP�N���|O�ɳ����E9{/�*��.��
�����2��){��zQ"
�����3��)��sGA,ޗ4���0�+�\�*/��*T1��ؗK�W�N�y�dm[�o6������v8ބ�5D/q���ǅ>�~�5�yqᾩU��[ܨ%��v`��	����h�1v�OVr 
X'_���N��#	��`N�Q2 �><��'�?E���&�##��*�'��K[���J;�1����XB�!d�JFr����Hj��6�q;$��*y��Cc��B6O;�u�2��:���j�"�)���.�d/x�Ӛ^���hm��+E�3�^�8���֫k������Â�[��A$��K�]�$� �(A�!H,�S5v��N	�A�� ��	�o� ym�>�2 ��� ƈC����-k��j�&��4�Ai�� M�IOjߦP�̜`yvF�*b�,�h!���t��+�p-b��9b� �z��I�	 i�P�g�ݡ�$�GsH�o���o�
������I�H�8��PR[�
lGHD"�-�4�ː��=�Dj��N�i�ާ�U~:L�Ӈ�⽛��2���8H��l��jl�ERf�.�8�`����2�2��Dw����1�QЎ�ku8�?p̶`��GN`��U
�k���(�\����U��4ɪY��74K��l�� y�_�͛eK���U`_�˂�u�K��;ViqE�J&6A��՛��� +�c��_�,���$�]�h+��٪t�~.��m�����)��&��:�6Xy-�ui7��E/{�.o���Zr���.���zלں�Y���+������/X\o]��Ug�^d��={�x�r{�\�o����5�����QI��+*��񬌸"��
� ��2b8�S�Cɼ>�̊+ e��E�
��R)s%��-3z-�|1�+)���C=�m�~�q6~����/f�ʤ+� ����&o_7+���ߏ`��*�M��$..�0��w�k�ܨ�~8�	������10�%Q�����⥒��4�O�}R�%��j������^��UVO�yd\E1\ɚ�GQ6��Q?!�`8�o�"&~�����{D�G�b�jJ�/_\_k�?1�>a�~�R�k���dU������%��"�T�����T���x��qqU��x�&��L��`R/%n�	�Gќ`�Q�.���ئ&�rc�<�F���;C�y��@ػY�� l�`�S����� `� �g�s�x|0�K(�B���B��M�vE�U�z�Ly�J���R��}���qU�����K��=�ת�fky���=�=U�zs4�z���j^���z8�/}��ߣH~�O���>	�!8ǹ�j�)Z���	e�KWN�1U��$S�^F�18ͯ�Q�}��d�ê�i�oHلcZ���;�G��Q�`۟>�hU��Gw��o�Y0�~c�ic�`���nu؎ӄ-��l������^��4a��"Q�`���l �Q�W���8[��2Q|�цmwt����=)�q���a��1^��e6��m�y�N��uf�_���K��Up}u�.���`7�@J�~r�9��A�F�z9�j�(�ы�r����7�w+��c=����TRzhm��=3�(��/�N�<������?X~���x�K�{�[/���d�'csd���87��W/S�Y�¿��![�xx��-�Sg�'��?��g�\I�����W�y�&p<EC�K�khz������{�x�z�y�&������"q�7O��=����HZ �A�v�2�C)�;[E�0
�.�����Dpf� �a��@pw@�`��'��D@�C,� xu0��S����*B�I�����Ap��b � �ݽ 8�Ě��w��6�ૂ<�"��U��P�	Mǆ&�[7��X@�}{E@p��,�K=$Ž �a���o
F�h�`d�c(T��D�-4�<���+t�ϑ�ޛH~և� �T�	Sn���+����z-C���JO��R�`�z���?��R�����=�n�R�A4��J��2|_�T�_�R�pm1�-��4)�Y�<���TG���4�p*���9�J�v�\�^Ww*�I]���Bkl�!ٯ���-�8�+Tr�*=Δr�~�#Rs ���E�x*�?���cj�K�g��{�,�J?
�Z�ҏC���t��Z��OUˍuI�T�M�)�\��O�U��.ݯY��C�j-w�*����ki"�(��K~-�j��T�Ъ�^-ߓ[�]��z-w`���I�Z��UL-77���m/Lc��c<�o�M�S�`E����[���9��t�9��DJ���QƒY�=��joK���S:H	y�u(O7�N�983�\�K39�g��R�]d$����s�J��q#"�Թ|>�\�^<���#=�a��8�[����Ĺ���[*���[:�-�[��U_�x����r�����E���ٮ:W�,�}I��.���3��k��0c|�v���$������f~����ߺ���W��X�����̟Ok�$��x^3�
�rX�E��P�(�_ЌWI����)Y��}H�$�
)��m?g������IUSdۺ�%����(��N�*}Y'��;< ��ƒ�J_�K�*SM��\�&UŐI�&�Ko�q��kS��>D̖d??��7���S,/<b�f��T������0���c�Z�x�'L�P}�i9ቦ�'-pB?���Q��=�	_z�*9a���|l�Sb��d)! �4k*�c Kptp����b=���\�ֱkp��}pe}u��%�U��Z'�d��Σ�r����O(�������#�
�S|?�I��'%[i9�. ���NR�����F\�)~���o�=>]�+n֑?c�H��ʟ��Or���I��-D��>Am�S,~fQ ު#~J�'G�?;t�O)< ����mz�g����`�O|>"�nfx�[��7ooG?3l�1ó,3\m ��0�U��������`vrQf�܈L/̦`�Ҵ�pm���V�Ё�<#2��^��&�.%����ya���v���FS1��8�&m兵��W��x�妹�.���)Z�5-3\Ѵ̰�3�b����2�
`�MA;��0ӑ���K�E����0���p>e��:%`k�
ǂ���+�4��7̣p]ٴܰ�i��B��`Yn�χr�&��۽}�N�g*�8�>�|�p�Uhڮ������U��j����7�{��hf����U'.4N܀���g7�U��D�8�k軸tÉ�(�����3W�9��Aת��?'�#nE�2�Da�w�pRl���?m��t�E�^dIt��q�.и��j���h.�����\�a�g�����ԣ���CrK��Q�#+��+���+�_H%�pKg��#s1��!��R:2$VJG5K�J�h�%������x��2�q)W%n�ʑ��}��o��r�Q��[�o=�I�Є�ЄoE��w���n�� �� �^n�Ȳ|_a�{��^���},�w	�I�Єo�Єo~����|�5 �����	o,�� 
�l7�udu����W��_αkK)|��໌�$/4��54�ۭy���|w��k ��^ �Nn�ڑ����t�������n	�+(N:�&|���?��4�y��i�w�%�랿��΍�8�2 ����M0 ����]I�h	��(Nڇ&|;���Mj�&[��6��
�}�`��t�8GV&���Q�˩X���7�|/�8ie��s����m��� �M�{!�w��O���)ݨ-`�M֕(p%��݋�+�fƕ8�+��$̂/�2c)�c=��ގL�v�n�+5�bHR���]*Ӥ�m�&�e���m�&ˍ�W�y��ZBzs�Q7�c������ġ�e�&�M�d��ohRk�&�Mn��aq����49l�&�rh����G�4����'4q�B����n,M�.�o`i���̡�:J��&ir��7h2�Nn�<�ci�ύp9�^�&���]���4��$M��4��M���#@�=�d�&�.0~���v5�D���l�4��$M��4���6�;��;���&o�Q��]�&F��vph����Tu\b���� {�1��M��&�4yэ��&�4y� M�[������4����U4�e�wNM�xY�<�F)@��Y�<c��^�J�gM�d3��n4�1F� O�$ed���<d"=�`}�]j�����j�%s�3��a������π����W��(yT��OW��(yL���8�q�3��[�����i�Owj`~P�Z�2w�%h�gQK�(�Gy�K���		4�>n�jP�{��l�n�����\�F����%������3=́V[)�n3��.q%؍滚�#,w5w Y>bj�K.3���m��^vƔ������×\n�nq��(Y���%W�d5|�z���%WZ�l^��Bg�M�:����\g�Mu6/��Bg���;�����l��y���9���R�Ǭt\i��y�#�Q�ʤ!s#%ȅ����IG���&��h8��VRC�� ��c��%tf��C��dr�Br�RKҚ�7�1+\�f*q�B2&R�($;���7Wg֒��;��a��|��Ix
�d�T���,_B�'��>[��Á��	�����s|m��g����W�Wψo�l������.�\z/7%���j����ꠌ{eJ|
\����R�K���i����LT����!�����B��%�U�(D���F1~+w*,���a�ʽ���&���4癷r�߲l�fY>g��!��W9r�6�[Ce+�3��n�ʽ�J��i�M+w�+w�i�r�Y�r�[�r'��r�1g�N�b�k��}A��6y�.�(s�Q	�c�&����Q��_�ms#��sTx���@Q�S:Ew`���AG��UI:{�����M`4�]�7b��y!�Ŋ&V �
ɁF9�N�YC�X�P-�#��ʽ�B�\�.*0JC�H-M#��Y2J���|H���A�z�?z'c�&�� Z4 ��3 x2�;)�{Y��)�C�g�&��4��Z �v ~ ���H	`P,� ��	�U�{0|�U,���y@���rU,]�OЛ�)q�Vv'�YNx�/YN����ᇨ���l������o�Y	�s�� N��w�XOI���Q4���U>���9����f�<Wĸ��+�\�-b��=�R�ȫ�S��b
.�F�üꔘB�W�XVx�&ObBpӤ�����RGh��z�B~>�|��w��Fۛ�>@_��je�J4T�D�$�_�=&Na-afZ�Ve�P��rO��a�
�꧕�C�vV%r��`e�Ht17r��'h�-$��P���x��L���شW��i�Ƣ�ԁ���I�Ep���n�j��B7:�)��st2ddi�ǀ��"�U�,K������d�R,��e�B�2���k�����o� ���u��J��O�on�@���ſ�����òO������#2E���ۙf,�i��Ip�>�'����M�eL�b��yȆ�Cm�#S�Q���tϛ�)���j�Y��)1]$������q+#�%��cT�Q�jJ_t��B�zX��S���J�˦Z�/�T[5`�}���n�ܨ���Z��U�s����I���x�"�~^�̺���ښ�'Ҵ��K�F���0?���ʉ�~�/�>�����u�j������G�򟗢DV{�uM�=<��_���µ_Μr�>��[�4b�˗�#9�8F�d��J��)Żݨ-�ڃ��ԑ����i>�t��!����$��~k�ߧ���"$�L�!Y�R|�Y���_Y��VP1�	��j�ʫ�K �a�h?��Og��"^�N����{A����X�_�by�B�����!��W͇X�_3b�J�Q�T�[G3�5 o�ބ߳�x�&������hML쵀�w̆�h�����v�a���[��8v��v+�P>c�nӤ���Y�q�,8���?�i�SD΂�n��{r�n��Q�F�-թ����(J/�6N�6
�Y�`���F�|�	ŏ����D�0�����D�4�����S�:Z�N8�r��N(�a2;�y*&o�U�Ϭ��Ú�|�ʣ;�I�z�&�w��},�br�1y�Y1ه��>�	��[�N(~�\vB񃦲����P��BvB[�	=ALN �	
1y����$.U���fK^�b�l��K�2J�NY���,��b�H����|��IZ�p��K^���h��?,&C�T��B�B��R���R�Pl�T��d�B��R�b+�
�JҎ(U��d�p���n����-qd�l{���n�k�*��-��5{��؊����M�٪��]h�k�f 1_�k� ���f���i�S�V%��N�mM��A!4'4u���4��-����%����]���!�U�i��|���!Y����<$u�:M�C���ӿ5贞��N��;��Z�dÑ��-e8��ix5'�&�N��ޫ�N&%����C�8ANI.�ͦ$?FR����#�)�r&%�xII���XR|��c��A���k��̫vU�&!U���%���K\����~=�U��媱�W��qՓ�!��չj�"�Nߕ|��N0)H��jJ`M@qh�,�-jBLR�jQR��������N��YeC�a�3�tAT�͂�h�|;��DOU/���8Ղ��瘂(.?y���w�8��S,�����o��QS]S�SW9�Q�;=��a�w{#`o*{{(�FZ�^Ǩ�������{� ��{E� 앒�T??�����ƌa������Ic�`l:c{)�X��;M���M��A0�� � c��4~��>�Reߥ��W5Uv
<�<����~�T��8;[eZ�V�l��A��O�-.�l���㮝*[\h0U6	���TY?/��y���C*$[fG��n~u��^�Z���f��^_=ߕg�]�[4���>���"W}���9�6h��{q߳Y�5�u��dVo�K�$��[���$�l��g�{tU��T��M"�ܜ����R�,՜ߴ����e6�~m+�M��Q���F�Y�@��X�R�N�o�-?�ُ�O�e'e�D�J��mZh:��C2ٷ��<��J�8�ᐸI8��2}�IG9�Y�>�]:��Zz?���D�����io>�7m��L�S�Ӊ���[�%�Mؓ����!��`=e������=X���_���v�T�KƏ��O��K��Q��Uv�}
��*�g<����yd��_||�9,����2���t��#��1&�c����cQ�1�;���b�_e4Гl�,�X�����C�}T�~��LK�'�I�O�贅�D�A8)���3O�PoaA�W����x�8J@�z�I@ ���uџ�z���|]��I��L�H��_�c���@�|ow�P? 2�:�
�,"��E?(s]�!��2_&�Y1�"Ǥ���U%��S)M�1�~���~�fp�wx*A6B�9�M���A/�I�dk��f����X�ըE?K&��:�T�<EG�E^�
��E�T(7��,L;�ۑ�zK�-<7��������R�ӕ�O��>}���az���Pl�_�M���ƶh\ [$�A�$GR�.5hަŜQ��vo�2��=?�td��%�����\�a�/({~z���K���&{~���{~A�S@,{����W��=���S<��Mg�,�w�U������A=���P����W]Z�(�Y⧞;���rɢp��nΡ�5}�7T���5�!5�hћ|��50�v�f�Eoi9T��{�N��w���uj9���P-�C���p��w�n����= ���ө?�w��<H_�ݳ�Y���E/j#o�K����2.��9{ �G3�|����|��h9i� D��
�y��������l��$]�$m!�-d��z'��&��="ƈ˘���{IB@�ϑ^t_�'-X���Ʉ�C�ry�R��[y\�P�̊v5i���a�H��^QL(��x�J���5��{��L��b�vK����㎦�ޝM����c��� �>��ޣ�	E[HB@�{׆BB�В��u&~����S�д�ִ��Ǝ�؛������4g��H�������L%m��P��tB@�NCB@�%��'��!��@�Y�����eV�֘O��x� �U� <%n��hB@�'����+<�sd~��̱���3oN`�P�_Vx��~+�%��;��8�~��8����8W��m�C��Ο��"�'f�\��M�Y�:/�wm�:���q�ը�w;B��8bQ"��8~���y
���#e^���lU����Fk���'#�p�C�O��(�U�l~��(��Sѐ(\�b^T��AK&�T<h���&���-����E�p��*4�-�o�=�Fj����AS���\�ڧz���Ĕ��'�FZ�Z���A��b�tr�A�.
��DD��M/�X� (���dOQ�YY+����t�iQ<z�cO��T�������BϦ�u���=3ul�� ~�]5�f��T��	�w�(�ҳ�Իjt
H"���m�.�1��.�v�xb����7�]�st��`/��Y�ct}'Z^��t�m���Դ�;���>�o3�Ű�c�_��E �ۚ�z�;����~��;�Z�	��F�dvH�Ȑ��+�,�~E�ͧ�t2rn~ 8�{���q0��I�� ��P�a�r`ק9 ���mE�6^,�$�Hq��m%�̶R=�M-4f[�d��aͶ�F�6�����f� �l���X�m6\{56�e��Af[�u|f[&pM��c��V�(i-ڱ��t�]�B�pvu���1r�kQr�� G�@x���ٯE܆�#[a���F+8g��I5O/g��@KA������J�������̰�s"�W׸*�����/��ZP_=�z^u�r;>�U���x4�sL��J��ԎiMD�yLk���i�wk�/_.��_zӼ'�G�e��p����X�-<e�LXϑ�ˋU�VpJ����4Z����<fB�3�"}3�����>3!�7\��9߰G~��gT}��蠚����oxL|}�:�]-����#�|�p�{��;�}U|Å��߉>��6$0.ռy:�ߡ6�Q�K�+�L����,j.�A;�����Ku��܆��ol_����-N�U�6�e�QG6�#�[u�q��H}�g�p��a2��8����>Լ��.l/{�E�ś���s��lx����țl���d5�7��nRdy�75$�D޴�TyӛEM�Ͱ�&&(4�"J��]��Fw� p���ual���8�"eLhxlhx\� x� �4�9 �Il��"E6!�U,%&��	4`�Wjd{�ןl�9���S$��Ta��Ђr�@K]؄y{�7�I��	-)S�.lyg�d��!滰u`��{^Q<,��ua��ej� D��m�[�Y������҅-���.ly��uaR�݂�\�E_&�J���滰u�jD�,t�	���I�@m�ޗ�d7Jud���R���h ��lpN���4�D��3f�v٫����	����Ԥq���&���[��%���ΰ�g���ަU�y�ݨ�#k,o'V]��,o9,��3�.o[k�g,���j�J6�2�M�3�y�D�g}��/n�9Q{�Art'/� �i��t��ܕH��t B	F�`����FIr<m\�� Q�m�:j�"Q�����Z殬��*F��m��v��k+��y&�Q���+�Uye��?���7�R9����m��t��O+��Da�M�1 lN@^���9������Mx��=~vf���Ko�3	��g��F~8��Ȏ��!;�Q�����L@�)��&�f��8*��NN������@v<�a#*�����	᪬i5��nQ�}t]�L�f��h�=����y6�(֤FF�RO5*�5��b��B&�C��z]G{�?��ݑ5V�3�d���f{?G���+�<j���� �!�h��*��>�5E�<�\�l����f�zĀ��!��a]Aq��uvd�waq�����8����u����64q�]H����?X��Ag�͂M�J��1��n�@��*�uF�_��݁*�+kNᯀ��s�?�e�1/'��u�(G��[��������ȗ��4f�C�:r8�����A^+��Z��X�KC�ȑ�qi$��.�/��D��OQk��ɔ�?��>ػW�8S���F��m��q�g�|��4w.��?���� �L̶4�����c,������|Y�W�"��;�Q�H��+��B&�������B*���!����FH��r�i����-�O��z8�Z���|�\_绦�z8߳��������{��zx�y��)�M��9hF|�_3>�Ț l{����ӌ��f|ҤfL���)�f�ыT��Ќ
F}:45�3���m͸ۂf|¢fl��j�4�#�HEN�٭��jFk�n��BF���Ú�h��X3Zhv�4��V��4�u�7��Ԍ��n�5�J�[=ͨ��VO3�4��Ԍ�Ќ���y�瞷��j.ˠ}��T�A��+j����Y�`�D�����='�{���4s����d8o4�e0;�9Y9LtʹQ''� j#�ı2��l��2�gQ�2p�G;�`$zF5� u���4��>&���M�]��[�+�)�b7�pd�d�c��Ku2��@r����Xf��NpP	�<�*�+�T�9W���s�����L%��W%����PI���p%�p��I��J:ڶ0�Ѝ2�&�63��HOƓ�Qlē9���<C~��Ɠ)�1�2�3ʙL�rJ�T��,���Cx�L��n�.>�d�;�I&�Dx���0`�L-Gdѝ�(k�(�T&��Dh��/��%�9�|>J��ll�{p�	��(F��3�O�B���1 Θn���= �� Θa��2R2�I gT�$�3f6��[ � ���Z<!�Wx�p��B�Θ��3W	Um�o?&�J�%q�.�� g�tk1jР�F�k��� ��C�Xa�(�W�X]|�
�D��m%�8�/��Qz�����=>�D��S|��������q��ǲ�!��S�U"�v��<�ܝ�=�D��UF�p�m�=�����S����&�F��N���*�{�&��o�2Fin�J�}U�]�n���ۮ,�5M�Wr/��}�
�m<�ڶ+c���k0zX}�Unp����a6��Z܄^B%���d��9Y��Lg2�*Y�(e���(�J�̚��!k���!�ȑ�:$UJz�(���U/����lo�xe��hO�Ont�#k��:�#d����Üꄞ�	��:�Lym�3��:!��I����NH�֯N�́%^��*�Fy��	�`m{3r<�A[$p�����K�ߤ'ԓ��Q0\G���w�����L����+�;5M'��p����w)��t �9@w���0F��-oq�bo��a��-�Y2vx�|�����Z��
�ǡs$�خa��l�>��e�ٮ�q�i�Ռа>\��6�55�q���kخp�8��Y�v�U����ڮa�d�U�˻qmW[ex�v����v���z~"pm׾��ض�ٮ�Y�lW��ȳ��mW��]���/n�,�PU��tMD]m+�b�5 �9� ��	e��G�ǒ�,��=��VE�f(;��	e�\ۨPvT`(;��[UBٴ�gJ3��S�3ʎR3��&��r��Pv�l({�w�Re�$�lʎT��S�k���)���[5C�>l���Sv����{K�1�۸�l[��~*���e۪�{rC�)w���ۢ�E�CTC�)w����N3�넥��Dx��:&���W��u���qdM�-av�H��*��	B9*�@����F����ѭ׹юe�]� ;����ʬ�*uY�����i����]�im汬u=D��B��P&:_5d��H�2��	Y�9����h.���q��Y^ ׾�Y�������5�A��|��{��S`
}�8G���9}]$�kt����}����u1e�^����%M��QQH�k�߀����k4��ׇP;�������F���(��FC��5F7���k4TM_�;��|ŋ��Sϲ��S�6\+�,[�'W_���� ��F#Y}}��r����'Ž���Z��ŗ���
�6aOΨ������4#IqaP"?Wr:[~5��6�
HZ��OZHĉ
���N����0������N2>@�'ɸ��ߑ�2�$���@D�1���d$Ws��r�D���f������[�6H�D���N|��0aȕ�I�V�2B�L	Ke:��p���y�id���~�N9�IG�δ�m,�h�_��w2�'~L^��O�u&9 �����G�g��ʥ�d[�(S�L���Td�\�E�����b�Qe�,��f5W�iQ���D�c�Qt4߻���X���c���*O��k^��ـ��_?��W؜�?gs6'��G,�/+��ǯ�3�Zx��'�(N�&c"�`�%7gk����X~M$�:ޯ˯t�u2���~@�5��V�L�l���dٔ�dَ�sNw��NR��om������W�p4�F��g1c �Wr0<�b8�2�Q��M�	M�5��Z��g�`�-4�x��B��̪�`�w<�`�a�e�M��M�hGX��{0��o��	�l&`��m�q� ���`x����?,cx���?CÅ�$��͂�¿-`�5n ��`�0i60\�b�'����0��-cx8ˑ���/���̓�_-`���u=8n@��pػ�+sI#�;|����<Z��UmBs����&�GϫN�F+ay�s=��J�C�	���}�z�|K��|�<�U�en�G1��Q��N���%}��/�n�2<�Z�G�g��F���U�>:��&,�Cq,:\�MX�G]�t���1��f}>o �T�����~A>����"�d� ��T������j>��56��C�"���Iv��ojE��\���·4�H>l�f}��E�G_K���=�(�j��T�5hE�V���F�
��G�� �]�N�f}tܪ����w+�/n`�����T�4�����)�c��8я1�+�5&���Ȋ�ee7V~�|\���+�
�=��K�}H����(�_��BE�4�� �"�ת��6/z�S��m��k�����*�]sj�g-�����㪷�Z\�`q�}t�RW��z��v��E�����s῕�k��w��'G%E����YC����� ��pL��lv�8�҅�j��/�š�8	��-�99����%���$�O����A��lr~�JM'�>�ϞWI��OȻ�����'�����_�����b)�C+9.p#���-��dq�C��.$ѿ�nd���Z�q�_Q�%�/�aB���{�T���F7�՟�����@��ߓ�z
w��Ä�����������-u���!J5��p�l�q"]��$�� 
DB�j����~���3���*�x����L:O��Rb�֚Ţ)`P��5���0��8H<�%�
$N��\J╄�-�1��h�á�$J�U������u�*+6�7��O�%�~�v��RKZn7� �v�v2%m!ml��R|�<���sH<��xF���<��M�vE�}�܍ƚۍN�ⲽ�n�kdg%Ƭ��ߍ�abO���$�ݨ��Si_�F;�U����ݨ�Wm7��@�l� {�o��сL;��`7�Xf��7��Y�d�_pD��DU�j�L��_$�\Ŷ���d�E�Q��GA�*.���;SF�mńc��/i`�C�Q���-�)��rg�ǿ^�����j,��Gn?�8��ʴ������nU�<��\Bj�<j���"w���4�T/�>�n�D�zc�J��i"��:j���������1uWL��!6T;>#){�/�n?/W�;���|6{D��ҳVp�L٤w^������Y�_�9/$}���!��v�4�	'�r���35 ���6l|�9��gp>��3 ���� >�Z~��"E�F�Y �V#�� ��l-,�,`�n �� �k�~l�9ޜM1�>�U���]h�z�9\�B莐P��~*�&j�0��PF�r��Z�<]��I�l
�rsm�����M�f�З����Z���#5c3B_�U7�&�.4y���z��:A;\��8�$��p�F����N�����C��ʛ_g)��ƴ]粗�_`US�wr�h��Q�TR��}��c�RN1�Fq7�����s4B�W mՃdu��yw�=����ww\}���:��Ů�Y��K��ͳ�t��\U�g���j��s+�ap�kϟ���^{�kVe�k�����fv�Iskj���e�\0W��g��� ��r�<�qW�ҏ���ϗ�xgo�3�D=��|��?2m���q>��q�Ga�G��|&�&�Ü���W��|&<!���<��~z����g���
o������Ivs����w�Bf%NK�rC���c�lip�a>�lMn�'b�A�r���rC��z�rCo7�R���pCo�� �����������<=n��?��+�n���7$�mL����M�0���^�Ƹa�T�p!�Rn�d��(\�;��VnHoZn�[�������r�F�Ug�t6��"��^E�:X��99���!�P#�6������)F��x�j5��=�՜F��[�5��{��bjQǓF���W/ZT][c�ZT� �o@��u���^K�E��= �K(��[�^�Ȗ�)�l�+_�,ɀ���C�*���^ �#k��XV�h(�3Ϋ�YJ�J�v��2�l���(,4}@����la����/|��k��_�9Ɠ{ҍZz�3;��?u<�. �G�.�a�_&O�[!S$�m����'��R�D��+�&��Q$�dK�_�ܷ�C���&�L�d��q4y�M�&yh��&�<�߹Q���&?�xf�'5�#4/��@�a�#4��o+|���:���������O��cGh�ܮ����k�+@�̸hG�|_��D�e�����y�q,�d�{�4�{��8r�jңys�nңys�?�7�F��y��a��Rio��䂜nE8 ��?X�sY�u:8^��	L\B��>�B�RJ�O,ݫ	]ҎΎ�!�(U��L!d�����:��:�����Ք���K&3�
����*\FE�s!Y���|H�*�`�T!�E#��h�B*©3�Z�L��WH� cb�	����P�fg����L�k�3u����g��7EY�o������z���O੔�侣(Ca�o^"M�ס例�P��W���2�=EJP�J������aǠ�N9(mU?��r��u�Wl���lG�oY���GV��:��ZG���XGk�u��4XG�S��ᦵ�iZ��Q��\���
?"�����"�Ӎbam��F�N;�9�5��6i]A)r��h���� �v�E�j+w��Ƈ��: ����is���iw+���i�S�9T�O;Γ;�V9��<2�ڕ�ed���W��&��%wB�yX��cmG�y(�ԱD��&��e:����fxn�+���bp��y]�[-,�8c�S\�*P���ȼ��պ �2���n6�s��C��������/��&��[�o�ĵ!�o�������HΝ`��z�iu��^o��:��a a�x�\#�P�B��9���+�5�,b��&��?�"WY��C|-��(��)_ �IR2Yf�|ͭ)S��< �bY��|Mj��cE�I��;��^�93x �����J�����9D�O��{�����Xi�����i���f���{���d^Ͻ��u�z�j��� �
:x�<���8�1��go��u.s~#.�nMC�MɞsI�H,D˞z��=Wr�s3e�N{n�|�S��\�V����Yc�=k%��Ų��{֝��4��{�=�f�,��Up��L���޼�Z�LZ[)��,X	�,�ǀ*{�{�S-�;�\e�u��U�l؎�A��(�N�WK^oG�U�M֪���y;�'��!w��S����T6�N�����/~��Vt��a6�Q:�o�wq*��/3Z��)x�R��1��ۆ�R�lHV6�.�V6��	i◴��ʓ�ύ� ��YE���"= ��_q]�B#ye�%$��ޑ��b.�W�[��W&=����mT���+k�D��A��ye7�����O���Ϯ��^Q[S_9/�W]}���y���5Uv�ϵ��k�((��&���b������c�H1#�W�����ֈ�3�=�(��|���4 �wp���T��2��NY��y�|Иp<�d��(ӯ+C[߱���dg�b���5r,~G��"?d��F���cg`�d�H�82��l2�g`��L��;߻E�#>�
�@0��$g�,
rTF�TF�J�-�TFuh��)��w��T�+�U���9�zERk8ER;(c9��H*yS$8F���V���W�(����
��-���:����N{EB�q�
ӛ��ۨ�L���y;�P�C���94��]�%�37ü�19� l�A�tE�q���HY �eRGr~�1�: �8���	�sDN�a�rNXK�S�P��!�&���4��B� jl�H��l��K���r��|6u$�&�b%6$1���I�YRG�X�D\�RG�q/y�'���k���)C5RG4SG|O=u$Q%u��&MT�i�OI��OeZ�ԑ4���ԑ$�ԑx��z�:�ԑ����^_)�':+�?���!sk2��Ξb1<�F�j�)�)2z�S,��)�	�q�ޞ�^]_���)����Y��vg@��,A�ut%�e�8��$G� �@�L.A��U'�E��Y�s�t!�K���D7�`�ёx��]�#8���`�]��4k��/�ߌ��4�3�!igtn#8��#x�N�<?� �F�)e��tYG�r �"������(�㭁�A����qbh��}󀸃�3b �堡�>3��f<$�C�0�`�F�ݢ�H:�������F�i7j3��=?'��32Zi3����32b�`���x'��3����`F=c	����1My�`�.�e�߭�xg��ն�>��j����f���`6��W8-4��2-4�N�� �ɑ���:�Zv.,a6�	Z�H>"K���l����Ʉ}�7�	_Z�H&���BO馾�L��l��ö��<��L��B�zD���&�����?�i��Zh|"ߓ��L���Bo$��7�7����cńJ��m���"oo�'~��V]�H���b �P;t-G<*���4�y��`!85�PpJ$�f�;4�	΍�&����l�Tz�T�qY@��T/����+�n�f��@����R��
�Ri�/�n�xÈ�#�(@��]�vR�%~�ÈOȌ��$#>)3b�m��#�M���� �q�%�[�O�o����,��~S3�<\��6����A�i�H�j���ɿ�Z��a�l5��Vsx=�t����ZͶy�w\�y%�0�B%�8�D�n�4��zV3~5�9�M��?�[�Y��<��!�
���^�]$�X�@�fq��(췉���'@?�T�Q��1�
	O��-a��q򀞡���l�B�_�+ U!�K�BNF���ɸ��m���~2�D~������-2~K�_�3��ϓ�҃*7�D%��:H��L&�L2n��Zd���ǵP�{��*l%�	��|�& ��;�~���(w�<&�YY��4 �r��e�ޭ'�F�˹$cEQ�b�V�	Xd���hx��v�Pu1G�?'o�
���c��Xێ=/���kCr;V�5$�c��Y�hx���-	/���v��&���ZGÛU�c�KUZn�o����S�v��v,]�'����۱��b��Xk���aE�SaPE�f�X���b2^HƵ�������S��=c����@֮���.f��:f�����W�L>|�Rx�y�2�OC�֓�}��O��1�/�)�����&2_�4��$M^�4��M~1F��I�X��d�P����R4��C�W(M���ɫ�&�-��!��:4�s����m?^�o0_�i7��VmV�w���S�ݪ�j������S�=U:�-�t���U�4��@o����c�����j~U:���@��8�{���Ks��{�6a��Ց�=�RG�{:N{�z��aW����^Ɍ�%f����TƼIߠ�8�$3�II2�3�i,c<9lLZũ��M̾���޾�* ���}�[�S|k$E��&E��o��%-ݚ��d�$cwڿ�Wn�x��U�BS)�U0^fm�^�^t�=�^t�����oS�<S�E�z
��(� �&����(潐x�xD| �T4$s%�c��ZaqZa��=c��p��Ȑ�Yu��.\P��	s�f��.�\�F'k�)^��,5�T4=[2eֲj�U�^J�a�­����W�,�]�H��gs�%��fi5:� u7uoC>� ���w��,S5 ��44UU����4��S��_� H�{v'�T1 h6lL��U3 *6l�Fq��Y ��QyX�M��ߣ/�/�<���=<����0��U{��;�c��(����	ǍD��BL��������S�U^�I\9+�LW�m��0�	Uñ��]�+Oa������%;$3�
sB2�0�Y2�
��3��6���}��1���I��* �z��};�<��)�S�u�/EJ��pZh8�y l� �u � �� pkr�х �+Y Ǚ��(��Z�G)�B��	���p� �2 �M ��A #�7� na ��8 ��8���Q��M G�&����� �� �� �7!�g�O���I9W1�����O���+�t�}*S�@���m�5�DY�Β\c�Ԃ�涩��7�Mu�z�]���,8�ߦފ���2�i�kmSo�{>L��ߦ���Sߊ﫲M-���6����M����G�a'�V�6�s�2?�&�ó�mS~�F��h�j�LҘF$�Å�m��8�V#�B��U����Z�S����D�Qt����O���!Om�߁N<�K�x� ���;;�����>�D�B���>�Y�;Y8�.�7��:>1�)lG�|!�΂O�����uT�g!�:>I�Y��YTg�Tg�s��}o�8��V���+���]o�xG�x�q��|׀�<��:��yOOuW�H_c��e�؟).�E��]��D3�gN��߿�41���3'R��f�i�]��$hҋ�(h�D��mfir��\���7�&kL����V8�0Fh�"�M��͙X��7��jb]�f��Tl��|kL�װ̦i�X�����*&�,Mk����Ue�Ě��rL�-,��]ܒ���cb}G_&K�ĂgQ5����w�Y��j�.ĘX��m:(S��wK���+�m�Z��	�������9�0'!W�@��B��sle�#�?�y��8�� D�?js{\h�8��r 3�'��m�q\���Ϸ��T�[��U7�U�����zmnk8mn�m������^��3�P���^J� �m�?0��3��J��#�&��g�X���3,õa���l|bhl~�8��˼�A�eN�&���q��m�|�����6�B�9O��n�p�AZ/u=#��є����N���)7+�rA�qNN����m:�s��h4�t��u��'�]�X�����Sm���W��9�;��=O��lS�||p1��e��7�+;���N��/tek,��QJ�ڦ]��M��K,��wV�jX�;qU=����ɝ����ncW�s�n��N��te�YZ�c��ÛveG4�ʞcae�X�U��W�n��0�������G��3P>n�o����E0q>_eZ���\�A��7����2E��*Fr���24�k$���������B�����2���������N��q��
4G�H��f#�d���\�vLv�x��!�yr�w����hA
��'�'d������lx��un7yG������'�rNJ��ZX��o`�v�u��;Z�,�r�"5Z92���mϮ�??ǔW�w����I�`��!��9�,^��y�`��x�G�]Q6���E�
ng$Iί
?n������9f����q��+�9DID#�s�r��L�ˁ.����������r�w���e�6��Q.�>4����d�����?Z`�;���sH|�s
ue�����r��: �,�?3 � �)�??-@��"fh�������|� � BZ�})����R�L�8E��Mj)���,h��ƴ�c@��*i9o��ny����ݺ	�@��r��o�V���&�i{��[+���=�ۭ�͘�
|_��V�^�ۭ�Э���m,�^Ԅ_7t'*�'��e^ҍI����'漬����u�-�/O�-�~C���D�Y�q�H���";�*2�#c=W�q7�q; ��t�5�[����^l ;�(�x\�%��]�'ةo/�Q	��l]��-H�Zc-�^��/��c��s�l�;X��u��4y�C��.sR�Fi������>�ɫ�%L�h�v)��ȹU/46����-�J�FBc9wH���`v]�q'��f���1984fkAͮ��Bcc�B39�1���q�'�nK�*_��>�v��.j��*�;�XXqp,�֒�&����F72�aJ�ba��~\�V��}�xeU�f�(�Vg'kk�3�gr8'�����aۢ��y]h�ׇ�Q|C���,����b�B��V�̀��:lg� �Y �7 dN#[4�ȭ(b6�&��
M ol o� �r@v�/� yNg�BW��a�|� _�r�ŧȱ1��&�/M �n _f�} y �%NpS��L �A�ĀA~ĭ�A�ԤAކRd���i� �D!.c�Is�QK��},Mj��^M�(MjMҤ-��4�l�&�&���,M�`4����ls�m��Q��1����4�k�&���4�(�㞶�3Ŕ;˖@�`��;��ڧꗚ�wg}��L����������;k��;�3|_5w�y�Y_�=\w�,��i�o%z]�Np��H_f��;�E՝5Ayѻ�ȳ黳�.�a��Yٿ�QqgE��J�!�9dL"c,������P��?�����(
� ��H?�-9�d��=�'��wr�ߞ��0��]���y��_�x��qq����2ɑ�K��h�,�&I�&L�$�Ҥ�M���]@�p~��&�$G�.�&�9r�:"�ҤФ��HiRd�&��I*��,�W����BW�u���0�Q�n�~�B]R�F���"<['j�����C��<;���w�?7�C� �7Kb����fSE������q�p�B��ۂ3`mi�g��n�czƌ�.{U�<��NW,N�N�t��΃���Ƀ�OȻ��<؜�<�Z�<�������C`���	l�sN������J1�θ�#��t};Z�R��8HM)�)4�TZ�H�tR�Rj x1
�,�b?F����6�,�#��Ep�5gP��M�M�7�, x�/oD�؃�r°�)]�ӳ��zV��#I�>�ѳ���A�:��4�g��ӳ��l����(z�az�,X�!���`�Q�ۜ������V�=�v�5�O�)��h7L܋�U�����{�%��{��������*�lѠ��E��c����8$�Qz�B��(�,�2��z��Y�<F�'�����n�j��&�H�\h@	��h�'��� \6E�d�c����S�d˂%����(Srd=�}ؠz�������>�5���S����-��-��Q�F�ڏգ?�g�'�-7j�kǕ���)���k�e˕�L�_�k�esR�|m�W�+���j��";�����3L?��=���.A���m {�Q�� ��z��Ǖ���~\������~\��d?���������Q���|@?�le�xP?.�'M�W�G���dt��G��X�WKt�4��X+{�W��T�}�Տ��|On?��O��q����я��E�qM�]��*� '�q�"'�s�����YV�kf���A��wpѭ�U�/Y�w�<�rH_	�q��ͣ�_����U>a�t|�?��j�k�ϱZ�>�u�e��ڃj��e����jZ�j=)�
M��+4�����#f�Z���V���Z-GKU�Lf?��զIU��S|���?�iOki���=�Z�m����Ե�tZ-_6���j��(�h�*O��k^G�� ���Ldo��8�
��.�b;�"�F#%�ۥ��@:]��<�_9��t�����~ݡ�R�ᇬ�L\�~#S2�B�}W��d�FJ&�EM����viMI؀�c��O_c���������
�?�;�b,{Ә ��-�S�E%��Aʓ�ӣ�a�~��<U䱬��XVt:0ԝ�r%�(�b�(�!D��w)5I�u��҄H���\i�r�P�Ī�5:�aw�G���*w��r_dU�:k*�/]�+BS�M�{����4��MVT�tU.�[]�^��r��&���k�*7�?�i��Tn�|O��ݢ�r��XM��3�rۡ���R�\P�mܨ5��^VǶ�ӱ�����n�G����ыى��[����^d�&�X2�'�42�!�B��؉���!��� Io����%���^�V�/�Q�5�qd^>u*��8cQ"���
��X��8���HHm2�UEC���90g'��3��bWl(?-����V�bcIP0HPlB%G)��ľ\0/�����t��^r�'{�t���d�l~� ˤ|�D�ۢT�b;B�� �o��\�F��4R��w�O��>����az�>���g%៘2�;��i-\{nv��\�"Y5��~"N�~���U�2�4G:�����*X�W� g��p�K�pu!5��'�ꖎ�����L����M.hҤ��yM��=�B���~���$n���<M:S�&i�_cx��ɗA���Y�b���Y^���M��3�v�gYX�H�x�xS���r�o��}�U|�u����G�J��Nh��8pi˨�;'4��U<���:(|n�x�'��'zx� 8I�<�7L��Rn�v@�,�+4�.�}���rj�4`���S�
5�m�%��l=�M��L�!���k����n�k�͆k�fM��ߌ�nI����x�����^H��r	MJr�r"�A@:��B DB��R�X�^@P@A�w,�T,��?;v�5r��3�w;;����.1��}ߏ�S�m�|���� ����S�n����{�*�ڹ�\�y <��v���	�O���F`�;�=^a:�t>]��$�k$��9gQ��G�����xg���$|�1�|V�Ȓ2oQe䐒Ro����
_�����"��`ԁf��1��b�}�;�T׳M��;�৵��WMc�ޮ�@�Z��Nc S��\^�g1��
ڮ��j��Jw�����aT�vOS��4z4���ӂ��@y�� �4=`*���v4Wz{}�f��[h�L��6�_z�	.|��yY�^��&x�����tKo7��j��f
��ލsKo�z-�i8p�'+6�~L+��[x�V�[��mGh�Y�?[�V�p#f�ڡ["����נ�@B3�Ȋ�?9H���38sUY��<n�#�7��@$ؚ�a0�JZ�g�H��v"��	�F(֌/����.��.�ʴ��!X7���*�Γ{8�c$U�(��\D��y�HgSA�c�;���΍3ƹ������n�~���n�����̲��Ý[ �o0 �����G) ���2�G+h�>� ���%��k�ْ�- ⣵[��H!��;5�C�w��d@ܩ� ��r@<Fq�h� ���SLX��Slx�����o@|� ����JN�5r�� �!�'@��7Nq��2��+hɨKgHa	�N�Q@��f�W��> �^�g��#�[�o� �Y �Ws@<���� �H��kx����q���?,�x��7�_@�O
��	��d|i.�r���4�*{�B�I� ���WY�F{4/�F/�����<�W��V7��?W#���`\�R|��Wy���d|�{�G��p�*���|lW�wъ�d|�WY�^Ԏ�̭G\n��UZ���M�^�]@��Lk�^�� ��\=�
��J�Lk�K�
Xϣb��,`�xE��U+��0���/`�x�^���w���<����
X�%=�X3��-`�[��5�M�֖�S�B~%j�!~����4.{K��u��Lnk���������Y��|�q���hak����Q���U���r�@;��9b`r��p�L�#�B	���kG�|��A�_����f�z�Ԡ�i�u��(�	m�e���
� '�{��nQ�D��ʂ��(UN�פx�&O�kʝxmv�D��*1稇CbΨ/.�}D��B�$����r����ͿA`��-�Ϳ�n��&7�B��7[��3�6�4>&��E����ty��9R���\ʡ�T��HmL�.�s8�k%q�)�&�kX:�����H	j �Z��ϑsJ}����n��/`x�tB�d,����c�� ��k9 .R;E2����4����]��-�%�T1�� 4��v��1���/3��4�K5�.3�N���}���t���,�;A��1�R��+��|�>�K��[�'�.+%ՋÓ9<y�n�F�VS���p��
=���~�]K<�]��.�}���(Su���_u��ש����ֺ�R��Eܪ;�_O�V�i�zjH�]JGb�4�#\u7�}����� "k�-z�	��V�r��~v&����{a'���gR� r�3۽
&�u��s��I?���%&=a�?�����C�?�>�o�g���?���S:�)}���Xp@�]���)�	x�wpxs�bS$�2iS�VЕx���5�W�'���N�ʦE��%�4�H3�{���
��]Ώ�^nFwh]��T/�{<s3y&?��p�n\�:�\��n�}4�!�����uo�gF9qݏ�E]j��j4��H���ņq]x��nԕ�Ȼݣ�M��q]��#@� U��X��5���S�FM�18��E����QX4*�$��RB%�gѴ�����Hlq���?��d^J�|��d	�&�)n:nH��P�&sE�d�L�#�xU�H�j<j;�BC�D�V ���xU��P���],�_�����^����ʰh�h��DHtf���[�j|r"�Tt��ʃ�5�������B�V�ܴw��5���P���W/4�eN/�)lӤ@K/؎Fxl�i	�&��z!P��Q����x�Q��;M��d��Q��|��^hr�N����"��>�����{G�mм��M���oI6R�U��B����.b��3m��<��R{�O]��7c��� �
R%:n�k�j�b����٣���	g'���1DS8��
�}+�pv;��'4�}"�n��q��څ��t�,	�n��y{�J�$�Y��ڟ�d�|A2Yx-�$+�~Q8��^�V�u^=�J��9��3ɺ��tz}�Ul �a���[{����$�.�8�
���y��i���s�d��y���4�n����n�x����=(�xq��F�����VL�����Wc�D���<.瘝��L����O�v���2��y:,{$y�1�b��1�b��v=�[H1{^0L1lk��={5S���sŞ}�s���4.ۯ�b�<��b����bv�4�M�f�b��>��j���xW3���x<��� pAǣ�9���"�b�<�粠�!J�s-Yo&�&�nWD�g���0�����A���:;���L�ң�vU��#��+�J�k��Py�3,ӄ���2M���(u����s�i�	�n�������C�=�[�aM��ἳANTr����,[L�a�e%��F&�Hm��2Y�WM�I0�9=����i˒�V��)/|r1%�m&Ǖ^BIr���ɥb$��\-�͒�Z���iǒ�z����RJ��&Qr%�(�#��@�j�^�$!νg�9��r�Wj}7�8r���=��A�p�mмl�^�w<�G��w�=ku��3�s5�{�:�b�Y�f�sߞE�b]�ɖ�V�s��~�Ø/���s�Bxs�����������}��UDk�!��U����2+�^*��C����X��m,�ĳz�cc-�[W"ҫ�S*�*���NOG� *30��[l���� *��|3����N�KwJ�{*H��V��:����eeE��8�T�O�:���.7��|=���2j��˞Ξʰ���2��|�6��WO��=�#�~�x=N�h��s�\0�s�ӓΊ�i&#9˩�+2�YA�5�B$����I���Nl$g��ܤ�xnR;`%��Fr2�H�(k��U���3�3&<#9c-Dr�Y��h�۵���IZ��I��&y�556�s���I����&���]��""S��*g �$�������'����LNM�����ւ�)������~j���`���V�&�vz�X����Ф5T�u�4'��zXSik)�z��J��*����c}���mE��Y����773�S`jf�����$���̤�4��D
�4����t�U���A�'��dA��;�0�:���I�q��S�ܙ����v�f��/(�%<�v�]KF=~�����n�q;x�Φ^ˢF���h2��z����h)��X���R%c�q/��m(n�hW�\��J�沓��mdݭ��
o祶N~S�ۿ�&�-{ �T�O:=��`R���$;�_�&]�|]�C"	��G��&����]��B����	I��I�d��{���]�Wo��A�lk�w�y?YN�=�<L�����V��[_4a��t��UV�u�/+*]t��2��1�;�[�-��u�/*��:J�U^��W���U������������[��dn�o��Ғ�%>X˼=�ՇŴ��<�����әv�u6w\a��Tp�o��DK^��jpg������Z*/�0��������s**��4�e������(���3�@=��H�6���i���q�im���ۋM;�"R�ƛv�2Q?�i���U���:��h �KD�a�|��� K4�/���Ӆe��}��C����-��p�a=e��M�`�6P��6,3�5,3�[`�/����d��f�+"�
Oʌ�:����L���V��\�	!IBc�~�f��%5Q����0��hz��1��WR6KuO��L�z��������\��t_��)�$)���O�=��
ي(�S�������������h����1uөGI�9����B[���7١ltaMH���݉�l��no���[����O�S>��[�$���/>�"�)k��D����R>�g���j<mXr��G��8�_���Ѝ��?
���1y3ݺ�t�dn�E1y�"&&bR��/%��H�[v)��@J�	�l�����R��W)y&�!��bR��7#)9F[J�!$%c@J��H*��v��G��������� ?����(?|a`6� ��y?�N��8�&��̰��_6,;|հ��v�+dAǀ��֠h`�ǂ�p�������.;�@�'�*;�I��}]v��[�ʽjC�a#e��ԃ<?���(`��� k����C,ܾ�~�P�������c���Am�B���������?�`���q���p%���pP�l����~�L��u�W�t�a�-�o4,C�ٰq�C�'� �A���R>Z����ѻ��?�y6ԅ�l���@_����3+m.���ꕏ�3+�3�壅�X'��E��壅��G��^�hO�S\7�#Ǳ�(�C��	��Qx����'�SX�``Wb�Y�����p#��Pbm�ʣeoc���p��c��%ѽ%��8���TR�g��}���%�+���N�$I�.��w�i�A��/بɠ}�R�ٚ���ϠK��1:4.�K�A��3דgj0�f�����Z�E�A�`�o���6�8���F}��ڣ$� ��s�.��7+�R��$�n��F^���^�-b��w+��x!\�T��[FV�ٳp!/���ҡ���M��u���,a�2���Q���$_�ԵЬ'u�_�S#�8��*��,��dY�K�&i���d�I��Oi��M.�ɧ҉j�4	�u��u��\�y�e��>�Rx)_�����2��.ӓu9�́���r]Y����%�#yxj*G�}�¯� ~��pෛ~L��5�)�|�����f,�"�H�3��Yn�W�<��u�f9�@s(� �qf�X��F��F�k˫�yP)p),	�J�G���u7�s����Ԩ�t�6����DC"���δ��<Y�o<	@����@���(�/�G�����0<�<5<�<�q�\d�M�#{���?1��M���̭A�*�sYGQ��tn���_N�GmU�'0�(���)`��X�#�Wq���cO�t��o\x�����	��-�Dc�89G�xr��S-��g"�߳F�0�aďQ��,�>��"�-���#�(s���>��|�'��������]M��L����!�4/��3�g ��������s�|��>����N��s�� P�#����g�3��h����+D��#yEE�����D֠8��jI�l���y
��Dm��'��*�lYA=���UP]���>ET�S������[x*��᩠Nk�Â�B�
JJ��(eI���`�`� �O,�[H��AE�@~�*�6J*ACI��i������ˌ����A����`���3�ѥx.:Կ�$I�u\�i��vu��M�Y�h�+�O}F���~{m�;�_�An��#�ƾС��>��ic�m��FZ����SW��ǹ=u�~�EHO��tr$��p��p兩�=Rn��w���g��K���2�Ҝ�6���h�~y��!I�~9��4�d=���e)���A����K3�堶��*l���/?��K��d:�?��L�яi*`�Ҵ_���w5Eu������9Zie�
�_
�_���HZגv����4�-XJ���*7��Nևɺ�����}�~N����bt��A�U��t#���+M��`+����'P�SP��)�-�����>o����[�()#0pv����J���Ғ�E���2G��CCJJq���kq�`����Xwjy;��R�3r���YV�W5R}Īn���`�D~��^�)�_PK�mi��1�;�IYs��"���yd- k!^�z���'G���?�Q9���;D����K����%_�'�W�|�B�_M{���@��\�W���2J��IE�/?����{c���Al���9���#�u����;U�MF�7{����M�ʹD�CҠ�E��%� �9�0~e̜���m^�&�(0~Vw��ǉ���S�KX���eǉ���P/��|ǉf/�+ �wHv�}��x$_6z����^�A���_Z����W�����4�����g<�B�	A��� ��v�A;a.�7�1�[�m^��j˫}�>\N;cvQ�,����u�Es˫�|oь�_�\/�fVz�U{�f,R]ѣ��m�̢i���3>A�1]^��	(����}���J�έ���lp�l_� �_�^P��v 8�cM�,��N/[d; ސw{�"ۂ��u�ZE��5�l�=87:�t`u��b�1�.�;�+m���]Α���>Dv�)>RpNϹ��{�Q��VxJܷ�2�W�N�H�w���m�����I��ڂ��rs�}��?Y�@�5�(~�:�R���(~9<Q�J��U(�M �] ���j���=���~2�MT���'��N��4'.xRnN�"p�~x�����Ӻ��5&3�d���g��`O��o���XH��_Q�S��[3dA��oF����ć(�����`?>�~������!r�~�����"�MO!���.!~)hY����eY�Z�;,�m��-���J^��+�߰|�b���5,w5�a�ۂay��a� �iҐ���n�e��X��Q��/��w�o��ƿK��]ᩎ7����q���������k@0�B�����W �'Yo@0o��a���!��ʍ�����77�o���9>T���k)W��)���5(�* ��E�r�i�p�^��p�&kާYf5���<\i��`�\��gj
֊�<h�N��W�P��t�C<�i�5"�>lsH�{
�5(�浠����+[b�l�9�+�9���Ua��
+�(͗6��}Y�Zٗ[���[ٵ[ ��K���b��5(~��Ɇ8�lȠ��&N��ež"-h�[�F�ڊ�d�F�9��bx����O�F�P4��6�:�� �͑�My�4�{C�� �pUT�� /����ʒ�%���^�]�[Y	=��[���9|E���>҅dFue�؅�%�q��$�_��X�C�lh"0�Tc}H�\e֟��G!_Ѱ5�a9��G2�(qm�l�(�o�&P�(��uaSR��Ϊ,*��N�}ೱ��! ��9�������>���6,�f6,�fY _� ���&��!��(�>��9J�Y]������Y��+ �9 ����<K ���`rÂlJÂ�| �! �� �~ha d!e�#͕�~F��lͲ�)��Z����e�i����F����3����ނ1�}���s5�z�
���=��7����|�V���[9�����VU��Uy�A��Q>�[Y5���ם�e���+-)�h�6���3��|��
�7ح� ��O.dP���̹�Q��]\�Q[h}2U�CT��Q���Q�ZգC�(�@�?T�*0ŷHg��C�֠ P�@���0�}�0�4̒g���qJ%�Shz�����-�����,�jN�|I�����y�6ׯkH��\A� �D���e����9������w~ _�-hz�	�0|j���{��|(�~_я�dt~ ������L]��F���|��&p~�v����m6s~ ��d��Y����W<&��(�+��nP�P"���HL�7��eP�_��k��,�!��{g����Uaٌ�p�M��[���pa|�Y#UpIUw���R_IE��QQTU"���+(�r�ygx���*������Hzo�QI�c��ף�_ �J����Cx�� �'4F�p�:��P���Pu�
P�7����mIu����e%>|���y���r��.m韌㹼r��"�K3�w�n�ϓU��lt1�M� uj�~G��vڵ	��Y���`mB21ʖ�ڄ��l���j�d�T�.������0]>'��2������0��ʫ�o	E�oa�!�@���J& �"�wd��������;
$U�V�i��jȪ�Kr��|l,�U�xFk���r��	���Y�����fU"�-�i)���(�2W;�E�*�6���:IZS߬J9�{-{�4�pV��݊�R1Ϊ�Ff��ˁ��2��aK�1�OΩ�HG������e�����_ȉ+��H����
�皎+���.�7��^,����Q\�\3��r@ ��N����������ǳ|B`�_�l��t��������a7������-l�%]��GK�B6��\�;��	����̠ �?j�c�u߯tw?�V���˂���aYa��e���e� &�q�
�}�[�cR;�`Z�H>��. 8�E�ۺ�@+�a�� �w�t��dA�#�؊��|AL�{��Șn���L�����	H�Na�M:l��2����_��"�u�����w9�q���߷_$����O9�{�#�	�j�� �����_T��&:X �p/t�:�?@B�#�gE�,)�UFb�?u|u�o��dzIi�o��b��9�PC�A�`@u}����B5{P�F����A�������n5c���j�����֓��������4�OS��1�X0�0v ~�I=�BH�"�~s�?}�]�ɋ�h#�R+����%�>�k^��^�t#<��L~�"�A��G�s5��	fI��fΜ�E*��ȿ� y��_r����c�6?�w)��%�6���6�CJ#�����E�N`(�U�C�mmm�Lt�J�cU�@���S@�#,�m$]��������z	����R�][%��.U��W�K'��=q-(�໚��~��z��_��m(���G_n�^oؼ�D�$�6�g6&�u��oF�DةL���LUs����jn�e���jnuaV�̮AMT�7����pV5�?���5��|В�ALė���XT>�QD��.�;�%�w���^��m`E5ӹrbhW�@;�b�
Y�j`(���D%��z`H���ൖw��^��qZ�5#ԥ^���!�@	��ﴀ�����N�P��Vd��E+W��o]I����#k�@�A�u�7��ՉW�/��Q�����h����҆�B1AS�)�@���4��[��5�b�Pb�y��U��b
�L�D噋���u�Ϛ��uB�������4[�d����$��S5z	t�d@TP7��L�vQ+V7�3��P�XiR7FQ�X�N�5��RN���n�O�8�qt��q�U�x������Ⱥ�A�n����E7�fu�Tk�1�bhZx�Ƣ�ԍ�-���@7δ�gYӍ�M��s��"+�q��x��n|���ڏ���r����"2����iFdDC�^���x~Df\8	=�y����x�$�L���D݈��\���9����QnD�-�9� "�Tp�K��G?f�aD�E��;�~DfzR;"Ӷ����Bkl�ۦ1!����C�S{6�]`@'�P�Xֈ�fP@�����D�x�U��]I���%�����!�v�Z�M��E۪�n�[�x�So[���Na�i��MI0qG�����@u6ܭ� ����"�4 ��Ap"EpKI��$
��ᙤ��N�{7N�&�B��n�`t>Z�|6��$T�ϓjP3z�<�Yې5�Ʀ�r��eI/��}�:~>�"��Yŗ)0�l�����4w�'"��8��1��?�Ƙ͔N�\����A�!����&�h�4�&�(Mڛ�I*�I4�^�&gMf��,M��ZG�=��I���i�&�MҤ9�I4�L�&.���#��c���-��j��O��s_�������|���e�z��x����y��}>?W�>O��G��g�}���O��h������c7��G��k��^���ON�i �u�qwG���.(-d\Lޞ�J�@,���9�<2�%��������*!�E�F�E����&�I�H��U>����c$��.�����#)�KE��k$�ϰ4���
K#)��F1��j�I����$鄿�`��}-����"�[_�Ap���!�-��������?Z@�Åk�-�b5�ǂ��-'�������.z�)�V��&����"_X��v�Y[��ڪE�1h2�g�49*@���t�49f�&*�|h�&w��� ��8���	)��سY���k[�J��~-��S��k�&����a��A�<�esVy��W4�������y��*�*�.\�vk^���U~	<sy&�*�;�k�_���a�������Vyg~���	����u��U�e�罠��e�qm���'IGQfH����p1���*�9vԭX��T^�}T�b��Y�b1�X���:����z)YW��z�p�c�=ҡ�lt������W��<���C�3����~��Ћ9�H���C���̔f��y;�m�m_"��v�����p����"�3�"swsڛI�<�9`�x�@��G|�Ye3*�s�e��R��-��,JB#�)��*D����8|u�X I���qs��������=Q�ه�IS{L��GU���w�Xտ�a�J�=AX"���4ˀ�ף�`�&5	Px�L��R�[dB�B�;S
�"��X^�+)�.��2"�0��P���e�> H�끺w�,�n���lN�u�Bs����:��5���|e�.�Fh^v���� ��I�����6뀟���	*�4��캱@��@�a�w'E܍~�m���]�R�y��u��*�,�RO�"~K���M�E$E7bR��#๜D��)�U���	��4��^-�`�����N=�5�<�Ө��6<=�u��y_�8���<�q��g�G�u�|����S@p�� x
�=(�[K��PY��"<��q����
 x+ �Ybը<OnZ� �'��*��E쳆��*�����������G �W���A�X�i����,��	 ��s(���!8�BeVx"xvx"��q<��{ x> �j4\�����r���W�5�uѸ�P�qY7��0q�,���� �|��lss�j�����2��h��-�Η/Ӽ�����ep�mW�����eڮjG��嫸�j.?�7�Á�8�j��1�oΗ��rU�?�/_�Y͜�R���kW�~�T1A���/�ϒjPsD�����vd�$+���~��/��u�&+���&]5]{��c6���fi+���!A�؀~s�+,��-r3���m-PaQ��7w7k^r��H��R��'
근U���i��oA����^*�@�
�!������9�c����PF�b�=
�������d����),-4�M��`������ �����R�F(�9��% 8�E�Q��1.� �E��֎gP��gk�l�֎�����i�m��c��	.F�^r�a9�� ��E�{�����[C�*�'���'��5�?���O���e0���_���
7%�M/�)z�)^r�!�^rJ���{�IG��d���Bۘ����ȁ��	��)�&;94@i�I��4y�M���4i�nQ�$��roů�pf�t&����`��@����6ڎD�ݳ�`�\������ڎL��9�^۽P,:�t����zm��7��^��v�F���k��N�+�k��c;���=\�P`��xv!���~��#��ݟ[ݳMڀ�%��{����{����`��|�a�!��`�G9�4�R�1#fzR��b&���t����<2�p�؟]�"���O�dj��9c5U�\���a�8Q�a�Kf��e�
�r��,��uY�%�md�J�����{�-h>z�9ʒ�U�|xk"m7��A���`�,*4�%BC�!�D��4��#�����#<����c%�od%��Ͳ;��>қhZ��&�Z��eA|� ��p@<���N� IѲ1<A�)<A|W�x��% �L q<���r����L�u;���R�Ϧ ^oģ(Z6�'��vS��8 ���o q<�8��
9�����U ^��h
�+-�xE���������: ^+���7 q�\���rq͏ ��,�/ ���� ��2��Q�\� ^� ^�8 �����8x��C�c�u]Q�y�u��
Փ���P�YO�-Doh�dW��I����|z�$ᙫ�3��$�պ�$�s5�I��֓\��q�I�q�=y��p�I&Џ�Ȱ��E��${�~e	zQ��$�zԓ���y���*�>�ڷ�[��`�ok�(]��%��/g�:�
��J��AL�������C�ڸ��ۚ=>,��f�;\�@Ϩok�D�}[�ϵз5�<þ� n;�ٓ5��~*_�o��=�߷u~�y���׷u��Ln�������Eh�mM�i.��N�c6gX��f�D_��f��mk��b�5��p���5.����g>J������j���s�4�0Aʹ�p5�V3����~DH�E}9��\�1}5���	��3�5�NT�m�1>��Z��u�@�A� E��:(K�CkG�GuP�����:��54�
��ᩃ����nA�V/��]�A9����t����꭫����A9:����|��2��렇��Ӹ̥��R���Anm�
:�e[Om��A��	騭[G�.V=��R��s�����(��M��O	Ⱀ��(�������L�g7�A1 
�g��~/K��`1����h~*�.�>}���#hٰu�,�4�#8
{�.*��/�M9����l�i�i���Oӊ�7��
����q�^SNx^O�����۔_�m|	6�݉�s����1h�i+@I�a3�~�aSN�.�Ɨ~SN�f[��xA�Ɨ-Y*�?��0���3|5�Pc$C���Ԉ@���$n�"q3Hܻ9�d�B��%Fw�&I����[*�5C�*�[��]�Z�H{�K.��xSc�ߨ_�V`у�~A3�w�{C��$��)�-�����D�^d=���)�&�߅T�����d�4����H���ɕFg��ot\������b��E�e{��拏?�|�^�	���&-E�P[F�f�).�t�d2	(_ʡ�LE�f�]��fh,P%�,ź�"�f��y"ki��aay�)�l�3��@�!�f2F��QN�\ �
�&��,C���N��Y�9ɻ��ٔ��&��R$�c�/VBXDI�-��������2�e�t�t����"���Ć�w�q���9�����m�dJ�)κ>��R�>�K��[ٔ�X]VJ��'�rx��D�$x��^���Z�5=$���&[�Z�j��j�j�?��\�>�����Vj˶9���p�f�+#�|%�-�7�6�����R��E�_ۍ~=M���_Oe(8۟� VI�=:���!l�)�jM�"�&ޢǝ�KK�C�\~��]ş���)�_a'�3��~L�?/��g���"���0B�Gaɟ�_�%�7ϟ�_��9�����[���ө��g�� ��璟�ϱ��Ȼ0��)��O^�;8�Y���f��r]��,�bݽ���*QG٦	���m.lS�|A�M3gv��Ҋ�$D�sf�������5�˙]��L���$t�͙]����I誄m�������n@wq�6��s�Lư��nN�l��1��93x�j�x�>�nA�hVs$�R�j�[�1I*B?��hw����.5��hC�8�^�Z��)CYU*J%�|�FF�-𪒵�(�0p�cZh��h�
$�6���ʕ��%�kX������B�V)Ѹ�:�:��,�a��Cy���:�:���b�ܙ�����u�ЯU�%q-��z�淀���c\��s2�Jᥦ�k�@����QZ¬�^���G(����ˎ��@x�p�(M��CY2bC���rM=���3�(�WF&��se�7M׭���ƭh�яq�@�.Z2��]�f�-�20���# �F�����֡G#v�k�,���ՠ�}Ջ��r���[��N춚�n�حt\z�����n;��lV$^cHk���x�mNV��>��u�u(Y�x������d��ܧ��%d��u1YאuY#�d}����{*Y���M�#��d%���u<Y�+��g؈�j�cQ�OZ��M"�Fb�t9�|�B�XWK��e(�;�p�b$�y��E������42/�6K��D�����U%����r/�)����F`�4`���q��Vy�`�%%Bl"a�?�<��A�������c���c
WlӞ��JZ�H9��Hq���W5�T'Nc�P�JC��O=\�<�;]�S�?���z��,*-�^Y�+)/�������u���NEA����%J�7�B,�]��{�=���#2`�ؕ.�ߩ���Y=9�Ԡ��ad��R'�}�Hć�^�����}|<0;��s�ӹ..���J��99�7\��Y�i��p��R�}g�l�p�Q\�������,�E�t4�iŐ������m��_���"m.��`���b����GG���ё���Ӛu����`�֬��g�:O�t&�i�9#{	}�|�@��=��O��h�p�s��e�z������mH=������ZmHO�i�ܸ-�Ⱦ��=JQ�q��cz��]4�C�ғ���=����!��R`pg�,�l?�v�mu��DlXw-�N�dmEV���ܿr5�icm��h�ȠDv�=.�BOe%r��D��!�R*�;Y��˨��O��O���89ۂD�b%�.��J�/�}����,��l�Oc�D�p���.��V[���
J�vl��ul-.Q Q�@>��J��%�(U-buW�m�m�攪����B�x����9�s��
U/�dVQ�63(T�Ţ��U�R
U/�7��^�P5/ͨP�\�P51ŸP��8�����P r�߰�3X���w%l��_I�o3�~��WS�hW�)����ˊ�	JAq�$h	�Xb�>����BQ�kI��_ǅ$>�"�@6�P��dusE뫫��"�Ӧڜ<ڟ�DN�MgҦyM�Ӧ)��N�������K#�_ɍkk��87����/B��y�$F�O8m:��M嗦DML�L����qtw�����Fol��Q��C�~�]h��2�������r���e����3��3����o]��B�\��]#d��G�m-��{1;F@�÷L�V鴖~��;y-�����Nn�f��h���C�w	����n��6[Ӑ�n��r�R/K��f�]j�Sd��K�f��$_�7S��)���l�*�cF�c�d���DDy�C��)Q> �pQ��D�؈(���rT,J�4a[��n�7�v�5ɓ�����PJ+�6��^��1�[TU]�-v���UT�c�x+�E�"GEyI��1�����u,(��.)s�R�(�[^U>���PT9��co �,�,��Y^�AC�_����|����//�.�9fVz�U{�f,��o�*zrf�4@�,v@F����T��|څY#Y��Z��w��d��'���:�xv����}M�9/#˹+�r�^1m݇똶����BӖ��Hдհ���f�ۋY����èc9��i9'���,g�௩�Uj˹��^R��4�Bgi]h��I�夁�Sl���u��Q;�]�1+6Pu����ϤH~�O�k��`4��*�/��Yz������z�vf���%9W1�%�+��r&,ߠ�ܯ�	�M"�T�c4[�+g����5��j�������s��jo`�W���C%,i� mio��}!@Z���!mio����c���X�։l�躧vl��;h�؈��J��4����?���zc��j^�����p���F��c��j�O	����5�/br�����( ��$��T��Je�zz4�d�W,s�ǝ�����������N���9�����ܳ���vp�Q�81�m�}��Z��k�T|�>��.eM��a��c�vjo̲��C	�g�e�]�axwy����0�O��81j�V�1�duc�b�a��c�1�5Y�Y2�UuP�I�Uu��NY��5'A4��sJ7�S������be}ε���}�v����"W�K����n�J?|�\�]�A���u$~2�PGzm��L](I�D�$��(c�#�Dv$|�M��]����a�����2i�i4�!3ٿ��#U�d������1:Ŋ��uDe�cP�1�I��r�1��k]���Pk����6+�������i���Zc�-
�\�ec(ןa�ʥ�4�`̨1��f�9�;�BsBw3�� o��P�T����;<�;�C%��qYG��P��3�����愝P�fc��%ƍ�j��~S�W������Cp�p�f�	�o+��ۤn�[э��L��{���ЍB����q+�ԝa�]��nt=�(�����8Ǫn�>�dtc�O��W��MэY?+�q��~���+��-,uc��a���0�����sO��]�-�F�
s��u�)��ڂnt]cA7N5֍��@7��n�6���&u�Ò�R�}��|A{��Y_�ÒC��I�A�˾�K�gN"��%��ѭ-������V0,9=�KV1��,�ı��C��V��p���!��)�1�,���(ܚR� ^����y�ѓLL�v }b�b������Z��m�ɝj&��uP �����v(�U���b�;��#,c�Yo�eL1�P���޲S�-S��e{�V���z���m�R��炥Uf{���%���*_y��D.(Pj�咂
RR������t�J]w���9F�ߖ:�/r�-����1�
=穴�a>��/�`"Q?����2ܯxYY{J�-��2�b`_��aj�,�R��R����Jz��|��R��Jz��n/Vʐ��Q)ÙڥY��A��֨K����A� ��|B���'���x�dQك�"OY(*k#V?u1e����)��U��&Y�< @��އ(M4I��)M�@�d1��/}k�F��h�?kMJT%G�!�49r=79RV{��4�)ɺG.YĒw�@M����G����Nj�GW� �Z^5���kT�0�S��(��{�l$�>�������ֲe#Yw�e#�$�(@�8$~��x!q������깎�eE��.�Vѹ�C��)��j���͂5$��K����朵'(Sߠ鬕�;~���u�^I��e7�א��ŗi;k7Ԑ�j;k�8ki sP�Y����:�B����$����0jg-�.Z��YW�G��`+�j�/ ���`/X ��A��(����d[���Q��%�Q���^ʚtW�(l��ʡ�S�[z.˄o�4u����o�\��b��f+�7[i�7��L�7{,���rlz�)�}5(`0����|�ȁ�3�N��P�,O/
O_�80������S �ђ�Ԡ�1>�s9�90��=Ka\J`�M`�9
���a�һF�p��eY(��?(��M��'�5��|VQj�]��W3�� �S��ؤ����׼���p�!4\��@�1���"kP�2>d5[�%҂.�S����M�
c��!Y��J��e��:��g��\�7�V{-J�.aƇd�+��KX L69>d�� �Op� <&���ڇ�����1���I�)>^x+�G��"�c��y��ϱa9>$k\xf?�7����	$�Nc�.��D�Ɏ�,�Y�2�P��C^��=�d��W(Eβ��w�1�n �+R_f|H��4��E]���]�([l|ȫ��N�x��(+��x�,OX�'�:��x��>�i�eL�������Yg����7	�'��s��9d|H�����#�+Y��69>� ��Χ�?RF����5<�����.��
�?{�������U�y���R���!Y��]X��M�y���6�]�l�b6�l���n��,�_�dЛ�����������VƇd�2=>$�5r��������+Y���V�i�>D?&�����D��C�V����:>d2z�i[��7ht�H�d�x��%'d���EB��CMqP��(Y�#�d�O֏��YU�/��	�Q�.t:��.d�`
\E����񵌭����R	�2�V^c��y��C��]	'x񎢄��^]����Y�X=��9�6jb7WSy;kd���Q�Γ"M�����kb���M�"��H]�]�^��w$���4��{��ҟ��('-l�ؖ�9��YT[���a���{>ϩ�{W����+����T9<�_��k�yX��8O4hsO������Hs�Tn]_�@��'�}������'ڈ#��F��1��j��7�*v�?��`lp2����#t�?�������a7������滷�wo�ބ��*f�ߕ�Xq��W���Am~���(����g�6����ˤ��L::�6J��y�Bh��@h�N@�ch�|�u�t�/�yp��,�_�EpZ	��G��������(F^	7��UK��k��;Ȧ�5��������S�8>S�����͛�&l�1�R�^\ϸv���];;��r���}H��	Ƶ}/�I~��d�-XW�_���r�ћAx�ŜT����Y�T@��͠�(p���92�*3bd����%eޢ��!%�����U3����%�%�E\�l4�j:g,��Tp?�=����c����R)�W����fV�G������~��$ح����)`*�*�>��u�j�L�
�Ry�+<M���i*<�8�L�&��J��hv�T����l.������hFW����+�0i�k���F��tD�Cz�Ս���3�����t���s5��έ�碷�����&�����`��a��p���ӏy�0�
�]MR?��}�]��Q0��[U<�>b���om1��c��נ�@�Y��ˣ1���/�5}��ј�)�֘�������-[��rUQ���� %�s˸�BJp"_*�&}���1_)�3��AC�erH�2r�{�8�`o#}k���ϊ��A-a�S[|3��#��K�M�x$S�\����n����߱���-����	��

�_'�9�mk��[��=M`p��[��=���wtz7�6�{,l�>�-�� [l�MOԝ�㮬A�ak�d�isz0�y�b0�n�8��S�.��y�_�F����i���Վߕ�_����=,}��o�AiR��E0��]���<8�
6x:�h����QTQQZ"�y(�V��/������J���^�����*�;fTWVz�|��E�����N���V��l���(M��	�V��h�>G�p���5 `?�<G%p� xC��9*و��ks�q�?k�Fq�PVh:�ə��  ?pp]ؔ����*�J��Ss� �nc��0Ja��ߏ
��Y�O
:_4(��4(��Z ������k�OiP����|̓�{&�º���k ���~4_م��LA��%��BQ� ��́�-A���^do[ �f�L#��siH �|I�`.�����e���)����/i���������3����K�f�>i�����G�/��f�x9~s���s� ������y+疔y�ު��*��W2���Q>�[Y5���ם�e���+-)�%_G�_�Q3��|��
�7ح��&B_kP���̹�(�oʮ$��6lU+���Sg��@���(�:�(R�F����HB%������>S�ڀ�nd��D�c�87|'���"0c?�zn�% �QX��c��e\>�D���㚛���]&pnx����N�@}nx�?v^j��İy�j�>C���c�e��'�ܫ�@h�p��R���q��Nj9����צ�U٫�ܝ�],�_�����^����̷Չ�N"$��)u�}h4>�6��Ɨ+*�hV,�k{�~!�kU�3����q��PW�[���l� �v ��X�u8~pGh��D�c�4yV�o�c��?~w�X�} ��|t	C���5�=�d3C����L^M�B��wM���&1X��41�t�D�Y���A�&[X��}�i��C���&cL��Oi2�M��I
Ф+���`:��S![�V���_�ly�n�u�経��Z�-o�!�f�n��$[��.~^Ӗ����2�{Tly|��-�@���-}������l׵坞����1ȣ���T$J	�Q���=[x�[l�k[��X���C�G�o�=^|��Q�}(�~���!�����k�>lz7���鮑\7$IK���أ*/��OHH:�I�^Ϋ� �H����k��'�dmA~C�6�A��E�ud����ɵO�5�o��!rU*Yے�b��s~!�<������t�t�L���*�ɭd���+�z	Y+�:��a	Y��}^#�>���;H�����O��BVr�E��>%[@�d�G�Ed]F֫�zY7�u��O
��:����kwD^n������ �69|����a��k�*����ӑ6�r�'PĜ
���z@�v�:s����p�@�Fh��;SF�b��#(a�����L�l�K�J�d�y@9��m�h+F7�֠������|O.;��.]�6#M>_���po6gcDFR�l�`cd�����K߳��o��ҭp��q�wo��YU7ل���$�㼰�T��Q<@;$��k�4rd��-�!�f�B�$�id���!�6p�id�m�!a܂4S/�ܞ		sQ���h��)�5�}|���fQ��֎堾)E������X����є �i����S�Pj�Q��֚�r/�O3��Q�Ǣ~� i�sPCQ���>�Y�LG��s��~(�P���p?�y�y�_i��������~�����}� �D��a��os/µ�ٶ6�&糛|��&Ws69�n�@���=��aE��+��m�����-�m���Ձh����S���*���m	T��L��DJ�j�-�w1��=�&�ꦢm��ݽ�rj������2���A/��9`n��A�ބb	�a�#���@�)RsҾQ,g�rҾ�y�d�Q�w�6#�2N� �I>A��uraK;y���{���-�=N�{|����ft�4��Ѱ{���/���'k�0{<@6��;=g��� ������t[:�F��!a��t��{X�$ �g�O@��3N@����s�����S�d!ՇR�mZe!uGH5� s*�T�9cVH�kX!eX��#����4����@%�&ɞUGy�Og��<�=����-�O>{܊n����v�/���GK�����B
Ep���a�n���r��%ԚZ�&��G����<6x�J� ��Or�&w�.!^H!K�y8u�>�m)=JMң��\��N����L�f#_h{	�f�ik�Rj���q�V���~.jvz�2�s}.��g8�k�����!�R�����u\�v-�h7E�v-�3�\ߐ(��$�m�d�<�!IGJ��M��AIr���b$i	�=Ē�gx��3�%ɯ$��!I:%I`��o�]?�dP��n�$�2I^ң�Y�'��>&j��[��N� �p�#P�����IѰ����	ES��~�?,]WmX�.�q\d��X��r1�u��ݑ�����d}YΠ����p �j���nB�Y�[��nB�+݄ܱ�	�㈱7�i&�N�o5r'��fB�k��	�D ��Q����w��V����rh7���ϲQ+=p������ښ�L��MX�w۰��v���[�7	�VĮ�F�V �.ǭ;9=�X w273�p�5 w�Hq�'����ٍ�� �Y ����� �G����V��U��^��F������b�����`�']������TOvS>�%��G���`�$��:B�7�Z@�>^+��'�ۙ��#�o�zdܳP��$��li�Y9��=��	~>��#�.�	�R-�#�º�LU4w9���G9����%4�j�1vH�V�瓃Q��	����z�x�g�D=$@�����D}ˈ��#�KԞ��o7��o�;,E��3��(:(:��ͬSB�z�T	}d/��dn��eSc�]�X��z���o�k��a3xc����,x�&cX�vy�����[�5<D���~�K�]�iO������~�J��S�cs���\���FL+��2�d��l� ���ΫA��>1�@�ـ�tz&���r��f��94�s���n.��LK��5-,�WQ��?���ퟘ��g�O�ŘS�t]3e�)��ƳRW����|�������>^���n�Eڇ�5�A�P�J�����P ߷��)�%Ꜯ�B.<�rzƱB�s�G��MAH�@q�T��*kNZ>���R��)��7��Z`AH}% ���?ي1��2�.��%�N�X��	 x&���[p!E�������� �� �q���s�W��β��j�+��c-� ^}*���ts�4��]۰Zv�-{R�r�-{�|\���ǣ�=�^�rj=g���=^**�Π���A+�\74h��F�SQS�C��V��W�g��nw��(��t��/h���#��L*��O}�1<�Ѧ��GwY�z�W�S룱~�=5���s6��f�R�D�s0�/p_��v�%J�)A�P��V�p=[�t�vzF���-P��(=0I���Z���>��T ��P{�s���t �+��)h��)�.�)�(0���8)�A4p�U(r���5����:�&Bܡ��Z�)�^?yq �ُ�<��v�������$���hV_�A��X
�h�nE�f���u�#�3���*�&4J�~"��A�d|��Ƀ�A%�}+K��4mɡ�PJ���zD�v4�h���(EGrL�
Q�6 j0R$}�M	!l?��ط�� @�vE	;0 �����HۍC�ᔴ���E�Z ]���f�7�fA�u�f�i%\|�f����fJ�h��e�4S<�A���f:]7͔����f�#�fJEk�#o��ke�q'�4�~L/�<��f���~��%�Z3�sW=�Lp#u���q�YD�-������o� <���D���!Ѹ�M��r�y�b���C#N��~==J���#x$���\<�*� ����s?c44X�����Э�W�1�Rp���9�n�^����^�gL��u,�yg�W.ʮrL�[e���UT�c�x+%U��3����/r�f��-�.��pD��}s�7�cݹ�ui���� 9�>������ס.�U���(YΜ�x�c�G�F���	��;zq���T(;�x㬳�<�3�;��AA�����7��^��� N����_;�s=q�t�}�&[_�#�g�����Y�A��CVYS(��`M͟P< ���A#l�YS3E65w��/U@{r��8��ӈ�NK�_�+w���1�
�?~��[a��/�(��tc�7�2oaly�4�C��E�$��-ϝ,��H܆C�	���AkHlD�.�N�4Mh,�3Q�FBIү����&۞���� �iҞCIIH�����^�5"��'Q7i�F	ҷ��44�_���9�\E\���i�� {�V˘̭�۠��7:�y�?z6��ٛ<�o��ԵA��6h�_�Ž^\�g����������W�W!��yԾ�IO�'`��l�dJ���x�F�������ܽ�%������]k
�����d�ğu���/C����"��	E��;���qM���
lM5!�%���C���[ �<�<S�_�"� ?W��zO}P��Y��Cn@�xO�S�~��\�[\@��?]�>G���H����a��+7fA���cP-�aO���"ԉ�_c���� <k�A��:h.��>�t����u��੔Jo����k3��ts�y�?wKJT�c�?��+���r���I�U>ǂ��RGi��XRU2����1�[�,���e'�U��s��_VQZ2��le��*J��c�-8@�����̢R��GY���`��@��n��������p�4h$(;r}9�\�b���g�����٦��Y۾���ۋ9r�;������Y۞�'��]�> ;����@�k;���S�~�$CϠy�C;�]�F8���9����\xZ  �0����݇�S�?]�z���{)4�1@�X��4���c�������"�Y#�O�F~W���ixK��Ņs�C,��$6_� &E��&�?�R�%�w�'��JHb3���%bj�Jl��˭AI�ù@<��y�%7E�"�eq�7����x݀x�8�+��+�#^6|�ZM�Eo�K<�$�'�x�p�?U�)��s��1��v��"���O�=�d�s9��TQf�g�/�8��E�B�(2�6��`6�	�2�!�|�^AV2* �&���;"��m�N�uM��&���	�ν��p�d�t�p00�J�[�t�±�aU�L�U��n�Ͻ-,+}ro�m��X�>�w4J�O�F�>�i˻h�J��h���u�| �:�|G/ ��,�� ���R_c�e-׆'��O_�8 ^o��~/�l+����@��e ^�q9�r� ��hY� ^� ^�8 ���� �g���z��x�|�%@�$� �ϣ ^dĕ-�'�/	O_�8 ���Nb�� �g�<! �#d�?łx� ��怸����2�}-��	����q�h ���� �1�>���9���rV��fh�������S1?g�.\��h^���Y-�g�&���Y͔�#"p�:g�?W+g5K0gu)>��Y=��Q&�g�c�጑���L1Έ�E3�t�~�i	zQ3��R=ƓÍ��(��֣_p�i����O���W��=�NϪ:L ��4��] �38ru��p[�l	�G�7�K
�:L4E)�rbi["mR�6P�
�C=xh_;��#�Qb=Ě�k#��5�P	$�z-��z-V	 ��x8z(�A]��p��;D�u��@o@����]A��$ŷ���?K}Y
t ����+�o0��7���� ���*5Z��#���2&(.Sѧ�T����n$VT\������4.;�Dq�K�g.^�;�5՜r�݄S��'�E�5(�i��P$�}I�@����4)�.�몃79�x$�H{���G��\s~3�@��w'��w��9��\s�b���Ƒ�Y�i��A���Gx&�N�ޣ��ٔ-Y]�Qϳ%�Ls��?����w15(�Z;�	$��Ӛ��g)sOkk������7�D���6������vr��~�EpO� �R����|��#<���hw���~9 ��\R��"m�{/�����#�2hq	G�\NM�Dc�2ɟ��ϕ�f�^ʃ�����J>�}������R����jd@,s��v�gl=<�������U��{���3����7r|I�7zBeIQ٬Ro��y�E�^��!��M�"���
�-��[c��!ع-e�0��z��n�i%�!8�mڠ�ڮ����Wĕ���A�)`�w�m�8E�vsa����"4�
�����H~X����.Ѽ��^Xa8<s
y�FX!J��а�p�\��BS���(��V�τrj���_�ȉe����cV�w�
+��+�C˴�X\��܈+����t/�c�ix��(N��ixX�i��#ٗS��!��q�i�y�������4�<�u$���g�!�K�i(��z�Q9_�tV(j"���\�B|�"-s�4��]!����o
�8���br地`rMh�r&JG��-��
vt ���B@��,��g�iXE��5_I��qx"���D𧍃��, x� ���gb�"T�kj��UV�p�J�9��jy]�i�9$;�/y�=^ay�m�N��K�Xs�*NCλ�4��lq8,���#a�4�g�i8ӬӀ�����<�!�sN����k�i��g�i��o�i�yѴӐ�E��5�ixҤ�p5���,9O[p:Yqp��8��0şsWj���uV�n18�%�A'���5���-�69��Z*O�	��n9[�w��f~�[�v�s�c?�Lw˹�5�-g�)���\x�[�M��F�V=�L�kP*���o�ls�����r��:ls5Cn����rϭa�ȹ-,� 9�7�I�����I�&���䮠C�4YW�� �o�V	���Ҏ#偈���/�ѻ�n�5���0n��̏�!�H
ܢ�es�ʤ8�ۅ/Oǫ�]q7W�u��+���z�;B��j<�=�М�i0
�A����^��%��]�%���w�н��{]x�j��'wD}p�
�O���}� �'s�m*��:B����^���>W�����������M�5���%6:�HH/!Y ��E�
�!MA��P�=�� �`}�VT�I����ܹ�wfnf���O�2��﹧�#���`!��_�o55F�����ϡ���y3D�ɚ
��
0nڂ�{ڟ�0�'�����BGJ�8}��b�Ŕ����9=K���d��؇�S����#�Z��[Tw�gUL��Tv�F����X��ΠϺE)�|�V��(��x����(��Z�r]NN�۬W����U�*�����B'��;�Lq0^
t���F��L5�A�D�u��2�F�����	�MLmd^Bfig]g��9�C!�:�
����o8��үBj$I;��#��c�h�f�l�06�O�J��y�B�B�}���_�24���J�e@�i��sx;�ty�0v W`��\����<����p�����"��e���Ix+Aՠ�Y�a�8�!/�_hho4��jk����ZqN�n�΀�SR���8��΀��΀���3 ��i���R�s�j��oOd�ez�=!��k%i������dg@- �+MCWq�F��ׄ���vwh�3pJ���;q����_�9����	&Y��ؠ�C�\�ɟɉɟ�l�g}�?������fg�?_���%�����?�T�"�Oم�(�"�GK���|X�)���)byq����:�6>��:��!�l��y�Z j����uP��b�E/��Y�@-�A�u/[k�Z�|?���<bX�X~�N �����@����eh-3 �1?�r�b�_�'���1+LP�.z(����[��@�<�����֣�� T�=�'�G���iU�����]�=�.��ţd��r�,Ί�,Rd���2�v��x��k^K�:�8.ڳ:U�@k�����"%i��S��z���t�&�B1|�Z�y�N=�<7rD�
6�c��ܔ`�6��v"m�[�"mE���3_�(����'Ҧ����/�湍����Β�w	�{�,���yqg\�-�>
�^h�4Vӑ6O������͇-���ae�4_et.|\ղ=3����ɜ�7"0n[���^��xfV�(MϬj��m}�f�\�m�~����`�{	1c=J��^�Fo�&IӣI1��lp��~��@Q�npq�np������Z���e1<^qb�����|9�_g0�F����b囈�����iO^B��=�5+�m������+��Bm��n7O@�4�"؉n�}���'��6�b��f��������R݆YO��WT�/h�m�?+I�3�$I���N���6��EZ�L�˔��/m��03�Bpx�c�v[�������Q�����������������!�a�Pf��Zl%��rM��V��!7�	G�mT��,Y�N�"w�_������%S�sCI}
�Mr�J����

³Ŝi�<�Q�����4�{K׎=+���w􈎰�2A*��WL��8�y8�k�t��U�=I�?̜�Ӛɷ:*_B�+�	��i�*tVFn_�u�>h�r�
w0ڧ�n]�������&���-1U��Q.��*\ʡ*���FT�X�jk�է��w����Ck�]7i'�wu\��ӽ�k�]]�܎���]�zW������9��ס�L��״w5Ӣw��1nS�*���w�c�]݀�{Wϭ�w�Yt����:
�d�]R�iR�����i�f��Ak]�8L�%Zk3���������	�Hj�Γvi�ڃZ�T�ҁ&��jI��Z�@�AE��PK�'PN�RK�(j� ���{�ՒzFjI�5�e�%;���ϡ�$���CϱԒ�Z�@�^��wD�^9b��I��pAIq�^#��R���qU�o)�3YiI��������Ҡ�ʏRGC%���G�1Vf���Ep/��PO�i&���̤r*3��5ʌ� ��t��ޓM֔�3$��)Rr���t��B���佤�|�<��iI�X��+^���hF�:�y	���C��5sz��U��x6��
����]����.�O���~,����x�zʸ6�'}�H��xYe�/x�n�'�{~8ǒ����a�O�]�͊��W��˿Y�s��h`����]����_��r�bVO��:�T�(~�M=�>��j�)�6��uP)�:��}U����Ԧ�?PM=���z��bHܖ�M=��	���������/���a��4�tk�G�z����o���ڰ�g޺M=���4�LA���9�߲�z6�>O粣FM=��d6�t���ӉڣL�m�|�j��<*[�}���Z��WE�&�΀3a ��O�l�X�����Q�gQ6�K�����Ճ�)� �zcB�Fq~B�FqA��F�6��C���*��_h�8�C6ng��7�l�Qe�ϴlaO6�E0421e�؄���8�Q�`_6��m�Fq�-�(X��ⵖd�XhC6��l���沱�O��-�[��m��5�����7A�?��E��9:��l�h_�pڮ{Y#�h_x��L�T�e��+?W�?*���^��a�GOP���&Ɇ���_�a��1i�M���.z�Ѵ��n��h��4yCڟ��h�h:���'�)%�L�x�
8��8�>���z8m/7e1�+2��LȀ�[����p8�8 L����:A@X'ur��؛�AU4.)G-ԿQ���[��r�zC�at\{���7qĵwH#��Mje�JL��Џ���o��_��L�ື����7iw\��$�n�,���:h��Fs6�_V���dq���J����m�'<�~|�M3���ZZ�1|�T4V�Z�~�@��xl�Rn��q���ں��X,���ʝ���mJ��+��l�Ҝ]��L�'�lk
��/H��Sx��Ε�d=�%����8W"��(�Խ,��Qx�+��3�
O��~o��
O�;���J��0�j�S�9I��aw���ԏi�g����4�i�<�Z�B_�C)<����$ ��*T��%��tD:�O�X[m����&d� �q*���Ss�ʈC����s��+��b�P�F�Pg�����bym�;E�q�hwwym3I^��&���u��W�5�o����zq���z���Z�׻����7#I�k:^E�v���x��}��5
��x-Q#h�:��C�#72ySj���q���9,�J�`�t����壌�����
Y<,����*�qXr���h����C72�W������赊�`�tYq|�ňaS��оb�`q0�.	�+K�����bA��75�xJ0_�4[O���[TRV��yS�pAQ~'���˂�y�5Wx��ε�k�v�N���n�5�� \v��a�jXdM��ΔKJ�*z2�0>T�������b��!����������U4d]kVѐ�[ѐ<ɼ�A�m�
>��!k�R�26�t[�1&�+�]�{�l�X��.9`m�L���v�AUy�gh7�"�}Vw�ڍ��<Ʈ:�L0��WC����vc#��5�ɏ0��wP��sҡ��T�r��O�6�iHB�A-a`�#��!õ1�����ޒ��c��	����IH�Gְ��c9ܺ�#y�9������ǠF�#2��u9��.ß5��E5���6Ԩ\j�p
Ԩ��\Z��S���s�.Ѽ}	o?Ơ�a�ۗ*���1s_m��?%,tYbrv����5��=mp�j7hY_
�c8�|�$�q���.�ˬ��g���Z���	E.����8{9e��Kyز:���IM�N=l_�x-��KB����G�/�&���i�TN�a@�J��c��8N���i�y&4����ŝ��'1ϻsj�kg�K�8�VH˄�(;漃+k�%��[r�w�>Bx��E���P��ޮ���[��w}4�=�"+����D4M[c}C �D?�����,����0�%����Z�i�r�6E7�=U �q�˽��u('y�����U_,�,Oc��N����x��_��w���.��x��K������{i�`�S�����^�GɉS���9q�Z?q\�\'N�f��ʷ�Y�68����]tK�J������D@��K�8�g��z�����5�K�i]�p��Q��0�%ѽ���^�N'�������W;�E��A�H�6�>ƞDY��P��_+ϫ�..���Y1������0��3�K�'���%d�������8n�Wo�{ܧęD��	���������u6��[�+U����Pi�4v"��σ{i�L��3ğ�F�81�����C6���4~��������ρ��A ��?A�xWمZ�N����O��=�:ů]���)^��)� U"3�/�/k�}��/x�Z/����zi޲�K;��^ځ�jr_e�����J����i��h���3Õ����Ǽd��v`��^ڮ�9��O7���vH��P6�fȯ�+G��F�h���<.�K.��d���.�JL���&��Jg�Q �G�{ԥ:��4l���R��+%��"�7s5\���Ŭ�&F�T�"�7\��((v�	EfM՚��+���j�S-��V��j�U���~�N$r��G�}�$�z�ڠ����6m�K`e9�{2��>�Z=ȑ�ԓq|�"{����� [���.�2[)�O�����H�R`��l%��C#�Qε� ��pq�C�r� �ۀ>��p84b��/�|,����4��2���|}�Bh;Z$Ҹ��&%�!Ф!M�I(_'����d�E�T�̷A��9M��ZtY'��N��(N�1@�T���f5���i7�ă]�.#ҏ����@^�ă�6���pă�H˄P[m<xL���r�p<pܘ�q-�8�9�.��xm��Ӌ�y�� I3�:��o�:hf��t��Y��{�=۸������ZծӋ�ޛeE=�/��N��p�� ���N��Djc؋���g�
��	���F�˩��)O{G�Ê4Xw�G��o1l屮���F�+IA��\FГp��0�\.�򿆍 1�GhR�`�89TR��46��+�s���%�a3�hE|�iW�zh�_�7�6V�4T�&����H�mnVhM#/�M`�&�����J��:���ev���R�nM�����0~�U��r*������u���Gbo�W��1~��ݼX����y�Y�u�}�5����f��?���M������h5��]������j�`��j�`�aܕc���OSZn���V9J��M�|�n�~��@�|�8����~���w���� d��ͩ��t���_�u��# {�ށ__=�����f��)��pA����|�����iE�������ψ��^��
��czt*��{��`^8��WG�]���i�y�И��_�"RpEJG;"%4�A�T�u����N7���f
ȧ �O2�ؐ��j�.��Q�DC�L��u9^o��Z�n��3j��;��X�-=�
Ї�DC�3�D�<�ؖt��M4t����yΉ���i#�@q�L0�����������e��_�g(�W��DCWk�-�A��k�
($���|<��ي�G6[�h�jL����\M�I��z�mի�l����3�h�Ra��Ji�f��*��Ae�[��������d�����4��U�?^����;8&��NEw�l�r%�p2�nZ��om����ݕU���͉�{�ӷ�'��k܃��媾�ͮq�jķ�^mݷ��?s߲�W�#A�e�U}��[�d�)����[�h�J'���c�ۂ`da��n�l�nwZ�h�KQ��Ҫ�=�0��^����#�D�#�6��h}���^�E�v�-N4t�$�w��a��j( ֝
�!Ү�y󉆮VD��hk���5q5�tf'ƞm��}�h�������j����4\U�P�hx����A[lm���-٭��M4���F&OKLUazb�
��QJm�
��Pv ~��������γ������[�h�Z�h�lg��{�剆��&:3���YO1�q�M|���,�1c�L4t��>�0��*t��;�p��LHB��W≆NM���M4t�M�ց���]��P��g=%m䫒��	�
���Uwwy�a;�Z��9���ɑ�A72�lu1wy�i�;!s��}2��ݷf�L?BfG����+d��.�Q��Y�Z z;Њ��'s�)C�>���\1��ј��y�^��3��4�����\Z?EZT���F�Q���F
#�L�]��L��������i�
��b�D��
f�fŒ�b0�����pnhJ0,��ye�P�8,��-,z�ܝ�N�@�v�����O9��	?e;rr\�99�}����s	+t�����j�Q��z�Ҥ��>��륧*�C�����J��(
�� ����ppJ(��J�;3����\|9�7���|�l��<�o���W���� ���w=j�6J�8
��Q�u�7�_Y{Z7�f�E@�(d��x����-�˷@ߢ#���2�,�Rf��(N�Q UN=X����땢VC�.�nD�#��X۶�5��K�?M׶}�F�t��fl�V�*ꄞԽ���m���	?SǶM����Z۶��\=۶�m+�m�m�8�=F��[��R�]��PQAqPK���q���bɌ`�tjII�#f��`8\XP<E�N��܄=��8*)TO�j�u�09�6@�c��BTVq�!ٕ�q�I��D�5�D�bڞn����>��H�K�:���{�*�U�^�}�]}-D�Ld���9ϧ���7-�9w��7����˩��O�9�yP�_�z|�w����*H���Ԝ�̯Ȝ��?@�M���H�r����Y���*G2OY����R�Hf��9��U�8󯄜�y:!g�fV��,��J�\�F؝�Z|�f�f�6���Y�3���ruv�f�f~kk�����ф��i��	��,��c�g�f�?�5�G�\3�5�5�gk�\3�4�5�Y���Z��t��,W)�r�t�ԓ�s���s�΄𭏞s�͢l��F1ۢl�@���v眻.T9U�,1e��Ĕ��֌l�̆l�jW6N>�s��X�s���F��*����T{�1�@;-!e��,!e��nc�y��s�igι�ޜ�s,�9?�ڜ�v�w�!�3���'A6�K���2���Y�H}�����9���Ҝ�v�gTX�s�Qioι3�r�d�5�ԙs|��v�g|a}�yRs΅$�]*P�2�'�J�N�)/x��朻.Qի���2�/UO��M	����9!��2��H�@Ƴ�3��9�� �M�j3ևT�����v�����nG��T�"c��nG��"K���8O�w;zN��1ݎ�L*Gm��Ñ�����i�G9�VF�����m$���=UĶ�Wk�Q�+��بm��b#�Q��vY��&��F���k\�{ut�f�A�I�^���|&�Y�VO��}2ū��	���zm���q�Ƒ�5HZ&LAg��Z..G�����1���o30�G=�[%[L��R������/q�, J6K�sk���@�n4M�< I?�&-϶H��U����Mv�Ѥ��,t�rJ���_���<=��Q����Z�J��i99�Å�:��^���r��Յ�[�ZN�(gel���|���^A,��t��rr�r4���<��)�5��0��+�D`F��P���13�I~=éy�!��&���sm8�r���1���:9��)�As�=��I�y~��ՓGA: l�Mų�e<٩��x������t_��r�OL<��/�n G��!����$�^�����	^%y��ȷ�!^��qQS�:�s�z����⻤/�����@g&��7��4����d_M���h.[$	�:�hms@�S�W�r�[�NNqp(أh�e��Q�������&k�7�J���E
3K���"Mq���>^��XMV��>&"��#��j�~@��h�>��#s��8�w���������T	U��W�\%osF$W&0/�+65���¡�"�Gqn��낡v�����`(X�głR9�EN���W��B�/��ה�������o�{�E%e�a�(v
�ErZ���,�˕a�5�X9�F6h<�#�F��GU4���1�Ͱg�*o�F������dQ�y�m���f�M&6MW0<�Xλ�lb�t�7dݞϦ	�bf��ӷiV��4 �W��s͐�iJ~��(ȧGA~-F[�d�89�^�-�,(,'ɹ[�ey��Ra�a�ⲢI�P��p���� �)�NS�-.	��YyA9gT���i2�=�#�F��&)��Q*
3�<X��a�T�1)�CG�))�Ѣ_V�b��P��&)�Y���Ya�I
j�!����0�,��>+,6OA�+l�gCRcU��<٣hAz���a�\\k�FRh�X�g��,G�T^��-�3�+ȯ�q퓍Qڗ�1\�}EF�p�%}]y�	}ݢ�W}�G�+���K����k'�v��%x��!x���NW�._fϵ��K�Bֻ�GR�t�l I��=��t���Ä������*E�a�Ѥ��Q���,��2����I�Y�Q�����/�*a�ʭ�0ꀿ��c�ʩٞ���a9�c��&S� '�x%R1��i�]#U�R`BB:�r�f �߬���l��fӾ.f�����2������hfT~E�΂�o�9ٓ�K��ad�k��3�Q���X�F�5�J�k �^�v�ko�"3�ә�J�St���u�®s<�chvb®�a�w1�u4aס��W��pǎ!����;,19vx�LZ�ڌ]G�kGl�	��:����
���s=�ci����B������q,�'&��'&���_iÓ8ڴ�n���`��bR��d��1|�of`x<��Ŷ1<�����𥉉��j��m`x���ڀ�E1�*}5��d��1����O$��p.KVbb����p��p��Y0|-��p��Y�O������$��slc8���]bb����p����y60����V�; �{ъ�cG����'��J7�y����;��5;�y\x�~E}��Q�,x���:���&0ϒ��7�8����\Z��\�+?�v&wE��0|�|K��ex���M���7�7����0|<94��p#��[�	-A���˱�5�y�����&�j{�goƩ:Y=���S
k<ٓ������)���+!�j�����_a���_I�հ������ɹ�u5���F��<����ð�&mݺ��S���	�J��] Hb���}��e.�����g2�j��u5��V�?t�j�m8jNUGӥ/5���
��O�5�Y�$>��XĈ|LU�e�G��Zw��ܝ!�
������I�� ҝ��"^�#��mt��r���o�S�ba��;]�)��#qt��h�9��+7�R�ݮT��FT�V���䄍�ɓ圍ؼ��$#��b���Lڈ��=h #�|��&�(]�z_�*��qA�KV�K�b�~F̺PU��?�Ĭ����r����I��oȺ=�H�_�b�=�c�)�1k���\!]�� +�7�n��������_�A~A� Ǩ��
�Rh��8���dA�����&���&+��h�
�q6Y�&=�䢞#:��K�9r��YZ�\FZY1��#�ie����<���!��?�l���Sie�����K���>��Gڴ2��G���=���'nɠp	��!LaW�La3��g�t!�a ��^���&TU���j�Z�P�=,��Д�������N(�S�~=�,@N�I�A3
cP8D(��F@�!'y�y;�~��ء���١��|[��
����U����v��W���cd���̀V���C�{[���su�P�^�Bڦ`�u����ۡS�Ż>Q�b�0�/w��լ�9^Qtr�:�5 c�)�5*��{��-�.D�t�(���E1�D$#���h�'��ҕ¼X�>e�3�����1x�r]>��pa��u.{��@+��\���Z��qV�_�~d��n|a����~G��������n1��c�!p#_���?�\���脮��/��j�7� M����ǥC�am����ɓ}-m���ʺ�q>�&���Uc�Ӿ�x4V&�O��@B���ֈOۿ�zJ�cG\&�](��q�B%.s�'��� f�e� �e/�p.A�݉	�{���TZ��o���h�pH�Ŏ��o��R�#���J��Rtu�[�*��CH�.��t.�o�R����3uT�F1�r}��V>�B8��T�"ڪcz��\O��:����*���Ø�p������)�n�UR1����G��J0��_�]
%4��59M�:��o ZW��ʅ	���W.8�sU.H��>��E�A<�qJ�-Q���?�s_���d2�\VX,�J��rQ�x6��(�;[K�%K�ɾ[w�DO�4Z�M4�����b�no"[�k����2���f"+'ѽ�}����[���g����E;V}��&�������s�n�f��>��[�u�[�9@�^4�.��Q��Q��Q�N�!:�"D����
D�^m����^���^����b���>�Jc �GQ�D!�7
�rTkh_�_�8���e�%JH��T̛�[<%��X��M���18I�'��[}wV���~�	Ї��1�~zC�+�����	Я�
�Y�G� H��ZǏ�T���U�Ǐ8&q�~6����Ut���x�'��֠m%�j?724���-jP�Az�Р��ҠPФ��������d�i��"SHª�����B�$�(��l�d I����,[i���K=�e4I|$�� �݄$~�$���$`�$=�H�H򵴝&����dϠIҎ�$k$����\�$YLH��I.�#�S@���:Ir*|K�Qm�Wz��������S8>��W�G�߂T �b�͔�Rt ��S�ҹ��sr�'{&-�Z����1���#�u�7��|ܚ�ȴ]˟��8��$�?�mb��2j�!,�9�F�L'��N�i��.;Ӣ�(�����<ٳh���'�x6�K	�ٛ��� �qb�Ib�i� 8͆7�l ��;�� ')Φە<�ٴ�8��7-�,!�:XN@�=��all!���� �S����E�6g�W�B��_�9��
��ih=m
�N)ޚ;<��Q���/a�㐺_�_��H�׈��&kW�g��"!�*_eBU>�f�*dC���\�1�����xtz���0���E?@`|�L��A��_ƿ%&���Nڀ����	t���<`�ȝ�ݓ=�F�7H��@�*��o��W�ML$��H>V3H>n�H�	H�@>C	��+|���>��&��c����.�G-�,F�C�;�6r�T�mI�+|���]�~D���^���rAoT-DK4��)^�;#1��јA������P���p��`�ޟ�;)� �Q������	}�ar|�5�~�O�u��j�x�����{�{E�x�&�@)��*ބ����w}�QSPN	�V	}g�>
����g<$D��X���o{�p|P���W���o{_r`o`�͈��k�#��(�^�b�[<f��f�Zc7�{�c�{�`�M[[G@�V�b�����n;���>��Q��w0�2����z�y�����c���r}/�������^��Q5CSx�?��z�{ɰ����\��C�.�j�tY�ǦFYyK��G��[�R�:��PQA�ܤ�tZIqig!tKfC�SKJ�1[��a���r�8�WNT�+)�J
UN��[�(�7Ra:�6Ó@�1�I���]���[TD�K��=evQEk���D)�R��|p#MQ�j��w�0Π�e���1�z�M�2�&����k�L�.܉6�^�ֈ�6�3w�g�0�#�=V6���c�G9��I�=v��Ht3���X���F�r��8�}���MV�]�*�|�G������c&X�?���6i��̫�Rỳ�j�BS�n� :k��j �w�� �FB�{�Ѕ�|��G�*HZOw/,Gu#��}7�:u/h>���d4�<ͼ3�Q7%f�����č� 1�;�&�w続���n=2(,��g��!Of�!�|+�.�W�1���o&�FIt9Z@�K�M��/i�P(!�%�J�_�/l�_����r�,s�}�m�!�]gm�o��9侹6���Y�C.���/�)	$�Z�(G��R-΄��oo��Z�E���H�|{Rm�`bJ�ɉ)զԌT�jC��ڕj��w�Rm �T{�!ն�6H�j��Rm�=�����Ĕj�sjD��!Ն��6ҎTeO���(��X�jc�H�q6��T�����N�X_/k����{�9N���=�Q]Hv!�=p���e}�
��g����q��3,��G~����r�q"�"�-�aB/1'iW��P��'���c.6���w�u�]b\	�B�\߉w9�O��vH,j�:	[uY�ig���h��)tC0=C�Y�`��L�UL3bӳD�9S׿W0y8SU_˦`:ǆ`jW%�$w��&��w��`�/����)n���l��QG05ʕu$L'�`VG��s��@0	��g��O_05A텧�	�z�)�\0�(W��1Mi�ͫi(g�e�{輚F�y5.��������t��Ռ���h��q���n�%��9S\ү�uj+A�牥���դ�A֡n�n�]>u���!�N;#w�_�k!�z/oЧ�������T,7�[X0)����۫O�h����umO�N=E��1�&�N@u�����Zʌ&i��i���{Ť�͢�L�{��Dh{P�������'!�Rt���0-�v��(�Z��2�6��~%C��H��u,��_"lV׆��ߐP$i���86Ux%`o��b�&�Q�M�� p�4�G�d;�&m��[ȫ��4r�;��m��!)N��B� E�kϒWU��c���%��ae���J�vr�[+f�q8���a�V`�|2�+<H��,�r�A�:b��U�m'�ok����4#��~��L�7��	��MR���w�>�)�8�a���r���"G��r��/��W�*[�"Z���/�,�t����&j�r������UU�~���K)5���=��k*#x�HH���˄��x���{�ˣ)M�5�[�bM8�`���2��G�a�uB����m`�{�>�,����+1>�l}�ȫt�7���O�g��Ec��>���6���l����F��sٯ�&�g�P}&�4��b��g�F�tM#`$T��4���ME4�p�{p!���r�*��rʓ�e6L�(�ӣ�,b�C���&�}���:���V�U�P-��j-W�~X�����Uѧ�Y�B:lqG9sѡ1��rTgA�=���vxe{��Fpo�8�A�2��7�!Q�S\�-.I�J������u^���[j���Ƙ&�R�	�ji��&�r��E敻��'������Y�[D�{�S����d��X���|�_�!4]@3��J	�
���T���C>���\]v�~9�w�eaN�F�c�����pF��܊�a���a��kً0{_�-v� s�3d��n=Ȍ��2�w�Ǜ$�&�����"����fA�C�Y����P�9��c'Q��j���_���4!��� �؏��e"Х�A�w	c?��v=�ŞQ>O�ϓ�0}����aq��	+�7�p�$���fEĳR�*qxC^ߠ��-�^����bs��kM�UڟP�^=T5�3陨T�ݨ����pD?�I�
�n�F㴍��5�đ��{W��R�s��$�ߞ�t'����w�w3�J�v�.91l�͓G��"���Ep�Q,�G�N`��{�;���D鮰vb5͟�8�s�?�C�s����#�HL��?1�s��|@��U4������3���p��A�!�GԂ�O�))���S���Y�����Nq�E����{l���U[��D1�#yo�����|�-��O��b�E/�;�]��.|�ֽl�Q��bx�#���צּ�O����d�xo#<?H��v��2��>#��a~��O�Ʒ22x����cZ���k����1�4�
exp#z��9�S�t�{����5�.�h-M�BC9�E�����h��x�B�b,Z§����;��#���Q=��#4M�9"��1"����GX������EX��#�>��oEXQ�<>><*;�5�}���Q} ݣ4��r�����a�qe�Ǆ��`�q|��
��cf��!�d�'����J���s��t�^;��9��ݎ�u/f���<s7~����Ȑl������ʫI�pci��������u��\Gï�I��s��!�1W�f�»���+��]_q��/+c�]@E���l 7���-m���].��l�EeQgkh<�~ꢋ���ӎ>z�B]�h|>�MGg��.0B����7�3�hD�^?-?W����6H���јx�q!k��#�ȑS܎F��|K'C0F^E�ȫ�FC��G�L����3�j�Hu��{'�x�9ݽ%O6��B|�?jh����I��`R��%�g��CL"4bbб��JR@������bxf�R6�h\q�/��`S��}������?&����ǠOE����j�|�z����>Z*�u?�1�y�?+����� T�        c װ   $ BDHPAll Prefs Cluster.ctl          |   �x�c``�( ���/��W C�/���oN?��@�Q@����_�_��`�#�P��ƨb��r���C�� XG�����U��_�\��?�g�\ �*��[v���@$� ]'!I              \�   (                                        �       h      � �   o      � �   v      � �   }� � �   � �   � � �   � �   �� � �   � �TahomaTahomaTahoma012 RSRC
 LVCCLBVW ��  x      �`               4       LVSR      LIvi      ,CONP      @TM80      TDFDS      hLIds      |VICD      �vers      �ICON      �icl8      �CPC2      �DTHP      �LIfp      TRec  �  STR      v�FPHb      wLFPSE      w`LIbd      wtBDHb      w�BDSE      w�MUID      w�HIST      w�FTAB      w�    ����        |[�    ����       t\W�    ����      t�hW�    ����      �ĐY�    ����      ��lY�    ����      �\�[�    ����      �x\�   ����     ! l\�    ����     !0�\�    ����     !��\�    ����     %��\�    ����     Pȴ\�    ����     ���\�   ����     �d�\�   ����     ���\�   ����     ��\�   ����     �x�\�   ����     ��]�   ����     �$]�   
����     �p,]�   ����     ��8]�   ����     �D]�   ����     �xP]�   ����     ��h]�   ����     �t]�   ����     �h�]�   ����     �Ќ]�   ����     ��]�   ����     �x�]�   ����     ���]�   ����     ��]�   ����     �|�]�   ����     ���]�   ����     � ^�   ����     �l^�   ����     ��^�   ����     �(^�   ����     �t4^�   ����     ��@^�    ����     �L^�   !����     �hX^�   "����     ��d^�   #����     �p^�   $����     �p|^�   %����     ���^�   &����     ��^�   '����     �l�^�   (����     ���^�   )����     ��^�   *����     �h�^�   +����     ���^�   ,����     �$_�   -����     ��_�   .����     ��0_�   /����     �<<_�   0����     ��T_�   1����     ��`_�   2����     �0x_�   3����     ���_�   4����     �ܜ_�   5����     �D�_�   6����     ���_�   7����     ���_�   8����     �L�_�   9����     ���_�   :����     ��`�   ;����     �X`�   <����     �� `�   =����     � ,`�   >����     �XD`�   ?����     ��P`�   @����     �\`�   A����     �th`�   B����     ��t`�   C����     �$�`�   D����     �t�`�   E����     �Ę`�   F����     ��`�   G����     �h�`�   H����     �ļ`�   I����     �(�`�   J����     �t�`�   K����     ���`�   L����     ��`�   M����     �p�`�   N����     ��a�   O����     �a�   P����     �da�   Q����     ��(a�   R����     �4a�   S����     �`@a�   T����     ��La�   U����     �Xa�   V����     �Xda�   W����     °pa�   X����     ��|a�   Y����     �T�a�   Z����     ð�a�   [����     ��a�   \����     �\�a�   ]����     Ĵ�a�   ^����     ��a�   _����     �\�a�   `����     ż b�   a����     �b�   b����     �pb�   c����     ��$b�   d����     �0b�   e����     �l<b�   f����     ��Hb�   g����     �Tb�   h����     �l`b�   i����     ��lb�   j����     �xb�   k����     �p�b�   l����     �Ԩb�   m����     � �b�   n����     �t�b�   o����     ���b�   p����     � �b�   q����     �l�b�   r����     ���b�   s����     �c�   t����     �xc�   u����     �� c�   v����     �$,c�   w����     �p8c�   x����     ��Dc�   y����     �Pc�   z����     �|\c�   {����     ��hc�   |����     �,tc�   }����     �x�c�   ~����     �Ԍc�   ����     � �c�   �����     �|�c�   �����     �Ȱc�   �����     �$�c�   �����     �p�c�   �����     ���c�   �����     ��c�   �����     �t�c�   �����     ���c�   �����     �d�   �����     �hd�   �����     ��d�   �����     �(d�   �����     �h@d�   �����     ԴLd�   �����     �Xd�   �����     �\dd�   �����     ո|d�   �����     ��d�   �����     �\�d�   �����     ֨�d�   �����     ���d�   �����     �T�d�   �����     ׬�d�   �����     ���d�   �����     �Te�   �����     ؠe�   �����     �0e�   �����     �P<e�   �����     ٰTe�   �����     ��`e�   �����     �`xe�   �����     ڬ�e�   �����     ��e�   �����     �P�e�   �����     ۬�e�   �����     ���e�   �����     �T�e�   �����     ܰ�e�   �����     ���e�   �����     �Pf�   �����     ݜf�   �����     �� f�   �����     �@,f�   �����     ތ8f�   �����     ��Df�   �����     �,Pf�   �����     �|\f�   �����     ��hf�   �����     � tf�   �����     ���f�   �����     �̌f�   �����     � �f�   �����     �p�f�   �����     ἰf�   �����     ��f�   �����     �`�f�   �����     ��f�   �����     ��f�   �����     �X�f�   �����     ��f�   �����     ��g�   �����     �Hg�   �����     �g�   �����     ��(g�   �����     �H4g�   �����     �@g�   �����     ��Lg�   �����     �@Xg�   �����     �dg�   �����     ��pg�   �����     �D|g�   �����     琈g�   �����     ��g�   �����     �<�g�   �����     般g�   �����     �ܸg�   �����     �(�g�   �����     �|�g�   �����     ���g�   �����     ��g�   �����     �l�g�   �����     �� h�   �����     � h�   �����     �|0h�   �����     ��<h�   �����     � Hh�   �����     �|Th�   �����     ��`h�   �����     �$lh�   �����     �pxh�   �����     �Ȅh�   �����     ��h�   �����     �h�h�   �����     h�   �����     ��h�   �����     �X�h�   �����     ��h�   �����     � �h�   �����     �T�h�   �����     ��h�   �����     � �h�   �����     �Ti�   �����     �i�   �����     �� i�   �����     �@,i�   �����     �8i�   �����     ��Di�   �����     �4Pi�   �����     �\i�   �����     ��hi�   �����     �4ti�   �����     �i�   �����     ���i�   �����     �,�i�   �����     ���i�   �����     �ذi�   �����     �4�i�   �����     ���i�   �����     ���i�   �����     �(�i�   �����     ��j�   �����     ��j�   �����     �(j�   �����     �t(j�   �����     ��4j�   �����     � @j�   �����     �tLj�   �����     ��Xj�   ����     � dj�  ����     �|pj�  ����     ��|j�  ����     ��j�  ����     �h�j�  ����     ���j�  ����     ��j�  ����     �X�j�  ����     ���j�  	����     ���j�  
����     �H�j�  ����     �� k�  ����     ��k�  ����     �Tk�  ����     ��$k�  ����     ��0k�  ����     �X<k�  ����     ��Hk�  ����     ��`k�  ����      @lk�  ����      ��k�  ����      �k�  ����     D�k�  ����     ��k�  ����     ��k�  ����     8�k�  ����     ��k�  ����     ��k�  ����     4l�  ����     � l�  ����     �8l�  ����     (Dl�   ����     t\l�  !����     �hl�  "����     �l�  #����     p�l�  $����     ��l�  %����     $�l�  &����     p�l�  '����     ��l�  (����     (�l�  )����     t�l�  *����     �m�  +����     $m�  ,����     |m�  -����     �(m�  .����     	(4m�  /����     	x@m�  0����     	�pm�  1����     
$|m�  2����     
��m�  3����     
ؔm�  4����     <�m�  5����     ��m�  6����     �m�  7����     @�m�  8����     ��m�  9����     ��m�  :����     0�m�  ;����     ��m�  <����     � n�  =����     44X�  >����     ��W�  ?����     �X�  @����     ,�X�  A����     ��X�  B����     �Y�  C����     <�Y�  D����     ��b�  E����     �b�  F����     D�]�  G����     �Xm�  H����     �dm�  I����     4�]�  J����     � \�  K����     ��[�  L����     8�e�  M����     ��e�  N����     ؐe�  O����     (X�  P����     |He�  Q����     �le�  R����     4 e�  S����     ��d�  T����     ��X�  U����     <$e�  V����     ��d�  W����     �d�  X����     H�Z�  Y����     �[�  Z����     �n�  [����     H�b�  \����     ��p�  ]����     ��p�  ^����     d�p�  _����     ��p�  `����     �p�  a����     d�p�  b����     ��p�  c����     �p�  d����     \�p�  e����     ��p�  f����      q�  g����     \q�  h����     �q�  i����     $q�  j����     d0q�  k����     �<q�  l����     Hq�  m����     XTq�  n����     �`q�  o����     (lq�  p����     txq�  q����     �q�  r����      D�q�  s����      ��q�  t����      �q�  u����     !4�q�  v����     !��q�  w����     !��q�  x����     ",�q�  y����     "x�q�  z����     "��q�  {����     #,�q�  |����     #�r�  }����     #�r�  ~����     $4 r�  ����     $�,r�  �����     $�8r�  �����     %@Dr�  �����     %�Pr�  �����     %�\r�  �����     &4hr�  �����     &�tr�  �����     &܀r�  �����     '(�r�  �����     '��r�  �����     'Ԥr�  �����     (,�r�  �����     (��r�  �����     (��r�  �����     )0�r�  �����     )|�r�  �����     )��r�  �����     * �r�  �����     *ts�  �����     *�s�  �����     +s�  �����     +d(s�  �����     +�4s�  �����     ,@s�  �����     ,`Ls�  �����     ,�Xs�  �����     -ds�  �����     -\ps�  �����     -�|s�  �����     -�s�  �����     .T�s�  �����     .��s�  �����     .��s�  �����     /T�s�  �����     /��s�  �����     0�s�  �����     0`�s�  �����     0��s�  �����     1�s�  �����     1\ t�  �����     1�t�  �����     1�t�  �����     2T$t�  �����     2�0t�  �����     2�<t�  �����     3HHt�  �����     3�Tt�  �����     3�`t�  �����     4Dlt�  �����     4�xt�  �����     4�t�  �����     5@�t�  �����     5��t�  �����     5�t�  �����     6L�t�  �����     6��t�  �����     6��t�  �����     7P�t�  �����     7��t�  �����     7��t�  �����     8L�t�  �����     8�u�  �����     8�u�  �����     9@ u�  �����     9�,u�  �����     9�8u�  �����     :HDu�  �����     :�Pu�  �����     :�\u�  �����     ;4hu�  �����     ;�tu�  �����     ;܀u�  �����     <4�u�  �����     <��u�  �����     <ؤu�  �����     =0�u�  �����     =��u�  �����     =��u�  �����     >D�u�  �����     >��u�  �����     >��u�  �����     ?D�u�  �����     ?�v�  �����     ?�v�  �����     @<v�  �����     @�(v�  �����     @�4v�  �����     A(@v�  �����     A�Lv�  �����     A�Xv�  �����     B dv�  �����     Blpv�  �����     B�|v�  �����     C�v�  �����     Cp�v�  �����     CРv�  �����     D�v�  �����     Dt�v�  �����     D��v�  �����     E�v�  �����     Ed�v�  �����     E��v�  �����     F�v�  �����     FX w�  �����     F�w�  �����     Gw�  �����     GT$w�  �����     G�0w�  �����     G�<w�  �����     HHHw�  �����     H�Tw�  �����     H�`w�  �����     I<lw�  �����     I�xw�  �����     I܄w�  �����     J(�w�  �����     J|�w�  �����     J̨w�  �����     K(�w�  �����     Kt�w�  �����     K��w�  �����     L�w�  �����     Lx�w�  �����     L��w�  �����     M�w�  �����     Mhx�  �����     M�x�  �����     N x�  �����     N\,x�   ����     N�8x�  ����     ODx�  ����     OdPx�  ����     O�\x�  ����     Phx�  ����     Pltx�  ����     P��x�  ����     Q�x�  ����     QT�x�  	����     Q��x�  
����     Q��x�  ����     RX�x�  ����     R��x�  ����     S�x�  ����     SP�x�  ����     S��x�  ����     S��x�  ����     TTy�  ����     T�y�  ����     T�y�  ����     U@(y�  ����     U�4y�  ����     U�@y�  ����     VHLy�  ����     V�Xy�  ����     V�dy�  ����     W@py�  ����     W�|y�  ����     W��y�  ����     X<�y�  ����     X��y�  ����     X�y�   ����     Y0�y�  !����     Y��y�  "����     Y��y�  #����     Z4�y�  $����     Z��y�  %����     Z��y�  &����     [( z�  '����     [|z�  (����     [�z�  )����     \8$z�  *����     \�0z�  +����     \�<z�  ,����     ](Hz�  -����     ]�Tz�  .����     ]�`z�  /����     ^,lz�  0����     ^xxz�  1����     ^؄z�  2����     _$�z�  3����     _��z�  4����     _Шz�  5����     `�z�  6����     `h�z�  7����     `��z�  8����     a�z�  9����     ad�z�  :����     a��z�  ;����     b�z�  <����     bl{�  =����     b�{�  >����     c {�  ?����     cd,{�  @����     c�8{�  A����     dD{�  B����     dhP{�  C����     d�\{�  D����     eh{�  E����     eht{�  F����     e��{�  G����     f �{�  H����     f`�{�  I����     f��{�  J����     g �{�  K����     gP�{�  L����     g��{�  M����     h�{�  N����     h\�{�  O����     h��{�  P����     i �{�  Q����     iX|�  R����     i�|�  S����     j |�  T����     jL(|�  U����     j�4|�  V����     j�@|�  W����     kHL|�  X����     k�X|�  Y����     k�d|�  Z����     l@p|�  [����     l�||�  \����     l�|�  ]����     mD�|�  ^����     m��|�  _����     m�|�  `����     nH�|�  a����     n��|�  b����     n��|�  c����     o8�|�  d����     o��|�  e����     o��|�  f����     p0 }�  g����     p|}�  h����     p�}�  i����     q$$}�  j����     qx0}�  k����     q�<}�  l����     r$H}�  m����     rpT}�  n����     r�`}�  o����     s(l}�  p����     sxx}�  q����     s̄}�  r����     t$�}�  s����     tp�}�  t����     t̨}�  u����     u�}�  v����     ux�}�  w����     u��}�  x����     v$�}�  y����     v|�}�  z����     v��}�  {����     w �}�  |����     wl~�  }����     w�~�  ~����     x ~�  ����     x`,~�  �����     x�8~�  �����     yD~�  �����     y\P~�  �����     y�\~�  �����     y�h~�  �����     zLt~�  �����     z��~�  �����     z��~�  �����     {D�~�  �����     {��~�  �����     {�~�  �����     |H�~�  �����     |��~�  �����     } �~�  �����     }L�~�  �����     }��~�  �����     ~ �~�  �����     ~`�  �����     ~��  �����     �  �����     `(�  �����     �4�  �����     �@�  �����     �lL�  �����     ��X�  �����     �,d�  �����     ��p�  �����     ��|�  �����     �(��  �����     ����  �����     �Р�  �����     �(��  �����     ����  �����     ����  �����     �,��  �����     ����  �����     ����  �����     �0��  �����     ��$��  �����     �� ��  �����     � ���  �����     �t\��  �����     �̰��  �����     �$���  �����     �p�x�  �����     �� ��  �����     ���  �����     �d��  �����     ��$��  �����     � 0��  �����     �l<��  �����     ��H��  �����     �$T��  �����     ��`��  �����     ��l��  �����     �0x��  �����     �|���  �����     �А��  �����     �$���  �����     �����  �����     �д��  �����     �0���  �����     �|̀�  �����     ��؀�  �����     �$䀦  �����     �����  �����     �����  �����     ���  �����     �x��  �����     �� ��  �����     �$,��  �����     �p8��  �����     ��D��  �����     �P��  �����     �t\��  �����     ��h��  �����     �t��  �����     �h���  �����     �̌��  �����     ����  �����     �h���  �����     �����  �����     ����  �����     �`ȁ�  �����     ��ԁ�  �����     ����  �����     �d쁦  �����     �����  �����     ���  �����     �\��  �����     ����  �����     �(��  �����     �h4��  �����     ��@��  �����     �L��  �����     �\X��  �����     ��d��  �����     ��p��  �����     �H|��  �����     �����  �����     �씂�  �����     �L���  �����     �����  �����     �츂�  �����     �@Ă�  �����     ��Ђ�  �����     ��܂�  �����     �8肦  �����     ���  �����     �� ��  �����     �4��  �����     ����  �����     ��$��  �����     �,0��  �����     ��<��  �����     ��H��  �����     �4T��  �����     ��`��  �����     ��l��  �����     �(x��  �����     �����  �����     �ܐ��  �����     �(���  �����     �t���  �����     �Դ��  �����     � ���  �����     �t̃�  �����     ��؃�   ����     �䃦  ����     �l���  ����     �����  ����     ���  ����     �h��  ����     �� ��  ����     �,��  ����     �\8��  ����     ��D��  	����     �P��  
����     �P\��  ����     ��h��  ����     ��t��  ����     �T���  ����     �����  ����     ����  ����     �T���  ����     �����  ����     � ���  ����     �`Ȅ�  ����     ��Ԅ�  ����     � ���  ����     �L섦  ����     �����  ����     ����  ����     �T��  ����     ����  ����     � (��  ����     �L4��  ����     ��@��  ����     ��L��  ����     �XX��   ����     ��d��  !����     ��p��  "����     �D|��  #����     �����  $����     �����  %����     �L���  &����     �����  '����     � ���  (����     �\ą�  )����     ��Ѕ�  *����     �܅�  +����     �h腦  ,����     ���  -����     � ��  .����     �\��  /����     ����  0����     ��$��  1����     �P0��  2����     ��<��  3����     ��H��  4����     �@T��  5����     ��`��  6����     ��l��  7����     �4x��  8����     �����  9����     �䐆�  :����     �0���  ;����     �����  <����     �ش��  =����     �4���  >����     ��̆�  ?����     ��؆�  @����     �(䆦  A����     �����  B����     �����  C����     �4��  D����     ����  E����     �� ��  F����     �(,��  G����     ��8��  H����     ��D��  I����     �(P��  J����     ��\��  K����     ��h��  L����     �0t��  M����     �|���  N����     �Ԍ��  O����     � ���  P����     �|���  Q����     �Ȱ��  R����     �$���  S����     �pȇ�  T����     ��ԇ�  U����     ����  V����     �h쇦  W����     �����  X����     ���  Y����     �d��  Z����     ����  [����     �(��  \����     �l4��  ]����     ��@��  ^����     �L��  _����     �`X��  `����     ´d��  a����     �p��  b����     �d|��  c����     ð���  d����     ����  e����     �d���  f����     İ���  g����     ����  h����     �XĈ�  i����     ŰЈ�  j����     ��܈�  k����     �P舦  l����     Ɯ�  m����     �� ��  n����     �@��  o����     ǜ��  p����     ��$��  q����     �<0��  r����     Ȍ<��  s����     ��H��  t����     �DT��  u����     ɔ`��  v����     ��l��  w����     �Hx��  x����     ʔ���  y����     �𐉦  z����     �@���  {����     ˌ���  |����     �ش��  }����     �4���  ~����     ̀̉�  ����     ��؉�  �����     �(䉦  �����     �|���  �����     �����  �����     �,��  �����     �x��  �����     �� ��  �����     �$,��  �����     ϐ8��  �����     ��D��  �����     �@P��  �����     Ќ\��  �����     ��h��  �����     �4t��  �����     ѐ���  �����     �܌��  �����     �@���  �����     Ҍ���  �����     �����  �����     �`���  �����     ӬȊ�  �����     �Ԋ�  �����     �h���  �����     Լ슦  �����     ����  �����     �d��  �����     ո��  �����     ���  �����     �\(��  �����     ֬4��  �����     �@��  �����     �XL��  �����     װX��  �����     � d��  �����     �Xp��  �����     ؤ|��  �����     �����  �����     �H���  �����     ٘���  �����     �𬋦  �����     �<���  �����     ژċ�  �����     ��Ћ�  �����     �8܋�  �����     ۈ苦  �����     ���  �����     �4 ��  �����     ܄��  �����     ����  �����     �8$��  �����     ݐ0��  �����     ��<��  �����     �DH��  �����     ސT��  �����     ��`��  �����     �Hl��  �����     ߬x��  �����     ����  �����     �d���  �����     ༜��  �����     ����  �����     �h���  �����     �����  �����     �̌�  �����     �t،�  �����     ��䌦  �����     ����  �����     �h���  �����     ����  �����     ���  �����     �h ��  �����     ��,��  �����     �8��  �����     �hD��  �����     �P��  �����     �\��  �����     �Th��  �����     �t��  �����     �����  �����     �T���  �����     砘��  �����     �����  �����     �D���  �����     蘼��  �����     ��ȍ�  �����     �@ԍ�  �����     ����  �����     ��썦  �����     �L���  �����     ���  �����     ����  �����     �@��  �����     �(��  �����     ��4��  �����     �L@��  �����     �L��  �����     ��X��  �����     �4d��  �����     �p��  �����     ��|��  �����     �4���  �����     ��  �����     �ࠎ�  �����     �,���  �����     ��  �����     ��Ď�  �����     �DЎ�  �����     �܎�  �����     ��莦  �����     �@�  �����     � ��  �����     ����  �����     �8��  �����     �$��  �����     ��0��  �����     �,<��  �����     �xH��  �����     ��T��  �����     �`��  �����     �|l��  �����     ��x��  �����     �,���  �����     �����  �����     �Ԝ��  �����     �4���  �����     �����  �����     �����  �����     �(̏�  �����     �|؏�   ����     ��䏦  ����     � ���  ����     �����  ����     ����  ����     �4��  ����     �� ��  ����     ��,��  ����     �48��  ����     ��D��  	����     ��P��  
����     �@\��  ����     ��h��  ����     ��t��  ����     �@���  ����     �����  ����     �옐�  ����     �L���  ����     �����  ����     �����  ����     �DȐ�  ����     ��Ԑ�  ����     �����  ����     �P쐦  ����     �����  ����     ����  ����      D��  ����      ���  ����      �(��  ����     P4��  ����     �@��  ����      L��  ����     LX��   ����     �d��  !����     �p��  "����     \|��  #����     ����  $����     ���  %����     X���  &����     ����  '����     ���  (����     lđ�  )����     �Б�  *����     ܑ�  +����     d葦  ,����     ����  -����     ( ��  .����     x��  /����     ���  0����     ,$��  1����     �0��  2����     �<��  3����     	0H��  4����     	|T��  5����     	�`��  6����     
(l��  7����     
�x��  8����     
̄��  9����     $���  :����     p���  ;����     ̨��  <����     ���  =����     t���  >����     �̒�  ?����      ؒ�  @����     l䒦  A����     ��  B����     ���  C����     x��  D����     ���  E����     , ��  F����     �,��  G����     �8��  H����     (D��  I����     |P��  J����     �\��  K����     $h��  L����     xt��  M����     Ȁ��  N����     4���  O����     ����  P����     ओ�  Q����     <���  R����     ����  S����     �ȓ�  T����     Hԓ�  U����     ����  V����     �쓦  W����     H���  X����     ���  Y����     ���  Z����     d��  [����     �(��  \����     4��  ]����     t@��  ^����     �L��  _����     4X��  `����     �d��  a����     �p��  b����     0|��  c����     ����  d����     ؔ��  e����     @���  f����     ����  g����     𸔦  h����     <Ĕ�  i����     �Д�  j����     �ܔ�  k����     <蔦  l����     ����  m����     � ��  n����     D��  o����     ���  p����     �$��  q����     40��  r����     �<��  s����     �H��  t����     8T��  u����     �`��  v����     �l��  w����      (x��  x����      ����  y����      А��  z����     !(���  {����     !����  |����     !д��  }����     "$���  ~����     "p̕�  ����     "�ؕ�  �����     #䕦  �����     #`�  �����     #����  �����     $ ��  �����     $\��  �����     $� ��  �����     %,��  �����     %\8��  �����     %�D��  �����     %�P��  �����     &P\��  �����     &�h��  �����     &�t��  �����     '8���  �����     '����  �����     '蘖�  �����     (D���  �����     (����  �����     (𼖦  �����     )<Ȗ�  �����     )�Ԗ�  �����     )����  �����     *H얦  �����     *����  �����     *���  �����     +<��  �����     +���  �����     +�(��  �����     ,44��  �����     ,�@��  �����     ,�L��  �����     -$X��  �����     -�d��  �����     -�p��  �����     . |��  �����     .p���  �����     .Ĕ��  �����     / ���  �����     /l���  �����     /̸��  �����     0ė�  �����     0xЗ�  �����     0�ܗ�  �����     1 藦  �����     1l���  �����     1� ��  �����     2��  �����     2x��  �����     2�$��  �����     30��  �����     3`<��  �����     3�H��  �����     4T��  �����     4``��  �����     4�l��  �����     5x��  �����     5X���  �����     5����  �����     5����  �����     6H���  �����     6����  �����     6����  �����     7<̘�  �����     7�ؘ�  �����     7�䘦  �����     88�  �����     8����  �����     8���  �����     9,��  �����     9x ��  �����     9�,��  �����     : 8��  �����     :tD��  �����     :�P��  �����     ;$\��  �����     ;ph��  �����     ;�t��  �����     <0���  �����     <|���  �����     <И��  �����     = ���  �����     =t���  �����     =м��  �����     >ș�  �����     >tԙ�  �����     >����  �����     ?왦  �����     ?h���  �����     ?���  �����     @��  �����     @x��  �����     @�(��  �����     A(4��  �����     At@��  �����     A�L��  �����     B X��  �����     B|d��  �����     B�p��  �����     C,|��  �����     Cx���  �����     CԔ��  �����     D ���  �����     D����  �����     Dܸ��  �����     E8Ě�  �����     E�К�  �����     E�ܚ�  �����     F,蚦  �����     F����  �����     F� ��  �����     G$��  �����     Gp��  �����     G�$��  �����     H0��  �����     Hp<��  �����     H�H��  �����     IT��  �����     I``��  �����     I�l��  �����     Jx��  �����     JX���  �����     J����  �����     K ���  �����     KL���  �����     K����  �����     K����  �����     LP̛�  �����     L�؛�   ����     L�䛦  ����     MH�  ����     M����  ����     M���  ����     NT��  ����     N� ��  ����     N�,��  ����     O<8��  ����     O�D��  	����     O�P��  
����     PD\��  ����     P�h��  ����     P�t��  ����     Q<���  ����     Q����  ����     QԘ��  ����     R(���  ����     R����  ����     Rм��  ����     S0Ȝ�  ����     S|Ԝ�  ����     S����  ����     T(윦  ����     T����  ����     T���  ����     U,��  ����     Ux��  ����     U�(��  ����     V,4��  ����     Vx@��  ����     V�L��  ����     W$X��   ����     Wpd��  !����     W�p��  "����     X|��  #����     Xp���  $����     X����  %����     Y ���  &����     Yl���  '����     Y����  (����     Zĝ�  )����     Z`Н�  *����     Z�ܝ�  +����     [ 蝦  ,����     [`���  -����     [� ��  .����     \ ��  /����     \T��  0����     \�$��  1����     \�0��  2����     ]L<��  3����     ]�H��  4����     ]�T��  5����     ^H`��  6����     ^�l��  7����     ^�x��  8����     _<���  9����     _����  :����     _䜞�  ;����     `0���  <����     `����  =����     `����  >����     a4̞�  ?����     a�؞�  @����     a�䞦  A����     b(�  B����     b����  C����     b���  D����     c0��  E����     c| ��  F����     c�,��  G����     d,8��  H����     d�D��  I����     d�P��  J����     e(\��  K����     exh��  L����     e�t��  M����     f(���  N����     f����  O����     f䘟�  P����     g0���  Q����     g����  R����     g༟�  S����     h,ȟ�  T����     h�ԟ�  U����     h����  V����     i4쟦  W����     i����  X����     i���  Y����     j@��  Z����     j���  [����     k (��  \����     kL4��  ]����     k�@��  ^����     k�L��  _����     lHX��  `����     l�d��  a����     l�p��  b����     m<|��  c����     m����  d����     m䔠�  e����     n8���  f����     n����  g����     nܸ��  h����     o(Ġ�  i����     o�Р�  j����     o�ܠ�  k����     p@蠦  l����     p����  m����     p� ��  n����     qD��  o����     q���  p����     q�$��  q����     rP0��  r����     r�<��  s����     sH��  t����     sXT��  u����     s�`��  v����     t l��  w����     ttx��  x����     tĄ��  y����     u���  z����     up���  {����     u����  |����     v���  }����     vh���  ~����     v�̡�  ����     wء�  �����     wh䡦  �����     w��  �����     x���  �����     xd��  �����     x���  �����     y  ��  �����     y`,��  �����     y�8��  �����     y�D��  �����     zXP��  �����     z�\��  �����     {h��  �����     {Pt��  �����     {����  �����     {����  �����     |\���  �����     |����  �����     }���  �����     }T���  �����     }�Ȣ�  �����     }�Ԣ�  �����     ~Xࢦ  �����     ~�좦  �����      ���  �����     L��  �����     ���  �����     ���  �����     �L(��  �����     ��4��  �����     ��@��  �����     �8L��  �����     ��X��  �����     ��d��  �����     �,p��  �����     ��|��  �����     �̈��  �����     � ���  �����     �l���  �����     �����  �����     ����  �����     �dģ�  �����     ��У�  �����     �ܣ�  �����     �`裦  �����     �����  �����     � ��  �����     �h��  �����     ����  �����     �$��  �����     �X0��  �����     ��<��  �����     � H��  �����     �TT��  �����     ��`��  �����     �l��  �����     �\x��  �����     �����  �����     ����  �����     �T���  �����     �����  �����     �����  �����     �H���  �����     ��̤�  �����     ��ؤ�  �����     �D䤦  �����     ���  �����     �����  �����     �8��  �����     ����  �����     �� ��  �����     �8,��  �����     ��8��  �����     ��D��  �����     �<P��  �����     ��\��  �����     ��h��  �����     �,t��  �����     �����  �����     �̌��  �����     �$���  �����     �p���  �����     �̰��  �����     ����  �����     �lȥ�  �����     ��ԥ�  �����     �०  �����     �d쥦  �����     �����  �����     �,��  �����     �x��  �����     ����  �����     �(��  �����     �p4��  �����     ��@��   ����     ��\�   	����     �` ]�   ����     ��\]�   ����     �P�]�    ����     ��L��    ����     ��X��    ����     ��d��    ����     �p��    ����     ��|��    ����     �����    ����     �����   �����     �ؠ��