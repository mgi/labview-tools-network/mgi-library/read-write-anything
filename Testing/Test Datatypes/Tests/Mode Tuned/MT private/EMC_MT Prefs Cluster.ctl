RSRC
 LVCCLBVW  �  h      �   p!� 0           < � @�      ����            %��+��L���� �@�          V?�70doO�/~%f�=U��ُ ��	���B~  � LVCCEMC_MT Prefs Cluster.ctl       LVCC   EMC_Test Header Cluster.ctlPTH0   #     EMC_Test Header Cluster.ctl    @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  F ��)3�   EMC_Test Header Cluster.ctl "@P        Test Header                         LVCC    BWFreqRange Cluster.ctlPTH0   1      Shared	BandwidthBWFreqRange Cluster.ctl    @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P       BWFreqRange                         LVCC      EMC_M Tuner Motor Cluster.ctlPTH0   ,    privateEMC_M Tuner Motor Cluster.ctl    @0����Name  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P        Motor                         LVCC     EMC_ML Motor Radio Button.ctlPTH0   /    
ML privateEMC_ML Motor Radio Button.ctl    V ��F��   EMC_ML Motor Radio Button.ctl 0@ Incremental
Continuous Motor Control                          LVCC      DwellListbox.xctlData.ctl PTH0   %      SharedDwellListboxData.ctl    @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P   Dwell                         LVCC     Frequency Specifier.xctlData.ctlPTH0   ,      SharedFrequency SpecifierData.ctl    d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P         Data                          LVCC     Freq_Spec Method Enum.ctlPTH0   =      SharedFrequency SpecifierFreq_Spec Method Enum.ctl    d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method                           LVCC      EMC_MT Test Type Enum.ctlPTH0      EMC_MT Test Type Enum.ctl    � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type                          LVCC    EMC_UI PID Cluster.ctl PTH0   &      SharedEMC_UI PID Cluster.ctl    @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P     PID Cluster                         LVCC      #EMC_ML Controlled Variable Enum.ctlPTH0   5    
ML private#EMC_ML Controlled Variable Enum.ctl    ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable                          LVCC      EMC_Timing Generator Params.ctlPTH0   '     EMC_Timing Generator Params.ctl    @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp T �       EMC_Timing Generator Params.ctl ,@P         Timing Gen Cluster                          LVCC      EMC_MT Calc Type Enum.ctlPTH0      EMC_MT Calc Type Enum.ctl    R ��8��   EMC_MT Calc Type Enum.ctl 0@ 
Rx AntennaRF Probe AveMT Calc Type                           LVCC      "EMC_M Network Analyzer Cluster.ctl PTH0   1    private"EMC_M Network Analyzer Cluster.ctl    @0����Name  @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) M �       "EMC_M Network Analyzer Cluster.ctl !@P      NetworkAnalyzer                             "   j @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @!Show Diagnostics? @0����
Test Title  @0����Name  @0����BW File @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ���� [Bw]  @P  
 Test Params $@@ ���� TestParam (Freq, Test)  @
Attenuation @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 	         SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  	      !Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  	 # $ % 	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  	 ' (Amp @FreqSettling Time(ms) @
Nominal Dwell @
Data 2  @@ ���� ,[Frequency] @@ ���� ,[Time]  7 �       DwellListbox.xctlData.ctl %@P  - .Dwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  0 1 2 3 4 5 6Freq  � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  : ; <
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  9 = > ? @ A B C D E F G H ISeek  @2����Seek File (Rel) @
SigGen Offset @P  K LTest  @0����PV File/Level (dBm) @
PV Tol (dB) @P  N OCal @
App Pwr EStop Offset (dB) @
SigGen Max Offset @0����Cable @P  	 SSigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  U V W X Y Z [Timing  @P  	 \ArbGen  @
Fwd Pwr EStop Offset (dB) @
Numeric @@ ���� _[Probe Ranges]  @0����String  @@ ���� a[Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  	 d e fNetwork Analyzer  @@ ���� aProbe VISA[]  c ��)O�   EMC_MT Prefs Cluster.ctl A@P      " & ) * + / 7 8 J M P Q R T ] ^ ` b c g hPrefs  i       � 
 c  x    
 d   0   `    
 P    
 c   0  @flg @oRt @eof @P    udf @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P  
     Header  @!Show Diagnostics? @0����
Test Title  @0����Name  @0����BW File @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ���� [Bw]  @P   Test Params $@@ ���� TestParam (Freq, Test)  @
Attenuation @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
       ! " # $ %SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P   ' ( ) * +Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P   - . / %	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P   1 2Amp @FreqSettling Time(ms) @
Nominal Dwell @
Data 2  @@ ���� 6[Frequency] @@ ���� 6[Time]  7 �       DwellListbox.xctlData.ctl %@P  7 8Dwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  : ; < = > ? @Freq  � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  D E F
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  C G H I J K L M N O P Q R SSeek  @2����Seek File (Rel) @
SigGen Offset @P  U VTest  @0����PV File/Level (dBm) @
PV Tol (dB) @P  X YCal @
App Pwr EStop Offset (dB) @
SigGen Max Offset @0����Cable @P   ]SigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  _ ` a b c d eTiming  @P   fArbGen  @
Fwd Pwr EStop Offset (dB) @
Numeric @@ ���� i[Probe Ranges]  @0����String  @@ ���� k[Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P   n o pNetwork Analyzer  @@ ���� kProbe VISA[]  >@P     & , 0 3 4 5 9 A B T W Z [ \ ^ g h j l m q rPrefs  0����  T     P  t u u t v t  !  
  P  y y y y y  @ ���� z 
 P  t {  @ ���� |  P 
 t } y y t t y y y y  P  t    y v  P  t t t y y  P  t t t  @ ���� y 
 P  � �    2����  P  �  y � y y �  P  y y y " P  t � y � y y y � y y y y  � 
 P  � y 
 P  t y 
 P  t t  P  x y y y y y y 
 P  t �  @ ���� t  P  t t t y <@P  w x t ~  � �  y � � v � � � y y � � y � � v � �dfd <@P  w x t ~  � �  y � � v � � � y y � � y � � v � �txd <@P  w x t ~  � �  y � � v � � � y y � � y � � v � �old <@P  w x t ~  � �  y � � v � � � y y � � y � � v � �ext  P  	 s � � � �  @ �     P  � � � k  P  v v v  @ ���� v   
Rx AntennaRF Probe Ave  @ ���� � 
 P  � �  P  � � � i  P  t t v � t t  P  �  P  � � �  @ ���� S 
 P  � �  P       	App Power	Fwd Power  @ ���� � 
 P  � � X  Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)  @ ���� � 
 P  � � *  LogLinearFileSusceptibility Spec  @ ���� � 
 P  � �  P  � � � 6  P  � � �   P  � � �  @T Minimum @T Maximum @
	Increment  P  � � � 
 c   $  
 c      � s 
 c     
 c           `    > P  � � � � � �    � � � �     � � � � � � � � � � � 
 d    �    w       �  �   �0  �0  �   �   �   �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �   �0  �   �0  �0  �0  �0  �0  �0  �   �0  �0  �0  �   �0  �0  �0  �0  �0  �   �   �0  �0  �0  �   �0  �   �0  �   �0  �   �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �   �   �0  �0  �0  �0  �0  �0  �0  �0  �0  �  �  �  �  �  �0  s    
     &         ,  ' + 0  3  9  7  A  : =  B T  G M S W  Z  ^  g  f l  m q  �      R  
fx�c`�	�j�t0L�?@Ž��n���l|b# �x )��pX�Q"C�ৱ�A�K�>�0�����05!�1&b�# ��>\����lr�SK�T��*�L�4N$��*� �3��l�\<�B"@99]�(�
�-�@M��TC��RА@I��3p���X���*����� ��y���� ܊K��4�Օ�4�@f�@�>\��n�V�!���M��� 4fd����%��s��õ�hZ����s��{�hV��l�RS,xJ�430���b�eK@V���H| �I�     ( VIDSEMC_MT Prefs Cluster.ctl          <  10x��[P�_�;��A� �D4`�b#
c�!#���zV/�k�Fq��pc;�#�Ie}RqJ*MIK��%S&��9Tpj:t�S���t`ζ$5�M�\����?���˟���������}������8���E+��Q���ξ�	�[�q��e�͉��%KS�ۃx.�ٗc_C�|� ����`mm��o��c��Z:�cߺ��lG99�xN�����q<u#�.�^���GC"?��F�	-��8�4��"��ڊ��dL������k���N��X��L���8�u��g����63��gȏ2�x O贽sư6���/$q��q~�,�9�0Dc���x�xH�y�����!�!�鈋#OE�N�h8F�o0h��I3²LЂ8���5����t��7OYr�	e�=�sI�]��/'G0�dU�`���;|��N�|?	ʀ�_�A�.�_/(o'�A�gx�<cy��PN�$�4��k���߱��{x����s�=��p��z���q�����mf��#z���;��o�v�n+a	]�N�>��=x�z$���쾕(��ǰ�==��ې�	}>�1T�p�<��<�K]̥�R��R��KVť.�f��)aj�~���:���n��6M��6�o��?��?]�`�f�߽������`v;����zy�E�F#b1ʐ����m�ەi�E��u�i#Cq��a�h�qm뚙uM^�kBB����'�@��a�iF�&u�6I�Thf��5I6�E|�]v��t��ذ��,�Ǎ"��|�6�05�m�T4��c�]jd؀|�Hh� �@&k˔����R#�3�>�j���n�*E~���� �uJʾ���Ⱦ����-W�h=�:<�uR���"|6N�p=�:�CB=�Nc��Z���qF�Q���9EG-�3�w	��:&�z���jf�Il�Iɶj]�-�m�O��ݢ7�TR�U���n' �U��+VTR���W�:�E�%��^2����PϦQKm�������6����k�al��Tn��L�"H���fh�XY(V��ow ,�C�pڋ�OgE�ɋ�*��t#��=��c�4V���C�0�wz#���	�̰�k��N�͝h�}����U���4e^�)�ʖ}�r#F.c���y��3��.�I�,Mc1H� �������˫�^^�^^H��4�6�׏8��r���|edP�e��V�j�n�f��C3N7M�2H�2H� !��p��Ɓu�ذ�+�z�?�0fsLP��A6��-�1W�@�GH	�
��46�q`���l����֬{�zG�:ǉ�u�+h��x���B�udZo)^g9����d"[��hb��H2��e�8P������j_��5�j��;O/NȊV�DJ�%KO�H�O��K-�7���i��r����˷�"��b,��c�-?+��0��|�0d��&*��\�� �`u�������ZP�(��������ݸi�0(���<a@����ߟ5�ׁ�>8�YV�﯉y��q]z]�{�bJ��O�F�U���e��G�����DO��� ��� �jO��ӓ�� ^BO�g�C�����Nb~+6��.G�ík�n���1���
ک�u]n���<�u.�w�\y%��pR,I�b�&Ko&a��9=�7���4YVK!��_��_������l�y��<�cN�UqD����m��c��'Dr`�� ����Uu"���ֻ��m�y��'����9�]��]���W��k���?}�~_L'�H��߯��i0,��i0�ma������-lFm�3j��"zn����@{j�q�NJ�pR�6]i��1�0�!M�k�T���w�Hk��Xi��(���d˛������S�y+U��q��M�S�h5I�;������z/�)ꍼIT�i�j	�z�y�T{��]Τ��l9��Qm̈�q��g�gg����X�j���S��+�E/_M b-S-
�P-�^�T��Lu�PI���	��m���E��^H!^�IQ��N����q�J,�:E�V�T�Q���_pO���(v¾�������	��
�`D�w�_���������#C@��ϥ3�Q;v���Nr�����0*����(���^���W+s'�Fﭕ��Y|�	�;%�(�]��跗5C6�H&l%��ɘ�
�N�b3�8Y���7�$�S���/͔2��kv�|hإ��&ʠ4��ҳB��j��C��T̶$M/'�1ۦ��1�[,�=q�[��e*QȔ�������=�me�m���ߡ72���F"���L�?���!�U�������Y�<�)s��-y?��]L��vێbK��b�'l��fش�!���������yR����0�k��_�'v٢��E�"��82������G�bU�bc	���KLظI��T��lTX�m��6�u�����ۢ�f��c4N/(q*�qz<,2�E��/а�"��y��V=�V(�P�V k�	VQ>'vY@ߕ+�����+�.�jX
ȍ~�4�*UN��]��J��8����K�_����۹b{	�������-3��>�]�!������E�����eC����r�E�H�ai�� ��G"�N"�:�R �#�A�G��<������'v�������#����0�xh�=thy� -�:�����<t����f3�����ɩ�U�~��A�8�-��5��jZ�\��4��F���,4�y�:ɍ�|vI9Q%��e*��Yޘ�|S,&�?:�~�3`�UCcRV��idy(v!��"$�� �*���`�d�Ǘ�ʝ�~��;��G���ۈ�ۃ��[��ɭ��\'��,��>XF����G6L��[|���n~�7�2$9I�4�K�9���[��q���Z(2�}� ÅV��1��0�F�6�7̰
σ�?c荧r}��~I"�&��K�ɰ��$�כD�5�>�'�%N��f��E��q���)���;���/�RD��FqK{��n&ŧ�������fQ|��_n��qmx
P���=���(>��>�Y�g~�0n)�v��3|1�"���=�a>�7>��Y��\ײ�e����vs)ϩ>��C��a!�    �   8.2     ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������  "   j @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ���v�   EMC_Test Header Cluster.ctl @P        Header  @!Show Diagnostics? @0����
Test Title  @0����Name  @0����BW File @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ���� [Bw]  @P  
 Test Params $@@ ���� TestParam (Freq, Test)  @
Attenuation @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 	         SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  	      !Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  	 # $ % 	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  	 ' (Amp @FreqSettling Time(ms) @
Nominal Dwell @
Data 2  @@ ���� ,[Frequency] @@ ���� ,[Time]  7 �       DwellListbox.xctlData.ctl %@P  - .Dwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  0 1 2 3 4 5 6Freq  � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  : ; <
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  9 = > ? @ A B C D E F G H ISeek  @2����Seek File (Rel) @
SigGen Offset @P  K LTest  @0����PV File/Level (dBm) @
PV Tol (dB) @P  N OCal @
App Pwr EStop Offset (dB) @
SigGen Max Offset @0����Cable @P  	 SSigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  U V W X Y Z [Timing  @P  	 \ArbGen  @
Fwd Pwr EStop Offset (dB) @
Numeric @@ ���� _[Probe Ranges]  @0����String  @@ ���� a[Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  	 d e fNetwork Analyzer  @@ ���� aProbe VISA[]  c ��)3�   EMC_MT Prefs Cluster.ctl A@P      " & ) * + / 7 8 J M P Q R T ] ^ ` b c g hPrefs  i    �   { @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  @!Show Diagnostics? @0����
Test Title  @0����Name  @0����BW File @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ���� [Bw]  @P  
 Test Params $@@ ���� TestParam (Freq, Test)  @
Attenuation @
	Ref Level @0����Receive Cable @0����Receive Antenna @
Start Factor  @
Stop Factor @
TrigDelay(s)  @
TrigOffset(s) @P 
 	         SA  
@Start 
@Stop  
@Step  @
Speed%  P ��F��   EMC_ML Motor Radio Button.ctl *@ Incremental
Continuous Control B �       EMC_M Tuner Motor Cluster.ctl @P  	      !Motor @0����Fwd Pwr Cable @0����Rev Pwr Cable @
Capture Time(s) @P  	 # $ % 	Pwr Meter @0����Stimulus Antenna  @0����Stimulus Gain @P  	 ' (Amp @FreqSettling Time(ms) @
Nominal Dwell @
Data 2  @@ ���� ,[Frequency] @@ ���� ,[Time]  7 �       DwellListbox.xctlData.ctl %@P  - .Dwell d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P  0 1 2 3 4 5 6Freq  � �       EMC_MT Test Type Enum.ctl f@ Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)	Test Type @0����Limit File(V/m) @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  : ; <
AppPwr PID  @
Max RF Level Change @2����UUT Result (rel)  @
RxAntenna neta  @
R Factor  @
SigGen Freq Chg ^ �       #EMC_ML Controlled Variable Enum.ctl 2@ 	App Power	Fwd PowerControlled Variable @
Offset (dB) @
Tol (dB)  @
Initial SigGen  @
TxAntenna neta  @CLF Ave Cnt @Step Used in ACF  (@P  9 = > ? @ A B C D E F G H ISeek  @2����Seek File (Rel) @
SigGen Offset @P  K LTest  @0����PV File/Level (dBm) @
PV Tol (dB) @P  N OCal @
App Pwr EStop Offset (dB) @
SigGen Max Offset @0����Cable @P  	 SSigGen  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  U V W X Y Z [Timing  @P  	 \ArbGen  @
Fwd Pwr EStop Offset (dB) @
Numeric @@ ���� _[Probe Ranges]  @0����String  @@ ���� a[Probe Names] N ��8��   EMC_MT Calc Type Enum.ctl ,@ 
Rx AntennaRF Probe Ave	Calc Type @0����Fwd Gain  @0����Cal Set @
Freq Dwell(s) O �       "EMC_M Network Analyzer Cluster.ctl #@P  	 d e fNetwork Analyzer  @@ ���� aProbe VISA[]  >@P      " & ) * + / 7 8 J M P Q R T ] ^ ` b c g hPrefs    P  	 d e f   
Rx AntennaRF Probe Ave  P  U V W X Y Z [  !   	App Power	Fwd Power  P  : ; < X  Unit Cal (Search)Unit Cal (Network Analyzer)Unit Test (Search)Unit Test (File)  P  0 1 2 3 4 5 6 *  LogLinearFileSusceptibility Spec 
 P  - .  P  	      !   Incremental
Continuous  @!Incremental @!
Continuous   P        P         � i i j h a k f e d 	 g l c j b a j ` _ ^ ] m [ Z U X Y V W \ 	 T S 	 R Q P O N M L n K J I H G F E D o C B A @ n ? > p < ; : = 9 q 8 r n 6 5 4 2 n 3 s 0 1 7 t j . , j - , / + * ) ( ' 	 &  % $ # 	 u v w x !      	 "         j   
 j  y       	      n  n    z     � FPHPEMC_MT Prefs Cluster.ctl       TDCC   EMC_Test Header Cluster.ctlPTH0   #     EMC_Test Header Cluster.ctl                                  �PTH0         TDCC    BWFreqRange Cluster.ctlPTH0   1      Shared	BandwidthBWFreqRange Cluster.ctl                                 �PTH0         TDCC      EMC_MT Test Type Enum.ctlPTH0      EMC_MT Test Type Enum.ctl                                 )PTH0         TDCC      EMC_UI PID Cluster.ctl PTH0   &      SharedEMC_UI PID Cluster.ctl                                 PTH0         TDCC     DwellListbox.xctlData.ctl PTH0   %      SharedDwellListboxData.ctl                                 1PTH0   .      SharedDwellListboxDwellListbox.xctl TDCC    Freq_Spec Method Enum.ctlPTH0   =      SharedFrequency SpecifierFreq_Spec Method Enum.ctl                                 6PTH0         TDCC    Frequency Specifier.xctlData.ctlPTH0   ,      SharedFrequency SpecifierData.ctl                                 �PTH0   <      SharedFrequency SpecifierFrequency Specifier.xctl TDCC     #EMC_ML Controlled Variable Enum.ctlPTH0   5    
ML private#EMC_ML Controlled Variable Enum.ctl                                 �PTH0         TDCC      EMC_MT Calc Type Enum.ctlPTH0      EMC_MT Calc Type Enum.ctl                                 �PTH0         TDCC      "EMC_M Network Analyzer Cluster.ctl PTH0   1    private"EMC_M Network Analyzer Cluster.ctl                                 PTH0         TDCC      EMC_ML Motor Radio Button.ctlPTH0   /    
ML privateEMC_ML Motor Radio Button.ctl                                 �PTH0         TDCC      EMC_M Tuner Motor Cluster.ctlPTH0   ,    privateEMC_M Tuner Motor Cluster.ctl                                 �PTH0         TDCC     EMC_Timing Generator Params.ctlPTH0   '     EMC_Timing Generator Params.ctl                                 �PTH0             R          (�5  (  �  5                                       String     H     �   =�L2  =  �  =  1     ���                               X         "�/:  "  �  /  :                                     Probe VISA[]   H     �    9�F�  9  �  9  �     ���                               \     D    �`��  �  a  �  �                                     Network Analyzer   Y     D     W G d �   W   H   d   �                                     Freq Dwell(s)      H     �     U  b @   U      U   ?     ���                               S     D     ; R H u   ;   S   H   u                                     Cal Set    H     �    ; 	 J K   ;      ;   J     ���                               T     D      j + �      k   +   �                                     Fwd Gain   H     �    " 	 1 c   "      "   b     ���                               P     D     	 R  n   	   S      n                                     Name   H     �    	 	  K   	      	   J     ���                               P          � �� �  �   �  �   �                                     Pane   L          �`�g  �  a  �  g                                          U     D    �a��  �  b  �  �                                     	Calc Type      L          �a�h  �  b  �  h                                         H     �    �b�p  �  d  �  o     ���                               H     �   � ��Z  �   �  �  Y     ���                               R          ��)  �    �  )                                     String     H     �   � T  �    �  S     ���                               Y         �_��  �  `  �  �                                     [Probe Names]      H     �    � ��  �   �  �       ���                               S      D    >        >                                     Numeric    H     �    !G        F     ���                               Z         R�    S    �                                     [Probe Ranges]     H     �     �     �         ���                               e     D    ���!  �  �  �  !                                     Fwd Pwr EStop Offset (dB)      H     �    ���  �  �  �       ���                               R             � - �       �   -   �                                     Timing     U     D     A c N �   A   d   N   �              	                    	Amplitude      Y     D     R � _   R   �   _                                    Amplitude Vpp      H    �D     S e ` �   S   g   S   �                                   R     D      c ) �      d   )   �                                  Offset     U     D     - � :   -   �   :                	                    	Offset(V)      H    �D     . e ; �   .   g   .   �                                   [     D     
 �  �   
   �      �                                  Gating Enabled?    Y           � '      �   '        
                        =   Enable Gating      f    �D        �            �                                  Arbitrary Signal Generator     P     D       ) )         )   )                                  Freq   P     D     0 c = z   0   d   =   z                                  Freq   H    �D     .  ; O   .      .   N                                   W     D     A  N K   A      N   K                                  Pulse Width    W     D     R � _ �   R   �   _   �                                  Pulse Width    H    �D     S  ` O   S      S   N                                   R     D     . � ;   .      ;                                    Gating     W     D     1 � >8   1      >  8                                  Gating Freq    H    �D     / � < �   /   �   /   �                                   V     D     B � O4   B      O  4              
                    
Duty Cycle     `     D     B3 O�   B  4   O  �                                  Gating Duty Cycle(%)   H    �D     C � P �   C   �   C   �                                   P           " 	 / "   "   
   /   "                                     Pane   L     D       � - �       �   -   �                                          P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          $        $                                     Pane   R      D    �$	I  �  %  	  I                                     ArbGen     Q     D      j + �      k   +   �                                     Cable      H    �    " 	 1 c   "      "   b     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          $ 1 1  $     1   1                                     Pane   R     D      �- �      �  -   �                                     SigGen     ]     D    FPS�  F  Q  S  �                                     SigGen Max Offset      H     �    D	QI  D    D  H     ���                               e     D    .P;�  .  Q  ;  �                                     App Pwr EStop Offset (dB)      H     �    ,	9I  ,    ,  H     ���                               W     D     % [ 2 �   %   \   2   �                                     PV Tol (dB)    H     �     #  0 T   #      #   S     ���                               _     D      j  �      k      �                                     PV File/Level (dBm)    H     �    	 	  c   	      	   b     ���                               P          � �� �  �   �  �   �                                     Pane   O      D    ����  �  �  �  �                                     Cal    Y     D     % [ 2 �   %   \   2   �                                     SigGen Offset      H    �     #  0 T   #      #   S     ���                               [     D      �  �      �      �                                     Seek File (Rel)    H     �    	   c   	      	   b     ���                               P          f    g                                         Pane   P     D    7M    8    M                                     Test   \     D    6 TC �  6   U  C   �                                  Step Used in ACF   H     �    9 UF c  9   W  9   b      
                               H    �   6 E >  6   	  6   =      
                               W     D    # G0 �  #   H  0   �                                     CLF Ave Cnt    H     �    ! . @  !     !   ?     ���                               Z     D     G �     H     �                                     TxAntenna neta     H     �    	  @  	     	   ?     ���                               Z     D     � Y � �   �   Z   �   �                                  Initial SigGen     H    �D     �  � E   �   	   �   D                                   T     D     � [ � �   �   \   �   �                                     Tol (dB)   H     �     �  � T   �      �   S     ���                               W     D     � [ � �   �   \   �   �                                     Offset (dB)    H     �     �  � T   �      �   S     ���                               _           � V � �   �   W   �   �                                     Controlled Variable    L     D     � V � ]   �   W   �   ]                                       H     �     � W � e   �   Y   �   d      
                            H    �    �  � @   �   	   �   ?      
                            [     D     � Y � �   �   Z   �   �                                  SigGen Freq Chg    H    �D     �  � E   �   	   �   D                                   T     D     � G � q   �   H   �   q                                     R Factor   H     �     �  � @   �      �   ?     ���                               Z     D     n G { �   n   H   {   �                                     RxAntenna neta     H     �     l  y @   l      l   ?     ���                               \     D     R j _ �   R   k   _   �                                     UUT Result (rel)   H     �    R  a c   R      R   b     ���                               _     D     ; Y H �   ;   Z   H   �                                  Max RF Level Change    H    �D     <  I E   <   	   <   D                                   V     D      i + �      j   +   �              
                       
AppPwr PID     M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P           " 	 / "   "   
   /   "                                     Pane   L     D      i + p      j   +   p                                          [     D     	 j  �   	   k      �                                     Limit File(V/m)    H     �    	 	  c   	      	   b     ���                               P          �f�  �  g  �                                       Pane   P     D    �*�B  �  +  �  B                                     Seek   U     D    ��	  �  �  	                	                    	Test Type      L     D    ��	�  �  �  	  �                                       H     �    &        %      
                            H    �D   �    �           
                            P     D     k8 xO   k  9   x  O                                     Freq   U     D     � � � �   �   �   �   �                                     	Spec Path      H     �    �  � l   �      �   k     ���                               Z     D     x u � �   x   v   �   �                                  Stop Frequency     H    �D     y  � a   y   	   y   `                                   [     D     d u q �   d   v   q   �                                  Start Frequency    H    �D     e  r a   e   	   e   `                                   V     D     9 F F y   9   G   F   y              
                       
Delta Freq     H     �     7  D @   7      7   ?     ���                               U     D     M � Z �   M   �   Z   �              	                       	Freq Path      H     �    N  ] c   N      N   b     ���                               ^     D      z  �      {      �                                     Freq Select Method     L     D      z  �      {      �                                         H     �     
 |  �   
   ~   
   �      
                               H    �       e      	      d      
                               Y     D     ! F . �   !   G   .   �                                     Points/Decade      H     �       , @            ?     ���                               P           o_ |x   o  `   |  x                                     Pane   L           k8 x?   k  9   x  ?                                         Q     D    E R:  E  !  R  :                                     Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          IjV�  I  k  V  �                                     Pane   L          E R'  E  !  R  '                                         Y     D    ���  �  �  �                                       Nominal Dwell      H     �    ���  �  �  �       ���                               a     D     S� `   S  �   `                                       FreqSettling Time(ms)      H    �     Xq e�   X  s   X  �     ���                               Y     D     7 j D �   7   k   D   �                                     Stimulus Gain      H     �    ; 	 J c   ;      ;   b     ���                               \     D      j + �      k   +   �                                     Stimulus Antenna   H     �    " 	 1 c   "      "   b     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          � � 1  �     �   1                                     Pane   O      D    � �� �  �   �  �   �                                     Amp    Y     D     o G | �   o   H   |   �                                     TrigOffset(s)      H     �     m  z @   m      m   ?     ���                               [     D     W G d �   W   H   d   �                                     Capture Time(s)    H     �     U  b @   U      U   ?     ���                               Y     D     7 j D �   7   k   D   �                                     Rev Pwr Cable      H     �    ; 	 J c   ;      ;   b     ���                               Y     D      j + �      k   +   �                                     Fwd Pwr Cable      H     �    " 	 1 c   "      "   b     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          R _ 1  R     _   1                                     Pane   U     D    N �[ �  N   �  [   �                                     	Pwr Meter      Q      D    � �� �  �   �  �   �                                     Motor      S     D     ~ Y � }   ~   Z   �   }                                  Control    W     D    ��   ?����         ?                                  Incremental    W     D        N            N                                  Incremental    V     D        <            <              
                    
Continuous     V     D       ! K         !   K              
                    
Continuous     P           � 	 � "   �   
   �   "                                     Pane   L           ~ Y � `   ~   Z   �   `                                          R     D     f G s q   f   H   s   q                                     Speed%     H     �     k  x @   k      k   ?     ���                               P     D     N G [ ^   N   H   [   ^                                     Step   H     �     S  ` @   S      S   ?     ���                               P     D     6 G C ^   6   H   C   ^                                     Stop   H    �     ;  H @   ;      ;   ?     ���                               Q     D      G + `      H   +   `                                     Start      H    �     #  0 @   #      #   ?     ���                               P     D      j  �      k      �                                     Name   H     �    	 	  c   	      	   b     ���                               P          � � 1  �     �   1                                     Pane   L          � �� �  �   �  �   �                                          Y     D    � G� �  �   H  �   �                                     TrigOffset(s)      H     �    � � @  �     �   ?     ���                               X     D    v G� �  v   H  �   �                                     TrigDelay(s)   H     �    t � @  t     t   ?     ���                               W     D    W Gd �  W   H  d   �                                     Stop Factor    H    �    \ i @  \     \   ?     ���                               X      D    ? GL �  ?   H  L   �                                     Start Factor   H    �    D Q @  D     D   ?     ���                               [     D    & j3 �  &   k  3   �                                     Receive Antenna    H     �   * 	9 c  *     *   b     ���                               Y     D     j �     k     �                                     Receive Cable      H     �    	  c          b     ���                               U     D     � H
 v   �   I  
   v              	                    	Ref Level      H     �D     �  3   �   	   �   2                                   S     D     	 �  �   	   �      �                                     BW File    H     �    	 	  �   	      	   �     ���                               W     D      2 # x      3   #   x                                     BWFreqRange    ]     D     e _ r �   e   `   r   �                                     Sweep/Hz or Sweep      H     �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                     Video BW   H     �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                     Res BW     H     �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �                                     	Stop Freq      H     �     "  / X   "      "   W     ���                               V      D      _  �      `      �                                     
Start Freq     H     �     
   X   
      
   W     ���                               P           ) 6 6 O   )   7   6   O                                     Pane   L     D      2 # 9      3   #   9                                         P          =  J ,   =      J   ,                                     [Bw]   H     �     %  2 (   %      %   '     ���                               P           9 6 F O   9   7   F   O                                     Pane   W     D     $ 2 1 n   $   3   1   n                                     Test Params    b            + v         +   v                                     TestParam (Freq, Test)     H     �     5  B (   5      5   '     ���                               P     D     	 �  �   	   �      �                                     Name   H     �    	 	  �   	      	   �     ���                               W     D     � H � �   �   I   �   �                                  Attenuation    H    �D     �  � 3   �   	   �   2                                   P            1          1                                     Pane   N     D    LZ    M    Z                                     SA     V     D     � �    �   �                   
                       
Test Title     H    �    � A �   �   C   �   �     ���                               R     D     N � [   N   �   [                                       Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �    P�� _ !   P����   P         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �    	��  !   	����   	         ���                               P           R A _ Z   R   B   _   Z                                     Pane   L     D     N � [ �   N   �   [   �                                         ]      D     � \ � �   �   ]   �   �                                     Show Diagnostics?      P           �� (��   ����   (����                                     Pane   Q     D     �� ��   ����   ����                                     Prefs      P                                                               Pane  4� (zx��]xTU���Lf&���@ ��(��tB�DA��L3QT�,����*��ѵ,X�.��bY���bKd��ϻo��w��̈��o���$�n;�w�=��s�s��#B�=�Q�w|XDEc�=�R[�~|ȗ�$S���9�$�7x:�	1G�s� B�>����<��rMr��ncC|����+�I�b���\��Ev����h;��1� �q��2�l�B�Ȇ��|.9m��B��V3B�bWq��[˒ZC�qyny9���T�ǥ�����Tr��.���=�t����������Ie�A��ރ�2^*�7�����L}��g�KfWP�>�0���H�+�t�ƥ��e��͆Z���
�W	~��0|��o������jH��S*sysw��6zў>.�[���)k�6�{9bO����u�ЇueEZX�C�F��͍=�2�P��Z@<��@����
��r�4�.܂;辤����?�R]��Bӡz�H�P���p�eA��f��2�%a�{hL�
�g a>�����A�Ъ�&/r7�h|?��~����23���#P*Z-�d<E><��Aw9�D�s�Y���A�9R��B~�%4��U�r�4����&�]K��n=%讐)���OCP��p>Fj�#�F��&wAZ��z��������� O�W�&���a�.vu�:��f觯��:xJ�iwV��U�b����k��4������
b�.�E�P/�y��nF<���ZE�r���욻�P����F�ʚR���Z��2�PW K�FG�J>�#��R �����ر*C�{([���:5XH�z��<��r��r�.� L�54#G��Ix�C�j9����hFI�T{���r�H��#���+�׌��=�
�j��\������=-3����?���TB3i̤\��N-��̙a�"��cZ�OR�pA�E����?�TqБ�����>�~\���dE�;�ii\<�>8Q�~�,����qLn�d�C�;񼜑yM5�iN4�N�vZ���?:�IR�^�p���r�dLG�A|E�{�"'�-�D
J�����o[ '䧭�k��j�*+��Ӱ���Ѱh��&�)�p5:���0��Ko�=�*����3`X~<,�d:I��Oj"����s��s���(��%I }ndP �hC��b���mL�1x�8"�[|��)�;&\s ���2QE����4��Z��1Ά�2Qk�$r��2Q��c�-%xP�.�Al�(�C���Md��} Pl��!�pY�%H
!�+���s�=�4 A�ߦ	�u2M�~6r�� m	AƆ!Ƞ�"�b� gA�{d���c$Ef��G�찙!����Z;>�L��Db+dGl���Rt�Z�MG'i��V��
�P�m5��Ye�)�Bv�V(�>�q���]���-KEI~�kCՕ���J�z�(8J͎]@��s���P�tR�W=�=�)�dsa�D��ݽd*�2�L��H ;�u��5��9�C�uk�a��ĺ��j���]��0���a�s �ɧXT�/�`֧�<�����-eW��3����z0@^��h�2�Է���y��w`�G�B^��O�l��6�4�۴瞧b��+�@����ߦm ��T�ݒ4��������ܓ�1y�J���@UUI��񾙵Ջ��+t�)QV&�"-����K&��ƒ�5�b'��J}_�Y�e�b`�xX�X�`�����c�tB����9(�Cl��	�� h0�"Ҝ�}�]��"4;�B�����V�{)Y���&CQs���j2j�?������8a����ո*���3��d���:Є��J�W��mN����騀��8!}5��������?m��a�G�j�/�|5�OWc_����FA�������W�D���N�k}_Mj�?�j�1�դ*}��j���j8���j��Y�j�&�e^!��J ���/yɂ�=���WMe� �T���Y�n�_�˿щ���	����ҿ��m��F���Mk�f�T%�7���[𷣵ؽ+�: ��Z-֙h���K���qi�l������LL-�ˆ�2�bŨŲjI����bY�Z�1K-�6�b�j�����	}-��gP�I3-�Q���bO�k���d��^��Ŏ� �e�C�l&o��
ym:������G�A�EQ`�WIX�o#����׳lLe/c��@#@K���>J�uU�+�jS�򢾀�S��ud�E&�lVϘ��"K��T��/��Y�VcE���@�FCE��c�ȸ��3����&���/r�&޽bq�»8"b��c�5��X�:�3x/ڈw�ذӈ�-�X��R�"�5sw��ٽ�Ո9���&]w�27ـs���ɸ���<�}qY�˅��)���?Jħ�R�2<��H0�B�%�����A�4��4�=Y��,��k������U%�_�ͫ��,	��J�%_y��.P﫯��C�:x�z	|�?�ߗT��o*KV�WF��*�+�롬
�����;���0���s/ּ��W ����;=*�ӈZ(�v���LC�jyZja*�B7EH��$�[��d��i55���Hkj�P��A2�����w�(RZȌHȎ��������\���[?�P}�F��,R��˒.}��|'h��n�W���\�y*��h87��R\B.U�ԑ������Xt��lf��h,�	D��w�Œ���6�3�� �;Ҭp�V�e����`�;���KX�t�
�0+��A�'��XFX#,�f��+�3@�H
(�ce.��,/Yt��4=�0#���!QD3��=�z�');��M��i;��dqE�7����j��I�t&i0�)k���	B�<�Vf�;�(�ʐW��*�-��B�"�"�G�zW�����~�*V}��zY@��؋�2̌࿠��h���>�'Em9JO��_� ��������_����d2�*�����-eM�^'e~�$�MP�e�ξG4�j���SLB�'��>�(�
�h�8|����^位4���h��k���D>���1IX8�f�V�q�V<�B8e��x	2L�8�xc!G� ���)�Z�Ǚ��q�-���x�$U1�}au.䥆y������i^(���jy�7���z����B���/���
��O,+:��08Vȴe2��d�f���%�y#�pƟ��iV8ɔ�^T�e����LY��ڣ�ZV�GX��=��¿vz�П���)/� ��xx�dS^H���������n��+6x���0�x�h!/-�9^�B�B'��-/^�l��/�^�/d����3�v9�����2CN�0˖b�`���!*�ӭ�h�9�?�(��)D%�8�(N�W?����e
����@���O��Ow;zI�����_��Ow{���f�^�g&�*O2ݳ��Ի�A��T����0`��O�oШ��@W�3]c�� �*k"��AO�9xDB�f�w�tE �c���^�����ۋT������4�Hu�+[kǏ=<�E��.��/����Ő/����4#�.��˵��5*�m2�˵��5�O}��1�^+�k���V|�/�:f�_fа[b;�Je��{�~2�*S��Ĉ/�jc�I����"C��j,���fn[	��hy�3ٔcfHeƏʞ���M������@׫6�fA-�)׍"kF�&7G��T��3B��;��6C���3�-n��Y<r�=��Ν03�x�N�)�M�W�bo#�0����A�G~kF5��},&�6R��q�3'�P������ *�E��=i&�
�ϱ�O}��}�T�H��7��6͎Ah��x�Q���f���Z>E���#1:s��e9?�d9�C��w�~$�Do�ș���N���B9�,�u��2��Zr�V�19�q�L�9c8#$��	��cL	�L����L4�i�m�9�Z%4g�U�\C�L���s���0�I833�݆��.��b�� ���oۂ�/c)pᅄD1RB���k�8�^��8�v(N�2�K(��8("������7aK��bi���M�p��Yei΄Z8:*]�D1D�96�r-C�S�ÿb��Ѧ�xx�Iȸ��������@��C�wc��Y���sĜ�����R}0 ^����n��:xQ�:�����b[L$����W��S��gE�?Ѭ��V����A�����?z���gC����PqzѰ�h�:�}��}8?��A�_#�ksu0���:(ر�X�n/���Z�9��[����hu�~g����r��֫n+��7�S��� �|3jCT�G$s{��ܫ��BGB5� q;���=X%e�^�J�^���n�Rv������C%MUd?�7!U�FB�$��VQI�?cWI�>k�$zB��'���U�` f�)�O�A}d!��j��4E+9�m����k��
>�T�����(�)jjk��d��Z|7�z�]�)��P?-���t��[��5��Q!CV� �E��9˸j���t|"����g�����n0ey�a���0i�.E����$2z��Ӭ�ӟ��欞��"����s|T#e��^T1�%��ڲ�$7"�|%�b���\	n��51H�E��fL6���,{�����p�3�Y�V{��f��?2�Y�o[�,{m�l�,��l���͒n!��ݏh5�LbudذY��,�V6�;�f��6��YÜ�VF�,��f��mް�����%��ʌ�J��L
�|�7c��V\"��ތ�G���.�o�����um�n�:��ö�iԂ7�{����!a3$a3�8P(��*���_C]��W^��0vYI��@�olIE��ڒ���*���}��+��������}T���˦d�}ȇ;)��ȕY���䣎�HV��@3�"XDe�"7�>���r�p&�;�}E���\�e!.�r.��r�T&��'�W �7P�?�T W�[H �6 ��T(�V(W$�6Xne(��ٿ �����S�DSdᤄ�G�66�D��f11��K�&p�f�4����geF�v�T�U�ˌ�$>7sns�̮���!�����Wv�f��a�%��)���?./�ʐ9
&�G�ٝZ�b�b�8�&�Y�<�� �?��Y��9��_l�����D�:�%�W�4B^'�ߗF�a�F���z?�+�<�O��%&z?o�~z����J@�z��F/���}3�s�|�S�`�;{�oB�* 
�Z
��n��i��"`K��T-u��|9��������()]�/�`#`Im�܆@U����'�eɬ�-Y$��Q �S>d��E�j�jO'�p�*�p�}8��1R�)͎g���ݑ ?���1�̝��J�9M�-;�׼��X��H��Q\l�� �Vg�� �V������f�U�����]�-6<xk��|�v��o�)E��ַ2�����5!%-mB����Zg�����w�6$�h@�b��8���t���<�XF�Z�IV>θ|�����;!�%���M��-�\�n��2���.���y]�Dx@�߳Ɉ�e'���8G��*�P��w��y�}��S�{[΃��6�8)���y��]S�Y}^��=��2���j}�6L��X�?��g�t����?�9)�����26���iEM^�"�Ro�G{}a32���f�@ʌL�aÌ�KkF.V0�z��U80�U�b+�TD��'ԊL��Z���݊L�׆� ���xڊL
��n��Vokc�^��aeD�x�Z�Ȣ85!��ӛ�k���Y���c���Z����"�-�V�l�����`
���m�W���%
z=[�B�R&�k���	�^ϵ��^�u���s�����J�F� 3�%y=�P
���W��4�����-C_�_��m���K���.l��U<��ױ�/p�8|a����_����v����Y)Ju����0�B����˚���hFm��ܰ9�~�����ӡ���aN�֚������w0��Qd1{䄚��jN�?�nN�?[�ӡ; ���g��� �ٌR�H:�k��C���m�x�
2�.v�[�N��{�pt�?p���`H�������{�B:�йh+���">fG�
��6�uƗ�G�Jjj*ʁ�}e������+R��@m-|�8P^ P�/�]�ǉ@Jjk�(85H��גB1����-Xj.��B-KUW�Ke�����_7��np�9��8xi�5/�h넖���)��8)��m|㴐)��,�-�h�~�vd��s"�`xgj�WM<���^q��tB���|B����&� �( ��"
x���yཤ�icl�ו6��N�p����\�O�X-A�g'`��X�}��� Xo �(�2���)Kc�ĭ#�_f�� �^a��\?7]?Z`Xm�Y$n:�)�>N�c��(]����P��H܎�:��č���>sR��\u`l�j+˫��@]MuU] ��}�+�u˪��{c����W�W-�4[�+Y���U���
����e)p*�f�EH�dWVjs3Փwr�&=ZtH1��0�Y湙�Q�qn��Z��	�m�df
m ��q�:3�Q3j+�GѩK����]kv7(~��'�PH�|G��lNU=6�����b/�)�w���e7S$���ˉ$�+^�
b�݆���wC�}��%�5�����$Ξxa&JgY�����3�5\�*����n%��ϵ
��Fb�_k
9f*>�h�Qg�Ϭdְ{��T�Y����j�먷Z���ϧt�2$G����x/����3,�q��Ĳ|Iy�uX�$PZ_'�bI膳Q����}�� |�M�꫃*u�}���5_MI]���ڀ�_R�
���Jj�Ƕ\U]|-���T^���� d�1���,@@��E)Eot��d��փb�V'@`	��N�[U^/����,�Ԗ.�ՙ�e:X�յ�(iH{�����)�{U��U��
/���꼐l��g^�N
/p�/p��^���:)�@�D�fv�IlanS��l�{��#��Ӂ'��b����~�2�E��:d���~J]+��e7\������{���$���N�u�:�H|����~�?�,���1��˟M@�g��X���;$�����	Z���gųCm�CRi�C�/�;$E�	_eg����O���<���M�>	9�ɗ��'A١h�X&퓐b� �4;%��L�����C,��Fo�a\���C���m$?;�q �]B���Zx��W��n�98F�׼M��+�A�q���6<.x��b �5�vM�Ưw�L8n��>��z��P(�׻��?�ľ^�}��8^���x^�K\�z_�gO���tD�[�,�Q��Ť���F�n߉#�ZZ�(>�@BF�p�%f<�}4���'ڈ�~1�C�@̖�K"Z���LJ���L������q�^�l\m,!-�����K�����b�V�Ӳ[l�蘖��\wg�)9��P�����Q)d�Q�p��J/�[�� C������Lo��	7;N�	y}�:A�EiG�`��g�Ip��'�t��%�&`/A.7�zi���*X���"�}qC]i���|qyEy�����u��h�-���H����.���-:WЀ�����r���8��B�o��/����d�F	����S�H -]Nm�Ĵ�$����:B�8,��^@�b߰����m. �+������ѭ��M�,���[��h�a�Nf{��B��>�>;�f!�U��h�3����R�d���X:�y�E�E��!�Je.��i�`(��4��K��dtP���BC��#��L�|�3[M�Q�7�T��{X�Q�Б�W��ڊ���7����o�S:R7���P��A�P��q6ԂA{�Y�#���xV�Ȭ7�JK�8�A�:/�jb�?av���G<����c�_,�#@}qLj�"ϟ���P3O˼�hj�Y���j�YP1�nw3rZ�0m���(��f�wKO4����+��0�#��T�%�:X6i�l;΃<Se*��YH��	�r&$P�F��Z���d�j���!<&�� O�#?m��ݖ&�'�a��ѰJ�W����r�8�uB�`L�����6�Sn�M���욻��ġ�*kJɪ%�߯�f�^�
dQ�1]Qo�<]��y��4lX�ԩ�B���1��ȉΒ+K�gu�%A@ w� �D�3��K�Ȋ�@��պ������S/����꿧�����2��C��EV쫺�t��#щ�
y�\Z'^�N����bԉ��x#}�j:�§7%�N�91u�-���š��N,�sh�x���KG'�Ht�Z��i���N���g}b��KS'n�C'n�t��xt�����+bԉWƦ��G'^�N�bC'>�9tH�*��h�ˤ��/7��O6���B�����)����Pq�eXm���e<�9�i�9���2^����Rf��2=���A�l��<�0%�^����o����_�S��/��ƺgX�/��aNf���|ʛݥy��A����aA%	���M�JۈU�kc�;�C�h��0h�D̛��f�F��a��a�0��1+N����0������7;$qOTd�^��l��W� ;\���=��	y���4~ϳ�����:�w���l�n�������w�q$dnU���\�nw�5~�f����2�D��pM;r`}���7���7t�Qݛ�N�?~-�Lu`|��w*ϧt�G�
]7H��ݥ#���,�=��r�24q�Ba�����<i%�㫥�uU�*'5���$I�� �S:$�C!IҦI�]!I���I�t�=�L�,AwS$A_KQ��3)���b�h�s�B�k,ǵ��
I�V�結=�I��\5��Kk��.�B6�ɟBS��XI�E�&��DOV���&�}���48��lj�O��5:���M�|�Ь��Ϡl�@� ��:�x�2�}K�|�t M�&O��N��2�C;��=�
x㉺�������߁/b���:����#>�1��y���&�Q����ƚ�6�������6}tc���l�a@�{ӈ__(���_���*|yB�F�9����"�@�Jk�(�)	棚�Ԃ�A���!�Z���n���
~=9���>(_B���-!����:�{đ\0��u�!�o&Z���,����ZD���=�eci2S��Wl,����0��ac=`���l���2;=���zq��Iv�d'Y�\#I$���d�=���|�n�H��������C1Z����h�?����-v�����a �^K���%�0�놖�N�
=gxA�^}K|T܌vV{��_}n�}�[�웦��*�_K��g�_�ֳ�Ϣ������O��A��KK�b���9�֡ǌA��K| *�e�F�N��邿�Ν�Y�!njܫC�G�19x;q�w$1	�+=�sp���q�
�ȝ��zˁ�_��C���@Rx��0��鯌�7��������9�y��<%��z���S񖎻qy�ϵ�'�׈G���bU����]
�����7�$U@���I�A�x,<ĶAǽ�a���fRUim�2PU_RA��IZ�r(M�N�,Q�Z|\�z�I<����(���2d�C�OnX.ĶN�r��CbǄ>P}~_�� ����<�Nzp?懔�V�b����	`�*�' �o�n�%4�o�A�:~��L�6�c���˫���;Q��O�n�%�L�;mw�o�^&nt� m �-=��"07�)�+:�Ha9n��s]�b_4Ű�Mfz�+������7�f �*�k�����s��==WF��J='I��tv��&s��2s�H�9������s'Iy�Tz�-5)�`�����.04D��b�8Z��v��Km��_F_�~��]4�@�g���������*kZǚ�\j;.Iȥ�cm�,���Xj/��*z�{ -P�~���
�r�6�@��	~+�;��JUb�:1�u�{^��v\�; ��b�F�߅8��0����oI��}��wq|�}� �41�[����~�ā����C��98��p����l��,��H�;/>��!@9=1�{Fb�w~��wA��k�+ �W��j��
:F��a�M'l4x_R����1��_&�����-'l�,��
)�N���ol�W��.Pס[�X��f��1�u���_��/P-���3^��}L|����F������Z��d.i6��o0\���:&��x��E��7QTv#*�����1�� ��e`�a2�.�����G��v[�t�,�)8��q�D5��nP��|g�s�7��Q���~��HE�!���0��1�Q���6~]|k��%�7&�n����m���0�K['�bCa�az�xm��(�+��˅};#�_�n��(����o��_Wj|�ݫ ��MH����6s�k���ǎ_W���~/Dt_�};�����=����"��A�����M"�V%$~���E�#/�C����ߩ��2��^s"����4~Sm�WO����}|��G�����픘��n�����cC��ߩ��%k�9@��Z����6~R�o)k�!1���V�24��q����u�����-���]PO�ĵ�I��!ɿ��ވ�$��f�$q��IX Igt��$ ��⬈c�J��}ҹ��-@cQ{T��{�"�t�b��.V�������\�Ej�~(���δL����`�z�ۼ3��Uj�ц����6Rco�E�ԩ�g�r����̥8ȯ$A<α8��Y\�M7��.q+��xTA<��.�9��$��טGO^>։~|W�$�<��N��{
��gXq��1m�"[�$��)~�ı�"F��]�ק����%��h-��҉qREk���&���V��NT��H�>��z�o��C\~���W�zT�տ ށV����)��_e�࿈^�����A�>�#G�'��cXZ���3W�4���JH���sB�F�_Zgi���it���k�a��������%v�������/Ӂ��������71!��Ą�ց�7q@�[��n� �!��L��i��d���Z��@�P�>D��qbB�pbB��ց�q@�3N�YQ~;X&������l`x��?&~'n&`ٟ�>��~�u0�^>h#)�Q�p������8�f`��/���:��`xO��������+���W[ïŁ��-09��!�]�J�[I:�����mE�|F���a��3h%z�(����~��J��=cX�)������Fܧ~Ԉ�iӨ��R�Q#�glF�\(]����vB>h&.®�N���d.�,�F`(FQ#��̽&k�K��{�Q#�}����r�A:��o�H��::�
��E�RNM�P�B؎,|8�*ߪ#'�|�_*���K�|��n	�o��=�|����[u�G�UG�e�U�5��Ϸ��i�o��\Q?q��d�|�+"�T;�,��
�O�|�����V7���:�X�[?۪F�0:������*�+����P%���3N#��ߩ���C�G�)T�a���C���>�U��J�.�k����m���Q��JZI�v��T%}��B#tT��d.W[�$��Jⷙ���P�x����'�Q`��S�'ʣ��X��|سmdV��Q>�Q��+���aK�S>�U�<�8!�k}��5�[��a-R>����e�S@��0S>R�|c��V�*�0��[e�|:0���Z���W>E�3�Vc�|R��U>��ʧ=(�W���ʇ9ӆ�i/�g��<�á",��щG���|Og���'��� ht��h���P3Ф�r bf}Ԍ�B�d)L ��Hih��'�Z'N����$�q�*x�yB��	�`�b�`�وx^�7R��gpu�����h�Fӗ��eh9��\�MP�^����L.�/Telpu33����Ww{�Wf���UE\��3�2u���\2�.i(�WS����66�:�0���
�+��>����f�	Be=�p��8��ӓ�?(����!k����	'VY�ڻ�OqU��ڏ�o���Jk�`��a}���\Iő������ ݫ��D�o'�Ĺ��n�|����\���H\�U�I�wFE��R4ɵ�BT�&��͂Z0�⥴;,�FLލ:3<��|W�c�~">��q�{���?E\��Pg*&�����)@����N^+M��:d��l*�c�+�v2�B�!�{Gb�($%�$��K�ƦB�ﻩ��=������m>E����_ �������M: �U�k����Y��3_;�E6Q�K���>2oxO�\F��YYt�����w��"��+T��F�1fp�J���L-�W8���9���`T��	���1x�Mw4��%��5�a�3�ѰG����f��:����t~,
�؈Q�F&*�����]��vx��f��-�[��M*#�������Ӭ�Ӌ�OsVO/�(�,���$Ϙ8U���"K:)���"�MK��7�1��Z`П�F�j�|��O���f��6�s�{	{>�;�g����ɞ/$&{�{��s͞/����߃=��`Ͻ����A�=��xD~����9<X��6��E�9"����=��a��^���G�;&�Ð�o�G�]�0�8��#�*ގ�V�i�Gv5�y;�S�e�|�t��j�_��s��?����]���u� ����w��j�ɁNo�1,��m��d0#���o�Ȼ�i��'[�E�6��? -@{�<��rF�Ռ:������R���9�$�hq�RA�du�MH�J��U��ݙ�օ:����v�JEGLЕ���e��4��O�-j��նJ���[��IY�I��<�`��5"ԥ������l<Ͷ�i���*��c0��0[j��ۏ��6��
c���Jh��d��)�t#Y��@>!�;ܰ�`����E "�@Jj("P�n(�P^�t��>v��ך�����m�l���q���1���I@�o,�@�\v���k�k�$~ğ�v/k��֊�<.�H�`�2ZE�t�X�0�L�k�B�>��R�ԶV�i�(Lt�.1����i�gw��.u�(l���5�X�n��t
��Cx�[ �~Q�C�*e���؞J�=S�/���hH�M9���_C����a��        I    ( BDHPEMC_MT Prefs Cluster.ctl           |   �x�c``�( ���/��W C�/���oN?��@�Q@����_�_��`�#�P��ƨb��r���C�� XG�����U��_�\��?�g�\ �*��[v���@$� ]'!I                     %�   (         h                               �       h      � �   o      � �   v      � �   }� � �   � �                   � � �   � �TahomaTahomaTahoma02   RSRC
 LVCCLBVW  �  h      �               4       LVSR      LIvi      ,CONP      @TM80      TDFDS      hLIds      |VICD      �vers      �ICON      �icl8      �CPC2      �DTHP      �LIfp      TRec    FPHb      �FPSE      �LIbd      �BDHb      �BDSE      �VITS      �MUID      �HIST      FTAB           ����        얦    ����       t���    ����      ���    ����      8p��    ����      +Xd��    ����      ,�X��    ����      ,�L��   ����      9@��    ����      9,4��    ����      9���    ����      =��X�    ����      HܠX�    ����      V|\��   ����      ^D��   ����      ^�|��   ����      ^�X��   ����      _D��   ����      _��W�   ����      _�\��   ����      `P藦   	����      `����   
����      `�ș�   ����      a@��   ����      a�,��   ����      a�왦   ����      b8���   ����      b��[�   ����      b�̞�   ����      c(to�   ����      c���   ����      c�H��   ����      d T��   ����      dlh�   ����      d���   ����      eԙ�   ����      ep@��   ����      e�`��   ����      f�o�   ����      f`�^�   ����      f�H\�   ����      g�n�   ����      gxD��   ����      g�L��    ����      hĚ�   !����      hx�n�   "����      h��   #����      i$��   $����      i| ��   %����      i�,��   &����      j$8��   '����      j�D��   (����      j�P��   )����      kP\��   *����      k�h��   +����      k�t��   ,����      lD���   -����      l����   .����      l����   /����      mH���   0����      m����   1����      m����   2����      nH���   3����      n����   4����      o���   5����      oT���   6����      o����   7����      o���   8����      pL��   9����      p���   :����      p�(��   ;����      qD4��   <����      q�@��   =����      q�L��   >����      r<X��   ?����      r�d��   @����      r�p��   A����      s4|��   B����      s����   C����      s���   D����      tP���   E����      t����   F����      t����   G����      uD���   H����      u����   I����      u����   J����      vH���   K����      v����   L����      v� ��   M����      wH��   N����      w���   O����      w�$��   P����      xH0��   Q����      x�<��   R����      x�H��   S����      yHT��   T����      y�`��   U����      y�l��   V����      z<x��   W����      z����   X����      z���   Y����      {H���   Z����      {����   [����      {���   \����      |8���   ]����      |����   ^����      |����   _����      }D���   `����      }����   a����      }����   b����      ~,��   c����      ~���   d����      ~� ��   e����      0,��   f����      |8��   g����      �D��   h����      �(P��   i����      ��\��   j����      ��h��   k����      �8t��   l����      �����   m����      �����   n����      �4ؘ�   o����      �����   p����      �����   q����      � ���   r����      �t���   s����      �����   t����      ����   u����      �d���   v����      �����   w����      ����   x����      �d���   y����      �����   z����      ����   {����      �d��   |����      ����   }����      �� ��   ~����      �P,��   ����      ��8��   �����      ��D��   �����      �XP��   �����      ��\��   �����      �h��   �����      �Pt��   �����      �����   �����      �����   �����      �T���   �����      �����   �����      ����   �����      �T���   �����      �����   �����      �����   �����      �L���   �����      �����   �����      ��z�   �����      �<�v�   �����      ��$z�   �����      ��h{�   �����      �<Щ�   �����      ����   �����      ��H��   �����      �,���   �����      �|�[�   �����      �Ԁ��   �����      � t��   �����      �|���   �����      ��@[�   �����      ��   �����      �l䘦   �����      ����   �����      � ��   �����      ��`��   �����      �̰o�   �����      �,���   �����      �x���   �����      ��D'�   �����      �$�n�   �����      �x�X�   �����      ��T��   �����      �ؕ�   �����      �l���   �����      ����   �����      ���   �����      �x���   �����      ��H��   �����      �$T��   �����      �p`��   �����      ��Ȗ�   �����      ���   �����      �p$��   �����      ��<��   �����      ����   �����      �ll��   �����      ��x��   �����      ����   �����      �xЦ�   �����      ��Ħ�   �����      �0���   �����      �����   �����      ��|[�   �����      �0\W�   �����      ��hW�   �����      ��ܦ�   �����      �(�o�   �����      �th��   �����      ��,��   �����      �8��   �����      �l���   �����      ��К�   �����      �X��   �����      �Xd��   �����      ��pX�   �����      ��̘�   �����      �\���   �����      �����   �����      ���   �����      �P��   �����      �����   �����      �����   �����      �T���   �����      ��@��   �����      � t��   �����      �L�[�   �����      ��P��   �����      ��D��   �����      �Tx��   �����      �����   �����      ��l��   �����      �D���   �����      ��8��   �����      ���   �����      �P0��   �����      ��l_�   �����      �����   �����      �L ��   �����      �����   �����      �����   �����      �@Ԗ�   �����      ��䕦   �����      ��n�   �����      �<���   �����      ����   �����      ����   �����      �,���   �����      ��$��   �����      ��p��   �����      �D���   �����      �����   �����      ��8~�   �����      �0D��   �����      ���X�   �����      �ؐn�   �����      �,̧�   �����      �����   �����      �ܴ��   �����      �(<��   �����      ��|��   �����      �܈��   �����      �(�W�   �����      ��8��   �����      ��,��   �����      �( ��   �����      �t䧦   �����      ��\��   �����      �P��   �����      �lD��   �����      ���   ����      �ؤ�  ����      �`t��  ����      ��h��  ����      �ا�  ����      �hp��  ����      �����  ����      �0��    ����      �h ��    ����      ����    ����      � ���    ����      �L���    ����      �����    ����      ��\��    ����      �ఙ�    ����      ��̕�   �����      ��o�