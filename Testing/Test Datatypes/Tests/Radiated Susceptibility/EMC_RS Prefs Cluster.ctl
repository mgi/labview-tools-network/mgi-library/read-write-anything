RSRC
 LVCCLBVW �  �     l   p!� 0           < � @�      ����            r9�In�EO�����ι           ��Tg��D��������ُ ��	���B~  t LVCCEMC_RS Prefs Cluster.ctl       LVCC   EMC_Test Header Cluster.ctlPTH0   "    EMC_Test Header Cluster.ctl    @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  F ��)3�   EMC_Test Header Cluster.ctl "@P        Test Header                         LVCC     Frequency Specifier.xctlData.ctlPTH0   +     SharedFrequency SpecifierData.ctl    d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P         Data                          LVCC      Freq_Spec Method Enum.ctlPTH0   <     SharedFrequency SpecifierFreq_Spec Method Enum.ctl    d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method                           LVCC   Instrument_Pulse Cluster.ctl PTH0   ;     Instruments
InstrumentInstrument_Pulse Cluster.ctl    @!Enabled @
Pulse Frequency(Hz) @
Width(S)  A ��b�   Instrument_Pulse Cluster.ctl @P     
Pulse Info                          LVCC     !Instrument_Modulation Cluster.ctlPTH0   @     Instruments
Instrument!Instrument_Modulation Cluster.ctl    V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P     Mod Channel                         LVCC   !Instrument_Waveform Type Enum.ctlPTH0   @     Instruments
Instrument!Instrument_Waveform Type Enum.ctl    ^ ��d��   !Instrument_Waveform Type Enum.ctl 4@ <None>SineTriangleSquareWaveform Type                          LVCC     DwellListbox.xctlData.ctl PTH0   $     SharedDwellListboxData.ctl    @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P   Dwell                         LVCC      !EMC_RS Probe Read Method Enum.ctlPTH0   '   !EMC_RS Probe Read Method Enum.ctl    ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod                          LVCC      EMC_UI PID Cluster.ctl PTH0   %     SharedEMC_UI PID Cluster.ctl    @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P     PID Cluster                         LVCC   EMC_RS Test Type Enum.ctlPTH0      EMC_RS Test Type Enum.ctl    � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type                           LVCC    #EMC_RS Pulse Type Prefs Cluster.ctlPTH0   )   #EMC_RS Pulse Type Prefs Cluster.ctl   + @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P        Timing  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P      BWFreqRange @@ ����  
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  # $
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P            	 
          ! " % & ' ( )Pulse  *                       LVCC      EMC_Timing Generator Params.ctlPTH0   &    EMC_Timing Generator Params.ctl    @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  
@
Freq  @
Pulse Width @
	Offset(V) @
Amplitude Vpp T �       EMC_Timing Generator Params.ctl ,@P         Timing Gen Cluster                          LVCC   BWFreqRange Cluster.ctlPTH0   0     Shared	BandwidthBWFreqRange Cluster.ctl    @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P       BWFreqRange                         LVCC   EMC_SA BWTable RB.ctlPTH0       EMC_SA BWTable RB.ctl    *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  : �       EMC_SA BWTable RB.ctl @P    BW UI Control                           �   n @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P    	 
   Freq  @!Show Diagnostics? @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P    Pulse Channel V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? @0����RF Signal Generator @
RF Sig Gen Freq Change  @
Nominal Dwell @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P  ! "Dwell ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  & ' (
PID Params  @0����
Test Title  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P  & ' (Amp PID @
Numeric @@ ���� -[Probe Ranges]  @0����String  @@ ���� /[Probe Names] @@ ���� /[Amp Names]  @@ �������� -[Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
FM Channel  @@ ���� /[Amp Cables]  @
Max RF Level Change @
Test Offset(%)  @0����Sensor Correction @@ ���� /AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  O P Q  R S TTiming  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  V W X Y ZBWFreqRange @@ ���� [
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ^ _
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P  < = > ? @ A B C D E F G H I J K L M N U \ ] ` a b c dPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @@ ���� /Probe VISA[]  @
SA Sweep Multiplier � ��)O�   EMC_RS Prefs Cluster.ctl a@P )              # $ % ) * + , . 0 1 2 3 4 5 6 7 8 9 : ; e f g h i j k lPrefs  m     �   � 
 c  x    
 d   0   `    
 P    
 c   0  @flg @oRt @eof @P    udf @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P  
     Header  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P        Freq  @!Show Diagnostics? @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P    Pulse Channel V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P     
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? @0����RF Signal Generator @
RF Sig Gen Freq Change  @
Nominal Dwell @
Data 2  @@ ���� *[Frequency] @@ ���� *[Time]  7 �       DwellListbox.xctlData.ctl %@P  + ,Dwell ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  0 1 2
PID Params  @0����
Test Title  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P  0 1 2Amp PID @
Numeric @@ ���� 7[Probe Ranges]  @0����String  @@ ���� 9[Probe Names] @@ ���� 9[Amp Names]  @@ �������� 7[Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P     
FM Channel  @@ ���� 9[Amp Cables]  @
Max RF Level Change @
Test Offset(%)  @0����Sensor Correction @@ ���� 9AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  Y Z [  \ ] ^Timing  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  ` a b c dBWFreqRange @@ ���� e
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  h i
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P  F G H I J K L M N O P Q R S T U V W X _ f g j k l m nPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @@ ���� 9Probe VISA[]  @
SA Sweep Multiplier ^@P )     ! " # $ % & ' ( ) - . / 3 4 5 6 8 : ; < = > ? @ A B C D E o p q r s t u vPrefs  0����  T     P  x y y x z x    
  2����  P  |  } ~ } } ~  !  P  � } }  P  | } }  @ ���� } 
 P  � �  P  } } }  @ ���� x  @ �������� }  P  � } } } } } }  P  } } } } }  @ ���� � 
 P  z } < P  x x x x x x x x x x x x } } } } } } } � � x �  ~ x x \@P ) {  � � � } ~ } } � x } } � | } � x z � � � � � z x } � � } } x � � z } } } } � }dfd \@P ) {  � � � } ~ } } � x } } � | } � x z � � � � � z x } � � } } x � � z } } } } � }txd \@P ) {  � � � } ~ } } � x } } � | } � x z � � � � � z x } � � } } x � � z } } } } � }old \@P ) {  � � � } ~ } } � x } } � | } � x z � � � � � z x } � � } } x � � z } } } } � }ext  P  	 w � � � �  @ �     P  � � � 9  P  x x z ~ x x  P  �  P      P  � � � e  P  | | |  @ ���� z "  <None>SineTriangleSquare  @ ���� � 
 P  � �  P  z z z �  		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File   @ ���� � 
 P  � �  @ �     P  � � � 7  P  � � � 7 @T Minimum @T Maximum @
	Increment  P  � � � *  LogLinearFileSusceptibility Spec  @ ���� � 
 P  � �    MaxMinMeanFirst Probe   @ ���� � 
 P  � �  P  � � � * 
 c   $   � w 
 c     
 c           `    > P  � � � � � �    � � � �     � � � � � � � � � � � 
 d    �    u       �  �0  �   �0  �0  �0  �0  �   �0  �0  �   �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �   �0  �0  �   �0  �0  �0  �   �0  �0  �   �   �0  �   �   �   �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �0  �   �0  �0  �0  �   �0  �   �0  �0  �0  �0  �   �0  �0  �0  �   �0  �0  �   �0  �   �0  �   �0  �0  �0  �0  �  �  �  �  �0  w    
             ! -  +  . 3 :  <  = o  _ f  j p �     +  Dx�c`�	D�tL�?@��tn���l|b�!@��v8�s����;@�z�$�?�N+?���6B��?Լ�R�8h�z�%w v?6��@��hJ��h4�3cs?>��s��õ��#.q\�!U=��*�H3$���rN�r�p�Z�cͥ�r RM��M/�D���8}O�]Jl�+��{�hV�2������N��v����f�ș��ɵ
m`@���M��rus���� �q����eX� (a ׋L!��Ȋ��8� o�    ( VIDSEMC_RS Prefs Cluster.ctl          �  7�x��[}p�ߐ�X��%!@�K$h�Ė�FH����1VS-����4�R[w`z�p�&�u�)�B��J�d8�d�4a�6�q�5�X�i�m��d$r}��}wo�n7�Y�������{��������v�ZskA巣�ѝ��Ws�4ҵ��J�{z6T���zY��͏V�A��;
�IE"�m�~x�#[�#��+����7��TT�OUT^��H�6q���/	�~�I� ���q�	I�'c@K��2'm�p�G �6��yi8���#�3���Ɂ�O�����|V�o��J��봉���d^��Y�,��X����(�ǈ_�����+��<x%:�2!uF��m��G�� ǳ2�#����ۨ�n�����8���|h�F�����'�ab_+�8!&eq�-`Eڢ�N����z�J�	Y�LQy��D"0��<i���9��&N�9U�T�Z������E�Y�7@~(�)�3M�1E�"E�;��3�As�Ch4��I�&C�8��h1++���FO��YԴP�}�����hY�c�t2��|�sB�$�;'���\
��=� �@*2's����[��I8_�,�?� =��>�J��JӁY)n��>�5���wF��K����|~5��B�#(�9��An_�%/�T/�/����Y��YI�/DM�Иg��3����o���g�c����c��3��ޠثc?�L3:�י^�9�lA�NZ���Q7��p�0I�E�M��)��)�螂螒�V�V����7��K�h���ߞ����9e-�k#�{�į=(���_�Ӵ����8kU=�m��hx�E"ģ��wx�0Q�p���?kh�X1����o���G�����u�O��&�܅�4�7ץ���a؊��6����'��M�_ؔ�ۦ\�M��8���Ʀ��mʭ�)�En���bSʜ������Ií��=����
VJ1�Pɩۣ�o�ޝa�����Q�`��a�HT�n���X�M%�s�ɝ�N��v�Z��.D�˒Q��:��"ː�퇵h?��\���Y�,�B{Hݺf��w����)PȄL8��ﰺ�t������5A,v�!���}]S�d�N�\7i�qɺf71���E�#m�L$H�)�]n^�q��s�]�ȁqYK�Nܚ�>�A�8Qo��7�\��d��y��#����ٷ�<�Z)���n��.%������{���b�]��;�ֵ{��=Q�D܉��4�f�ot���~\nԬ��^,7��W׎��~Ұ�Y;�˭\ ������{:��D�O�T����|�L�Jj_p]px�^���]+b.�y*s8VG���CGʀ��2�<��83K믣�l{�a����~�]��˼ܢ�C.oW����њ�T�>Ƣ�	J���ò8d��Kr3�2�6�.,Qmfh��2��:j� �c��1(��}��%jd8@�k�CJ�62��pa10|@����?s�c�pd��M0���G�����öK۶`)�_j�`��^(���,�Uj��[Pߛ�5g�/��/�d������>?^\�
��b��#�Ľd/�V�-��F{%1&}�3���&Ek�^1��W�1�砤,�����Ēܩ-)GK�g�->���Lp�z�b,�ߐ��>_����a_�}mԾ���%����v���Jl�ک}��5���B����oKӿ�ԪYhq�YŹP�U4���zȹ`�.1�!���'�Z�,7g܋�?N2�2<n���z��e�L���6j_-6�z{��Nm�潣�{-�x�\�Z�|U�j{ãV�, �O�g�@�W�xO=��(
�Q�EIk'�Q�Q��0�"tE�mޛY�?>�6}4?}����_���?�Ec`~��x<����wB��	�4^�%��D�X��yI.�����O���?2�v���� ��j:��N�3�~�j}���h�vc���h�vf}�j=���N�M�/J�{��ۙ�ES�sOh�j�f�m��V���r&��'�6j�:�\�؀�5ڂ͘�] �}�i%�'���˓�k��8���Wk+lf6cm3�j�x��&Œ:3�Ѓ���&�l�;A�9���t��g>'����.�=�=��0~;7d�q����V��r0 r��A�D3��|�>�L�ˤZG�Z���İ�ٲ��P��A\!0|Q`���*!v��*�U��BB[��e[����ke�5{� ض��Q6�&[1��bR&�"���MR�8IOR�l�*�I��MR
��������OQfg+��mu��Np��}�bx^5d0��Oz�VN���g��10���[�7����9���2�����������k���Cr�NH���\eE�
��a�hv6X�d�徬���
K�',bVXVg���Y�`y9+,;����b��3�?����x�l��Ķ�j��vy&��cfv؞G�g��v?�ݓ%�\d��7b#;XȑBct
�L)4Jg�1l	�7�'x�9��(j��7�tl$5Kj:}�Y���[��N���ֈl���V�l������ކXV$�ˊd��ͦe�i���i�L�s%Ǔ�)����*���N��K�~e����B�D�9W
������>�6�G�����T�۟��=�FdܕE��ȸ ��r 㛎�1>��O8`�X쮄����o]�&v��=#fyCw:��B~�kĢ��S�^:��� �w�}1�z>Ɋ�5��cn[.�m6ORG���zz�����>���g��r��OZ��|�n�}.�$��3����2��mȰ�:#�=��fT��cz�u'k�dp��:�3u���
����U�Ϗ��iΎ�AdL�0+��^|���>�olR��ys���u��	p�w
8N�T-� 7D�k 7�k��I���$���C_�'z�����>�+s)v���{�=KfX���;iz!Y��@��旹Y�MӣnZo=O��xZ��L�~�a]��ϣ�[fQ���9��3���(0v0��p�l�[a�5����94��K�>����-���pM�r�f��ϥ�[��q\;����(6/�����הRt��Ql��b��b�2�{ξ���ex��J�?`��W�VE1����r�ޕT���4}�a�k��w�M��sH����R��5w�ܭ�Ͻ;��(p�������"3    �   8.2     ������  �  �@ �� a�� ��  �� a�� �� ��� a�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��              ��    ��      +����            �  �   ��   ��V                    �   ��       ���              ��    ��       ���                �   ��       ���             �  �   ��       ���              ��    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������  �   n @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ���v�   EMC_Test Header Cluster.ctl @P        Header  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P    	 
   Freq  @!Show Diagnostics? @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P    Pulse Channel V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? @0����RF Signal Generator @
RF Sig Gen Freq Change  @
Nominal Dwell @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P  ! "Dwell ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  & ' (
PID Params  @0����
Test Title  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P  & ' (Amp PID @
Numeric @@ ���� -[Probe Ranges]  @0����String  @@ ���� /[Probe Names] @@ ���� /[Amp Names]  @@ �������� -[Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
FM Channel  @@ ���� /[Amp Cables]  @
Max RF Level Change @
Test Offset(%)  @0����Sensor Correction @@ ���� /AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  O P Q  R S TTiming  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  V W X Y ZBWFreqRange @@ ���� [
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ^ _
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ���Lx   #EMC_RS Pulse Type Prefs Cluster.ctl F@P  < = > ? @ A B C D E F G H I J K L M N U \ ] ` a b c dPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @@ ���� /Probe VISA[]  @
SA Sweep Multiplier � ��)3�   EMC_RS Prefs Cluster.ctl a@P )              # $ % ) * + , . 0 1 2 3 4 5 6 7 8 9 : ; e f g h i j k lPrefs  m     �   � @0����Description @T Start 
@T End @0����Version @	CheckWord @0����
E-Stop Str  B ��)3�   EMC_Test Header Cluster.ctl @P        Header  d �       Freq_Spec Method Enum.ctl B@ LogLinearFileSusceptibility SpecFreq Select Method  @Points/Decade @

Delta Freq  @2����	Freq Path @
Start Frequency @
Stop Frequency  @2����	Spec Path H �       Frequency Specifier.xctlData.ctl 6@P    	 
   Freq  @!Show Diagnostics? @!Enabled @
Pulse Frequency(Hz) @
Width(S)  C ��b�   Instrument_Pulse Cluster.ctl @P    Pulse Channel V ��d��   !Instrument_Waveform Type Enum.ctl ,@ <None>SineTriangleSquareType  
@
Freq  @
	Depth/Dev F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
AM Channel  @
Mod SG Adjust @2����Mod File (Rel Path) @
Tol+(%) @
Gate Freq(Hz) 
@!Gate? @0����RF Signal Generator @
RF Sig Gen Freq Change  @
Nominal Dwell @
Data 2  @@ ����  [Frequency] @@ ����  [Time]  7 �       DwellListbox.xctlData.ctl %@P  ! "Dwell ^ ��1�   !EMC_RS Probe Read Method Enum.ctl 4@ MaxMinMeanFirst Probe ProbeReadMethod @
Overshoot EStop @
P @
I @
D ; �       EMC_UI PID Cluster.ctl @P  & ' (
PID Params  @0����
Test Title  @Delay after output(ms)  7 �       EMC_UI PID Cluster.ctl @P  & ' (Amp PID @
Numeric @@ ���� -[Probe Ranges]  @0����String  @@ ���� /[Probe Names] @@ ���� /[Amp Names]  @@ �������� -[Amp Settings]  � �       EMC_RS Test Type Enum.ctl �@ 		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File Type  @0����Test Limit File @
Tol-(%) F �Œ�O   !Instrument_Modulation Cluster.ctl @P    
FM Channel  @@ ���� /[Amp Cables]  @
Max RF Level Change @
Test Offset(%)  @0����Sensor Correction @@ ���� /AmpAntenna[]  @0����SigGen Name @0����Amp Name  @0����
Timing Gen  @0����Power Meter @0����
Probe Name  @0����SA Name @0����
Rx Antenna  @0����
Tx Antenna  @0����Tx Cable  @0����PM Fwd Pwr Gain @0����SigGen Cable  @0����Rx Cable  @
SA Ref Level  @
SA Atten(Auto:-1) @
SA Trig Delay @
SA Trig Offset  @
PM Capture Time @
PM Trig Offset  @
SafetyMargin(dB)  @!Gating Enabled? @
Gating Freq @
Gating Duty Cycle(%)  @
Pulse Width @
	Offset(V) @
Amplitude Vpp H �       EMC_Timing Generator Params.ctl  @P  O P Q  R S TTiming  @

Start Freq  @
	Stop Freq @
Res BW  @
Video BW  @
Sweep/Hz or Sweep @ ��չf   BWFreqRange Cluster.ctl  @P  V W X Y ZBWFreqRange @@ ���� [
Bandwidths  @0����BW File *@ AutoBWTable	SweepTime BW Table RB @

Sweep Time  8 �       EMC_SA BWTable RB.ctl @P  ^ _
BW Control  @SA #Pts(-1:Auto)  @2����	File(Rel) @0����PwrMeter(V/m)^2/W Eq  "@0����SpecAnalyzer(V/m)^2/W Eq  r ��)3�   #EMC_RS Pulse Type Prefs Cluster.ctl F@P  < = > ? @ A B C D E F G H I J K L M N U \ ] ` a b c dPulse $@ WattsdBmOutput Graph Units  @
Test Offset(V/m)  @
	Tol+(V/m) @
	Tol-(V/m) @
Overshoot(V/m)  @@ ���� /Probe VISA[]  @
SA Sweep Multiplier ^@P )              # $ % ) * + , . 0 1 2 3 4 5 6 7 8 9 : ; e f g h i j k lPrefs   
@!Watts @!dBm  ! @!	SweepTime 
@!Auto  @!BWTable 
 P  ^ _  P  V W X Y Z  P  O P Q  R S T B@P  < = > ? @ A B C D E F G H I J K L M N U \ ] ` a b c dPulse  P          P     "  <None>SineTriangleSquare �  		CW Search	AM SearchCW FileAM FileFM FilePulse CalibrationPulse Search(PwrMtr)Pulse Search(SpecAnalyzer)
Pulse File   P  & ' (  P    	 
    *  LogLinearFileSusceptibility Spec    MaxMinMeanFirst Probe  
 P  ! "  P     � m m l n k / j i h g f o p d c q b a _ r s t ^ u ` ] Z Y X W V v [ n \ G T S O  R P Q w U @ N M L K J I H F x    q  q    y e n ; / : 9 8 n 7 / z   {  6 5 4 | 3 n n 2 - n 1 / n 0 / n . - } ( ' & , + * < = E > ? D C B A  } ( ' & ) ~ q    	 q 
     z   {      � $ %  � n "   n !   # q  �            r FPHPEMC_RS Prefs Cluster.ctl       TDCC   DwellListbox.xctlData.ctl PTH0   $     SharedDwellListboxData.ctl                                 �PTH0   -     SharedDwellListboxDwellListbox.xctl TDCC      !EMC_RS Probe Read Method Enum.ctlPTH0   '   !EMC_RS Probe Read Method Enum.ctl                                 bPTH0         TDCC      Instrument_Pulse Cluster.ctl PTH0   ;     Instruments
InstrumentInstrument_Pulse Cluster.ctl                                 �PTH0         TDCC      !Instrument_Waveform Type Enum.ctlPTH0   @     Instruments
Instrument!Instrument_Waveform Type Enum.ctl                                   fPTH0         TDCC     !Instrument_Modulation Cluster.ctlPTH0   @     Instruments
Instrument!Instrument_Modulation Cluster.ctl                                 �  	/PTH0         TDCC     Freq_Spec Method Enum.ctlPTH0   <     SharedFrequency SpecifierFreq_Spec Method Enum.ctl                                 �PTH0         TDCC     Frequency Specifier.xctlData.ctlPTH0   +     SharedFrequency SpecifierData.ctl                                 PTH0   ;     SharedFrequency SpecifierFrequency Specifier.xctl TDCC   EMC_Test Header Cluster.ctlPTH0   "    EMC_Test Header Cluster.ctl                                 TPTH0         TDCC     EMC_UI PID Cluster.ctl PTH0   %     SharedEMC_UI PID Cluster.ctl                                 ~  �PTH0         TDCC      EMC_RS Test Type Enum.ctlPTH0      EMC_RS Test Type Enum.ctl                                 �PTH0         TDCC      #EMC_RS Pulse Type Prefs Cluster.ctlPTH0   )   #EMC_RS Pulse Type Prefs Cluster.ctl                                 �PTH0         TDCC      EMC_Timing Generator Params.ctlPTH0   &    EMC_Timing Generator Params.ctl                                 PPTH0         TDCC     BWFreqRange Cluster.ctlPTH0   0     Shared	BandwidthBWFreqRange Cluster.ctl                                 �PTH0         TDCC   EMC_SA BWTable RB.ctlPTH0       EMC_SA BWTable RB.ctl                                 	GPTH0              _     D    4Ap  4    A  p                                     SA Sweep Multiplier    H     �    5�B  5  �  5       ���                               R          ��  �  �                                         String     H     �   �;    �    :     ���                               X         F�    G    �                                     Probe VISA[]   H     �    
��  
  �  
  �     ���                               Z     D    �l  �      l                                     Overshoot(V/m)     H     �    ��   �  �  �       ���                               U     D    �"�O  �  #  �  O              	                       	Tol-(V/m)      H     �    ���  �  �  �       ���                               U     D    �"�S  �  #  �  S              	                       	Tol+(V/m)      H     �    ���  �  �  �       ���                               \     D    �!�s  �  "  �  s                                     Test Offset(V/m)   H     �    ���  �  �  �       ���                               Q     D    ��   #����         #                                  Watts      Q     D        2            2                               <   Watts      O     D    �� 4  I����   5      I                                  dBm    O     D      C  X      D      X                               <   dBm    P          ����  �  �  �  �                                     Pane   ^     D    o�|&  o  �  |  &                                  Output Graph Units     Q      D     /� <�   /  �   <  �                                      Pulse      d     D    {"    |  "                                        SpecAnalyzer(V/m)^2/W Eq   H     �   )�8�  )  �  )  �     ���                               `     D    ���  �  �  �                                        PwrMeter(V/m)^2/W Eq   H     �   ��    �    �     ���                               U     D    ����  �  �  �  �                                      	File(Rel)      H     �   ���  �  �  �       ���                               \     D    F�SP  F  �  S  P                                      SA #Pts(-1:Auto)   H     �    K�X�  K  �  K  �     ���                               c     D     �� �6   �  �   �  6                                      Bandwidths & Sweep Time    V     D     �� ��   �  �   �  �                                      
BW Control     L     D      � & �      �   &   �                                       V     D      � " �      �   "   �              
                    
Sweep Time     H    �D      k # �      m      �                                   U     D      ]  �      ^      �              	                    	SweepTime      _     D      l  �      m      �                               <   Auto BW, User Sweep    P     D     ��     ����                                        Auto   P     D       !          !                                  <   Auto   S     D     ��      ����                                         BWTable    X     D     #  0 G   #      0   G                               <   Use BW Table   P                                                                Pane   e     D    ������ �������������   �                                  Bandwidths and Sweep Time      W     D    ������ <������������   <                                  BW Table RB    P           �� ��   �  �   �  �                                      Pane   L     D     �� ��   �  �   �  �                                          L     D     �� ��   �  �   �  �                                          S     D     � �7   �     �  7                                      BW File    H     �    ��   �  �   �       ���                               W     D    �    �                                        BWFreqRange    ]     D     e _ r �   e   `   r   �                                    Sweep/Hz or Sweep      H     �     j  w X   j      j   W     ���                               T     D     M _ Z �   M   `   Z   �                                    Video BW   H     �     R  _ X   R      R   W     ���                               R     D     5 _ B �   5   `   B   �                                    Res BW     H     �     :  G X   :      :   W     ���                               U     D      _ * �      `   *   �              	                      	Stop Freq      H     �     "  / X   "      "   W     ���                               V      D      _  �      `      �              
                      
Start Freq     H     �     
   X   
      
   W     ���                               P          �+�    �  +  �                                    Pane   L     D    ��    �    �                                         V          �	    �    	              
                      
Bandwidths     H     �    �'�    �    �     ���                               T     D    �k��  �  l  �  �                                      Rx Cable   H     �   �
�d  �    �  c     ���                               R     D    �	�(  �  
  �  (                                      Timing     U     D     � S � �   �   T   �   �              	                    	Amplitude      Y     D     }  � W   }      �   W                                  Amplitude Vpp      H    �D     �  � ?   �      �   >                                   R     D     y S � s   y   T   �   s                                  Offset     U     D     i  v @   i      v   @              	                    	Offset(V)      H    �D     {  � ?   {      {   >                                   [     D     
   `   
         `                                  Gating Enabled?    Y           ! ' d      "   '   d      
                        =   Enable Gating      P     D     Q c ^ z   Q   d   ^   z                                  Freq   P     D     A  N )   A      N   )                                  Freq   H    �D     S  ` O   S      S   N                                   W     D     U  b K   U      b   K                                  Pulse Width    W     D     e c r �   e   d   r   �                                  Pulse Width    H    �D     g  t O   g      g   N                                   R     D     * c 7 �   *   d   7   �                                  Gating     W     D     - c : �   -   d   :   �                                  Gating Freq    H    �D     +  8 O   +      +   N                                   V     D     > c K �   >   d   K   �              
                    
Duty Cycle     `     D     > � K   >   �   K                                    Gating Duty Cycle(%)   H    �D     ?  L O   ?      ?   N                                   P          ��&  �    �  &                                      Pane   L          �	�  �  
  �                                            V     D    k�    l    �                                      
Probe Name     H     �   
d        c     ���                               \     D    PK]�  P  L  ]  �                                      SafetyMargin(dB)   H     �    UbD  U    U  C     ���                               Z     D    8KE�  8  L  E  �                                      PM Trig Offset     H     �    =JD  =    =  C     ���                               [     D     K-�     L  -  �                                      PM Capture Time    H     �    %2D  %    %  C     ���                               Z     D    K�    L    �                                      SA Trig Offset     H     �    D        C     ���                               Y     D    �J��  �  K  �  �                                      SA Trig Delay      H     �    �C  �    �  B     ���                               ]     D    �J��  �  K  �  �                                      SA Atten(Auto:-1)      H     �    ��C  �    �  B     ���                               X     D    �J��  �  K  �  �                                      SA Ref Level   H     �    ��C  �    �  B     ���                               X     D    �k��  �  l  �  �                                      SigGen Cable   H     �   �
�d  �    �  c     ���                               W     D     �k ��   �  l   �  �                                      SigGen Name    H     �    �
 �d   �     �  c     ���                               T     D     �k ��   �  l   �  �                                      Amp Name   H     �    �
 �d   �     �  c     ���                               [     D    �k��  �  l  �  �                                      PM Fwd Pwr Gain    H     �   �
�d  �    �  c     ���                               V     D     �k ��   �  l   �  �                                      
Timing Gen     H     �    �
 �d   �     �  c     ���                               W     D     �k�   �  l    �                                      Power Meter    H     �    �
d   �     �  c     ���                               T     D    lky�  l  l  y  �                                      Tx Cable   H     �   l
{d  l    l  c     ���                               V     D    Qk^�  Q  l  ^  �                                      
Tx Antenna     H     �   U
dd  U    U  c     ���                               V     D    :kG�  :  l  G  �                                      
Rx Antenna     H     �   >
Md  >    >  c     ���                               S     D    #k0�  #  l  0  �                                      SA Name    H     �   '
6d  '    '  c     ���                               P           D� Q�   D  �   Q  �                                      Pane   Q      D     /� <�   /  �   <  �                                     Pulse      R           {� �   {  �   �                                       String     H     �    �� �3   �  �   �  2     ���                               X          �> ��   �  ?   �  �                                     AmpAntenna[]   H     �     �� ��   �  �   �  �     ���                               ]     D    R�_�  R  �  _  �                                     Sensor Correction      H     �   V$e~  V  &  V  }     ���                               Z     D    avn�  a  w  n  �                                     Test Offset(%)     H     �    _/lo  _  1  _  n     ���                               _     D    6$C�  6  %  C  �                                  Max RF Level Change    H     �D    H"U|  H  $  H  {                                   R          <�I  <  �  I                                       String     H     �   Q�`3  Q  �  Q  2     ���                               X         P>]  P  ?  ]                                       [Amp Cables]   H     �    M�Z�  M  �  M  �     ���                               V     D     � �:   �     �  :              
                       
FM Channel     U     D     � d � �   �   e   �   �              	                       	Depth/Dev      H     �     � 2 � ^   �   4   �   ]     ���                               P     D     � d � {   �   e   �   {                                     Freq   H     �     ~ 2 � ^   ~   4   ~   ]     ���                               P     D     _ � l �   _   �   l   �                                     Type   L           _ � l �   _   �   l   �                                         H     �     d � q �   d   �   d   �     ���                               H    �    d 2 s |   d   4   d   {     ���                               P           �A �Z   �  B   �  Z                                     Pane   L     D     � �   �     �                                            S     D     1 >;   1     >  ;                                     Tol-(%)    H     �     /� <   /  �   /       ���                               [      D    ��    �    �                                     Test Limit File    H    �   $~    &    }     ���                               P          ��    �    �                                     Type   L     D    ��    �    �                                       H     �    ��    �    �      
                            H    �   "�    $    �      
                            S      D    �    �                                         Numeric    H     �    �'&    �    %     ���                               Z         1y    2    y                                     [Amp Settings]     H     �    1�>�  1  �  1  �     ���                               H     �    �"�    �    �     ���                               R           �� �   �  �   �                                       String     H     �    ��3   �  �   �  2     ���                               W          �>   �  ?                                         [Amp Names]    H     �     �� �   �  �   �  �     ���                               R           �� �   �  �   �                                       String     H     �    �� �3   �  �   �  2     ���                               Y          �> ��   �  ?   �  �                                     [Probe Names]      H     �     �� ��   �  �   �  �     ���                               S      D     �� �   �  �   �                                       Numeric    H     �     �� �&   �  �   �  %     ���                               Z          �1 �}   �  2   �  }                                     [Probe Ranges]     H     �     �� ��   �  �   �  �     ���                               S     D    ;�H�  ;  �  H  �                                     Amp PID    M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P          :$G=  :  %  G  =                                     Pane   L     D    ;�H�  ;  �  H  �                                          b     D    �b��  �  c  �  �                                     Delay after output(ms)     H     �    �/�[  �  1  �  Z     ���                               V     D    ����  �  �  �  �                                     
Test Title     H     �   �$�~  �  &  �  }     ���                               R     D     (� 5   (  �   5                                       Header     V      D     } ( � Z   }   )   �   Z                                     
E-Stop Str     H     �    ��� � !   �����   �         ���                               U     D     e  r =   e      r   =                                     	CheckWord      H     �     j�� w��   j����   j����     ���                               S      D     L ( Y L   L   )   Y   L                                     Version    H     �    P�� _ !   P����   P         ���                               O     D     5 # B 6   5   $   B   6                                     End    H     �     9�� F    9����   9        ���                               Q      D      # + <      $   +   <                                     Start      H     �     "�� /    "����   "        ���                               W      D     	 (  ^   	   )      ^                                     Description    H     �    	��  !   	����   	         ���                               P           , 98   ,      9  8                                     Pane   L     D     (� 5�   (  �   5  �                                         V     D    r X  r  !    X              
                       
PID Params     M     D     w x � �   w   y   �   �                                  D      H     �D     x % � c   x   '   x   b                                   M     D     c x p }   c   y   p   }                                  I      H     �D     d % q c   d   '   d   b                                   M      D     O x \    O   y   \                                     P      H    �D     P % ] c   P   '   P   b                                   P          �$�=  �  %  �  =                                     Pane   L     D    r '  r  !    '                                          P     D    #�0�  #  �  0  �                                     Freq   U     D     � s � �   �   t   �   �                                     	Spec Path      H     �    �  � l   �      �   k     ���                               Z     D     | v � �   |   w   �   �                                  Stop Frequency     H    �D     z  � a   z   	   z   `                                   [     D     e v r �   e   w   r   �                                  Start Frequency    H    �D     f  s a   f   	   f   `                                   V     D     : F G y   :   G   G   y              
                       
Delta Freq     H     �     8  E @   8      8   ?     ���                               U     D     N � [ �   N   �   [   �              	                       	Freq Path      H     �    O  ^ c   O      O   b     ���                               ^     D      v  �      w      �                                     Freq Select Method     L     D      v  }      w      }                                         H     �      |  �      ~      �      
                               H    �       e      	      d      
                               Y     D     " F / �   "   G   /   �                                     Points/Decade      H     �        - @              ?     ���                               P          '�4   '  �  4                                        Pane   L          #�0�  #  �  0  �                                         V     D     {  �Z   {  !   �  Z              
                       
AM Channel     U     D     � d � �   �   e   �   �              	                       	Depth/Dev      H     �     � 2 � ^   �   4   �   ]     ���                               P     D     � d � {   �   e   �   {                                     Freq   H     �     ~ 2 � ^   ~   4   ~   ]     ���                               P     D     _ � l �   _   �   l   �                                     Type   L           _ � l �   _   �   l   �                                         H     �     d � q �   d   �   d   �     ���                               H    �    d 2 s |   d   4   d   {     ���                               P           �$ �=   �  %   �  =                                     Pane   L     D     {  �'   {  !   �  '                                          Y     D     �� ��   �  �   �  �                                  Mod SG Adjust      H     �D     �" �|   �  $   �  {                                   Y     D    �a��  �  b  �  �                                     Gate Freq(Hz)      H     �    �/�[  �  1  �  Z     ���                               Q     D    �?�\  �  @  �  \                                     Gate?      [      D     H Ui   H     U  i                                     ProbeReadMethod    L      D     H U   H     U                                           H     �     N [   N     N        
                               H    �D    K� Z�   K  �   K  �      
                               [     D     e rg   e     r  g                                     Overshoot EStop    H     �     c� p   c  �   c       ���                               S     D    %v2�  %  w  2  �                                     Tol+(%)    H     �    #/0o  #  1  #  n     ���                               Q     D    ���  �  �  �                                       Dwell      L     D     ! = . D   !   >   .   D                                          R     D     .  ; ,   .      ;   ,                                     Data 2     H     �     4 A A m   4   C   4   l     ���                               R          2 x ? �   2   y   ?   �                                     [Time]     H     �     /  < (   /      /   '     ���                               L     D    �� =  D����   >      D                                          R     D        ,            ,                                     Data 2     H     �      A  m      C      l     ���                               W           x  �      y      �                                     [Frequency]    H     �        (            '     ���                               P          ��$  �    �  $                                     Pane   L          ����  �  �  �  �                                         _     D     �� �    �  �   �                                        Mod File (Rel Path)    H     �    �-~   �  /   �  }     ���                               Y     D     $  1d   $  !   1  d                                     Pulse Channel      T     D     3 G @ r   3   H   @   r                                     Width(S)   H     �     1  > @   1      1   ?     ���                               _     D      G ! �      H   !   �                                     Pulse Frequency(Hz)    H     �       & @            ?     ���                               S     D        ;            ;                                  Enabled    R      D        <            <                                  OFF/ON     P           8$ E=   8  %   E  =                                     Pane   L           $  1'   $  !   1  '                                         Y     D    maz�  m  b  z  �                                     Nominal Dwell      H     �    n/{[  n  1  n  Z     ���                               b     D    &�3  &  �  3                                    RF Sig Gen Freq Change     H     �D    $"1|  $  $  $  {                                   ]      D     v� �5   v  �   �  5                                     Show Diagnostics?      _     D    ���  �  �    �                                     RF Signal Generator    H     �   �$~  �  &  �  }     ���                               P          �� 2�� K����   3����   K                                     Pane   Q     D    �� �� )����   ����   )                                     Prefs      P           /  <    /      <                                        Pane  6C D�x��}	|E��1W� i�K@��
��� 
	$í�"�,����ϛ�/T� ����z"�<� ����W��tuMSt2��rL���_}�Qｪ��]9mx�.�8�06�rF���AA��V{�e���޲�\yS�=9gs��oq2w,��P�e�ގ�7�E��#sn�e������I�O~�g�'�| Q5�;2+�>8���;�����^�����}� �V�W��*8��08����M�c��lƟGm�.��op��함u�ğ��v5��R�>�Z�c�f�ƿ��?�~g+n��v;j�K��j�������6��0j=���[xn9�u��O�����u�����d<��Q[�jK��ڜ���K	�z׼/r\�z���C�z�B��S�+�^��'�D�/��?w៧��k����sp��vAm�����<�r���n �a �U�Tȍʹ�����Jv_#u��Ex�O���|xP�4k��z
��vAm��4<��� � �i=��㔗Fr�@Bl���x]�p��۲�<��
��2ʛ<�sK��9E�,x����*�]���;�6�c�9�wG���9��۶m�߆-�mO)���! ����V���i�>�iͦ��oN��?��Zr�e��������ȏ��@̊��"����s������T����C��k׮8^�E�@:$�0H����5�kb}�śB�Y�˄�����+
�HS�4@yq��_���o��4�`x���|H!������߉1,�0�{��#G�Ƞb8w�a16>a�0��tzE�� ���L �C�K��(x����q��>]r�A���2q���7!p��P��*o�/�/�ݏ��sH~��'�F�)_�����^��36��p�i"�E<�#��.4lP��k�1��@.h$��4�\
W�G)n�UYp�� ˄��@�	pU��ɸ
N+#�ʲ4-}d4-ZAteFe���	ڂ�pN��ѕDW����/{��lC����Aך�Oo���Q[v���l�#ޥt�3��P& ;F'�}��ΰ-;XL�%�jO�,�{	t����_�v�0ڱ�j�I�R$�b�:��@ݲ��3���I�j7�9-�9��K㝏�;w��:ue�R����שk��:�7��������$�IK�F�9%��R�%GL��&C��D��݊Z/9�p�H�z����sa2�^���=
c�|��ʽ�{F���*�� �˶��~/�1�~�d��J�1��~�6~�0���k
߁� �\��M������8M�q� ���~��0.�8%m�-��O�ø�����|_���W`��
��sK~���0ޠ�q���[� g*xɾ5%a�}[J�8����q��a�}'�E�b�cq2|���H�4�W0��ja�E|�0�&[�ꔄ�8;%a,�i)�5�a,�Ø�����#�0>p���~B�֪q[A�g~�{�{������ְO~(�(���x��/��3(��CA��� ��"vg�c�9�f��\��=��)��`#=Tk2��l��gM��3W�Ò��*��AV(���e�D"e��E[�LH۾x=����?$k%������{�s�R��B����`>v^d6�^FZ�<<P��9�ㄭ�N�L��U�-��!�d�]�!�_����!U�h���NE椓��D���rz$�I��4��3S���=*���٠��)��p-(GD$�����I��\�Z��#�D�o����7
L2�6J��P6����{�<�l�!5�S��ݡ귅]��W���Q�*	΂�)	zA��}jMS����?���4x;�*��d.�*��*�k�~�c3��KF	�Dد�F�����fP����]0�&��FDƞ�Y�4��;������ '�A�	;օ�k]���}>���0E�E�}�)�tM�b�/��21w_H��_2ϡ�)���0�0|i,��!!�:��C�=��7Q�>�:���Bܾ�[$r}����2�_�g�ٸm�[	��p;��ݏ�=Cp;�w��!�>�۽�r��P��T@���E�c��`%�P5�kP��V*~o��~�|�H`����F���c�w��l0T�{���܁�a.$7`�wQ� �cőM�E�,����$Ʉ�T�%H[B�=f�@�,4&��lyT�w�� q�1�=�i�?�2�=�0��-M��<�@��4)Vb�c*�pj���o!49;�t(ՍL�4���-�|�=n�F2tMa����V����O�<��5��·"VBz�"b�^^��h�M�\��%�����9x��8}��k��N�:x��ိ0�D��o�K����_����_���'����oɾ�E��a�SX�g5	IT�@��B��A��[$��1���	�Ĉ�M��+��5M�"B*�l�N�t�ߊ昻�p�p�17k9�����숆�M�d$$��8%�q�f�������ntZ����eo�빯EB��B�F�/N^��m�=�9�x�/���_D+��5Н�6�Ҡ*�3	J����>u����!���ρ��Zk��l�qDd{ۭ�u�BYGp %���"�"��9��8�%#g/��Þ�r۠��-<q~�;�:�W߰���X�0/�_\�$譨_h��6z���m�?������za���`�X�i�V^�}"��hq���*� # ��"��v���iɬ��B&�F#�S#t$B�F$��iV�EQ�AF|$�����V�D`?Z�t"|sfge��q)�fht�^8Cv�m�V�v���4[�ߚ)�G�E�eQL����ZՐר>���o;f?8p>-!�"�_�~�P�_��~�6��_d��TdN�oM��2Ź䙃2Rz���q�����Q��,���K` ��1,����ϧ�y#�'d��R�xB<���)?oؒ`�w��n�I��U��E�\#k]#Y�kZ�5�ٜ�ۄ~�	��b�n�-���x�P@��
S�F3��j�ۙ�w-��CD�MFЮZ��A��x�П���l<�����:E�h��oM�u]�k�څ��bL��W[(�g/$��D-}��^�ؓB�Ռ�-���F���<��|y�!��Зw����r�}_^)��2��h��+��셟����M}y���<�_J��>������ �皩���݈�>����D��3S�1F�]�n��[ ��ػy=�q�4�)���mӴ�8ql��f�q�|R�QWD"j�ZBmw2�醨=�2���������t�a�ZH*�9d�ڙ��Ŕ6B�,FԞ
��Cm�����ܠ��=j+P�юГcBR��w1Dm�9j{��Q�Mw�g�[w�E!�F�r{�p$�(��*Þ!�aӖ�oÒ���{�^���2�u�j�"�H�ql䅂���2����k��*�넰{?
��\우��u���yv���@���0u�G���:K����)���u�'l"�_��쀏`<�����������n��-��\)�&�#��셷�Y�����d����A��}�ޭ΃p���Y+���&����gL����OMр��L�#�j̬Ǔ��"��n�Y�a����y|ْ��pg _����>� �G��%5�55|J� �T ���=!�G�O% �j�H��4��2 x���	���� ����p��pI� ���2 ����h�3p�)W�� �b �&-� g��� %'5��� n�2 γ�&�� .�������V���0� ��Z �A ,�� �15�HM ;[�. ~3	��)�_�� �w�@���r������gC��`9���g����Cj9��`��W3��r�����)w(����j��r�\���7F������!奼%�oM�gG�:�3ɫ�zKL�Ip&F�$�QsO�e`�1�vE=%I�I���6��R��C��o��L�߉:���{���s�V�!�� ��E$G�ж�'th�(���PE����vh�P�5ϡm�;l��OxZ�m���ph�H�ж��:��4I|h���<�}�Cۨ�)��v��m����`��vhph;�v�?}��H�������4;��Fy��m�'Ƈ��!��~��h�s/uh[7c�*��E2T隕a��P�_F�W���iE�0ŭ�~՚[y8��^�ne�B����p�[#�Tn嚰�RR�w���XR�;h)2�pR�yr܏��~��q��u������B��q����g�>�/#G�Oy\�dPc��+.�wa�r^,U��D<U�B��T�(0'ذd�wX]��+�F���@C�nN���z�� �WB�J�zo�h�_���[������fa��څ���j�a[�qRFR�$j�~�4�
�ape�	P��O�F������$�����N�B����N^�(YUOm�(Ŭv?�$��'HgZN�r�N�F�NPox�4*�s�Ҩ��Qy:�iTx3����Xv�?�\^����,�qvx࿁$g�d`�9ZVMX�!V�e��+<�Y�c�p6A�#���6/#<f�
iFH�LB#쇌�<_���A
(�wD�@,�f�J~m&�&�'dVh���Ğ�v���UY�#�P^�wtu��n�j�4������Tz�9dU�f��7!�����T��Q-P���:�*=�^5�̱u$�Jy�:oJ���3[��u��'������<��Jt�V��%��4�9��"��9�H�h.��4>C��g�a�d�!�#�Y>ɒ���	��(|�&3W�>N�}g"/쀼�\�����N��q^X���]i^����w���	/\m���MZn� �����uX��`uS��5��
�����'�Pw"+<Ya�|m+��w��Ya�+�B��%�j��@��۟M ��ڔD8B^�hf�"�p�M�0������˛������h^����D^X	ya�fsV{J�6q^����Si^X��Wiyaᅆ$ǽ������d����$Y`�)���e�%��Km0��43���b"3l�̰C�&��1�,��#ӯ6�!n��m���9�1����M`�a��f1���7�g�kM��ף��/d�!n[�b���=�y�1Ĩe�C�J^ej�"��aqZ�Z���?������c���;��T��"/�~��C:�O�U��g��G���I4&�U��owh}�ӈ�:�Z�`:!�X����-)��p�[��PU�4�W$�{Ғ{������;􄮷O$����9*��)����"9*�r�g�K$����e|����d�+�%_[�ߋFo�����g��n��L��&zS󔁻eN���20F��>�{�A�f�\z!�3$_;��^z�i�{Ao[�&0隚�=%5�{jˠ��u�:b@��e�19�wAo�Ȕ|Œ�7��b��ܤ2����-��!0)IM���&z�-��6�{z�B�~(��f�@��k/�����e@����5��l�7@`�����gD�ɭ��.h�����M�}$����-�bdK��ߗF����i�;���e��Lܩ�^Oj�޴�Ao���d@����y����5r$_��?�B��7��N������-��*0q��{�B�z]rJ��i����0����¿�eoe��}�J�~���#��~-t/T\��c�\�)�p��BtldK@�r����l��h%�ki�|m� A��F��� �Z'��(�OuA8�a��+�]�B O�_H�r�� u� �Y#H=!��6�o6�\	r��M�ȗ��1/Q%�n-A�|h� �	A>��2K�X��2�4A�@��_@��UK�B�Z#H#!�;6�B�C��sh���%�B� M�WK� !�_�d	!�[6�2Yl�		�_.�	�rJ�:� �2d�� K	A�Z#�2B�}6Xf0˔A�,;i�,�6���&ȟ��ܩ%�rB�]����	�A2!A:��A�=B�H�/��<�� wi	�R!���5��R��c� ��X�'H�l��&H�h+���b�d�ń ]��B�SldAB�K�dXB慳��̋��J�2/���3�CF)�R�����Mn�a7�I�|�v��!��g��ۨ�a慳��y�����?���y�v�̋���յ����*�"�J�y�fb�y!��!���a;�8c�E.� ���Iux{�p>�(�5�Ls{B��C�?&��)J�[�C>�;�L�R�+����+���[�D�7ٮD��#�s휹=�D�fb��JkO�O|w�p�T;���R�p�+ww8%��s_��v.l�X;/Z�E^Dj��ݙj��k�G۪E��Y�E�/�T��_j�9������v�����[�&�0�r�p��Ĵ��\��7@Q��E	�V���ȓ(���պ����I.���~�0����Դ��&v
�T�N4��+]����"�BɅ˩���-�
G[���#%��:�)YM��j�j�N��j�N��x8��殥��98G�c4?~4e��S�0�IQ���w�e��Xe���f�xk�'��<�f������麺��L��Z�p�/�$Ы3����;�ϖ��Wǳ���(G��f��� :H��~��p�wp3�
�e74n�
���q;T򏡍۳,�W��l���FB�16��sl��$�ٴqk�����ꨶMĸ��g�����3n�Q�'�ej�GSӸ�͆q���m��؞q���q�Úq�ˎq�ۆq;(�q�m�7q�SWyz�@��}[�����EE��Y.*:Cǚ۬���/ᦢH�K����0~S.<����r�^��H�O�o$��O�~ �RU�)�د�7�I���}�oIb���g����(�km�1���(�ߚ�i8�l��INd�oj�D�V��EW,��4�<�-$�_���$��4~W2�w�~�%�]e�[	P.NM�^���]�2���~�����<H��(�!4���y�5�?��*��dZ�H�U:��uD�]����)a~!*>w�'�:K�q4����s�M���9\O�h����o�M�Q�hf5\�ň��$����2��bt
�G�sи����bUm]�=����nނ��j�� ���Vm�븉��hаD*xA��(K���L0�2��:,�̂s2`�i����u�@q.�r�ny��$�y+����H^h����U`��1k~����1��t����ov:{|�t�L�!?#

��'����M�~����x:{�T/Fx��5O��� ;򖎌��켾Mz<{2�=�AB�Ts��p�1�d��y�%t<(ǳ�o�$~3�V�Ň�.������.��j��fE��O���nW���|JZ%�)i���Z��+�f��+���0�5�j!]��sAt:�?������t,�[H,g��XέDXl�˹�-Yi<8���Q �2���HYi�s+��@.ةC���*e��ܳ��v���*������]�G��JG]�+��PYij�s�j '���������p�;+�=[i+<#�x��s�{����?U� ��?���onŒ��X�����������O�ݺT,k87�е-�Ct-P�F��i�_����gDոr;N>X���;Fw�C�'~݉W��k����u\���ʕ?������N���X�<�P(�ĉ�!w�:E|4r��*��%_7��e�4u�� ����⻈��oo߻�����0I�(�[-���j� ����<����񩄐ʩ�
�oZ�O$�0��ꚉ��nrw��$*��ȶQ��GnJ�TmS2��hg=��h�R��#Q�b�J<S�0��|ר�L�A|�^��^"M�II<�Ϧ$���� >g5�Fʢ�/j��	!6�q�M|Ig�M�ؠ�3��{T�2:�6#�Q?V&��be�[V�y�^QE�4!�,噺!6��p���:����7<�Bl��M��lU��nB�U,A�(��S���9�)�q����	a�d=A�b;���F� ��-� <@�
�0��r�=�?D]�& =Fǐ�I@^�& OxZ6�j�R��y�9��rĵ�r�u�r�+m� ��� �W1� ��<��@��y��� �g��cQ@>Hd�E�!L��a[@���3()�{��r���|X��7X@�O	H�=��"��������w)iA
�3H���~�/ ��6��-)�lM@
�X�¯6�b�})��p�(�y�dZ@.���L�-�zI�j�?&�ՙ�c�*�1�֞�1�a��L�(%�c�	��Ǆ:������Z�]��{YG�=N��%X�͢�٥���N���R��.OIs_Xc�����͖��_iG��O����!�U����"l��fI�!��f�,�?D�h����!�&s?j��¹*mV��V�RG�5��@9�EG��DA���E��Q��Bn��vI6>F'����r���<=��J6f�d��b%E���"�GU�	�7x�-ZP�AU<j�Kk��K�TG���zv �,�������ǜ%��`��G�?n�h�9T~�Pm*{90	�Y:�v�a��v0:���1<���	��G�x0�l*-qM�8���[��v�IqB�A��'au�I���ArD��̤+��p�p�8�F��7R�ں``^C���3Nz54�F3@�|�=I�w�-�=E�QѼЛؼ�;��&1@��ޠ���e���Oz�q��B�n��F!�!f~ipb3u �4�X[{�`��y!֮y!����ˇ���_����Z����_�`���/0Lg�M?6� J�6�&�e���gJ��Y��E��Y�`̂m��zbl�����|^���Q�j�6G0а��.�m4.��kx��ݼ�K�����0[6���u�bE��B�EU�����(��lCG�l���'�� j_.�q��&���Yb:o������y��b0�0����y�@eM�\^~58�o�V�sy�m�F?__�?��"q�&���ɻtry�Sd%���TDR/�&a�yeg�=��77p�����3-�E➵�E�v3\��%8�E���x����I�/0\�x����%LĽh1��EB��m$�Ö��݆[C'�B�ͺ��]�����Z'�V{���[�D�H<�F�v֮����]+p{9n���]�}�O*{mW������>�\-/�{mgl�]O��� }����ۚ�h��(K�z��M�p����w=k�~�al���=�By0u��A��;]���l�����u��"�]�4_n&��b�*b��(�OrL��XJ�\��S�@���
t�N�m���@��Jً�D���¹�~���f��:��K���\���^�u6��B��{��� �����'��\}���%:Ty�p����U�4H��SZ�S�9s�1{#"��z��:���;rΊS�-e�{g���6Sc-N��Ҵ���hZ�L]7F�5#L�M�.�Ƈ]7�Z��30(��{nǠP�5h�����/)j^�N�������/i��j���d߾@�m!ٷgQ�v݂͑�L�CU	\Q.�B��M��h���X�Y�+�mʫ�UF�|�%��4s^����t��k� �|�p���d�+S�9��`Ϋ0s�����2���G0��ə3�dN�\J1'4&V�EB�v���[���c�b��D��%6��L�D��=��a����Ys�����bC'�m�Z�7�ՠ�D�v�a\��h�D�>s~���4�nu7t���'�[�?}'���~�UCi���gA	�w����"�R��*8���]��fp�1�2�}p��P���"�΃��z�	s�qu�e���*/�_��W2ӫY��ܻ�ec�q�}��$�t�����F�����8�? �:4���A:�b^��8��7�h៶!�����i��l�F���G��J��Q9��<>������_BCo�����mA�m����z�4/��=� �>�#����??���q蝙
���4��Z����@����I00�y!Vռ�cb� �:�����5��2�����Z�?�V�?�r��o������������i�?v�̣c����ى�s����^�tW������%WI��5a>=ڎ�+�KY,�"�=�d���8a�K��=7n�����?���p�����,ܞE,�e	�����26�M�Hol�G�q�a/l��!�W��e�$����b�o�o� ����	
@��7�=�&�U	�S��ψ&Z8�B�C�`vJε^���D2t�B�oG"���Y��jD��9E=�M
��\�q掳"\<@�q&��>���L�`�8��p���3�[]w՗�$2����3�o��83��g	�3�
3O��� �0s��ñ7"ǙH����S�&Ҏ%b$ �DV�/�4q��,��TV]#�Wѹ�m�8��<�Gǖz_���DGw���謕�쩙:����#ObÖ́з^C�Ğ�	gh� �P�̞2)�q���x�}>�X	�7 �`��M��xy/�M,ӢxP�.���+:��!1�����{��f]^��f]^�{��Yrs92.o��I���a�3%�%���0\�TG�}L��={D*��?%�A�R�N�Ö�8�#� ���g@���׫/�-?Y"�/����)~p%d�gt���b��/0Xlz�C!/&��2O��ƿd�b��$�،�Q�����b[MYl��,�Q�������-6�mm�ͅc�A ����ش��,�2�3���-6����p;w9���I���s���8�P��L��{�w�\�I��p�QW���C�o^���� �Թ����y��
WX��z�+\?�����W��	c��9����Dt�j^Ւ�9�E��ٵj�+��V��V��l�4�ω�~�����o[�1޶���?TT��?�K�c�Q�6��K��p���B�N#a�����,�]'�����_Y�#5���S�H��e��{m	�%7 tK��p�HH�	��������>��N�sw��O�N�q'8`�m��O�N�̝��>፦�w����71�����3,��RMN���Ȏ�q�&�rQ�t8�t~�y:��=c�ucNgh����pw#��_�NgH�*�]����(%��,�Ou���J�p�n���O鬆Lܞ��!��;�[B�e�s�3r+��0�NG�wZGk�ak�}M��p{[�o�8��Z�<5��Ȗ���k��c�
89�"�j������W� �c��ڷ�}��A��	�~�	��-�3l �0�/�00��PD-S)�1|��˳ZE�(ф�L����%�F�Y��9���2j�N*�WF�{��������)|�YF�kj�Q;�z�􏭕Q������+���hm���B�qkeԸ�,�Q�~�SF���r5�W;eԠ�XK'^|f1��y����Q㾰^G-��H����v�Up���-��^�&�%���j�H���mC�u�N�8���q�#��4�o�[3�!�I���Dna����4o��F�]�Wx � �ۢ��Ξ!�^��w#�.�:�����*������~'dxj��/��p�g�_�����Qf�)y��s`��N�u��yFrÓo��=�������\Yѻ��㠗z��W
�p&B�h��*�G�x�9pԪ�C�����5��A�V�o�*k5Zwe�߮��o�V���	�-0��38S�y�`��J��2xe��x 
uWTcp;+÷�����mU��
sBA���7���v�g�@��7O{ Ao3���Nz��T��'u$gH�Xv�&�?�>��&�猑|g��_C"lJŚ
�3['���"��a�b�L���X�+��[x�S$�[�\8��N<7P]�>���1�h����P`���Ê�s��8t5�CWܿO(t5�
]�-�Zp�0P��!��|���}u��I�hڇt|:��B�:�eh#dS�xJ�W��z=�iټ�����
��2�Bg��}�&��iX'��!�A�8�B��ǀ�5Zs@�1��6�9� �$5q\��8��b8����g��m.����q\!�:B�D���k�^�:(�9��'�$��K�z���G�x�C��X�w���p�ȋ�Op�+��4������p�&���׺��;�Zm�`�ʑ-���udpBl�p���$j_H$�Ȋ��3�����������D8��F��?���l͓}p���Ifm�@��'�	t����T�-�q.d��Z��$��A�h1����s�B�Á���l;�*f˹~:�d�RG��,=�(�Q�V�_����F�GW[�:�X+��8�R�G7�:ݭ�p�H^�C���ͪ{�&@�
W�9���퍴ZleQ-��Z̳�=D-����>�����F� 5�b��T��-���P�E6�"J��FW�3�ŝZ���EQ���h�lհ�2~8�V'Y-�5�N�Zl�EѢZ��ԢˎZt�S��j1͚ZL��3l��L�X �� �!MF�����.S����aF�N0
�b� ��(;N;�7�(�9?S?�@��4�xz�AF��3cF�����(����U���OAv�ks!����I3
�\���o�3
Ɓg��˜\z��(��rO�k�uj��a��$�*!���>��dg�� �TG#e+��=ۙ9�1G���)vf�;f���~f��=zP�f��'3����s��=4gz\�C.8�'2���#\%�wt\����*����;;�[�wP�K�g�\��qn�o�@y.�~KM��ࢫ�;n(�K!G�yY~t�즫����;�J_v�g��R��J��͕p��tg.����d��08%ӝ�3[�6����ұݛ)�ɇ@F�B]��;�ׯ� ��pO�B� 8� ��= ����  ��2 �k��3 �C�P�Xd����Ѐ轝2V_���l�w B��6�sG�ֱ�3�BD�.��Q�z�G�Z5�����:��
+$K�5j�A	�7���"}�*8�̎
��>�K���9\��<����l�f2;��ꇑ"Ȁ9��mA�fd�m�!��3�m= � �]�$��ﶆ����nf��!�����-���nkz�Q�B�����n�������%o:��\y����-8�ݖ�n��7>�����h��8��� �r��3���B@�|!��w[;�{����*Z�����^��t�JɬL��l��yO1h)�s	��ɽ;��M�j)0� xit�|�b��,g8�pX{ �+& ^a� מ���f=���j���� p�$?��_�x5�.Z��w|l\�t���$�H	��
��
�������" õ3qU����j���a��������o���oo	|�,�W�C����4�.c(��R�Oq�����W�B��z�pS�~0d���S�$	�+�k�r�@^�t#�U��g��LP�O�r�g�-M��cح���
��;w0�\� �������
*�k��Lm�v��>zה�4��%�e!v$��=�ኧbd��f�+�bc���W-9�w�,��8��	٬U�O(+�H~����^�����y�C����.o�l�%C�>
��h�'�0����O�g���l�Ga��O��?��eM ��l����{�(^��� ?��R� ������c�X�k�^b��ҵ�����C��?��r      � &�   ( BDHPEMC_RS Prefs Cluster.ctl           |   �x�c``�( ���/��W C�/���oN?��@�Q@����_�_��`�#�P��ƨb��r���C�� XG�����U��_�\��?�g�\ �*��[v���@$� ]'!I              �   (         J                               �       h      � �   o      � �   v      � �   }� � �   � �                   � � �   � �TahomaTahomaTahoma02   RSRC
 LVCCLBVW �  �     l               4       LVSR      LIvi       CONP      4TM80      HDFDS      \LIds      pVICD      �vers      �ICON      �icl8      �CPC2      �DTHP      �LIfp      �TRec  !  FPHb      �FPSE      �LIbd      �BDHb      �BDSE      MUID      HIST      0FTAB      D    ����        ���    ����       t�X�    ����      �,o�    ����      ��W�    ����      3xD��    ����      4�P��    ����      4Ԝ��   ����      Al���    ����      A|���    ����      B 8��    ����      F0��    ����      R�`��    ����      bX�o�   ����      jа��   ����      k4���   ����      k���   ����      k��Y�   ����      l$ ]�   ����      l����   ����      l̀��   	����      m,覦   
����      mx<\�   ����      m��o�   ����      n �a�   ����      n|���   ����      n�8��   ����      o(���   ����      ot _�   ����      o̠��   ����      p$���   ����      px���   ����      p��a�   ����      q ���   ����      q����   ����      q܌��   ����      rD���   ����      r��o�   ����      r�̕�   ����      s@���   ����      s�\��   ����      s����   ����      tH���   ����      t����    ����      t����   !����      uX ��   "����      u�0��   #����      v���   $����      vPp��   %����      v�ا�   &����      wh��   '����      wdt��   (����      w�ؤ�   )����      x�   *����      xlD��   +����      x�P��   ,����      y,\��   -����      y�䧦   .����      y� ��   /����      z,,��   0����      z|8��   1����      zԼW�   2����      { ���   3����      {||��   4����      {�<��   5����      |,���   6����      |����   7����      |�̧�   8����      }(�n�   9����      }t�X�   :����      }�D��   ;����      ~8~�   <����      ~x���   =����      ~Ą��   >����      p��   ?����      h$��   @����      ����   A����      ���   B����      �h��   C����      �����   D����      ��n�   E����      �h䕦   F����      ��Ԗ�   G����      ����   H����      �l���   I����      �� ��   J����      ����   K����      �tl_�   L����      ��0��   M����      �(��   N����      �|8��   O����      �����   P����      �$l��   Q����      �����   R����      ��x��   S����      �$D��   T����      ��P��   U����      ���[�   V����      �(t��   W����      ��@��   X����      �؀��   Y����      �,���   Z����      �|���   [����      ����   \����      �$��   ]����      �����   ^����      �����   _����      �0̘�   `����      �|pX�   a����      ��d��   b����      �(X��   c����      ��К�   d����      �Ը��   e����      �48��   f����      ��,��   g����      ��h��   h����      �0�o�   i����      ��ܦ�   j����      ��hW�   k����      �4\W�   l����      ��|[�   m����      �ܬ��   n����      �(���   o����      ��Ħ�   p����      ��Ц�   q����      �,���   r����      �xx��   s����      ��l��   t����      � ���   u����      �|<��   v����      ��$��   w����      � ��   x����      �lȖ�   y����      ��`��   z����      �T��   {����      �pH��   |����      �����   }����      ���   ~����      �`��   ����      �����   �����      �ؕ�   �����      �dT��   �����      ���X�   �����      ��n�   �����      �XD'�   �����      �����   �����      ����   �����      �h�o�   �����      ��`��   �����      � ��   �����      �d��   �����      ��䘦   �����      ��   �����      �d@[�   �����      �����   �����      �t��   �����      �h���   �����      ���[�   �����      ����   �����      �TH��   �����      ����   �����      ��Щ�   �����      �Dh{�   �����      ��$z�   �����      ��v�   �����      �4z�   �����      �����   �����      �����   �����      �8���   �����      �����   �����      �ؼ��   �����      �(���   �����      �t���   �����      �����   �����      ����   �����      �d���   �����      ��t��   �����      �h��   �����      �\\��   �����      ��P��   �����      � D��   �����      �\8��   �����      ��,��   �����      �  ��   �����      �L��   �����      ����   �����      �����   �����      �P���   �����      �����   �����      �����   �����      �H���   �����      �����   �����      �����   �����      �@���   �����      �����   �����      �����   �����      �4���   �����      ��x��   �����      ��l��   �����      �$`��   �����      ��T��   �����      ��H��   �����      �4<��   �����      ��0��   �����      ��$��   �����      �4��   �����      ����   �����      �� ��   �����      �(���   �����      �����   �����      �����   �����      � ���   �����      �l���   �����      �ĸ��   �����      ����   �����      �l���   �����      �����   �����      ����   �����      �\|��   �����      ��p��   �����      �d��   �����      �XX��   �����      ��L��   �����      ��@��   �����      �L4��   �����      ��(��   �����      ����   �����      �<��   �����      ����   �����      �����   �����      �8���   �����      �����   �����      �����   �����      �D���   �����      �����   �����      ����   �����      �8���   �����      �����   �����      �����   �����      �D���   �����      ��t��   �����      ��h��   �����      �,\��   �����      ��P��   �����      ��D��   �����      �,8��   �����      �|,��   �����      �� ��   �����      �4��   �����      ����   �����      �����   �����      � ���   �����      �t���   �����      �����   �����      ����   �����      �\���   �����      �����   �����      � ���   �����      �`���   �����      �����   �����      ����   �����      �Xx��   �����      ��l��   �����      �`��   �����      �`T��   �����      ��H��   �����      ��<��   �����      �X0��   ����      ��$��  ����      ����  ����      �H��  ����      �� ��  ����      �����  ����      �H���  ����      �����  ����      �����  ����      �8���  	����      �����  
����      ����  ����      �,���  ����      ���  ����      �Ԉ��  ����      �(|��  ����      �xp��  ����      ��d��  ����      �(X��  ����      ĈL��  ����      ��@��  ����      �,4��  ����      Ő(��  ����      ����  ����      �4��  ����      ƌ��  ����      �����  ����      �0���  ����      ǐ���  ����      �����  ����      �D���  ����      Ȑ���  ����      �����   ����      �X���  !����      ɤ���  "����      �����  #����      �P���    ����      ʤt��    ����      �h��    ����      �\��    ����     $P��    ����     �D��    ����     �8��    ����     �,��   �����     � ��