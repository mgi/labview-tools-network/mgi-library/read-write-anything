<?xml version='1.0'?>
<Project Type="Project" LVVersion="8208000">
   <Item Name="My Computer" Type="My Computer">
      <Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.tcp.enabled" Type="Bool">false</Property>
      <Property Name="server.tcp.port" Type="Int">0</Property>
      <Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
      <Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="MGI RWA INI Tag Lookup.vi" Type="VI" URL="../MGI RWA INI Tag Lookup.vi"/>
      <Item Name="MGI RWA Options Cluster.ctl" Type="VI" URL="../MGI RWA Options Cluster.ctl"/>
      <Item Name="MGI RWA Tag Lookup Cluster.ctl" Type="VI" URL="../MGI RWA Tag Lookup Cluster.ctl"/>
      <Item Name="MGI RWA Unreplace Characters.vi" Type="VI" URL="../MGI RWA Unreplace Characters.vi"/>
      <Item Name="MGI RWA Build Array Name.vi" Type="VI" URL="../MGI RWA Build Array Name.vi"/>
      <Item Name="MGI RWA Build Line.vi" Type="VI" URL="../MGI RWA Build Line.vi"/>
      <Item Name="MGI RWA Get Type Info.vi" Type="VI" URL="../MGI RWA Get Type Info.vi"/>
      <Item Name="MGI RWA Replace Characters.vi" Type="VI" URL="../MGI RWA Replace Characters.vi"/>
      <Item Name="MGI Write Anything.vi" Type="VI" URL="../MGI Write Anything.vi"/>
      <Item Name="MGI Read Anything (Merge).vi" Type="VI" URL="../MGI Read Anything (Merge).vi"/>
      <Item Name="MGI Read Anything.vi" Type="VI" URL="../MGI Read Anything.vi"/>
      <Item Name="MGI RWA Anything to String.vi" Type="VI" URL="../MGI RWA Anything to String.vi"/>
      <Item Name="MGI RWA Enque Top Level Data.vi" Type="VI" URL="../MGI RWA Enque Top Level Data.vi"/>
      <Item Name="MGI RWA Handle Tag or Refnum.vi" Type="VI" URL="../MGI RWA Handle Tag or Refnum.vi"/>
      <Item Name="MGI RWA Process Array Elements.vi" Type="VI" URL="../MGI RWA Process Array Elements.vi"/>
      <Item Name="MGI RWA Read Strings from File.vi" Type="VI" URL="../MGI RWA Read Strings from File.vi"/>
      <Item Name="MGI RWA String To Anything.vi" Type="VI" URL="../MGI RWA String To Anything.vi"/>
      <Item Name="MGI RWA Unprocess Array Elements.vi" Type="VI" URL="../MGI RWA Unprocess Array Elements.vi"/>
      <Item Name="MGI RWA Write Strings to File.vi" Type="VI" URL="../MGI RWA Write Strings to File.vi"/>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
